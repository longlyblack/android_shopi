package com.liminyunbao.iyunbeishop;

import android.app.Application;

import com.baidu.mapapi.SDKInitializer;
import com.liminyunbao.iyunbeishop.activity.BusinessActivity;
import com.liminyunbao.iyunbeishop.activity.BusinessSaverActivity;
import com.lzy.okgo.OkGo;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.utils.Log;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;


import cn.jpush.android.api.JPushInterface;

/**
 * Created by Administrator on 2017/3/15.
 */

public class MyApp extends Application {
    private static MyApp instance;
    @Override
    public void onCreate() {
        super.onCreate();
        Config.DEBUG = true;
        Config.isJumptoAppStore = true;
        Log.LOG=false;
        UMShareAPI.get(this);
        SDKInitializer.initialize(getApplicationContext());
        Beta.autoCheckUpgrade = true;//设置不自动检查
        Beta.canShowUpgradeActs.add(BusinessActivity.class);
        Beta.canShowUpgradeActs.add(BusinessSaverActivity.class);
        Bugly.init(getApplicationContext(), "9b0bffe6b2", false);
        PlatformConfig.setWeixin("wxfa0154241ed573c7","365e5cd21ea3cb9b71e13e3e51c81be3");
        OkGo.init(this);
        ZXingLibrary.initDisplayOpinion(this);
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
        if (instance == null) {
            instance = this;
        }
    }
    public static MyApp getInstance() {
        return instance;
    }
}
