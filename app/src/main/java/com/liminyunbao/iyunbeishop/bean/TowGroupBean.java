package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/6/7 0007.
 */

public class TowGroupBean {

    /**
     * cmd : fenzuinfo
     * status : 1
     * msg : [{"cate_id":202,"status":"1","cate_name":"查询"},{"cate_id":196,"status":"1","cate_name":"牛逼"},{"cate_id":180,"status":"0","cate_name":"默认"}]
     */

    private String cmd;
    private String status;
    private List<MsgBean> msg;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MsgBean> getMsg() {
        return msg;
    }

    public void setMsg(List<MsgBean> msg) {
        this.msg = msg;
    }

    public static class MsgBean {
        /**
         * cate_id : 202
         * status : 1
         * cate_name : 查询
         */

        private int cate_id;
        private String status;
        private String cate_name;
        private int num;

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public int getCate_id() {
            return cate_id;
        }

        public void setCate_id(int cate_id) {
            this.cate_id = cate_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCate_name() {
            return cate_name;
        }

        public void setCate_name(String cate_name) {
            this.cate_name = cate_name;
        }
    }
}
