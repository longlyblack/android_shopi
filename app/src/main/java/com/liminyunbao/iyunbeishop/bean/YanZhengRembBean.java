package com.liminyunbao.iyunbeishop.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2017/4/8 0008.
 */

public class YanZhengRembBean {


    /**
     * cmd : yanzheng
     * status : 1
     * data : {"total":2,"per_page":20,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":2,"data":[{"verify_code":"56767866","order_id":101489,"created_at":"2017-04-04 15:14:02","shop_name":"云贝网官方店\u2014南阳","total_price":600,"title":"平安果","photo":"2017/03/10/20170310182703_1531.jpeg","settlement_price":600,"order_price":1500,"status":3,"goods_guige":"5g/袋","goods_price":50,"data":[{"id":2,"order_id":101489,"goods_id":2245,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"凉皮","goods_guige":"5g/袋","goods_num":2,"goods_price":50,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-13 09:46:02"},{"id":3,"order_id":101489,"goods_id":2240,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"锅巴","goods_guige":"300g/袋","goods_num":5,"goods_price":300,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-08 09:47:43"}]},{"verify_code":"56767866","order_id":101489,"created_at":"2017-04-04 15:14:02","shop_name":"云贝网官方店\u2014南阳","total_price":600,"title":"平安果","photo":"2017/03/10/20170310182703_1531.jpeg","settlement_price":600,"order_price":1500,"status":3,"goods_guige":"300g/袋","goods_price":300,"data":[{"id":2,"order_id":101489,"goods_id":2245,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"凉皮","goods_guige":"5g/袋","goods_num":2,"goods_price":50,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-13 09:46:02"},{"id":3,"order_id":101489,"goods_id":2240,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"锅巴","goods_guige":"300g/袋","goods_num":5,"goods_price":300,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-08 09:47:43"}]}]}
     */

    private String cmd;
    private String status;
    private DataBeanXX data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBeanXX getData() {
        return data;
    }

    public void setData(DataBeanXX data) {
        this.data = data;
    }

    public static class DataBeanXX {
        /**
         * total : 2
         * per_page : 20
         * current_page : 1
         * last_page : 1
         * next_page_url : null
         * prev_page_url : null
         * from : 1
         * to : 2
         * data : [{"verify_code":"56767866","order_id":101489,"created_at":"2017-04-04 15:14:02","shop_name":"云贝网官方店\u2014南阳","total_price":600,"title":"平安果","photo":"2017/03/10/20170310182703_1531.jpeg","settlement_price":600,"order_price":1500,"status":3,"goods_guige":"5g/袋","goods_price":50,"data":[{"id":2,"order_id":101489,"goods_id":2245,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"凉皮","goods_guige":"5g/袋","goods_num":2,"goods_price":50,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-13 09:46:02"},{"id":3,"order_id":101489,"goods_id":2240,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"锅巴","goods_guige":"300g/袋","goods_num":5,"goods_price":300,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-08 09:47:43"}]},{"verify_code":"56767866","order_id":101489,"created_at":"2017-04-04 15:14:02","shop_name":"云贝网官方店\u2014南阳","total_price":600,"title":"平安果","photo":"2017/03/10/20170310182703_1531.jpeg","settlement_price":600,"order_price":1500,"status":3,"goods_guige":"300g/袋","goods_price":300,"data":[{"id":2,"order_id":101489,"goods_id":2245,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"凉皮","goods_guige":"5g/袋","goods_num":2,"goods_price":50,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-13 09:46:02"},{"id":3,"order_id":101489,"goods_id":2240,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"锅巴","goods_guige":"300g/袋","goods_num":5,"goods_price":300,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-08 09:47:43"}]}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private Object next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<DataBeanX> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public Object getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(Object next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<DataBeanX> getData() {
            return data;
        }

        public void setData(List<DataBeanX> data) {
            this.data = data;
        }

        public static class DataBeanX {
            /**
             * verify_code : 56767866
             * order_id : 101489
             * created_at : 2017-04-04 15:14:02
             * shop_name : 云贝网官方店—南阳
             * total_price : 600
             * title : 平安果
             * photo : 2017/03/10/20170310182703_1531.jpeg
             * settlement_price : 600
             * order_price : 1500
             * status : 3
             * goods_guige : 5g/袋
             * goods_price : 50
             * data : [{"id":2,"order_id":101489,"goods_id":2245,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"凉皮","goods_guige":"5g/袋","goods_num":2,"goods_price":50,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-13 09:46:02"},{"id":3,"order_id":101489,"goods_id":2240,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"锅巴","goods_guige":"300g/袋","goods_num":5,"goods_price":300,"cost_price":null,"deleted_at":null,"updated_at":null,"created_at":"2017-04-08 09:47:43"}]
             */

            private String verify_code;
            private int order_id;
            private String created_at;
            private String shop_name;
            private int total_price;
            private String title;
            private String photo;
            private int settlement_price;
            private int order_price;
            private int status;
            private String goods_guige;
            private int goods_price;
            private List<DataBean> data;

            public String getVerify_code() {
                return verify_code;
            }

            public void setVerify_code(String verify_code) {
                this.verify_code = verify_code;
            }

            public int getOrder_id() {
                return order_id;
            }

            public void setOrder_id(int order_id) {
                this.order_id = order_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getShop_name() {
                return shop_name;
            }

            public void setShop_name(String shop_name) {
                this.shop_name = shop_name;
            }

            public int getTotal_price() {
                return total_price;
            }

            public void setTotal_price(int total_price) {
                this.total_price = total_price;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public int getSettlement_price() {
                return settlement_price;
            }

            public void setSettlement_price(int settlement_price) {
                this.settlement_price = settlement_price;
            }

            public int getOrder_price() {
                return order_price;
            }

            public void setOrder_price(int order_price) {
                this.order_price = order_price;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getGoods_guige() {
                return goods_guige;
            }

            public void setGoods_guige(String goods_guige) {
                this.goods_guige = goods_guige;
            }

            public int getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(int goods_price) {
                this.goods_price = goods_price;
            }

            public List<DataBean> getData() {
                return data;
            }

            public void setData(List<DataBean> data) {
                this.data = data;
            }

            public static class DataBean  implements Serializable{
                /**
                 * id : 2
                 * order_id : 101489
                 * goods_id : 2245
                 * photo : 2017/03/10/20170310182703_1531.jpeg
                 * goods_name : 凉皮
                 * goods_guige : 5g/袋
                 * goods_num : 2
                 * goods_price : 50
                 * cost_price : null
                 * deleted_at : null
                 * updated_at : null
                 * created_at : 2017-04-13 09:46:02
                 */

                private int id;
                private int order_id;
                private int goods_id;
                private String photo;
                private String goods_name;
                private String goods_guige;
                private int goods_num;
                private int goods_price;
                private Object cost_price;
                private Object deleted_at;
                private Object updated_at;
                private String created_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getOrder_id() {
                    return order_id;
                }

                public void setOrder_id(int order_id) {
                    this.order_id = order_id;
                }

                public int getGoods_id() {
                    return goods_id;
                }

                public void setGoods_id(int goods_id) {
                    this.goods_id = goods_id;
                }

                public String getPhoto() {
                    return photo;
                }

                public void setPhoto(String photo) {
                    this.photo = photo;
                }

                public String getGoods_name() {
                    return goods_name;
                }

                public void setGoods_name(String goods_name) {
                    this.goods_name = goods_name;
                }

                public String getGoods_guige() {
                    return goods_guige;
                }

                public void setGoods_guige(String goods_guige) {
                    this.goods_guige = goods_guige;
                }

                public int getGoods_num() {
                    return goods_num;
                }

                public void setGoods_num(int goods_num) {
                    this.goods_num = goods_num;
                }

                public int getGoods_price() {
                    return goods_price;
                }

                public void setGoods_price(int goods_price) {
                    this.goods_price = goods_price;
                }

                public Object getCost_price() {
                    return cost_price;
                }

                public void setCost_price(Object cost_price) {
                    this.cost_price = cost_price;
                }

                public Object getDeleted_at() {
                    return deleted_at;
                }

                public void setDeleted_at(Object deleted_at) {
                    this.deleted_at = deleted_at;
                }

                public Object getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(Object updated_at) {
                    this.updated_at = updated_at;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }
            }
        }
    }
}
