package com.liminyunbao.iyunbeishop.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2017/4/11 0011.
 */

public class PuTongListBean {


    /**
     * cmd : orderlist
     * status : 1
     * data : {"total":29,"per_page":5,"current_page":1,"last_page":6,"next_page_url":"http://1shop.iyunbei.net/api/Package/orderlistt?page=2","prev_page_url":null,"from":1,"to":5,"data":[{"order_id":209960,"order_amount":500,"total_amount":500,"order_status":1,"created_at":"2017-05-04 19:19:47","ok_time":null,"ps_time":null,"verify_code":"52625081","goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11449,"order_id":209960,"goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 19:19:47","created_at":"2017-05-04 19:19:47","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]},{"order_id":209959,"order_amount":380,"total_amount":380,"order_status":3,"created_at":"2017-05-04 17:23:04","ok_time":null,"ps_time":null,"verify_code":"1232","goods_id":2786,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"喜之郎什锦果冻","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11448,"order_id":209959,"goods_id":2786,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"喜之郎什锦果冻","goods_guige":"","goods_num":1,"point_price":null,"goods_price":380,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 17:23:04","created_at":"2017-05-04 17:23:04","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]},{"order_id":209958,"order_amount":500,"total_amount":500,"order_status":3,"created_at":"2017-05-04 17:01:56","ok_time":null,"ps_time":null,"verify_code":"1234234123","goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11447,"order_id":209958,"goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 17:01:56","created_at":"2017-05-04 17:01:56","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]},{"order_id":209950,"order_amount":500,"total_amount":500,"order_status":1,"created_at":"2017-05-04 15:43:35","ok_time":null,"ps_time":null,"verify_code":"321","goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11439,"order_id":209950,"goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 15:43:35","created_at":"2017-05-04 15:43:35","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]},{"order_id":209949,"order_amount":500,"total_amount":500,"order_status":1,"created_at":"2017-05-04 14:43:04","ok_time":null,"ps_time":null,"verify_code":"123","goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11438,"order_id":209949,"goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 14:43:04","created_at":"2017-05-04 14:43:04","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]}]}
     */

    private String cmd;
    private String status;
    private DataBean data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * total : 29
         * per_page : 5
         * current_page : 1
         * last_page : 6
         * next_page_url : http://1shop.iyunbei.net/api/Package/orderlistt?page=2
         * prev_page_url : null
         * from : 1
         * to : 5
         * data : [{"order_id":209960,"order_amount":500,"total_amount":500,"order_status":1,"created_at":"2017-05-04 19:19:47","ok_time":null,"ps_time":null,"verify_code":"52625081","goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11449,"order_id":209960,"goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 19:19:47","created_at":"2017-05-04 19:19:47","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]},{"order_id":209959,"order_amount":380,"total_amount":380,"order_status":3,"created_at":"2017-05-04 17:23:04","ok_time":null,"ps_time":null,"verify_code":"1232","goods_id":2786,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"喜之郎什锦果冻","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11448,"order_id":209959,"goods_id":2786,"photo":"2017/03/10/20170310182703_1531.jpeg","goods_name":"喜之郎什锦果冻","goods_guige":"","goods_num":1,"point_price":null,"goods_price":380,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 17:23:04","created_at":"2017-05-04 17:23:04","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]},{"order_id":209958,"order_amount":500,"total_amount":500,"order_status":3,"created_at":"2017-05-04 17:01:56","ok_time":null,"ps_time":null,"verify_code":"1234234123","goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11447,"order_id":209958,"goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 17:01:56","created_at":"2017-05-04 17:01:56","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]},{"order_id":209950,"order_amount":500,"total_amount":500,"order_status":1,"created_at":"2017-05-04 15:43:35","ok_time":null,"ps_time":null,"verify_code":"321","goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11439,"order_id":209950,"goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 15:43:35","created_at":"2017-05-04 15:43:35","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]},{"order_id":209949,"order_amount":500,"total_amount":500,"order_status":1,"created_at":"2017-05-04 14:43:04","ok_time":null,"ps_time":null,"verify_code":"123","goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":11438,"order_id":209949,"goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 14:43:04","created_at":"2017-05-04 14:43:04","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private String next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<OrderDmsg> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(String next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<OrderDmsg> getData() {
            return data;
        }

        public void setData(List<OrderDmsg> data) {
            this.data = data;
        }

        public static class OrderDmsg implements Serializable {
            /**
             * order_id : 209960
             * order_amount : 500
             * total_amount : 500
             * order_status : 1
             * created_at : 2017-05-04 19:19:47
             * ok_time : null
             * ps_time : null
             * verify_code : 52625081
             * goods_id : 2757
             * photo : 2017/03/16/20170316155353_5048.jpeg
             * goods_name : 梅溪郭麻花芝麻球糕点
             * goods_guige :
             * goods_num : 1
             * shop_name : 云贝网官方店—南阳
             * data : [{"rec_id":11449,"order_id":209960,"goods_id":2757,"photo":"2017/03/16/20170316155353_5048.jpeg","goods_name":"梅溪郭麻花芝麻球糕点","goods_guige":"","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-05-04 19:19:47","created_at":"2017-05-04 19:19:47","note":null,"pay_id":null,"spec_id":null,"spec_name":null}]
             */

            private int order_id;
            private String  order_amount;
            private String total_amount;
            private int order_status;
            private String created_at;
            private Object ok_time;
            private Object ps_time;
            private String verify_code;
            private int goods_id;
            private String photo;
            private String goods_name;
            private String goods_guige;
            private int goods_num;
            private String shop_name;
            private List<ShopData> data;

            public int getOrder_id() {
                return order_id;
            }

            public void setOrder_id(int order_id) {
                this.order_id = order_id;
            }

            public String getOrder_amount() {
                return order_amount;
            }

            public void setOrder_amount(String order_amount) {
                this.order_amount = order_amount;
            }

            public String getTotal_amount() {
                return total_amount;
            }

            public void setTotal_amount(String total_amount) {
                this.total_amount = total_amount;
            }

            public int getOrder_status() {
                return order_status;
            }

            public void setOrder_status(int order_status) {
                this.order_status = order_status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public Object getOk_time() {
                return ok_time;
            }

            public void setOk_time(Object ok_time) {
                this.ok_time = ok_time;
            }

            public Object getPs_time() {
                return ps_time;
            }

            public void setPs_time(Object ps_time) {
                this.ps_time = ps_time;
            }

            public String getVerify_code() {
                return verify_code;
            }

            public void setVerify_code(String verify_code) {
                this.verify_code = verify_code;
            }

            public int getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(int goods_id) {
                this.goods_id = goods_id;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getGoods_guige() {
                return goods_guige;
            }

            public void setGoods_guige(String goods_guige) {
                this.goods_guige = goods_guige;
            }

            public int getGoods_num() {
                return goods_num;
            }

            public void setGoods_num(int goods_num) {
                this.goods_num = goods_num;
            }

            public String getShop_name() {
                return shop_name;
            }

            public void setShop_name(String shop_name) {
                this.shop_name = shop_name;
            }

            public List<ShopData> getData()  {
                return data;
            }

            public void setData(List<ShopData> data) {
                this.data = data;
            }

            public static class ShopData implements Serializable {
                /**
                 * rec_id : 11449
                 * order_id : 209960
                 * goods_id : 2757
                 * photo : 2017/03/16/20170316155353_5048.jpeg
                 * goods_name : 梅溪郭麻花芝麻球糕点
                 * goods_guige :
                 * goods_num : 1
                 * point_price : null
                 * goods_price : 500
                 * cost_price : 0
                 * deleted_at : null
                 * updated_at : 2017-05-04 19:19:47
                 * created_at : 2017-05-04 19:19:47
                 * note : null
                 * pay_id : null
                 * spec_id : null
                 * spec_name : null
                 */

                private int rec_id;
                private int order_id;
                private int goods_id;
                private String photo;
                private String goods_name;
                private String goods_guige;
                private int goods_num;
                private Object point_price;
                private float goods_price;
                private int cost_price;
                private Object deleted_at;
                private String updated_at;
                private String created_at;
                private Object note;
                private Object pay_id;
                private Object spec_id;
                private Object spec_name;

                public int getRec_id() {
                    return rec_id;
                }

                public void setRec_id(int rec_id) {
                    this.rec_id = rec_id;
                }

                public int getOrder_id() {
                    return order_id;
                }

                public void setOrder_id(int order_id) {
                    this.order_id = order_id;
                }

                public int getGoods_id() {
                    return goods_id;
                }

                public void setGoods_id(int goods_id) {
                    this.goods_id = goods_id;
                }

                public String getPhoto() {
                    return photo;
                }

                public void setPhoto(String photo) {
                    this.photo = photo;
                }

                public String getGoods_name() {
                    return goods_name;
                }

                public void setGoods_name(String goods_name) {
                    this.goods_name = goods_name;
                }

                public String getGoods_guige() {
                    return goods_guige;
                }

                public void setGoods_guige(String goods_guige) {
                    this.goods_guige = goods_guige;
                }

                public int getGoods_num() {
                    return goods_num;
                }

                public void setGoods_num(int goods_num) {
                    this.goods_num = goods_num;
                }

                public Object getPoint_price() {
                    return point_price;
                }

                public void setPoint_price(Object point_price) {
                    this.point_price = point_price;
                }

                public float getGoods_price() {
                    return goods_price;
                }

                public void setGoods_price(float goods_price) {
                    this.goods_price = goods_price;
                }

                public int getCost_price() {
                    return cost_price;
                }

                public void setCost_price(int cost_price) {
                    this.cost_price = cost_price;
                }

                public Object getDeleted_at() {
                    return deleted_at;
                }

                public void setDeleted_at(Object deleted_at) {
                    this.deleted_at = deleted_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public Object getNote() {
                    return note;
                }

                public void setNote(Object note) {
                    this.note = note;
                }

                public Object getPay_id() {
                    return pay_id;
                }

                public void setPay_id(Object pay_id) {
                    this.pay_id = pay_id;
                }

                public Object getSpec_id() {
                    return spec_id;
                }

                public void setSpec_id(Object spec_id) {
                    this.spec_id = spec_id;
                }

                public Object getSpec_name() {
                    return spec_name;
                }

                public void setSpec_name(Object spec_name) {
                    this.spec_name = spec_name;
                }
            }
        }
    }
}
