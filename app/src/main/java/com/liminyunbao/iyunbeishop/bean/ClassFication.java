package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/26 0026.
 */

public class ClassFication {


    /**
     * cmd : fenlei
     * status : 1
     * data : [{"cate_id":1,"catename":"男士用品"},{"cate_id":2,"catename":"女性用品"},{"cate_id":3,"catename":"吃货专区"},{"cate_id":4,"catename":"逗比专享"}]
     */

    private String cmd;
    private String status;
    private List<DataBean> data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * cate_id : 1
         * catename : 男士用品
         */

        private int cate_id;
        private String catename;

        public int getCate_id() {
            return cate_id;
        }

        public void setCate_id(int cate_id) {
            this.cate_id = cate_id;
        }

        public String getCatename() {
            return catename;
        }

        public void setCatename(String catename) {
            this.catename = catename;
        }
    }
}
