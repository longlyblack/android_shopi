package com.liminyunbao.iyunbeishop.bean;

/**
 * Created by Administrator on 2017/4/10 0010.
 */

public class MoneyInforBean {


    /**
     * cmd : moneyinfo
     * status : 1
     * date : 5944.21
     */

    private String cmd;
    private String status;
    private double date;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getDate() {
        return date;
    }

    public void setDate(double date) {
        this.date = date;
    }
}
