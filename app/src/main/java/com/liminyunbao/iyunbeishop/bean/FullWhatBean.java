package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/6/30/030.
 */

public class FullWhatBean  {

    /**
     * status : 1
     * cmd : cardlist
     * msg : 读取成功
     * data : [{"coupon_id":12,"coupon_type":3,"shop_id":1083,"area_id":0,"goods_agent_id":null,"offline":0,"number":"43d545f0e1cd","title":"新春大红包 折扣卡2！","views":0,"downloads":0,"intro":null,"num":-1,"limit_num":1,"closed":0,"discount":8.2,"money":100,"discount_type":2,"url":null,"start_time":1498579200,"end_time":1498751999,"deleted_at":null,"updated_at":"2017-06-28 16:31:41","created_at":"2017-06-28 16:31:41","islong":0,"isdead":0}]
     */

    private int status;
    private String cmd;
    private String msg;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * coupon_id : 12
         * coupon_type : 3
         * shop_id : 1083
         * area_id : 0
         * goods_agent_id : null
         * offline : 0
         * number : 43d545f0e1cd
         * title : 新春大红包 折扣卡2！
         * views : 0
         * downloads : 0
         * intro : null
         * num : -1
         * limit_num : 1
         * closed : 0
         * discount : 8.2
         * money : 100
         * discount_type : 2
         * url : null
         * start_time : 1498579200
         * end_time : 1498751999
         * deleted_at : null
         * updated_at : 2017-06-28 16:31:41
         * created_at : 2017-06-28 16:31:41
         * islong : 0
         * isdead : 0
         */

        private int coupon_id;
        private int coupon_type;
        private int shop_id;
        private int area_id;
        private Object goods_agent_id;
        private int offline;
        private String number;
        private String title;
        private int views;
        private int downloads;
        private Object intro;
        private int num;
        private int limit_num;
        private int closed;
        private String discount;
        private int money;
        private int discount_type;
        private Object url;
        private String start_time;
        private String end_time;
        private Object deleted_at;
        private String updated_at;
        private String created_at;
        private int islong;
        private int isdead;
        private String shop_name;

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public int getCoupon_id() {
            return coupon_id;
        }

        public void setCoupon_id(int coupon_id) {
            this.coupon_id = coupon_id;
        }

        public int getCoupon_type() {
            return coupon_type;
        }

        public void setCoupon_type(int coupon_type) {
            this.coupon_type = coupon_type;
        }

        public int getShop_id() {
            return shop_id;
        }

        public void setShop_id(int shop_id) {
            this.shop_id = shop_id;
        }

        public int getArea_id() {
            return area_id;
        }

        public void setArea_id(int area_id) {
            this.area_id = area_id;
        }

        public Object getGoods_agent_id() {
            return goods_agent_id;
        }

        public void setGoods_agent_id(Object goods_agent_id) {
            this.goods_agent_id = goods_agent_id;
        }

        public int getOffline() {
            return offline;
        }

        public void setOffline(int offline) {
            this.offline = offline;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getViews() {
            return views;
        }

        public void setViews(int views) {
            this.views = views;
        }

        public int getDownloads() {
            return downloads;
        }

        public void setDownloads(int downloads) {
            this.downloads = downloads;
        }

        public Object getIntro() {
            return intro;
        }

        public void setIntro(Object intro) {
            this.intro = intro;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public int getLimit_num() {
            return limit_num;
        }

        public void setLimit_num(int limit_num) {
            this.limit_num = limit_num;
        }

        public int getClosed() {
            return closed;
        }

        public void setClosed(int closed) {
            this.closed = closed;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public int getMoney() {
            return money;
        }

        public void setMoney(int money) {
            this.money = money;
        }

        public int getDiscount_type() {
            return discount_type;
        }

        public void setDiscount_type(int discount_type) {
            this.discount_type = discount_type;
        }

        public Object getUrl() {
            return url;
        }

        public void setUrl(Object url) {
            this.url = url;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getIslong() {
            return islong;
        }

        public void setIslong(int islong) {
            this.islong = islong;
        }

        public int getIsdead() {
            return isdead;
        }

        public void setIsdead(int isdead) {
            this.isdead = isdead;
        }
    }
}
