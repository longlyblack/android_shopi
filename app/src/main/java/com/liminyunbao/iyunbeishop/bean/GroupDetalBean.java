package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/6/6 0006.
 */

public class GroupDetalBean {

    /**
     * cmd : teaminfo
     * status : 1
     * msg : {"total":1,"per_page":15,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":[{"goods_agent_id":30514762,"base_goods_id":null,"title":"我拉你","simple_title":"我拉你","shop_id":862,"parent_cate_id":40,"custom_cate_id":185,"brand_id":null,"catea_id":42,"cate_id":180,"area_id":null,"photo":"2017/06/01/20170601104210_5605.jpeg","goods_price":"2211","market_price":"33","purchase_price":null,"num":22,"sold_num":0,"score_num":0,"details":null,"audit":0,"is_mall":1,"deleted_at":null,"updated_at":null,"created_at":"2017-06-08 11:02:35","pv":null,"guige":"哦哦","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":5,"sort":0,"sales_sum":0,"goods_remark":"话术","goods_view":null,"is_recommend":0,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":0,"buyknow":"啥球","id":377,"goods_id":30514762}]}
     */

    private String cmd;
    private String status;
    private MsgBean msg;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MsgBean getMsg() {
        return msg;
    }

    public void setMsg(MsgBean msg) {
        this.msg = msg;
    }

    public static class MsgBean {
        /**
         * total : 1
         * per_page : 15
         * current_page : 1
         * last_page : 1
         * next_page_url : null
         * prev_page_url : null
         * from : 1
         * to : 1
         * data : [{"goods_agent_id":30514762,"base_goods_id":null,"title":"我拉你","simple_title":"我拉你","shop_id":862,"parent_cate_id":40,"custom_cate_id":185,"brand_id":null,"catea_id":42,"cate_id":180,"area_id":null,"photo":"2017/06/01/20170601104210_5605.jpeg","goods_price":"2211","market_price":"33","purchase_price":null,"num":22,"sold_num":0,"score_num":0,"details":null,"audit":0,"is_mall":1,"deleted_at":null,"updated_at":null,"created_at":"2017-06-08 11:02:35","pv":null,"guige":"哦哦","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":5,"sort":0,"sales_sum":0,"goods_remark":"话术","goods_view":null,"is_recommend":0,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":0,"buyknow":"啥球","id":377,"goods_id":30514762}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private Object next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<DataBean> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public Object getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(Object next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * goods_agent_id : 30514762
             * base_goods_id : null
             * title : 我拉你
             * simple_title : 我拉你
             * shop_id : 862
             * parent_cate_id : 40
             * custom_cate_id : 185
             * brand_id : null
             * catea_id : 42
             * cate_id : 180
             * area_id : null
             * photo : 2017/06/01/20170601104210_5605.jpeg
             * goods_price : 2211
             * market_price : 33
             * purchase_price : null
             * num : 22
             * sold_num : 0
             * score_num : 0
             * details : null
             * audit : 0
             * is_mall : 1
             * deleted_at : null
             * updated_at : null
             * created_at : 2017-06-08 11:02:35
             * pv : null
             * guige : 哦哦
             * goods_num : 0
             * start_time : null
             * end_time : null
             * points_price : null
             * goods_type : 5
             * sort : 0
             * sales_sum : 0
             * goods_remark : 话术
             * goods_view : null
             * is_recommend : 0
             * recommend_time : null
             * is_new : null
             * is_hot : null
             * is_shop : null
             * is_send : null
             * is_guige : 0
             * buyknow : 啥球
             * id : 377
             * goods_id : 30514762
             */

            private int goods_agent_id;
            private Object base_goods_id;
            private String title;
            private String simple_title;
            private int shop_id;
            private int parent_cate_id;
            private int custom_cate_id;
            private Object brand_id;
            private int catea_id;
            private int cate_id;
            private Object area_id;
            private String photo;
            private String goods_price;
            private String market_price;
            private Object purchase_price;
            private int num;
            private int sold_num;
            private int score_num;
            private Object details;
            private int audit;
            private int is_mall;
            private Object deleted_at;
            private Object updated_at;
            private String created_at;
            private Object pv;
            private String guige;
            private int goods_num;
            private Object start_time;
            private Object end_time;
            private Object points_price;
            private int goods_type;
            private int sort;
            private int sales_sum;
            private String goods_remark;
            private Object goods_view;
            private int is_recommend;
            private Object recommend_time;
            private Object is_new;
            private Object is_hot;
            private Object is_shop;
            private Object is_send;
            private int is_guige;
            private String buyknow;
            private int id;
            private int goods_id;

            public int getGoods_agent_id() {
                return goods_agent_id;
            }

            public void setGoods_agent_id(int goods_agent_id) {
                this.goods_agent_id = goods_agent_id;
            }

            public Object getBase_goods_id() {
                return base_goods_id;
            }

            public void setBase_goods_id(Object base_goods_id) {
                this.base_goods_id = base_goods_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSimple_title() {
                return simple_title;
            }

            public void setSimple_title(String simple_title) {
                this.simple_title = simple_title;
            }

            public int getShop_id() {
                return shop_id;
            }

            public void setShop_id(int shop_id) {
                this.shop_id = shop_id;
            }

            public int getParent_cate_id() {
                return parent_cate_id;
            }

            public void setParent_cate_id(int parent_cate_id) {
                this.parent_cate_id = parent_cate_id;
            }

            public int getCustom_cate_id() {
                return custom_cate_id;
            }

            public void setCustom_cate_id(int custom_cate_id) {
                this.custom_cate_id = custom_cate_id;
            }

            public Object getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(Object brand_id) {
                this.brand_id = brand_id;
            }

            public int getCatea_id() {
                return catea_id;
            }

            public void setCatea_id(int catea_id) {
                this.catea_id = catea_id;
            }

            public int getCate_id() {
                return cate_id;
            }

            public void setCate_id(int cate_id) {
                this.cate_id = cate_id;
            }

            public Object getArea_id() {
                return area_id;
            }

            public void setArea_id(Object area_id) {
                this.area_id = area_id;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(String goods_price) {
                this.goods_price = goods_price;
            }

            public String getMarket_price() {
                return market_price;
            }

            public void setMarket_price(String market_price) {
                this.market_price = market_price;
            }

            public Object getPurchase_price() {
                return purchase_price;
            }

            public void setPurchase_price(Object purchase_price) {
                this.purchase_price = purchase_price;
            }

            public int getNum() {
                return num;
            }

            public void setNum(int num) {
                this.num = num;
            }

            public int getSold_num() {
                return sold_num;
            }

            public void setSold_num(int sold_num) {
                this.sold_num = sold_num;
            }

            public int getScore_num() {
                return score_num;
            }

            public void setScore_num(int score_num) {
                this.score_num = score_num;
            }

            public Object getDetails() {
                return details;
            }

            public void setDetails(Object details) {
                this.details = details;
            }

            public int getAudit() {
                return audit;
            }

            public void setAudit(int audit) {
                this.audit = audit;
            }

            public int getIs_mall() {
                return is_mall;
            }

            public void setIs_mall(int is_mall) {
                this.is_mall = is_mall;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public Object getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(Object updated_at) {
                this.updated_at = updated_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public Object getPv() {
                return pv;
            }

            public void setPv(Object pv) {
                this.pv = pv;
            }

            public String getGuige() {
                return guige;
            }

            public void setGuige(String guige) {
                this.guige = guige;
            }

            public int getGoods_num() {
                return goods_num;
            }

            public void setGoods_num(int goods_num) {
                this.goods_num = goods_num;
            }

            public Object getStart_time() {
                return start_time;
            }

            public void setStart_time(Object start_time) {
                this.start_time = start_time;
            }

            public Object getEnd_time() {
                return end_time;
            }

            public void setEnd_time(Object end_time) {
                this.end_time = end_time;
            }

            public Object getPoints_price() {
                return points_price;
            }

            public void setPoints_price(Object points_price) {
                this.points_price = points_price;
            }

            public int getGoods_type() {
                return goods_type;
            }

            public void setGoods_type(int goods_type) {
                this.goods_type = goods_type;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public int getSales_sum() {
                return sales_sum;
            }

            public void setSales_sum(int sales_sum) {
                this.sales_sum = sales_sum;
            }

            public String getGoods_remark() {
                return goods_remark;
            }

            public void setGoods_remark(String goods_remark) {
                this.goods_remark = goods_remark;
            }

            public Object getGoods_view() {
                return goods_view;
            }

            public void setGoods_view(Object goods_view) {
                this.goods_view = goods_view;
            }

            public int getIs_recommend() {
                return is_recommend;
            }

            public void setIs_recommend(int is_recommend) {
                this.is_recommend = is_recommend;
            }

            public Object getRecommend_time() {
                return recommend_time;
            }

            public void setRecommend_time(Object recommend_time) {
                this.recommend_time = recommend_time;
            }

            public Object getIs_new() {
                return is_new;
            }

            public void setIs_new(Object is_new) {
                this.is_new = is_new;
            }

            public Object getIs_hot() {
                return is_hot;
            }

            public void setIs_hot(Object is_hot) {
                this.is_hot = is_hot;
            }

            public Object getIs_shop() {
                return is_shop;
            }

            public void setIs_shop(Object is_shop) {
                this.is_shop = is_shop;
            }

            public Object getIs_send() {
                return is_send;
            }

            public void setIs_send(Object is_send) {
                this.is_send = is_send;
            }

            public int getIs_guige() {
                return is_guige;
            }

            public void setIs_guige(int is_guige) {
                this.is_guige = is_guige;
            }

            public String getBuyknow() {
                return buyknow;
            }

            public void setBuyknow(String buyknow) {
                this.buyknow = buyknow;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(int goods_id) {
                this.goods_id = goods_id;
            }
        }
    }
}
