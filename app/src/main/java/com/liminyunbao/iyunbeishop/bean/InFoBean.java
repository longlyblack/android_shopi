package com.liminyunbao.iyunbeishop.bean;

/**
 * Created by Administrator on 2017/4/12 0012.
 */

public class InFoBean {

    /**
     * cmd : info
     * status : 1
     * data : {"dingdanshu":"5","jiaoyie":"0"}
     */

    private String cmd;
    private String status;
    private DataBean data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * dingdanshu : 5
         * jiaoyie : 0
         */

        private String dingdanshu;
        private String jiaoyie;

        public String getDingdanshu() {
            return dingdanshu;
        }

        public void setDingdanshu(String dingdanshu) {
            this.dingdanshu = dingdanshu;
        }

        public String getJiaoyie() {
            return jiaoyie;
        }

        public void setJiaoyie(String jiaoyie) {
            this.jiaoyie = jiaoyie;
        }
    }
}
