package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/5/10 0010.
 */

public class CategoryBean {

    /**
     * cmd : maincategory
     * status : 1
     * data : {"key":[{"id":1,"name":"吃喝","data":[{"cate_id":2,"cate_name":"火锅"},{"cate_id":3,"cate_name":"自助烧烤"},{"cate_id":4,"cate_name":"小吃快餐"},{"cate_id":5,"cate_name":"日韩料理"},{"cate_id":6,"cate_name":"川湘菜"},{"cate_id":7,"cate_name":"甜点饮品"},{"cate_id":8,"cate_name":"香锅烤鱼"},{"cate_id":9,"cate_name":"其他"}]},{"id":10,"name":"玩乐","data":[{"cate_id":11,"cate_name":"KTV"},{"cate_id":12,"cate_name":"电影"},{"cate_id":13,"cate_name":"运动健身"},{"cate_id":14,"cate_name":"桌游棋牌"},{"cate_id":15,"cate_name":"本地游玩"},{"cate_id":16,"cate_name":"养生"},{"cate_id":17,"cate_name":"其他"},{"cate_id":37,"cate_name":"电玩网吧"}]},{"id":18,"name":"生活","data":[{"cate_id":19,"cate_name":"汽车服务"},{"cate_id":20,"cate_name":"家装服务"},{"cate_id":21,"cate_name":"洗浴按摩"},{"cate_id":22,"cate_name":"婚纱写真"},{"cate_id":23,"cate_name":"丽人"},{"cate_id":24,"cate_name":"家电维修"},{"cate_id":25,"cate_name":"教育"},{"cate_id":26,"cate_name":"其他"}]},{"id":27,"name":"购物","data":[{"cate_id":28,"cate_name":"男女服饰"},{"cate_id":29,"cate_name":"鞋靴箱包"},{"cate_id":30,"cate_name":"数码家电"},{"cate_id":31,"cate_name":"首饰珠宝"},{"cate_id":32,"cate_name":"个护化妆"},{"cate_id":33,"cate_name":"母婴玩具"},{"cate_id":34,"cate_name":"家居家纺"},{"cate_id":36,"cate_name":"其他"}]}]}
     */

    private String cmd;
    private String status;
    private TatilBean data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TatilBean getData() {
        return data;
    }

    public void setData(TatilBean data) {
        this.data = data;
    }

    public static class TatilBean {
        private List<KeyBean> key;

        public List<KeyBean> getKey() {
            return key;
        }

        public void setKey(List<KeyBean> key) {
            this.key = key;
        }

        public static class KeyBean {
            /**
             * id : 1
             * name : 吃喝
             * data : [{"cate_id":2,"cate_name":"火锅"},{"cate_id":3,"cate_name":"自助烧烤"},{"cate_id":4,"cate_name":"小吃快餐"},{"cate_id":5,"cate_name":"日韩料理"},{"cate_id":6,"cate_name":"川湘菜"},{"cate_id":7,"cate_name":"甜点饮品"},{"cate_id":8,"cate_name":"香锅烤鱼"},{"cate_id":9,"cate_name":"其他"}]
             */

            private int id;
            private String name;
            private List<DataBean> data;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<DataBean> getData() {
                return data;
            }

            public void setData(List<DataBean> data) {
                this.data = data;
            }

            public static class DataBean {
                /**
                 * cate_id : 2
                 * cate_name : 火锅
                 */

                private int cate_id;
                private String cate_name;

                public int getCate_id() {
                    return cate_id;
                }

                public void setCate_id(int cate_id) {
                    this.cate_id = cate_id;
                }

                public String getCate_name() {
                    return cate_name;
                }

                public void setCate_name(String cate_name) {
                    this.cate_name = cate_name;
                }
            }
        }
    }
}
