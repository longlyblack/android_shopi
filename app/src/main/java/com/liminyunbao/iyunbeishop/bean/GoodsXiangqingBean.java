package com.liminyunbao.iyunbeishop.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2017/6/8 0008.
 */

public class GoodsXiangqingBean  implements Serializable {

    /**
     * cmd : goodsinfo
     * status : 1
     * data : {"title":"小雪秒杀","photo":[{"pic_id":669,"photo":"2017/06/19/20170619141658_4444.jpg","goods_id":30515356,"created_at":"2017-06-19 16:36:03","updated_at":"2017-06-19 16:36:03","deleted_at":null}],"buyknow":"恶 让他去问他","simple_title":"额七","sunnum":110,"guige":"税费","sold_num":1,"market_price":"1","goods_price":"0.01","goods_remark":"请我","base_goods_id":30515356,"cate_id":447,"xinghaolist":[{"spec_id":791,"goods_id":30515356,"name":"drew5","price":"0.01","num":"110","sort":"17","create_time":null,"deleted_at":null,"updated_at":"2017-06-26 09:53:15","created_at":"2017-06-19 14:17:01"}],"cate_name":"打底裤","catea_name":[{"cate_name":"用途的","cate_id":455},{"cate_name":"阿斯额","cate_id":453}],"picture":[{"pic_id":2966,"goods_id":30515356,"photo":"2017/06/19/20170619141640_5777.jpg","title":null,"orderby":100,"audit":0,"create_time":null,"create_ip":null,"deleted_at":null,"updated_at":"2017-06-26 09:53:15","created_at":"2017-06-19 16:36:03"}],"always":[{"cate_id":439,"cate_name":"针织衫"},{"cate_id":441,"cate_name":"卫衣"}]}
     */

    private String cmd;
    private int status;
    private DataBean data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : 小雪秒杀
         * photo : [{"pic_id":669,"photo":"2017/06/19/20170619141658_4444.jpg","goods_id":30515356,"created_at":"2017-06-19 16:36:03","updated_at":"2017-06-19 16:36:03","deleted_at":null}]
         * buyknow : 恶 让他去问他
         * simple_title : 额七
         * sunnum : 110
         * guige : 税费
         * sold_num : 1
         * market_price : 1
         * goods_price : 0.01
         * goods_remark : 请我
         * base_goods_id : 30515356
         * cate_id : 447
         * xinghaolist : [{"spec_id":791,"goods_id":30515356,"name":"drew5","price":"0.01","num":"110","sort":"17","create_time":null,"deleted_at":null,"updated_at":"2017-06-26 09:53:15","created_at":"2017-06-19 14:17:01"}]
         * cate_name : 打底裤
         * catea_name : [{"cate_name":"用途的","cate_id":455},{"cate_name":"阿斯额","cate_id":453}]
         * picture : [{"pic_id":2966,"goods_id":30515356,"photo":"2017/06/19/20170619141640_5777.jpg","title":null,"orderby":100,"audit":0,"create_time":null,"create_ip":null,"deleted_at":null,"updated_at":"2017-06-26 09:53:15","created_at":"2017-06-19 16:36:03"}]
         * always : [{"cate_id":439,"cate_name":"针织衫"},{"cate_id":441,"cate_name":"卫衣"}]
         */

        private String title;
        private String buyknow;
        private String simple_title;
        private int sunnum;
        private String guige;
        private int sold_num;
        private String market_price;
        private String goods_price;
        private String goods_remark;
        private int base_goods_id;
        private int cate_id;
        private String cate_name;
        private List<PhotoBean> photo;
        private List<XinghaolistBean> xinghaolist;
        private List<CateaNameBean> catea_name;
        private List<PictureBean> picture;
        private List<AlwaysBean> always;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBuyknow() {
            return buyknow;
        }

        public void setBuyknow(String buyknow) {
            this.buyknow = buyknow;
        }

        public String getSimple_title() {
            return simple_title;
        }

        public void setSimple_title(String simple_title) {
            this.simple_title = simple_title;
        }

        public int getSunnum() {
            return sunnum;
        }

        public void setSunnum(int sunnum) {
            this.sunnum = sunnum;
        }

        public String getGuige() {
            return guige;
        }

        public void setGuige(String guige) {
            this.guige = guige;
        }

        public int getSold_num() {
            return sold_num;
        }

        public void setSold_num(int sold_num) {
            this.sold_num = sold_num;
        }

        public String getMarket_price() {
            return market_price;
        }

        public void setMarket_price(String market_price) {
            this.market_price = market_price;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getGoods_remark() {
            return goods_remark;
        }

        public void setGoods_remark(String goods_remark) {
            this.goods_remark = goods_remark;
        }

        public int getBase_goods_id() {
            return base_goods_id;
        }

        public void setBase_goods_id(int base_goods_id) {
            this.base_goods_id = base_goods_id;
        }

        public int getCate_id() {
            return cate_id;
        }

        public void setCate_id(int cate_id) {
            this.cate_id = cate_id;
        }

        public String getCate_name() {
            return cate_name;
        }

        public void setCate_name(String cate_name) {
            this.cate_name = cate_name;
        }

        public List<PhotoBean> getPhoto() {
            return photo;
        }

        public void setPhoto(List<PhotoBean> photo) {
            this.photo = photo;
        }

        public List<XinghaolistBean> getXinghaolist() {
            return xinghaolist;
        }

        public void setXinghaolist(List<XinghaolistBean> xinghaolist) {
            this.xinghaolist = xinghaolist;
        }

        public List<CateaNameBean> getCatea_name() {
            return catea_name;
        }

        public void setCatea_name(List<CateaNameBean> catea_name) {
            this.catea_name = catea_name;
        }

        public List<PictureBean> getPicture() {
            return picture;
        }

        public void setPicture(List<PictureBean> picture) {
            this.picture = picture;
        }

        public List<AlwaysBean> getAlways() {
            return always;
        }

        public void setAlways(List<AlwaysBean> always) {
            this.always = always;
        }

        public static class PhotoBean  implements Serializable{
            /**
             * pic_id : 669
             * photo : 2017/06/19/20170619141658_4444.jpg
             * goods_id : 30515356
             * created_at : 2017-06-19 16:36:03
             * updated_at : 2017-06-19 16:36:03
             * deleted_at : null
             */

            private int pic_id;
            private String photo;
            private int goods_id;
            private String created_at;
            private String updated_at;
            private Object deleted_at;

            public int getPic_id() {
                return pic_id;
            }

            public void setPic_id(int pic_id) {
                this.pic_id = pic_id;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public int getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(int goods_id) {
                this.goods_id = goods_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }
        }

        public static class XinghaolistBean implements Serializable {
            public XinghaolistBean(int spec_id, String name, String price, String num) {
                this.spec_id = spec_id;
                this.name = name;
                this.price = price;
                this.num = num;
            }
            /**
             * spec_id : 791
             * goods_id : 30515356
             * name : drew5
             * price : 0.01
             * num : 110
             * sort : 17
             * create_time : null
             * deleted_at : null
             * updated_at : 2017-06-26 09:53:15
             * created_at : 2017-06-19 14:17:01
             */

            private int spec_id;
            private int goods_id;
            private String name;
            private String price;
            private String num;
            private String sort;
            private Object create_time;
            private Object deleted_at;
            private String updated_at;
            private String created_at;

            public int getSpec_id() {
                return spec_id;
            }

            public void setSpec_id(int spec_id) {
                this.spec_id = spec_id;
            }

            public int getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(int goods_id) {
                this.goods_id = goods_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public Object getCreate_time() {
                return create_time;
            }

            public void setCreate_time(Object create_time) {
                this.create_time = create_time;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }
        }

        public static class CateaNameBean {
            /**
             * cate_name : 用途的
             * cate_id : 455
             */

            private String cate_name;
            private int cate_id;

            public String getCate_name() {
                return cate_name;
            }

            public void setCate_name(String cate_name) {
                this.cate_name = cate_name;
            }

            public int getCate_id() {
                return cate_id;
            }

            public void setCate_id(int cate_id) {
                this.cate_id = cate_id;
            }
        }

        public static class PictureBean {
            /**
             * pic_id : 2966
             * goods_id : 30515356
             * photo : 2017/06/19/20170619141640_5777.jpg
             * title : null
             * orderby : 100
             * audit : 0
             * create_time : null
             * create_ip : null
             * deleted_at : null
             * updated_at : 2017-06-26 09:53:15
             * created_at : 2017-06-19 16:36:03
             */

            private int pic_id;
            private int goods_id;
            private String photo;
            private Object title;
            private int orderby;
            private int audit;
            private Object create_time;
            private Object create_ip;
            private Object deleted_at;
            private String updated_at;
            private String created_at;

            public int getPic_id() {
                return pic_id;
            }

            public void setPic_id(int pic_id) {
                this.pic_id = pic_id;
            }

            public int getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(int goods_id) {
                this.goods_id = goods_id;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public Object getTitle() {
                return title;
            }

            public void setTitle(Object title) {
                this.title = title;
            }

            public int getOrderby() {
                return orderby;
            }

            public void setOrderby(int orderby) {
                this.orderby = orderby;
            }

            public int getAudit() {
                return audit;
            }

            public void setAudit(int audit) {
                this.audit = audit;
            }

            public Object getCreate_time() {
                return create_time;
            }

            public void setCreate_time(Object create_time) {
                this.create_time = create_time;
            }

            public Object getCreate_ip() {
                return create_ip;
            }

            public void setCreate_ip(Object create_ip) {
                this.create_ip = create_ip;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }
        }

        public static class AlwaysBean {
            /**
             * cate_id : 439
             * cate_name : 针织衫
             */

            private int cate_id;
            private String cate_name;

            public int getCate_id() {
                return cate_id;
            }

            public void setCate_id(int cate_id) {
                this.cate_id = cate_id;
            }

            public String getCate_name() {
                return cate_name;
            }

            public void setCate_name(String cate_name) {
                this.cate_name = cate_name;
            }
        }
    }
}
