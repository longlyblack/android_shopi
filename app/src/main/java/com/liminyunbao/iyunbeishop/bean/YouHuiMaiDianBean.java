package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/10 0010.
 */

public class YouHuiMaiDianBean {

    /**
     * cmd : discount
     * status : 1
     * data : {"total":19,"per_page":5,"current_page":1,"last_page":4,"next_page_url":"http://1shop.iyunbei.net/api/Package/discount?page=2","prev_page_url":null,"from":1,"to":5,"data":[{"need_pay":2,"create_time":1487232203},{"need_pay":2,"create_time":1487555928},{"need_pay":2,"create_time":1487556121},{"need_pay":2,"create_time":1487556174},{"need_pay":33,"create_time":1483692737}]}
     */

    private String cmd;
    private String status;
    private DataBeanX data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class DataBeanX {
        /**
         * total : 19
         * per_page : 5
         * current_page : 1
         * last_page : 4
         * next_page_url : http://1shop.iyunbei.net/api/Package/discount?page=2
         * prev_page_url : null
         * from : 1
         * to : 5
         * data : [{"need_pay":2,"create_time":1487232203},{"need_pay":2,"create_time":1487555928},{"need_pay":2,"create_time":1487556121},{"need_pay":2,"create_time":1487556174},{"need_pay":33,"create_time":1483692737}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private String next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<DataBean> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(String next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * need_pay : 2
             * create_time : 1487232203
             */

            private int need_pay;
            private int create_time;

            public int getNeed_pay() {
                return need_pay;
            }

            public void setNeed_pay(int need_pay) {
                this.need_pay = need_pay;
            }

            public int getCreate_time() {
                return create_time;
            }

            public void setCreate_time(int create_time) {
                this.create_time = create_time;
            }
        }
    }
}
