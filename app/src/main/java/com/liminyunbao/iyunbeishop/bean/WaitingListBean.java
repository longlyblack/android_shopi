package com.liminyunbao.iyunbeishop.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2017/4/28 0028.
 */

public class WaitingListBean {


    /**
     * cmd : orderlist
     * status : 1
     * data : {"total":5,"per_page":5,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":5,"data":[{"order_id":213481,"consignee":"周遥","mobile":"18338131623","address":"锦江之星酒店(南阳建设中路店) 文秀花园","order_amount":"3.98","total_amount":"3.98","order_status":1,"created_at":"2017-05-22 18:13:52","receive_time":"2017-05-22 20:07:47","ok_time":null,"freight":0,"ps_time":"\n\t\t\t\t\t\t预约配送\n\t\t\t\t\t","shipping_time":null,"goods_id":30513645,"photo":"2017/05/22/20170522112156_1513.png","goods_name":"新鲜香蕉","goods_guige":"1斤","goods_num":1,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15857,"order_id":213481,"goods_id":30513645,"photo":"2017/05/22/20170522112156_1513.png","goods_name":"新鲜香蕉","goods_guige":"1斤","goods_num":1,"point_price":null,"goods_price":"3.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 18:13:52","created_at":"2017-05-22 18:13:52","note":null,"pay_id":null}]},{"order_id":213464,"consignee":"海岚博","mobile":"13721839080","address":"南阳市卧龙区便民服务中心 文化路与中心市场交叉口向东50米天冠小区七号楼一单元二楼西户","order_amount":5092,"total_amount":5092,"order_status":2,"created_at":"2017-05-22 16:06:46","receive_time":"2017-05-22 17:43:37","ok_time":null,"freight":0,"ps_time":"\n\t\t\t\t\t\t预约配送\n\t\t\t\t\t","shipping_time":"2017-05-22 18:45:02","goods_id":30513658,"photo":"2017/05/09/20170509162441_7777.png","goods_name":"库尔勒小香梨","goods_guige":"1斤","goods_num":2,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15837,"order_id":213464,"goods_id":30513658,"photo":"2017/05/09/20170509162441_7777.png","goods_name":"库尔勒小香梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"7.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15838,"order_id":213464,"goods_id":30513661,"photo":"2017/05/09/20170509162936_2871.png","goods_name":"砀山梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"2.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15839,"order_id":213464,"goods_id":30513723,"photo":"2017/05/22/20170522103757_8334.jpg","goods_name":"水果拼盘（含绿提、火龙果等约8种水果）","goods_guige":"","goods_num":1,"point_price":null,"goods_price":"29","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null}]},{"order_id":213464,"consignee":"海岚博","mobile":"13721839080","address":"南阳市卧龙区便民服务中心 文化路与中心市场交叉口向东50米天冠小区七号楼一单元二楼西户","order_amount":5092,"total_amount":5092,"order_status":2,"created_at":"2017-05-22 16:06:46","receive_time":"2017-05-22 17:43:37","ok_time":null,"freight":0,"ps_time":"\n\t\t\t\t\t\t预约配送\n\t\t\t\t\t","shipping_time":"2017-05-22 18:45:02","goods_id":30513661,"photo":"2017/05/09/20170509162936_2871.png","goods_name":"砀山梨","goods_guige":"1斤","goods_num":2,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15837,"order_id":213464,"goods_id":30513658,"photo":"2017/05/09/20170509162441_7777.png","goods_name":"库尔勒小香梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"7.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15838,"order_id":213464,"goods_id":30513661,"photo":"2017/05/09/20170509162936_2871.png","goods_name":"砀山梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"2.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15839,"order_id":213464,"goods_id":30513723,"photo":"2017/05/22/20170522103757_8334.jpg","goods_name":"水果拼盘（含绿提、火龙果等约8种水果）","goods_guige":"","goods_num":1,"point_price":null,"goods_price":"29","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null}]},{"order_id":213464,"consignee":"海岚博","mobile":"13721839080","address":"南阳市卧龙区便民服务中心 文化路与中心市场交叉口向东50米天冠小区七号楼一单元二楼西户","order_amount":5092,"total_amount":5092,"order_status":2,"created_at":"2017-05-22 16:06:46","receive_time":"2017-05-22 17:43:37","ok_time":null,"freight":0,"ps_time":"\n\t\t\t\t\t\t预约配送\n\t\t\t\t\t","shipping_time":"2017-05-22 18:45:02","goods_id":30513723,"photo":"2017/05/22/20170522103757_8334.jpg","goods_name":"水果拼盘（含绿提、火龙果等约8种水果）","goods_guige":"","goods_num":1,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15837,"order_id":213464,"goods_id":30513658,"photo":"2017/05/09/20170509162441_7777.png","goods_name":"库尔勒小香梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"7.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15838,"order_id":213464,"goods_id":30513661,"photo":"2017/05/09/20170509162936_2871.png","goods_name":"砀山梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"2.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15839,"order_id":213464,"goods_id":30513723,"photo":"2017/05/22/20170522103757_8334.jpg","goods_name":"水果拼盘（含绿提、火龙果等约8种水果）","goods_guige":"","goods_num":1,"point_price":null,"goods_price":"29","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null}]},{"order_id":213412,"consignee":"丁建云","mobile":"15890889251","address":"中国建设银行(建西分理处) 二楼东户","order_amount":3900,"total_amount":3900,"order_status":2,"created_at":"2017-05-22 10:05:20","receive_time":"2017-05-22 10:20:16","ok_time":null,"freight":0,"ps_time":"立即配送","shipping_time":"2017-05-22 11:13:54","goods_id":30513721,"photo":"2017/05/10/20170510120159_8746.jpg","goods_name":"水果拼盘","goods_guige":"","goods_num":1,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15727,"order_id":213412,"goods_id":30513721,"photo":"2017/05/10/20170510120159_8746.jpg","goods_name":"水果拼盘","goods_guige":"","goods_num":1,"point_price":null,"goods_price":"39","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 10:05:20","created_at":"2017-05-22 10:05:20","note":null,"pay_id":null}]}]}
     */

    private String cmd;
    private String status;
    private DataBean data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * total : 5
         * per_page : 5
         * current_page : 1
         * last_page : 1
         * next_page_url : null
         * prev_page_url : null
         * from : 1
         * to : 5
         * data : [{"order_id":213481,"consignee":"周遥","mobile":"18338131623","address":"锦江之星酒店(南阳建设中路店) 文秀花园","order_amount":"3.98","total_amount":"3.98","order_status":1,"created_at":"2017-05-22 18:13:52","receive_time":"2017-05-22 20:07:47","ok_time":null,"freight":0,"ps_time":"\n\t\t\t\t\t\t预约配送\n\t\t\t\t\t","shipping_time":null,"goods_id":30513645,"photo":"2017/05/22/20170522112156_1513.png","goods_name":"新鲜香蕉","goods_guige":"1斤","goods_num":1,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15857,"order_id":213481,"goods_id":30513645,"photo":"2017/05/22/20170522112156_1513.png","goods_name":"新鲜香蕉","goods_guige":"1斤","goods_num":1,"point_price":null,"goods_price":"3.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 18:13:52","created_at":"2017-05-22 18:13:52","note":null,"pay_id":null}]},{"order_id":213464,"consignee":"海岚博","mobile":"13721839080","address":"南阳市卧龙区便民服务中心 文化路与中心市场交叉口向东50米天冠小区七号楼一单元二楼西户","order_amount":5092,"total_amount":5092,"order_status":2,"created_at":"2017-05-22 16:06:46","receive_time":"2017-05-22 17:43:37","ok_time":null,"freight":0,"ps_time":"\n\t\t\t\t\t\t预约配送\n\t\t\t\t\t","shipping_time":"2017-05-22 18:45:02","goods_id":30513658,"photo":"2017/05/09/20170509162441_7777.png","goods_name":"库尔勒小香梨","goods_guige":"1斤","goods_num":2,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15837,"order_id":213464,"goods_id":30513658,"photo":"2017/05/09/20170509162441_7777.png","goods_name":"库尔勒小香梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"7.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15838,"order_id":213464,"goods_id":30513661,"photo":"2017/05/09/20170509162936_2871.png","goods_name":"砀山梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"2.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15839,"order_id":213464,"goods_id":30513723,"photo":"2017/05/22/20170522103757_8334.jpg","goods_name":"水果拼盘（含绿提、火龙果等约8种水果）","goods_guige":"","goods_num":1,"point_price":null,"goods_price":"29","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null}]},{"order_id":213464,"consignee":"海岚博","mobile":"13721839080","address":"南阳市卧龙区便民服务中心 文化路与中心市场交叉口向东50米天冠小区七号楼一单元二楼西户","order_amount":5092,"total_amount":5092,"order_status":2,"created_at":"2017-05-22 16:06:46","receive_time":"2017-05-22 17:43:37","ok_time":null,"freight":0,"ps_time":"\n\t\t\t\t\t\t预约配送\n\t\t\t\t\t","shipping_time":"2017-05-22 18:45:02","goods_id":30513661,"photo":"2017/05/09/20170509162936_2871.png","goods_name":"砀山梨","goods_guige":"1斤","goods_num":2,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15837,"order_id":213464,"goods_id":30513658,"photo":"2017/05/09/20170509162441_7777.png","goods_name":"库尔勒小香梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"7.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15838,"order_id":213464,"goods_id":30513661,"photo":"2017/05/09/20170509162936_2871.png","goods_name":"砀山梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"2.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15839,"order_id":213464,"goods_id":30513723,"photo":"2017/05/22/20170522103757_8334.jpg","goods_name":"水果拼盘（含绿提、火龙果等约8种水果）","goods_guige":"","goods_num":1,"point_price":null,"goods_price":"29","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null}]},{"order_id":213464,"consignee":"海岚博","mobile":"13721839080","address":"南阳市卧龙区便民服务中心 文化路与中心市场交叉口向东50米天冠小区七号楼一单元二楼西户","order_amount":5092,"total_amount":5092,"order_status":2,"created_at":"2017-05-22 16:06:46","receive_time":"2017-05-22 17:43:37","ok_time":null,"freight":0,"ps_time":"\n\t\t\t\t\t\t预约配送\n\t\t\t\t\t","shipping_time":"2017-05-22 18:45:02","goods_id":30513723,"photo":"2017/05/22/20170522103757_8334.jpg","goods_name":"水果拼盘（含绿提、火龙果等约8种水果）","goods_guige":"","goods_num":1,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15837,"order_id":213464,"goods_id":30513658,"photo":"2017/05/09/20170509162441_7777.png","goods_name":"库尔勒小香梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"7.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15838,"order_id":213464,"goods_id":30513661,"photo":"2017/05/09/20170509162936_2871.png","goods_name":"砀山梨","goods_guige":"1斤","goods_num":2,"point_price":null,"goods_price":"2.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null},{"rec_id":15839,"order_id":213464,"goods_id":30513723,"photo":"2017/05/22/20170522103757_8334.jpg","goods_name":"水果拼盘（含绿提、火龙果等约8种水果）","goods_guige":"","goods_num":1,"point_price":null,"goods_price":"29","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 16:06:46","created_at":"2017-05-22 16:06:46","note":null,"pay_id":null}]},{"order_id":213412,"consignee":"丁建云","mobile":"15890889251","address":"中国建设银行(建西分理处) 二楼东户","order_amount":3900,"total_amount":3900,"order_status":2,"created_at":"2017-05-22 10:05:20","receive_time":"2017-05-22 10:20:16","ok_time":null,"freight":0,"ps_time":"立即配送","shipping_time":"2017-05-22 11:13:54","goods_id":30513721,"photo":"2017/05/10/20170510120159_8746.jpg","goods_name":"水果拼盘","goods_guige":"","goods_num":1,"shop_name":"南阳市高新区果品汇","rid_nickname":"云贝骑士","data":[{"rec_id":15727,"order_id":213412,"goods_id":30513721,"photo":"2017/05/10/20170510120159_8746.jpg","goods_name":"水果拼盘","goods_guige":"","goods_num":1,"point_price":null,"goods_price":"39","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 10:05:20","created_at":"2017-05-22 10:05:20","note":null,"pay_id":null}]}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private Object next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<OrderMsg> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public Object getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(Object next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<OrderMsg> getData() {
            return data;
        }

        public void setData(List<OrderMsg> data) {
            this.data = data;
        }

        public static class OrderMsg implements Serializable {
            /**
             * order_id : 213481
             * consignee : 周遥
             * mobile : 18338131623
             * address : 锦江之星酒店(南阳建设中路店) 文秀花园
             * order_amount : 3.98
             * total_amount : 3.98
             * order_status : 1
             * created_at : 2017-05-22 18:13:52
             * receive_time : 2017-05-22 20:07:47
             * ok_time : null
             * freight : 0
             * ps_time :
             预约配送

             * shipping_time : null
             * goods_id : 30513645
             * photo : 2017/05/22/20170522112156_1513.png
             * goods_name : 新鲜香蕉
             * goods_guige : 1斤
             * goods_num : 1
             * shop_name : 南阳市高新区果品汇
             * rid_nickname : 云贝骑士
             * data : [{"rec_id":15857,"order_id":213481,"goods_id":30513645,"photo":"2017/05/22/20170522112156_1513.png","goods_name":"新鲜香蕉","goods_guige":"1斤","goods_num":1,"point_price":null,"goods_price":"3.98","cost_price":0,"deleted_at":null,"updated_at":"2017-05-22 18:13:52","created_at":"2017-05-22 18:13:52","note":null,"pay_id":null}]
             */

            private int order_id;
            private String note;
            private String consignee;
            private String mobile;
            private String address;
            private String order_amount;
            private String total_amount;
            private int order_status;
            private String created_at;
            private String receive_time;
            private Object ok_time;
            private String freight;
            private String ps_time;
            private Object shipping_time;
            private int goods_id;
            private String photo;
            private String goods_name;
            private String goods_guige;
            private int goods_num;
            private String shop_name;
            private String rid_nickname;
            private List<GoodsList> data;

            public String getNote() {
                return note;
            }

            public void setNote(String note) {
                this.note = note;
            }

            public int getOrder_id() {
                return order_id;
            }

            public void setOrder_id(int order_id) {
                this.order_id = order_id;
            }

            public String getConsignee() {
                return consignee;
            }

            public void setConsignee(String consignee) {
                this.consignee = consignee;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getOrder_amount() {
                return order_amount;
            }

            public void setOrder_amount(String order_amount) {
                this.order_amount = order_amount;
            }

            public String getTotal_amount() {
                return total_amount;
            }

            public void setTotal_amount(String total_amount) {
                this.total_amount = total_amount;
            }

            public int getOrder_status() {
                return order_status;
            }

            public void setOrder_status(int order_status) {
                this.order_status = order_status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getReceive_time() {
                return receive_time;
            }

            public void setReceive_time(String receive_time) {
                this.receive_time = receive_time;
            }

            public Object getOk_time() {
                return ok_time;
            }

            public void setOk_time(Object ok_time) {
                this.ok_time = ok_time;
            }

            public String getFreight() {
                return freight;
            }

            public void setFreight(String freight) {
                this.freight = freight;
            }

            public String getPs_time() {
                return ps_time;
            }

            public void setPs_time(String ps_time) {
                this.ps_time = ps_time;
            }

            public Object getShipping_time() {
                return shipping_time;
            }

            public void setShipping_time(Object shipping_time) {
                this.shipping_time = shipping_time;
            }

            public int getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(int goods_id) {
                this.goods_id = goods_id;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getGoods_guige() {
                return goods_guige;
            }

            public void setGoods_guige(String goods_guige) {
                this.goods_guige = goods_guige;
            }

            public int getGoods_num() {
                return goods_num;
            }

            public void setGoods_num(int goods_num) {
                this.goods_num = goods_num;
            }

            public String getShop_name() {
                return shop_name;
            }

            public void setShop_name(String shop_name) {
                this.shop_name = shop_name;
            }

            public String getRid_nickname() {
                return rid_nickname;
            }

            public void setRid_nickname(String rid_nickname) {
                this.rid_nickname = rid_nickname;
            }

            public List<GoodsList> getData() {
                return data;
            }

            public void setData(List<GoodsList> data) {
                this.data = data;
            }

            public static class GoodsList implements Serializable {
                public GoodsList(String photo, String goods_name, String goods_guige, int goods_num, String goods_price) {
                    this.photo = photo;
                    this.goods_name = goods_name;
                    this.goods_guige = goods_guige;
                    this.goods_num = goods_num;
                    this.goods_price = goods_price;
                }

                /**
                 * rec_id : 15857
                 * order_id : 213481
                 * goods_id : 30513645
                 * photo : 2017/05/22/20170522112156_1513.png
                 * goods_name : 新鲜香蕉
                 * goods_guige : 1斤
                 * goods_num : 1
                 * point_price : null
                 * goods_price : 3.98
                 * cost_price : 0
                 * deleted_at : null
                 * updated_at : 2017-05-22 18:13:52
                 * created_at : 2017-05-22 18:13:52
                 * note : null
                 * pay_id : null
                 */


                private int rec_id;
                private int order_id;
                private int goods_id;
                private String photo;
                private String goods_name;
                private String goods_guige;
                private int goods_num;
                private Object point_price;
                private String goods_price;
                private int cost_price;
                private Object deleted_at;
                private String updated_at;
                private String created_at;
                private Object note;
                private Object pay_id;

                public int getRec_id() {
                    return rec_id;
                }

                public void setRec_id(int rec_id) {
                    this.rec_id = rec_id;
                }

                public int getOrder_id() {
                    return order_id;
                }

                public void setOrder_id(int order_id) {
                    this.order_id = order_id;
                }

                public int getGoods_id() {
                    return goods_id;
                }

                public void setGoods_id(int goods_id) {
                    this.goods_id = goods_id;
                }

                public String getPhoto() {
                    return photo;
                }

                public void setPhoto(String photo) {
                    this.photo = photo;
                }

                public String getGoods_name() {
                    return goods_name;
                }

                public void setGoods_name(String goods_name) {
                    this.goods_name = goods_name;
                }

                public String getGoods_guige() {
                    return goods_guige;
                }

                public void setGoods_guige(String goods_guige) {
                    this.goods_guige = goods_guige;
                }

                public int getGoods_num() {
                    return goods_num;
                }

                public void setGoods_num(int goods_num) {
                    this.goods_num = goods_num;
                }

                public Object getPoint_price() {
                    return point_price;
                }

                public void setPoint_price(Object point_price) {
                    this.point_price = point_price;
                }

                public String getGoods_price() {
                    return goods_price;
                }

                public void setGoods_price(String goods_price) {
                    this.goods_price = goods_price;
                }

                public int getCost_price() {
                    return cost_price;
                }

                public void setCost_price(int cost_price) {
                    this.cost_price = cost_price;
                }

                public Object getDeleted_at() {
                    return deleted_at;
                }

                public void setDeleted_at(Object deleted_at) {
                    this.deleted_at = deleted_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public Object getNote() {
                    return note;
                }

                public void setNote(Object note) {
                    this.note = note;
                }

                public Object getPay_id() {
                    return pay_id;
                }

                public void setPay_id(Object pay_id) {
                    this.pay_id = pay_id;
                }
            }
        }
    }
}
