package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/5/13 0013.
 */

public class ShangPinGroup {


    /**
     * cmd : fenzu
     * status : 1
     * data : {"msg":[{"catea_id":1,"catea_name":"西餐"},{"catea_id":2,"catea_name":"中餐"},{"catea_id":3,"catea_name":"霸王餐"},{"catea_id":4,"catea_name":"川菜菜"},{"catea_id":5,"catea_name":"主菜单"},{"catea_id":6,"catea_name":"鱼生刺身"},{"catea_id":7,"catea_name":"大盘鱼"},{"catea_id":8,"catea_name":"测试1"},{"catea_id":9,"catea_name":"测试1-1"},{"catea_id":10,"catea_name":"一鱼多食"},{"catea_id":11,"catea_name":"粥水锅底"},{"catea_id":12,"catea_name":"套餐类"},{"catea_id":13,"catea_name":"A1套餐498元"},{"catea_id":14,"catea_name":"A2套餐428元"},{"catea_id":15,"catea_name":"A1套餐498元"},{"catea_id":16,"catea_name":"B1套餐438元"},{"catea_id":17,"catea_name":"B2套餐308元"},{"catea_id":18,"catea_name":"C1套餐188元"},{"catea_id":19,"catea_name":"C2套餐188元"},{"catea_id":20,"catea_name":"川味厨房"},{"catea_id":21,"catea_name":"特色菜"},{"catea_id":22,"catea_name":"红烧罗非鱼"},{"catea_id":23,"catea_name":"焗鱼头"},{"catea_id":24,"catea_name":"鱼类"},{"catea_id":25,"catea_name":"菜谱"},{"catea_id":26,"catea_name":"小炒"},{"catea_id":27,"catea_name":"素菜"},{"catea_id":28,"catea_name":"凉菜"},{"catea_id":29,"catea_name":"汤类"},{"catea_id":30,"catea_name":"特色菜"},{"catea_id":31,"catea_name":"小炒"},{"catea_id":32,"catea_name":"素菜"},{"catea_id":33,"catea_name":"凉菜"},{"catea_id":34,"catea_name":"汤类"},{"catea_id":35,"catea_name":"主菜单"},{"catea_id":36,"catea_name":"主食类"},{"catea_id":37,"catea_name":"小食类"},{"catea_id":38,"catea_name":"饮料类"},{"catea_id":39,"catea_name":"套餐类"},{"catea_id":40,"catea_name":"环境照片"},{"catea_id":41,"catea_name":"甜点小吃"},{"catea_id":42,"catea_name":"甜点小吃"},{"catea_id":43,"catea_name":"娱乐游玩"},{"catea_id":44,"catea_name":"儿童乐园"},{"catea_id":45,"catea_name":"游玩"},{"catea_id":46,"catea_name":"运动健身"},{"catea_id":47,"catea_name":"运动"},{"catea_id":48,"catea_name":"测试分类1"},{"catea_id":49,"catea_name":"测试分类2"},{"catea_id":50,"catea_name":"ceshi"},{"catea_id":51,"catea_name":"ceshi1"},{"catea_id":52,"catea_name":"测试分类2"},{"catea_id":53,"catea_name":"测试分类2"},{"catea_id":54,"catea_name":"较好的发给客户过来"},{"catea_id":55,"catea_name":"是大法官的是否个"},{"catea_id":56,"catea_name":"人个人个人风格"},{"catea_id":57,"catea_name":"水电费感受到分公司答复"},{"catea_id":58,"catea_name":"啊啊"},{"catea_id":59,"catea_name":"测1"},{"catea_id":60,"catea_name":"00"},{"catea_id":61,"catea_name":"00"},{"catea_id":62,"catea_name":"00"}]}
     */

    private String cmd;
    private String status;
    private DataBean data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<MsgBean> msg;

        public List<MsgBean> getMsg() {
            return msg;
        }

        public void setMsg(List<MsgBean> msg) {
            this.msg = msg;
        }

        public static class MsgBean {
            /**
             * catea_id : 1
             * catea_name : 西餐
             */

            private int catea_id;
            private String catea_name;

            public int getCatea_id() {
                return catea_id;
            }

            public void setCatea_id(int catea_id) {
                this.catea_id = catea_id;
            }

            public String getCatea_name() {
                return catea_name;
            }

            public void setCatea_name(String catea_name) {
                this.catea_name = catea_name;
            }
        }
    }
}
