package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/6/23 0023.
 */

public class DisplayWindowBean {

    /**
     * status : 1
     * cmd : recommendlist
     * data : [{"goods_agent_id":30515374,"goods_name":"你的","num":2874,"sold_num":0,"market_price":12300,"goods_price":3,"photo":"2017/06/19/20170619145947_4533.jpeg"}]
     * max : 15
     * now : 1
     */

    private int status;
    private String cmd;
    private String max;
    private int now;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public int getNow() {
        return now;
    }

    public void setNow(int now) {
        this.now = now;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * goods_agent_id : 30515374
         * goods_name : 你的
         * num : 2874
         * sold_num : 0
         * market_price : 12300
         * goods_price : 3
         * photo : 2017/06/19/20170619145947_4533.jpeg
         */

        private int goods_agent_id;
        private String goods_name;
        private int num;
        private int sold_num;
        private String market_price;
        private String goods_price;
        private String photo;

        public int getGoods_agent_id() {
            return goods_agent_id;
        }

        public void setGoods_agent_id(int goods_agent_id) {
            this.goods_agent_id = goods_agent_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public int getSold_num() {
            return sold_num;
        }

        public void setSold_num(int sold_num) {
            this.sold_num = sold_num;
        }

        public String getMarket_price() {
            return market_price;
        }

        public void setMarket_price(String market_price) {
            this.market_price = market_price;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
