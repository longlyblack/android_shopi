package com.liminyunbao.iyunbeishop.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2017/6/22 0022.
 */

public class MealDetailsBean {

    /**
     * status : 1
     * cmd : mealinfo
     * msg : 读取成功
     * data : {"goods_agent_id":30515432,"goods_name":"肯德基鸡腿堡套餐'","is_mall":0,"miaoshu":"这是介绍","goods_price":12.5,"market_price":222,"buyknow":"notes购买须知","goods_remark":"detail详情描述","img":[{"pic_id":3019,"photo":"2017/06/17/20170617092055_1145.jpeg"}],"tuwen":[{"pic_id":3019,"photo":"2017/06/17/20170617092055_1145.jpeg"}]}
     */

    private int status;
    private String cmd;
    private String msg;
    private GoodsMsgBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public GoodsMsgBean getData() {
        return data;
    }

    public void setData(GoodsMsgBean data) {
        this.data = data;
    }

    public static class GoodsMsgBean implements Serializable{
        /**
         * goods_agent_id : 30515432
         * goods_name : 肯德基鸡腿堡套餐'
         * is_mall : 0
         * miaoshu : 这是介绍
         * goods_price : 12.5
         * market_price : 222
         * buyknow : notes购买须知
         * goods_remark : detail详情描述
         * img : [{"pic_id":3019,"photo":"2017/06/17/20170617092055_1145.jpeg"}]
         * tuwen : [{"pic_id":3019,"photo":"2017/06/17/20170617092055_1145.jpeg"}]
         */

        private int goods_agent_id;
        private String goods_name;
        private int is_mall;
        private String miaoshu;
        private String goods_price;
        private String market_price;
        private String buyknow;
        private String goods_remark;
        private List<ImgBean> img;
        private List<TuwenBean> tuwen;

        public int getGoods_agent_id() {
            return goods_agent_id;
        }

        public void setGoods_agent_id(int goods_agent_id) {
            this.goods_agent_id = goods_agent_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public int getIs_mall() {
            return is_mall;
        }

        public void setIs_mall(int is_mall) {
            this.is_mall = is_mall;
        }

        public String getMiaoshu() {
            return miaoshu;
        }

        public void setMiaoshu(String miaoshu) {
            this.miaoshu = miaoshu;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getMarket_price() {
            return market_price;
        }

        public void setMarket_price(String market_price) {
            this.market_price = market_price;
        }

        public String getBuyknow() {
            return buyknow;
        }

        public void setBuyknow(String buyknow) {
            this.buyknow = buyknow;
        }

        public String getGoods_remark() {
            return goods_remark;
        }

        public void setGoods_remark(String goods_remark) {
            this.goods_remark = goods_remark;
        }

        public List<ImgBean> getImg() {
            return img;
        }

        public void setImg(List<ImgBean> img) {
            this.img = img;
        }

        public List<TuwenBean> getTuwen() {
            return tuwen;
        }

        public void setTuwen(List<TuwenBean> tuwen) {
            this.tuwen = tuwen;
        }

        public static class ImgBean {
            /**
             * pic_id : 3019
             * photo : 2017/06/17/20170617092055_1145.jpeg
             */

            private int pic_id;
            private String photo;

            public int getPic_id() {
                return pic_id;
            }

            public void setPic_id(int pic_id) {
                this.pic_id = pic_id;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }
        }

        public static class TuwenBean implements Serializable {
            /**
             * pic_id : 3019
             * photo : 2017/06/17/20170617092055_1145.jpeg
             */

            private int pic_id;
            private String photo;

            public int getPic_id() {
                return pic_id;
            }

            public void setPic_id(int pic_id) {
                this.pic_id = pic_id;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }
        }
    }
}
