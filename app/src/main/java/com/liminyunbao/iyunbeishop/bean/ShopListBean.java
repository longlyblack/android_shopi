package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/12 0012.
 */

public class ShopListBean {

    /**
     * cmd : shoplist
     * status : 1
     * data : [{"addr":"南阳市府衙西城根B-150号","shop_name":"瓷韵欧美瓷艺生活馆","shop_id":168,"shoptype":0},{"addr":"南阳市府衙西城根17-18号","shop_name":"帽牌货冒菜","shop_id":169,"shoptype":0},{"addr":"南阳市卧龙区建设中路","shop_name":"雅莎皇后内衣店","shop_id":401,"shoptype":1},{"addr":"文化路与建设路交叉口向东约50米","shop_name":"云贝网官方店\u2014南阳","shop_id":567,"shoptype":1}]
     */

    private String cmd;
    private String status;
    private List<DataBean> data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * addr : 南阳市府衙西城根B-150号
         * shop_name : 瓷韵欧美瓷艺生活馆
         * shop_id : 168
         * shoptype : 0
         */

        private String addr;
        private String mapaddr;
        private String shop_name;
        private int shop_id;
        private int shoptype;
        private int area_id;

        public String getMapaddr() {
            return mapaddr;
        }

        public void setMapaddr(String mapaddr) {
            this.mapaddr = mapaddr;
        }

        public int getArea_id() {
            return area_id;
        }

        public void setArea_id(int area_id) {
            this.area_id = area_id;
        }

        public String getAddr() {
            return addr;
        }

        public void setAddr(String addr) {
            this.addr = addr;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public int getShop_id() {
            return shop_id;
        }

        public void setShop_id(int shop_id) {
            this.shop_id = shop_id;
        }

        public int getShoptype() {
            return shoptype;
        }

        public void setShoptype(int shoptype) {
            this.shoptype = shoptype;
        }
    }
}
