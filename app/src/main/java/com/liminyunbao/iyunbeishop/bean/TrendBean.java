package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/25 0025.
 */

public class TrendBean {

    /**
     * cmd : trend
     * status : 1
     * data : {"day":[{"total":"1000","shijian":"2017-04-22"}],"month":[{"total":"1600","shijian":"2017-04-01"},{"total":"1500","shijian":"2017-04-04"},{"total":"1600","shijian":"2017-04-11"},{"total":"1000","shijian":"2017-04-22"}],"year":[{"total":"1400","shijian":"2017-02-09"},{"total":"1600","shijian":"2017-04-01"},{"total":"1500","shijian":"2017-04-04"},{"total":"1600","shijian":"2017-04-11"},{"total":"1000","shijian":"2017-04-22"}]}
     */

    private String cmd;
    private String status;
    private DataBean data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<DayBean> day;
        private List<MonthBean> month;
        private List<YearBean> year;

        public List<DayBean> getDay() {
            return day;
        }

        public void setDay(List<DayBean> day) {
            this.day = day;
        }

        public List<MonthBean> getMonth() {
            return month;
        }

        public void setMonth(List<MonthBean> month) {
            this.month = month;
        }

        public List<YearBean> getYear() {
            return year;
        }

        public void setYear(List<YearBean> year) {
            this.year = year;
        }

        public static class DayBean {
            /**
             * total : 1000
             * shijian : 2017-04-22
             */

            private String total;
            private String shijian;

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getShijian() {
                return shijian;
            }

            public void setShijian(String shijian) {
                this.shijian = shijian;
            }
        }

        public static class MonthBean {
            /**
             * total : 1600
             * shijian : 2017-04-01
             */

            private String total;
            private String shijian;

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getShijian() {
                return shijian;
            }

            public void setShijian(String shijian) {
                this.shijian = shijian;
            }
        }

        public static class YearBean {
            /**
             * total : 1400
             * shijian : 2017-02-09
             */

            private String total;
            private String shijian;

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getShijian() {
                return shijian;
            }

            public void setShijian(String shijian) {
                this.shijian = shijian;
            }
        }
    }
}
