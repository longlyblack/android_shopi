package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/6/22 0022.
 */

public class PackageRecommendation {

    /**
     * status : 1
     * cmd : meallist
     * max : 5
     * now : 4
     * data : [{"goods_agent_id":30515432,"goods_name":"肯德基鸡腿堡套餐'","miaoshu":"这是介绍","goods_price":10.5,"photo":"2017/06/17/20170617092055_1145.jpeg","is_mall":1},{"goods_agent_id":30515431,"goods_name":"肯德基鸡腿堡套餐'","miaoshu":"这是介绍","goods_price":10.5,"photo":"2017/06/17/20170617092055_1145.jpeg","is_mall":1},{"goods_agent_id":30515430,"goods_name":"肯德基鸡腿堡套餐'","miaoshu":"这是介绍","goods_price":10.5,"photo":"2017/06/17/20170617092055_1145.jpeg","is_mall":1},{"goods_agent_id":30515429,"goods_name":"肯德基鸡腿堡套餐'","miaoshu":"这是介绍","goods_price":10.5,"photo":"2017/06/17/20170617092055_1145.jpeg","is_mall":1},{"goods_agent_id":30515428,"goods_name":"肯德基鸡腿堡套餐'","miaoshu":"这是介绍","goods_price":10.5,"photo":"2017/06/17/20170617092055_1145.jpeg","is_mall":1}]
     */

    private int status;
    private String cmd;
    private int max;
    private int now;
    private List<ShopMassageBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getNow() {
        return now;
    }

    public void setNow(int now) {
        this.now = now;
    }

    public List<ShopMassageBean> getData() {
        return data;
    }

    public void setData(List<ShopMassageBean> data) {
        this.data = data;
    }

    public static class ShopMassageBean {
        /**
         * goods_agent_id : 30515432
         * goods_name : 肯德基鸡腿堡套餐'
         * miaoshu : 这是介绍
         * goods_price : 10.5
         * photo : 2017/06/17/20170617092055_1145.jpeg
         * is_mall : 1
         */

        private int goods_agent_id;
        private String goods_name;
        private String miaoshu;
        private String goods_price;
        private String photo;
        private int is_mall;

        public int getGoods_agent_id() {
            return goods_agent_id;
        }

        public void setGoods_agent_id(int goods_agent_id) {
            this.goods_agent_id = goods_agent_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getMiaoshu() {
            return miaoshu;
        }

        public void setMiaoshu(String miaoshu) {
            this.miaoshu = miaoshu;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public int getIs_mall() {
            return is_mall;
        }

        public void setIs_mall(int is_mall) {
            this.is_mall = is_mall;
        }
    }
}
