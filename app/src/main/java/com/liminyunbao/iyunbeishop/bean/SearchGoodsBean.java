package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/6/7 0007.
 */

public class SearchGoodsBean {

    /**
     * cmd : teamsel
     * status : 1
     * msg : [{"goods_agent_id":30511382,"base_goods_id":2851,"title":"禹明珠五香八宝","simple_title":null,"shop_id":701,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":49,"area_id":null,"photo":"2017/04/20/20170420142712_4581.png","goods_price":"1","market_price":"1","purchase_price":100,"num":0,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":2,"deleted_at":null,"updated_at":"2017-04-21 11:43:40","created_at":"2017-04-21 11:43:40","pv":null,"guige":"80g","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30511385,"base_goods_id":2854,"title":"禹明珠外公菜","simple_title":null,"shop_id":701,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":49,"area_id":null,"photo":"2017/04/20/20170420142811_3351.png","goods_price":"1","market_price":"1","purchase_price":100,"num":0,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":2,"deleted_at":null,"updated_at":"2017-04-21 11:43:42","created_at":"2017-04-21 11:43:42","pv":null,"guige":"80g","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30511517,"base_goods_id":3121,"title":"超能透明皂 清怡柠檬草","simple_title":null,"shop_id":701,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":71,"area_id":null,"photo":"2017/04/20/20170420175016_7615.png","goods_price":"9","market_price":"9","purchase_price":0,"num":-1,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":1,"deleted_at":null,"updated_at":"2017-05-11 09:44:10","created_at":"2017-04-21 11:46:44","pv":null,"guige":"226g*2","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30511518,"base_goods_id":3122,"title":"超能透明皂 海风椰果香","simple_title":null,"shop_id":701,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":71,"area_id":null,"photo":"2017/04/20/20170420175314_5048.png","goods_price":"9","market_price":"9","purchase_price":0,"num":-1,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":1,"deleted_at":null,"updated_at":"2017-05-16 13:39:04","created_at":"2017-04-21 11:46:44","pv":null,"guige":"226g*2","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30511797,"base_goods_id":3116,"title":"雕牌透明皂","simple_title":null,"shop_id":701,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":71,"area_id":null,"photo":"2017/04/20/20170420174918_6560.png","goods_price":"6.5","market_price":"6.5","purchase_price":0,"num":-1,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":1,"deleted_at":null,"updated_at":"2017-05-11 09:40:02","created_at":"2017-04-21 11:56:30","pv":null,"guige":"202g*2","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30512812,"base_goods_id":3116,"title":"雕牌透明皂","simple_title":null,"shop_id":703,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":71,"area_id":null,"photo":"2017/04/20/20170420174918_6560.png","goods_price":"6.5","market_price":"6.5","purchase_price":0,"num":99,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":1,"deleted_at":null,"updated_at":"2017-04-22 19:29:30","created_at":"2017-04-22 19:29:30","pv":null,"guige":"202g*2","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30512817,"base_goods_id":3121,"title":"超能透明皂 清怡柠檬草","simple_title":null,"shop_id":703,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":71,"area_id":null,"photo":"2017/04/20/20170420175016_7615.png","goods_price":"9","market_price":"9","purchase_price":0,"num":99,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":1,"deleted_at":null,"updated_at":"2017-04-22 19:29:32","created_at":"2017-04-22 19:29:32","pv":null,"guige":"226g*2","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30512819,"base_goods_id":3122,"title":"超能透明皂 海风椰果香","simple_title":null,"shop_id":703,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":71,"area_id":null,"photo":"2017/04/20/20170420175314_5048.png","goods_price":"9","market_price":"9","purchase_price":0,"num":99,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":1,"deleted_at":null,"updated_at":"2017-04-22 19:29:32","created_at":"2017-04-22 19:29:32","pv":null,"guige":"226g*2","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30512854,"base_goods_id":2851,"title":"禹明珠五香八宝","simple_title":null,"shop_id":703,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":49,"area_id":null,"photo":"2017/04/20/20170420142712_4581.png","goods_price":"1","market_price":"1","purchase_price":100,"num":100,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":1,"deleted_at":null,"updated_at":"2017-04-22 19:29:53","created_at":"2017-04-22 19:29:53","pv":null,"guige":"80g","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30512859,"base_goods_id":2854,"title":"禹明珠外公菜","simple_title":null,"shop_id":703,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":49,"area_id":null,"photo":"2017/04/20/20170420142811_3351.png","goods_price":"1","market_price":"1","purchase_price":100,"num":100,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":1,"deleted_at":null,"updated_at":"2017-04-22 19:29:54","created_at":"2017-04-22 19:29:54","pv":null,"guige":"80g","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30513215,"base_goods_id":3840,"title":"超能洗衣皂椰果透明皂","simple_title":null,"shop_id":701,"parent_cate_id":null,"custom_cate_id":null,"brand_id":null,"catea_id":null,"cate_id":71,"area_id":null,"photo":"2017/05/03/20170503165659_2908.png","goods_price":"1","market_price":"1","purchase_price":0,"num":0,"sold_num":0,"score_num":0,"details":"","audit":0,"is_mall":2,"deleted_at":null,"updated_at":"2017-05-03 17:48:23","created_at":"2017-05-03 17:48:23","pv":null,"guige":"226g*2块/组","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":0,"sort":0,"sales_sum":null,"goods_remark":null,"goods_view":null,"is_recommend":null,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":null,"buyknow":null},{"goods_agent_id":30514766,"base_goods_id":null,"title":"明年","simple_title":null,"shop_id":862,"parent_cate_id":40,"custom_cate_id":178,"brand_id":null,"catea_id":42,"cate_id":384,"area_id":null,"photo":"2017/06/01/20170601141616_7039.jpeg","goods_price":"665","market_price":"6565656","purchase_price":null,"num":656,"sold_num":0,"score_num":0,"details":null,"audit":0,"is_mall":1,"deleted_at":null,"updated_at":null,"created_at":"2017-06-01 14:16:43","pv":null,"guige":"好","goods_num":0,"start_time":null,"end_time":null,"points_price":null,"goods_type":5,"sort":0,"sales_sum":0,"goods_remark":"弄","goods_view":null,"is_recommend":0,"recommend_time":null,"is_new":null,"is_hot":null,"is_shop":null,"is_send":null,"is_guige":0,"buyknow":null}]
     */

    private String cmd;
    private String status;
    private List<MsgBean> msg;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MsgBean> getMsg() {
        return msg;
    }

    public void setMsg(List<MsgBean> msg) {
        this.msg = msg;
    }

    public static class MsgBean {
        /**
         * goods_agent_id : 30511382
         * base_goods_id : 2851
         * title : 禹明珠五香八宝
         * simple_title : null
         * shop_id : 701
         * parent_cate_id : null
         * custom_cate_id : null
         * brand_id : null
         * catea_id : null
         * cate_id : 49
         * area_id : null
         * photo : 2017/04/20/20170420142712_4581.png
         * goods_price : 1
         * market_price : 1
         * purchase_price : 100
         * num : 0
         * sold_num : 0
         * score_num : 0
         * details :
         * audit : 0
         * is_mall : 2
         * deleted_at : null
         * updated_at : 2017-04-21 11:43:40
         * created_at : 2017-04-21 11:43:40
         * pv : null
         * guige : 80g
         * goods_num : 0
         * start_time : null
         * end_time : null
         * points_price : null
         * goods_type : 0
         * sort : 0
         * sales_sum : null
         * goods_remark : null
         * goods_view : null
         * is_recommend : null
         * recommend_time : null
         * is_new : null
         * is_hot : null
         * is_shop : null
         * is_send : null
         * is_guige : null
         * buyknow : null
         */

        private int goods_agent_id;
        private int base_goods_id;
        private String title;
        private Object simple_title;
        private int shop_id;
        private Object parent_cate_id;
        private Object custom_cate_id;
        private Object brand_id;
        private Object catea_id;
        private int cate_id;
        private Object area_id;
        private String photo;
        private String goods_price;
        private String market_price;
        private int purchase_price;
        private int num;
        private int sold_num;
        private int score_num;
        private String details;
        private int audit;
        private int is_mall;
        private Object deleted_at;
        private String updated_at;
        private String created_at;
        private Object pv;
        private String guige;
        private int goods_num;
        private Object start_time;
        private Object end_time;
        private Object points_price;
        private int goods_type;
        private int sort;
        private Object sales_sum;
        private Object goods_remark;
        private Object goods_view;
        private Object is_recommend;
        private Object recommend_time;
        private Object is_new;
        private Object is_hot;
        private Object is_shop;
        private Object is_send;
        private Object is_guige;
        private Object buyknow;

        public int getGoods_agent_id() {
            return goods_agent_id;
        }

        public void setGoods_agent_id(int goods_agent_id) {
            this.goods_agent_id = goods_agent_id;
        }

        public int getBase_goods_id() {
            return base_goods_id;
        }

        public void setBase_goods_id(int base_goods_id) {
            this.base_goods_id = base_goods_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Object getSimple_title() {
            return simple_title;
        }

        public void setSimple_title(Object simple_title) {
            this.simple_title = simple_title;
        }

        public int getShop_id() {
            return shop_id;
        }

        public void setShop_id(int shop_id) {
            this.shop_id = shop_id;
        }

        public Object getParent_cate_id() {
            return parent_cate_id;
        }

        public void setParent_cate_id(Object parent_cate_id) {
            this.parent_cate_id = parent_cate_id;
        }

        public Object getCustom_cate_id() {
            return custom_cate_id;
        }

        public void setCustom_cate_id(Object custom_cate_id) {
            this.custom_cate_id = custom_cate_id;
        }

        public Object getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(Object brand_id) {
            this.brand_id = brand_id;
        }

        public Object getCatea_id() {
            return catea_id;
        }

        public void setCatea_id(Object catea_id) {
            this.catea_id = catea_id;
        }

        public int getCate_id() {
            return cate_id;
        }

        public void setCate_id(int cate_id) {
            this.cate_id = cate_id;
        }

        public Object getArea_id() {
            return area_id;
        }

        public void setArea_id(Object area_id) {
            this.area_id = area_id;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getMarket_price() {
            return market_price;
        }

        public void setMarket_price(String market_price) {
            this.market_price = market_price;
        }

        public int getPurchase_price() {
            return purchase_price;
        }

        public void setPurchase_price(int purchase_price) {
            this.purchase_price = purchase_price;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public int getSold_num() {
            return sold_num;
        }

        public void setSold_num(int sold_num) {
            this.sold_num = sold_num;
        }

        public int getScore_num() {
            return score_num;
        }

        public void setScore_num(int score_num) {
            this.score_num = score_num;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public int getAudit() {
            return audit;
        }

        public void setAudit(int audit) {
            this.audit = audit;
        }

        public int getIs_mall() {
            return is_mall;
        }

        public void setIs_mall(int is_mall) {
            this.is_mall = is_mall;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Object getPv() {
            return pv;
        }

        public void setPv(Object pv) {
            this.pv = pv;
        }

        public String getGuige() {
            return guige;
        }

        public void setGuige(String guige) {
            this.guige = guige;
        }

        public int getGoods_num() {
            return goods_num;
        }

        public void setGoods_num(int goods_num) {
            this.goods_num = goods_num;
        }

        public Object getStart_time() {
            return start_time;
        }

        public void setStart_time(Object start_time) {
            this.start_time = start_time;
        }

        public Object getEnd_time() {
            return end_time;
        }

        public void setEnd_time(Object end_time) {
            this.end_time = end_time;
        }

        public Object getPoints_price() {
            return points_price;
        }

        public void setPoints_price(Object points_price) {
            this.points_price = points_price;
        }

        public int getGoods_type() {
            return goods_type;
        }

        public void setGoods_type(int goods_type) {
            this.goods_type = goods_type;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public Object getSales_sum() {
            return sales_sum;
        }

        public void setSales_sum(Object sales_sum) {
            this.sales_sum = sales_sum;
        }

        public Object getGoods_remark() {
            return goods_remark;
        }

        public void setGoods_remark(Object goods_remark) {
            this.goods_remark = goods_remark;
        }

        public Object getGoods_view() {
            return goods_view;
        }

        public void setGoods_view(Object goods_view) {
            this.goods_view = goods_view;
        }

        public Object getIs_recommend() {
            return is_recommend;
        }

        public void setIs_recommend(Object is_recommend) {
            this.is_recommend = is_recommend;
        }

        public Object getRecommend_time() {
            return recommend_time;
        }

        public void setRecommend_time(Object recommend_time) {
            this.recommend_time = recommend_time;
        }

        public Object getIs_new() {
            return is_new;
        }

        public void setIs_new(Object is_new) {
            this.is_new = is_new;
        }

        public Object getIs_hot() {
            return is_hot;
        }

        public void setIs_hot(Object is_hot) {
            this.is_hot = is_hot;
        }

        public Object getIs_shop() {
            return is_shop;
        }

        public void setIs_shop(Object is_shop) {
            this.is_shop = is_shop;
        }

        public Object getIs_send() {
            return is_send;
        }

        public void setIs_send(Object is_send) {
            this.is_send = is_send;
        }

        public Object getIs_guige() {
            return is_guige;
        }

        public void setIs_guige(Object is_guige) {
            this.is_guige = is_guige;
        }

        public Object getBuyknow() {
            return buyknow;
        }

        public void setBuyknow(Object buyknow) {
            this.buyknow = buyknow;
        }
    }
}
