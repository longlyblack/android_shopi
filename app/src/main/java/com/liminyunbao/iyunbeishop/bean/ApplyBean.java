package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/11 0011.
 */

public class ApplyBean  {


    /**
     * cmd : apply
     * status : 1
     * msg : 提现成功
     * data : {"total":12,"per_page":5,"current_page":1,"last_page":3,"next_page_url":"http://1shop.iyunbei.net/api/Package/apply?page=2","prev_page_url":null,"from":1,"to":5,"data":[{"money":150,"create_time":1489451763,"cash_id":10159,"status":2},{"money":100,"create_time":1489459325,"cash_id":10160,"status":2},{"money":150,"create_time":1489459417,"cash_id":10161,"status":0},{"money":100,"create_time":1490003361,"cash_id":10162,"status":2},{"money":100,"create_time":1490003362,"cash_id":10163,"status":2}]}
     */

    private String cmd;
    private String status;
    private String msg;
    private DataBeanX data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class DataBeanX {
        /**
         * total : 12
         * per_page : 5
         * current_page : 1
         * last_page : 3
         * next_page_url : http://1shop.iyunbei.net/api/Package/apply?page=2
         * prev_page_url : null
         * from : 1
         * to : 5
         * data : [{"money":150,"create_time":1489451763,"cash_id":10159,"status":2},{"money":100,"create_time":1489459325,"cash_id":10160,"status":2},{"money":150,"create_time":1489459417,"cash_id":10161,"status":0},{"money":100,"create_time":1490003361,"cash_id":10162,"status":2},{"money":100,"create_time":1490003362,"cash_id":10163,"status":2}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private String next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<DataBean> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(String next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * money : 150
             * create_time : 1489451763
             * cash_id : 10159
             * status : 2
             */

            private int money;
            private int create_time;
            private int cash_id;
            private int status;

            public int getMoney() {
                return money;
            }

            public void setMoney(int money) {
                this.money = money;
            }

            public int getCreate_time() {
                return create_time;
            }

            public void setCreate_time(int create_time) {
                this.create_time = create_time;
            }

            public int getCash_id() {
                return cash_id;
            }

            public void setCash_id(int cash_id) {
                this.cash_id = cash_id;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }
        }
    }
}
