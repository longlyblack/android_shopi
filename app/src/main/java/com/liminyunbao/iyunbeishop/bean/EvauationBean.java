package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/24 0024.
 */

public class EvauationBean  {

    /**
     * cmd : evaluation
     * status : 1
     * data : {"total":15,"per_page":10,"current_page":1,"last_page":2,"next_page_url":"http://1shop.iyunbei.net/api/Retail/evaluation?page=2","prev_page_url":null,"from":1,"to":10,"data":[{"order_id":101093,"contents":"宝贝颜色很喜欢，用了几天质量感觉棒棒哒，楼梯道灯坏了作用体现出来了，出门携带也方便，给好评","reply":"谢谢亲的支持","create_time":1479472386,"reply_time":1480069121,"score":50,"nickname":"55769342"},{"order_id":101099,"contents":"宝贝质量很好，卖家态度也好，付款后半小时就收到货了，也许这就是同城的好处吧，强烈推荐","reply":"谢谢亲的支持","create_time":1479270010,"reply_time":1480069131,"score":33,"nickname":"90793516"},{"order_id":100950,"contents":"很不错很不错还很不错撒","reply":"积极","create_time":1479795172,"reply_time":1492680516,"score":50,"nickname":"77303784"},{"order_id":101272,"contents":"还不错的消费体验！","reply":"嗯。这待遇就一次","create_time":1481078893,"reply_time":1492819689,"score":43,"nickname":"朕"},{"order_id":101293,"contents":"呵呵","reply":"思考思考","create_time":1481096932,"reply_time":1492677694,"score":50,"nickname":"朕"},{"order_id":101311,"contents":"宝贝很实用，比想象中好，谢谢店家，送货速度超快。给好评","reply":"谢谢亲的支持","create_time":1482125886,"reply_time":1482126386,"score":20,"nickname":"55769342"},{"order_id":101317,"contents":"很好，第一次用云贝消费，面很好很正宗，环境很好值得一去！","reply":"项目明星们","create_time":1482225815,"reply_time":1492678073,"score":50,"nickname":"81951891"},{"order_id":102392,"contents":"建议购买，味道不错，老板也是相当的热情","reply":"大\n","create_time":1483264752,"reply_time":1492680137,"score":50,"nickname":"68415123"},{"order_id":102391,"contents":"建议购买，味道不错，老板也是相当的热情","reply":"那也要看姿色 ","create_time":1483264781,"reply_time":1492735232,"score":20,"nickname":"68415123"},{"order_id":102432,"contents":"真的真的非常好吃，她会喜欢，然后呢那个汤老板说他，是在，锅里煮的，永春鸭子出来的。非常好喝好喝。","reply":"没有春阳鸭店好。真的 去鸭店就认春阳鸭店","create_time":1483625438,"reply_time":1492735345,"score":50,"nickname":"82547653"}]}
     */

    private String cmd;
    private String status;
    private DataBeanX data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class DataBeanX {
        /**
         * total : 15
         * per_page : 10
         * current_page : 1
         * last_page : 2
         * next_page_url : http://1shop.iyunbei.net/api/Retail/evaluation?page=2
         * prev_page_url : null
         * from : 1
         * to : 10
         * data : [{"order_id":101093,"contents":"宝贝颜色很喜欢，用了几天质量感觉棒棒哒，楼梯道灯坏了作用体现出来了，出门携带也方便，给好评","reply":"谢谢亲的支持","create_time":1479472386,"reply_time":1480069121,"score":50,"nickname":"55769342"},{"order_id":101099,"contents":"宝贝质量很好，卖家态度也好，付款后半小时就收到货了，也许这就是同城的好处吧，强烈推荐","reply":"谢谢亲的支持","create_time":1479270010,"reply_time":1480069131,"score":33,"nickname":"90793516"},{"order_id":100950,"contents":"很不错很不错还很不错撒","reply":"积极","create_time":1479795172,"reply_time":1492680516,"score":50,"nickname":"77303784"},{"order_id":101272,"contents":"还不错的消费体验！","reply":"嗯。这待遇就一次","create_time":1481078893,"reply_time":1492819689,"score":43,"nickname":"朕"},{"order_id":101293,"contents":"呵呵","reply":"思考思考","create_time":1481096932,"reply_time":1492677694,"score":50,"nickname":"朕"},{"order_id":101311,"contents":"宝贝很实用，比想象中好，谢谢店家，送货速度超快。给好评","reply":"谢谢亲的支持","create_time":1482125886,"reply_time":1482126386,"score":20,"nickname":"55769342"},{"order_id":101317,"contents":"很好，第一次用云贝消费，面很好很正宗，环境很好值得一去！","reply":"项目明星们","create_time":1482225815,"reply_time":1492678073,"score":50,"nickname":"81951891"},{"order_id":102392,"contents":"建议购买，味道不错，老板也是相当的热情","reply":"大\n","create_time":1483264752,"reply_time":1492680137,"score":50,"nickname":"68415123"},{"order_id":102391,"contents":"建议购买，味道不错，老板也是相当的热情","reply":"那也要看姿色 ","create_time":1483264781,"reply_time":1492735232,"score":20,"nickname":"68415123"},{"order_id":102432,"contents":"真的真的非常好吃，她会喜欢，然后呢那个汤老板说他，是在，锅里煮的，永春鸭子出来的。非常好喝好喝。","reply":"没有春阳鸭店好。真的 去鸭店就认春阳鸭店","create_time":1483625438,"reply_time":1492735345,"score":50,"nickname":"82547653"}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private String next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<DataBean> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(String next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * order_id : 101093
             * contents : 宝贝颜色很喜欢，用了几天质量感觉棒棒哒，楼梯道灯坏了作用体现出来了，出门携带也方便，给好评
             * reply : 谢谢亲的支持
             * create_time : 1479472386
             * reply_time : 1480069121
             * score : 50
             * nickname : 55769342
             */

            private int order_id;
            private String contents;
            private String reply;
            private int create_time;
            private int reply_time;
            private int score;
            private String nickname;

            public int getOrder_id() {
                return order_id;
            }

            public void setOrder_id(int order_id) {
                this.order_id = order_id;
            }

            public String getContents() {
                return contents;
            }

            public void setContents(String contents) {
                this.contents = contents;
            }

            public String getReply() {
                return reply;
            }

            public void setReply(String reply) {
                this.reply = reply;
            }

            public int getCreate_time() {
                return create_time;
            }

            public void setCreate_time(int create_time) {
                this.create_time = create_time;
            }

            public int getReply_time() {
                return reply_time;
            }

            public void setReply_time(int reply_time) {
                this.reply_time = reply_time;
            }

            public int getScore() {
                return score;
            }

            public void setScore(int score) {
                this.score = score;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }
        }
    }
}
