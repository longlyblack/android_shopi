package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/6/2 0002.
 */

public class GroutListBean {


    /**
     * cmd : teamshow
     * status : 1
     * msg : [{"cate_id":196,"cate_name":"牛逼","num":0},{"cate_id":186,"cate_name":"这么多","num":0},{"cate_id":180,"cate_name":"默认","num":0},{"cate_id":185,"cate_name":"厉害","num":1}]
     */

    private String cmd;
    private String status;
    private List<MsgBean> msg;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MsgBean> getMsg() {
        return msg;
    }

    public void setMsg(List<MsgBean> msg) {
        this.msg = msg;
    }

    public static class MsgBean {
        /**
         * cate_id : 196
         * cate_name : 牛逼
         * num : 0
         */

        private int cate_id;
        private String cate_name;
        private int num;

        public int getCate_id() {
            return cate_id;
        }

        public void setCate_id(int cate_id) {
            this.cate_id = cate_id;
        }

        public String getCate_name() {
            return cate_name;
        }

        public void setCate_name(String cate_name) {
            this.cate_name = cate_name;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }
}
