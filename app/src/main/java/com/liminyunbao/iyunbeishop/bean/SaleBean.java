package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/25 0025.
 */

public class SaleBean {


    /**
     * cmd : sale
     * status : 1
     * data : {"total":10,"per_page":5,"current_page":1,"last_page":2,"next_page_url":"http://1shop.iyunbei.net/api/Retail/goodslist?page=2","prev_page_url":null,"from":1,"to":5,"data":[{"title":"明年","photo":"2017/06/01/20170601141616_7039.jpeg","sunnum":656,"guige":"好","sold_num":0,"market_price":656565600,"goods_price":66500,"simple_title":"弄","base_goods_id":30514766},{"title":"我拉你","photo":"2017/06/01/20170601104210_5605.jpeg","sunnum":22,"guige":"哦哦","sold_num":0,"market_price":3300,"goods_price":221100,"simple_title":"哦哦哦","base_goods_id":30514762},{"title":"哦哦哦","photo":"2017/06/01/20170601103750_1322.jpeg","sunnum":22,"guige":"的","sold_num":0,"market_price":3300,"goods_price":112200,"simple_title":"哦哦哦","base_goods_id":30514761},{"title":"eee","photo":"2017/05/31/20170531151812_3575.jpeg","sunnum":22,"guige":"哦哦哦","sold_num":0,"market_price":3300,"goods_price":2200,"simple_title":"陌陌摸摸","base_goods_id":30514760},{"title":"3123","photo":"2017/06/01/20170601115543_1153.jpeg","sunnum":22,"guige":"哦哦哦","sold_num":0,"market_price":3300,"goods_price":2200,"simple_title":"陌陌摸摸","base_goods_id":30514759}]}
     */

    private String cmd;
    private String status;
    private MassageBean data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MassageBean getData() {
        return data;
    }

    public void setData(MassageBean data) {
        this.data = data;
    }

    public static class MassageBean {
        /**
         * total : 10
         * per_page : 5
         * current_page : 1
         * last_page : 2
         * next_page_url : http://1shop.iyunbei.net/api/Retail/goodslist?page=2
         * prev_page_url : null
         * from : 1
         * to : 5
         * data : [{"title":"明年","photo":"2017/06/01/20170601141616_7039.jpeg","sunnum":656,"guige":"好","sold_num":0,"market_price":656565600,"goods_price":66500,"simple_title":"弄","base_goods_id":30514766},{"title":"我拉你","photo":"2017/06/01/20170601104210_5605.jpeg","sunnum":22,"guige":"哦哦","sold_num":0,"market_price":3300,"goods_price":221100,"simple_title":"哦哦哦","base_goods_id":30514762},{"title":"哦哦哦","photo":"2017/06/01/20170601103750_1322.jpeg","sunnum":22,"guige":"的","sold_num":0,"market_price":3300,"goods_price":112200,"simple_title":"哦哦哦","base_goods_id":30514761},{"title":"eee","photo":"2017/05/31/20170531151812_3575.jpeg","sunnum":22,"guige":"哦哦哦","sold_num":0,"market_price":3300,"goods_price":2200,"simple_title":"陌陌摸摸","base_goods_id":30514760},{"title":"3123","photo":"2017/06/01/20170601115543_1153.jpeg","sunnum":22,"guige":"哦哦哦","sold_num":0,"market_price":3300,"goods_price":2200,"simple_title":"陌陌摸摸","base_goods_id":30514759}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private Object next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<GoodsList> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public Object getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(Object next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<GoodsList> getData() {
            return data;
        }

        public void setData(List<GoodsList> data) {
            this.data = data;
        }

        public static class GoodsList {
            /**
             * title : 明年
             * photo : 2017/06/01/20170601141616_7039.jpeg
             * sunnum : 656
             * guige : 好
             * sold_num : 0
             * market_price : 656565600
             * goods_price : 66500
             * simple_title : 弄
             * base_goods_id : 30514766
             */

            private String title;
            private String photo;
            private int sunnum;
            private String guige;
            private int sold_num;
            private String market_price;
            private String  goods_price;
            private String simple_title;
            private int base_goods_id;

            private int is_recommend;

            public int getIs_recommend() {
                return is_recommend;
            }

            public void setIs_recommend(int is_recommend) {
                this.is_recommend = is_recommend;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public int getSunnum() {
                return sunnum;
            }

            public void setSunnum(int sunnum) {
                this.sunnum = sunnum;
            }

            public String getGuige() {
                return guige;
            }

            public void setGuige(String guige) {
                this.guige = guige;
            }

            public int getSold_num() {
                return sold_num;
            }

            public void setSold_num(int sold_num) {
                this.sold_num = sold_num;
            }

            public String getMarket_price() {
                return market_price;
            }

            public void setMarket_price(String market_price) {
                this.market_price = market_price;
            }

            public String getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(String goods_price) {
                this.goods_price = goods_price;
            }

            public String getSimple_title() {
                return simple_title;
            }

            public void setSimple_title(String simple_title) {
                this.simple_title = simple_title;
            }

            public int getBase_goods_id() {
                return base_goods_id;
            }

            public void setBase_goods_id(int base_goods_id) {
                this.base_goods_id = base_goods_id;
            }
        }
    }
}
