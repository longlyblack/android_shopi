package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/10 0010.
 */

public class PuTongLiuShuiBean{


    /**
     * cmd : details
     * status : 1
     * data : {"total":5,"per_page":2,"current_page":1,"last_page":3,"next_page_url":"http://1shop.iyunbei.net/api/Package/details?page=2","prev_page_url":null,"from":1,"to":2,"data":[{"money":-150,"intro":"店铺提现申请！","create_time":1489451763,"order_id":10159},{"money":150,"intro":"提现申请拒绝后返还！","create_time":1489459279,"order_id":10159}]}
     */

    private String cmd;
    private String status;
    private DataBeanX data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class DataBeanX {
        /**
         * total : 5
         * per_page : 2
         * current_page : 1
         * last_page : 3
         * next_page_url : http://1shop.iyunbei.net/api/Package/details?page=2
         * prev_page_url : null
         * from : 1
         * to : 2
         * data : [{"money":-150,"intro":"店铺提现申请！","create_time":1489451763,"order_id":10159},{"money":150,"intro":"提现申请拒绝后返还！","create_time":1489459279,"order_id":10159}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private String next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<DataBean> data;


        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(String next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * money : -150
             * intro : 店铺提现申请！
             * create_time : 1489451763
             * order_id : 10159
             */
            private String money;
            private String intro;
            private int create_time;
            private int order_id;

            public String getMoney() {
                return money;
            }

            public void setMoney(String money) {
                this.money = money;
            }

            public String getIntro() {
                return intro;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public int getCreate_time() {
                return create_time;
            }

            public void setCreate_time(int create_time) {
                this.create_time = create_time;
            }

            public int getOrder_id() {
                return order_id;
            }

            public void setOrder_id(int order_id) {
                this.order_id = order_id;
            }
        }
    }
}
