package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/3/31.
 */

public class CityName {

    private List<RECORDSBean> RECORDS;

    public List<RECORDSBean> getRECORDS() {
        return RECORDS;
    }

    public void setRECORDS(List<RECORDSBean> RECORDS) {
        this.RECORDS = RECORDS;
    }

    public static class RECORDSBean {
        /**
         * city_id : 231000
         * name : 牡丹江市
         * pinyin : mudanjiang
         * is_open : 0
         * lng : 129.608035
         * lat : 44.588521
         * orderby : 100
         * first_letter : M
         * provinces_id : 230000
         * hot : 0
         * bd_code : 49
         * deleted_at : null
         */

        private int city_id;
        private String name;
        private String pinyin;
        private int is_open;
        private String lng;
        private String lat;
        private int orderby;
        private String first_letter;
        private int provinces_id;
        private int hot;
        private int bd_code;
        private Object deleted_at;

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPinyin() {
            return pinyin;
        }

        public void setPinyin(String pinyin) {
            this.pinyin = pinyin;
        }

        public int getIs_open() {
            return is_open;
        }

        public void setIs_open(int is_open) {
            this.is_open = is_open;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public int getOrderby() {
            return orderby;
        }

        public void setOrderby(int orderby) {
            this.orderby = orderby;
        }

        public String getFirst_letter() {
            return first_letter;
        }

        public void setFirst_letter(String first_letter) {
            this.first_letter = first_letter;
        }

        public int getProvinces_id() {
            return provinces_id;
        }

        public void setProvinces_id(int provinces_id) {
            this.provinces_id = provinces_id;
        }

        public int getHot() {
            return hot;
        }

        public void setHot(int hot) {
            this.hot = hot;
        }

        public int getBd_code() {
            return bd_code;
        }

        public void setBd_code(int bd_code) {
            this.bd_code = bd_code;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }
    }
}
