package com.liminyunbao.iyunbeishop.bean;

/**
 * Created by Administrator on 2017/6/16 0016.
 */

public class XingHaoBean {

    private String name;
    private String price;
    private String spec_id;
    private String num;

    public XingHaoBean(String name, String price, String spec_id, String num) {
        this.name = name;
        this.price = price;
        this.spec_id = spec_id;
        this.num = num;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSpec_id() {
        return spec_id;
    }

    public void setSpec_id(String spec_id) {
        this.spec_id = spec_id;
    }
}
