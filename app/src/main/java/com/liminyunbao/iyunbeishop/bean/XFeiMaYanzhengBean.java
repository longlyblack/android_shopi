package com.liminyunbao.iyunbeishop.bean;

/**
 * Created by Administrator on 2017/4/10 0010.
 */

public class XFeiMaYanzhengBean {

    /**
     * cmd : code
     * status : 0
     * data : 3
     */

    private String cmd;
    private String status;
    private int data;
    private String msg;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
