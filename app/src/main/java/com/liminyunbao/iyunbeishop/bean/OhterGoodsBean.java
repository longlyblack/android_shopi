package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/6/14 0014.
 */

public class OhterGoodsBean {

    /**
     * cmd : othergoods
     * status : 1
     * msg : {"total":6,"per_page":15,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":6,"data":[{"title":"就是觉得","goods_agent_id":30515360,"photo":"2017/06/13/20170613173259_9306.jpeg","goods_price":"66","market_price":"36","num":3636,"sold_num":0},{"title":"啊啦啦阿狸","goods_agent_id":30515363,"photo":"2017/06/13/20170613184856_5661.jpeg","goods_price":"33","market_price":"36","num":33,"sold_num":0},{"title":"阿狸","goods_agent_id":30515364,"photo":"2017/06/13/20170613184944_5330.jpeg","goods_price":"25","market_price":"123","num":33,"sold_num":0},{"title":"1212","goods_agent_id":30515365,"photo":"2017/06/13/20170613190004_9905.jpeg","goods_price":"33","market_price":"3","num":11,"sold_num":0},{"title":"兔兔","goods_agent_id":30515369,"photo":"2017/06/13/20170613191944_8714.jpeg","goods_price":"33","market_price":"336","num":333,"sold_num":0},{"title":"123","goods_agent_id":30515374,"photo":"2017/06/14/20170614091133_4081.jpeg","goods_price":"123","market_price":"123","num":246,"sold_num":0}]}
     */

    private String cmd;
    private String status;
    private MsgBean msg;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MsgBean getMsg() {
        return msg;
    }

    public void setMsg(MsgBean msg) {
        this.msg = msg;
    }

    public static class MsgBean {
        /**
         * total : 6
         * per_page : 15
         * current_page : 1
         * last_page : 1
         * next_page_url : null
         * prev_page_url : null
         * from : 1
         * to : 6
         * data : [{"title":"就是觉得","goods_agent_id":30515360,"photo":"2017/06/13/20170613173259_9306.jpeg","goods_price":"66","market_price":"36","num":3636,"sold_num":0},{"title":"啊啦啦阿狸","goods_agent_id":30515363,"photo":"2017/06/13/20170613184856_5661.jpeg","goods_price":"33","market_price":"36","num":33,"sold_num":0},{"title":"阿狸","goods_agent_id":30515364,"photo":"2017/06/13/20170613184944_5330.jpeg","goods_price":"25","market_price":"123","num":33,"sold_num":0},{"title":"1212","goods_agent_id":30515365,"photo":"2017/06/13/20170613190004_9905.jpeg","goods_price":"33","market_price":"3","num":11,"sold_num":0},{"title":"兔兔","goods_agent_id":30515369,"photo":"2017/06/13/20170613191944_8714.jpeg","goods_price":"33","market_price":"336","num":333,"sold_num":0},{"title":"123","goods_agent_id":30515374,"photo":"2017/06/14/20170614091133_4081.jpeg","goods_price":"123","market_price":"123","num":246,"sold_num":0}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private Object next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<DataBean> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public Object getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(Object next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * title : 就是觉得
             * goods_agent_id : 30515360
             * photo : 2017/06/13/20170613173259_9306.jpeg
             * goods_price : 66
             * market_price : 36
             * num : 3636
             * sold_num : 0
             */

            private String title;
            private int goods_agent_id;
            private String photo;
            private String goods_price;
            private String market_price;
            private int num;
            private int sold_num;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getGoods_agent_id() {
                return goods_agent_id;
            }

            public void setGoods_agent_id(int goods_agent_id) {
                this.goods_agent_id = goods_agent_id;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(String goods_price) {
                this.goods_price = goods_price;
            }

            public String getMarket_price() {
                return market_price;
            }

            public void setMarket_price(String market_price) {
                this.market_price = market_price;
            }

            public int getNum() {
                return num;
            }

            public void setNum(int num) {
                this.num = num;
            }

            public int getSold_num() {
                return sold_num;
            }

            public void setSold_num(int sold_num) {
                this.sold_num = sold_num;
            }
        }
    }
}
