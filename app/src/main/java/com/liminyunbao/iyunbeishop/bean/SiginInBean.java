package com.liminyunbao.iyunbeishop.bean;

/**
 * Created by Administrator on 2017/4/7 0007.
 */

public class SiginInBean {


    /**
     * cmd : login
     * status : 1
     * data : {"user_id":"420","user_token":"7b3560c9d9c3ccfbc386c3782e46ff09","shop_id":"168"}
     */

    private String cmd;
    private String status;
    private DataBean data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 420
         * user_token : 7b3560c9d9c3ccfbc386c3782e46ff09
         * shop_id : 168
         */

        private String user_id;
        private String user_token;
        private String shop_id;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_token() {
            return user_token;
        }

        public void setUser_token(String user_token) {
            this.user_token = user_token;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }
    }
}
