package com.liminyunbao.iyunbeishop.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/4/10 0010.
 */

public class OrderListBean {


    /**
     * cmd : abc
     * status : 1
     * data : {"total":725,"per_page":15,"current_page":1,"last_page":49,"next_page_url":"http://1shop.iyunbei.net/api/abc?page=2","prev_page_url":null,"from":1,"to":15,"data":[{"order_id":103666,"shop_id":567,"user_id":2260,"ride_id":null,"ride_status":null,"consignee":null,"mobile":null,"address":null,"total_amount":200,"order_amount":200,"order_status":"已发货","pay_type":"人民币","pay_status":0,"pay_time":"2017-03-25 17:14:05","shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-25 17:14:05","created_at":"2017-03-11 10:49:35","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":"23456","goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 12:49:35","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1607,"order_id":103666,"goods_id":2615,"photo":"2017/03/10/20170310194217_6926.jpeg","goods_name":"乡巴佬鸡爪","goods_guige":"35g/袋","goods_num":2,"point_price":null,"goods_price":100,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 10:49:35","created_at":"2017-03-11 10:49:35","note":null}]},{"order_id":103616,"shop_id":567,"user_id":152,"ride_id":null,"ride_status":null,"consignee":"s1","mobile":"13333333333","address":"ssssss2222","total_amount":980,"order_amount":980,"order_status":"已关闭","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 04:09:32","created_at":"2017-03-11 04:09:32","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 06:09:32","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1543,"order_id":103616,"goods_id":2481,"photo":"2017/03/10/20170310180522_4914.jpeg","goods_name":"达利园法式软面包","goods_guige":"160g/袋 8枚","goods_num":1,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 04:09:32","created_at":"2017-03-11 04:09:32","note":null},{"rec_id":1544,"order_id":103616,"goods_id":2482,"photo":"2017/03/10/20170310180731_9825.jpeg","goods_name":"达利园法式软面包","goods_guige":"200g/袋 10枚","goods_num":1,"point_price":null,"goods_price":530,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 04:09:32","created_at":"2017-03-11 04:09:32","note":null}]},{"order_id":103663,"shop_id":567,"user_id":2260,"ride_id":234,"ride_status":6,"consignee":"白阳","mobile":"15639938618","address":"文秀花园利民云宝","total_amount":230,"order_amount":230,"order_status":"待评价","pay_type":"人民币","pay_status":0,"pay_time":"2017-03-25 14:26:00","shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 15:02:17","created_at":"2017-03-11 10:41:16","note":"来个美女送货","order_type":"普通订单","wuliu_type":"同城配送","verify_code":"","goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 12:41:16","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":"asdfasdf","jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1604,"order_id":103663,"goods_id":2650,"photo":"2017/03/11/20170311100727_2515.jpeg","goods_name":"金鸽多味葵花籽","goods_guige":"98g/袋","goods_num":1,"point_price":null,"goods_price":230,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 10:41:16","created_at":"2017-03-11 10:41:16","note":null}]},{"order_id":103618,"shop_id":567,"user_id":602,"ride_id":234,"ride_status":4,"consignee":"周遥","mobile":"18338131623","address":"测打的啊","total_amount":460,"order_amount":460,"order_status":"已关闭","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 08:18:59","created_at":"2017-03-11 08:18:59","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:18:59","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1546,"order_id":103618,"goods_id":2471,"photo":"2017/03/10/20170310174420_3569.jpeg","goods_name":"3+2苏打饼干","goods_guige":"125g/袋","goods_num":1,"point_price":null,"goods_price":460,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:18:59","created_at":"2017-03-11 08:18:59","note":null}]},{"order_id":103619,"shop_id":567,"user_id":4020,"ride_id":4,"ride_status":null,"consignee":"李啸祖","mobile":"13073733143","address":"建设路文化路文秀花园","total_amount":1090,"order_amount":1090,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":null,"created_at":"2017-03-11 08:30:13","note":"","order_type":"今日抢购","wuliu_type":"同城配送","verify_code":null,"goodsid":2257,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:30:13","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1547,"order_id":103619,"goods_id":2257,"photo":"2017/03/10/20170310143758_4817.jpeg","goods_name":"加多宝凉茶","goods_guige":"310ml*6罐/组","goods_num":1,"point_price":null,"goods_price":1780,"cost_price":0,"deleted_at":null,"updated_at":null,"created_at":"2017-03-11 08:30:13","note":null}]},{"order_id":103621,"shop_id":567,"user_id":152,"ride_id":4,"ride_status":0,"consignee":"s1","mobile":"13333333333","address":"ssssss2222","total_amount":1560,"order_amount":1560,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 08:32:51","created_at":"2017-03-11 08:32:51","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:32:51","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1549,"order_id":103621,"goods_id":2481,"photo":"2017/03/10/20170310180522_4914.jpeg","goods_name":"达利园法式软面包","goods_guige":"160g/袋 8枚","goods_num":1,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:32:51","created_at":"2017-03-11 08:32:51","note":null},{"rec_id":1550,"order_id":103621,"goods_id":2482,"photo":"2017/03/10/20170310180731_9825.jpeg","goods_name":"达利园法式软面包","goods_guige":"200g/袋 10枚","goods_num":1,"point_price":null,"goods_price":530,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:32:51","created_at":"2017-03-11 08:32:51","note":null},{"rec_id":1551,"order_id":103621,"goods_id":2486,"photo":"2017/03/10/20170310181030_1565.jpeg","goods_name":"达利园派草莓味","goods_guige":"250g","goods_num":1,"point_price":null,"goods_price":580,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:32:51","created_at":"2017-03-11 08:32:51","note":null}]},{"order_id":103665,"shop_id":567,"user_id":4101,"ride_id":6,"ride_status":6,"consignee":null,"mobile":null,"address":null,"total_amount":990,"order_amount":990,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":"2017-03-25 14:30:59","shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:39:22","created_at":"2017-03-11 10:47:18","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 12:47:18","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1606,"order_id":103665,"goods_id":2387,"photo":"2017/03/10/20170310111045_8009.jpeg","goods_name":"康师傅红烧牛肉面","goods_guige":"103g*5/袋","goods_num":1,"point_price":null,"goods_price":990,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 10:47:18","created_at":"2017-03-11 10:47:18","note":null}]},{"order_id":103623,"shop_id":567,"user_id":4020,"ride_id":6,"ride_status":5,"consignee":null,"mobile":null,"address":null,"total_amount":250,"order_amount":250,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:37:08","created_at":"2017-03-11 08:37:06","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:37:06","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1553,"order_id":103623,"goods_id":2389,"photo":"2017/03/10/20170310111818_3368.jpeg","goods_name":"康师傅鲜虾鱼板面 袋","goods_guige":"98g/袋","goods_num":1,"point_price":null,"goods_price":250,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:37:06","created_at":"2017-03-11 08:37:06","note":null}]},{"order_id":103624,"shop_id":567,"user_id":602,"ride_id":6,"ride_status":5,"consignee":null,"mobile":null,"address":null,"total_amount":380,"order_amount":380,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:36:44","created_at":"2017-03-11 08:37:24","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:37:24","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1554,"order_id":103624,"goods_id":2615,"photo":"2017/03/10/20170310194217_6926.jpeg","goods_name":"乡巴佬鸡爪","goods_guige":"35g/袋","goods_num":1,"point_price":null,"goods_price":100,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:37:24","created_at":"2017-03-11 08:37:24","note":null},{"rec_id":1555,"order_id":103624,"goods_id":2607,"photo":"2017/03/10/20170310193621_2164.jpeg","goods_name":"可比克薯片香辣味","goods_guige":"60g","goods_num":1,"point_price":null,"goods_price":280,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:37:24","created_at":"2017-03-11 08:37:24","note":null}]},{"order_id":103625,"shop_id":567,"user_id":718,"ride_id":6,"ride_status":5,"consignee":null,"mobile":null,"address":null,"total_amount":980,"order_amount":980,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:42:11","created_at":"2017-03-11 08:38:08","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:38:08","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1556,"order_id":103625,"goods_id":2481,"photo":"2017/03/10/20170310180522_4914.jpeg","goods_name":"达利园法式软面包","goods_guige":"160g/袋 8枚","goods_num":1,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:38:08","created_at":"2017-03-11 08:38:08","note":null},{"rec_id":1557,"order_id":103625,"goods_id":2482,"photo":"2017/03/10/20170310180731_9825.jpeg","goods_name":"达利园法式软面包","goods_guige":"200g/袋 10枚","goods_num":1,"point_price":null,"goods_price":530,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:38:08","created_at":"2017-03-11 08:38:08","note":null}]},{"order_id":103667,"shop_id":567,"user_id":2822,"ride_id":6,"ride_status":4,"consignee":"张贝贝","mobile":"13782187223","address":"中州路梅溪宾馆小区一单元","total_amount":900,"order_amount":900,"order_status":"待发货","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-10 12:00:07","created_at":"2017-03-11 10:52:41","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":"","goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 12:52:41","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":"2017-04-10 12:00:07","shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1608,"order_id":103667,"goods_id":2468,"photo":"2017/03/10/20170310173844_2454.jpeg","goods_name":"立白金桔洗洁精 1.29kg","goods_guige":"1.29kg/桶","goods_num":1,"point_price":null,"goods_price":900,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 10:52:41","created_at":"2017-03-11 10:52:41","note":null}]},{"order_id":103627,"shop_id":567,"user_id":514,"ride_id":6,"ride_status":5,"consignee":null,"mobile":null,"address":null,"total_amount":2250,"order_amount":2250,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:42:03","created_at":"2017-03-11 08:41:18","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:41:18","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1559,"order_id":103627,"goods_id":2470,"photo":"2017/03/10/20170310174207_4851.jpeg","goods_name":"3+2饼干榛子巧克力味","goods_guige":"80g/袋","goods_num":1,"point_price":null,"goods_price":320,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:18","created_at":"2017-03-11 08:41:18","note":null},{"rec_id":1560,"order_id":103627,"goods_id":2471,"photo":"2017/03/10/20170310174420_3569.jpeg","goods_name":"3+2苏打饼干","goods_guige":"125g/袋","goods_num":1,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:18","created_at":"2017-03-11 08:41:18","note":null},{"rec_id":1561,"order_id":103627,"goods_id":2481,"photo":"2017/03/10/20170310180522_4914.jpeg","goods_name":"达利园法式软面包","goods_guige":"160g/袋 8枚","goods_num":2,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:18","created_at":"2017-03-11 08:41:18","note":null},{"rec_id":1562,"order_id":103627,"goods_id":2488,"photo":"2017/03/10/20170310181146_8306.jpeg","goods_name":"达利园派蛋黄味","goods_guige":"250g","goods_num":1,"point_price":null,"goods_price":580,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:18","created_at":"2017-03-11 08:41:18","note":null}]},{"order_id":103628,"shop_id":567,"user_id":4020,"ride_id":6,"ride_status":4,"consignee":null,"mobile":null,"address":null,"total_amount":400,"order_amount":400,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 08:41:56","created_at":"2017-03-11 08:41:56","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:41:56","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1563,"order_id":103628,"goods_id":2361,"photo":"2017/03/10/20170310092830_6522.jpeg","goods_name":"脉动 青柠味","goods_guige":"600ml/瓶","goods_num":1,"point_price":null,"goods_price":400,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:56","created_at":"2017-03-11 08:41:56","note":null}]},{"order_id":103629,"shop_id":567,"user_id":666,"ride_id":6,"ride_status":4,"consignee":null,"mobile":null,"address":null,"total_amount":0,"order_amount":0,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":null,"created_at":"2017-03-11 08:46:23","note":null,"order_type":"今日抢购","wuliu_type":"同城配送","verify_code":null,"goodsid":2259,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:46:23","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1564,"order_id":103629,"goods_id":2259,"photo":"2017/03/10/20170310144536_8656.jpeg","goods_name":"吉祥结食用调和油","goods_guige":"5L/桶","goods_num":1,"point_price":null,"goods_price":5990,"cost_price":0,"deleted_at":null,"updated_at":null,"created_at":"2017-03-11 08:46:23","note":null}]},{"order_id":103630,"shop_id":567,"user_id":514,"ride_id":6,"ride_status":4,"consignee":"徐杰","mobile":"18548986157","address":"校场路鸭灌新区一号楼六楼中户","total_amount":3030,"order_amount":3030,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:46:32","city_cost":null,"yunbei_cost":3,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1565,"order_id":103630,"goods_id":2397,"photo":"2017/03/10/20170310112510_1509.jpeg","goods_name":"统一阿萨姆原味奶茶","goods_guige":"500ml/瓶","goods_num":1,"point_price":null,"goods_price":380,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":null},{"rec_id":1566,"order_id":103630,"goods_id":2412,"photo":"2017/03/10/20170310140920_1211.jpeg","goods_name":"仲景香菇酱 奥尔良味","goods_guige":"210g/瓶","goods_num":1,"point_price":null,"goods_price":1190,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":null},{"rec_id":1567,"order_id":103630,"goods_id":2418,"photo":"2017/03/10/20170310141240_9444.jpeg","goods_name":"双汇大肉块香肠","goods_guige":"30g*8支/袋","goods_num":1,"point_price":null,"goods_price":960,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":null},{"rec_id":1568,"order_id":103630,"goods_id":2432,"photo":"2017/03/10/20170310142017_8156.jpeg","goods_name":"南德调味料","goods_guige":"200g/袋","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":null}]}]}
     */

    private String cmd;
    private int status;
    private DataBeanXX data;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBeanXX getData() {
        return data;
    }

    public void setData(DataBeanXX data) {
        this.data = data;
    }

    public static class DataBeanXX {
        /**
         * total : 725
         * per_page : 15
         * current_page : 1
         * last_page : 49
         * next_page_url : http://1shop.iyunbei.net/api/abc?page=2
         * prev_page_url : null
         * from : 1
         * to : 15
         * data : [{"order_id":103666,"shop_id":567,"user_id":2260,"ride_id":null,"ride_status":null,"consignee":null,"mobile":null,"address":null,"total_amount":200,"order_amount":200,"order_status":"已发货","pay_type":"人民币","pay_status":0,"pay_time":"2017-03-25 17:14:05","shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-25 17:14:05","created_at":"2017-03-11 10:49:35","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":"23456","goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 12:49:35","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1607,"order_id":103666,"goods_id":2615,"photo":"2017/03/10/20170310194217_6926.jpeg","goods_name":"乡巴佬鸡爪","goods_guige":"35g/袋","goods_num":2,"point_price":null,"goods_price":100,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 10:49:35","created_at":"2017-03-11 10:49:35","note":null}]},{"order_id":103616,"shop_id":567,"user_id":152,"ride_id":null,"ride_status":null,"consignee":"s1","mobile":"13333333333","address":"ssssss2222","total_amount":980,"order_amount":980,"order_status":"已关闭","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 04:09:32","created_at":"2017-03-11 04:09:32","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 06:09:32","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1543,"order_id":103616,"goods_id":2481,"photo":"2017/03/10/20170310180522_4914.jpeg","goods_name":"达利园法式软面包","goods_guige":"160g/袋 8枚","goods_num":1,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 04:09:32","created_at":"2017-03-11 04:09:32","note":null},{"rec_id":1544,"order_id":103616,"goods_id":2482,"photo":"2017/03/10/20170310180731_9825.jpeg","goods_name":"达利园法式软面包","goods_guige":"200g/袋 10枚","goods_num":1,"point_price":null,"goods_price":530,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 04:09:32","created_at":"2017-03-11 04:09:32","note":null}]},{"order_id":103663,"shop_id":567,"user_id":2260,"ride_id":234,"ride_status":6,"consignee":"白阳","mobile":"15639938618","address":"文秀花园利民云宝","total_amount":230,"order_amount":230,"order_status":"待评价","pay_type":"人民币","pay_status":0,"pay_time":"2017-03-25 14:26:00","shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 15:02:17","created_at":"2017-03-11 10:41:16","note":"来个美女送货","order_type":"普通订单","wuliu_type":"同城配送","verify_code":"","goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 12:41:16","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":"asdfasdf","jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1604,"order_id":103663,"goods_id":2650,"photo":"2017/03/11/20170311100727_2515.jpeg","goods_name":"金鸽多味葵花籽","goods_guige":"98g/袋","goods_num":1,"point_price":null,"goods_price":230,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 10:41:16","created_at":"2017-03-11 10:41:16","note":null}]},{"order_id":103618,"shop_id":567,"user_id":602,"ride_id":234,"ride_status":4,"consignee":"周遥","mobile":"18338131623","address":"测打的啊","total_amount":460,"order_amount":460,"order_status":"已关闭","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 08:18:59","created_at":"2017-03-11 08:18:59","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:18:59","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1546,"order_id":103618,"goods_id":2471,"photo":"2017/03/10/20170310174420_3569.jpeg","goods_name":"3+2苏打饼干","goods_guige":"125g/袋","goods_num":1,"point_price":null,"goods_price":460,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:18:59","created_at":"2017-03-11 08:18:59","note":null}]},{"order_id":103619,"shop_id":567,"user_id":4020,"ride_id":4,"ride_status":null,"consignee":"李啸祖","mobile":"13073733143","address":"建设路文化路文秀花园","total_amount":1090,"order_amount":1090,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":null,"created_at":"2017-03-11 08:30:13","note":"","order_type":"今日抢购","wuliu_type":"同城配送","verify_code":null,"goodsid":2257,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:30:13","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1547,"order_id":103619,"goods_id":2257,"photo":"2017/03/10/20170310143758_4817.jpeg","goods_name":"加多宝凉茶","goods_guige":"310ml*6罐/组","goods_num":1,"point_price":null,"goods_price":1780,"cost_price":0,"deleted_at":null,"updated_at":null,"created_at":"2017-03-11 08:30:13","note":null}]},{"order_id":103621,"shop_id":567,"user_id":152,"ride_id":4,"ride_status":0,"consignee":"s1","mobile":"13333333333","address":"ssssss2222","total_amount":1560,"order_amount":1560,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 08:32:51","created_at":"2017-03-11 08:32:51","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:32:51","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1549,"order_id":103621,"goods_id":2481,"photo":"2017/03/10/20170310180522_4914.jpeg","goods_name":"达利园法式软面包","goods_guige":"160g/袋 8枚","goods_num":1,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:32:51","created_at":"2017-03-11 08:32:51","note":null},{"rec_id":1550,"order_id":103621,"goods_id":2482,"photo":"2017/03/10/20170310180731_9825.jpeg","goods_name":"达利园法式软面包","goods_guige":"200g/袋 10枚","goods_num":1,"point_price":null,"goods_price":530,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:32:51","created_at":"2017-03-11 08:32:51","note":null},{"rec_id":1551,"order_id":103621,"goods_id":2486,"photo":"2017/03/10/20170310181030_1565.jpeg","goods_name":"达利园派草莓味","goods_guige":"250g","goods_num":1,"point_price":null,"goods_price":580,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:32:51","created_at":"2017-03-11 08:32:51","note":null}]},{"order_id":103665,"shop_id":567,"user_id":4101,"ride_id":6,"ride_status":6,"consignee":null,"mobile":null,"address":null,"total_amount":990,"order_amount":990,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":"2017-03-25 14:30:59","shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:39:22","created_at":"2017-03-11 10:47:18","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 12:47:18","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1606,"order_id":103665,"goods_id":2387,"photo":"2017/03/10/20170310111045_8009.jpeg","goods_name":"康师傅红烧牛肉面","goods_guige":"103g*5/袋","goods_num":1,"point_price":null,"goods_price":990,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 10:47:18","created_at":"2017-03-11 10:47:18","note":null}]},{"order_id":103623,"shop_id":567,"user_id":4020,"ride_id":6,"ride_status":5,"consignee":null,"mobile":null,"address":null,"total_amount":250,"order_amount":250,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:37:08","created_at":"2017-03-11 08:37:06","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:37:06","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1553,"order_id":103623,"goods_id":2389,"photo":"2017/03/10/20170310111818_3368.jpeg","goods_name":"康师傅鲜虾鱼板面 袋","goods_guige":"98g/袋","goods_num":1,"point_price":null,"goods_price":250,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:37:06","created_at":"2017-03-11 08:37:06","note":null}]},{"order_id":103624,"shop_id":567,"user_id":602,"ride_id":6,"ride_status":5,"consignee":null,"mobile":null,"address":null,"total_amount":380,"order_amount":380,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:36:44","created_at":"2017-03-11 08:37:24","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:37:24","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1554,"order_id":103624,"goods_id":2615,"photo":"2017/03/10/20170310194217_6926.jpeg","goods_name":"乡巴佬鸡爪","goods_guige":"35g/袋","goods_num":1,"point_price":null,"goods_price":100,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:37:24","created_at":"2017-03-11 08:37:24","note":null},{"rec_id":1555,"order_id":103624,"goods_id":2607,"photo":"2017/03/10/20170310193621_2164.jpeg","goods_name":"可比克薯片香辣味","goods_guige":"60g","goods_num":1,"point_price":null,"goods_price":280,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:37:24","created_at":"2017-03-11 08:37:24","note":null}]},{"order_id":103625,"shop_id":567,"user_id":718,"ride_id":6,"ride_status":5,"consignee":null,"mobile":null,"address":null,"total_amount":980,"order_amount":980,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:42:11","created_at":"2017-03-11 08:38:08","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:38:08","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1556,"order_id":103625,"goods_id":2481,"photo":"2017/03/10/20170310180522_4914.jpeg","goods_name":"达利园法式软面包","goods_guige":"160g/袋 8枚","goods_num":1,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:38:08","created_at":"2017-03-11 08:38:08","note":null},{"rec_id":1557,"order_id":103625,"goods_id":2482,"photo":"2017/03/10/20170310180731_9825.jpeg","goods_name":"达利园法式软面包","goods_guige":"200g/袋 10枚","goods_num":1,"point_price":null,"goods_price":530,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:38:08","created_at":"2017-03-11 08:38:08","note":null}]},{"order_id":103667,"shop_id":567,"user_id":2822,"ride_id":6,"ride_status":4,"consignee":"张贝贝","mobile":"13782187223","address":"中州路梅溪宾馆小区一单元","total_amount":900,"order_amount":900,"order_status":"待发货","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-10 12:00:07","created_at":"2017-03-11 10:52:41","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":"","goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 12:52:41","city_cost":null,"yunbei_cost":0,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":"2017-04-10 12:00:07","shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1608,"order_id":103667,"goods_id":2468,"photo":"2017/03/10/20170310173844_2454.jpeg","goods_name":"立白金桔洗洁精 1.29kg","goods_guige":"1.29kg/桶","goods_num":1,"point_price":null,"goods_price":900,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 10:52:41","created_at":"2017-03-11 10:52:41","note":null}]},{"order_id":103627,"shop_id":567,"user_id":514,"ride_id":6,"ride_status":5,"consignee":null,"mobile":null,"address":null,"total_amount":2250,"order_amount":2250,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-04-05 16:42:03","created_at":"2017-03-11 08:41:18","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:41:18","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1559,"order_id":103627,"goods_id":2470,"photo":"2017/03/10/20170310174207_4851.jpeg","goods_name":"3+2饼干榛子巧克力味","goods_guige":"80g/袋","goods_num":1,"point_price":null,"goods_price":320,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:18","created_at":"2017-03-11 08:41:18","note":null},{"rec_id":1560,"order_id":103627,"goods_id":2471,"photo":"2017/03/10/20170310174420_3569.jpeg","goods_name":"3+2苏打饼干","goods_guige":"125g/袋","goods_num":1,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:18","created_at":"2017-03-11 08:41:18","note":null},{"rec_id":1561,"order_id":103627,"goods_id":2481,"photo":"2017/03/10/20170310180522_4914.jpeg","goods_name":"达利园法式软面包","goods_guige":"160g/袋 8枚","goods_num":2,"point_price":null,"goods_price":450,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:18","created_at":"2017-03-11 08:41:18","note":null},{"rec_id":1562,"order_id":103627,"goods_id":2488,"photo":"2017/03/10/20170310181146_8306.jpeg","goods_name":"达利园派蛋黄味","goods_guige":"250g","goods_num":1,"point_price":null,"goods_price":580,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:18","created_at":"2017-03-11 08:41:18","note":null}]},{"order_id":103628,"shop_id":567,"user_id":4020,"ride_id":6,"ride_status":4,"consignee":null,"mobile":null,"address":null,"total_amount":400,"order_amount":400,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 08:41:56","created_at":"2017-03-11 08:41:56","note":null,"order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:41:56","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1563,"order_id":103628,"goods_id":2361,"photo":"2017/03/10/20170310092830_6522.jpeg","goods_name":"脉动 青柠味","goods_guige":"600ml/瓶","goods_num":1,"point_price":null,"goods_price":400,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:41:56","created_at":"2017-03-11 08:41:56","note":null}]},{"order_id":103629,"shop_id":567,"user_id":666,"ride_id":6,"ride_status":4,"consignee":null,"mobile":null,"address":null,"total_amount":0,"order_amount":0,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":null,"created_at":"2017-03-11 08:46:23","note":null,"order_type":"今日抢购","wuliu_type":"同城配送","verify_code":null,"goodsid":2259,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:46:23","city_cost":null,"yunbei_cost":null,"yb_num":null,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1564,"order_id":103629,"goods_id":2259,"photo":"2017/03/10/20170310144536_8656.jpeg","goods_name":"吉祥结食用调和油","goods_guige":"5L/桶","goods_num":1,"point_price":null,"goods_price":5990,"cost_price":0,"deleted_at":null,"updated_at":null,"created_at":"2017-03-11 08:46:23","note":null}]},{"order_id":103630,"shop_id":567,"user_id":514,"ride_id":6,"ride_status":4,"consignee":"徐杰","mobile":"18548986157","address":"校场路鸭灌新区一号楼六楼中户","total_amount":3030,"order_amount":3030,"order_status":"待接单","pay_type":"人民币","pay_status":0,"pay_time":null,"shipping_time":null,"ok_time":null,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":"","order_type":"普通订单","wuliu_type":"同城配送","verify_code":null,"goodsid":null,"points_price":null,"pgoodsid":null,"ps_time":"2017-03-11 10:46:32","city_cost":null,"yunbei_cost":3,"yb_num":0,"freight":0,"ride_info":null,"jiedan_time":null,"shop_name":"云贝网官方店\u2014南阳","data":[{"rec_id":1565,"order_id":103630,"goods_id":2397,"photo":"2017/03/10/20170310112510_1509.jpeg","goods_name":"统一阿萨姆原味奶茶","goods_guige":"500ml/瓶","goods_num":1,"point_price":null,"goods_price":380,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":null},{"rec_id":1566,"order_id":103630,"goods_id":2412,"photo":"2017/03/10/20170310140920_1211.jpeg","goods_name":"仲景香菇酱 奥尔良味","goods_guige":"210g/瓶","goods_num":1,"point_price":null,"goods_price":1190,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":null},{"rec_id":1567,"order_id":103630,"goods_id":2418,"photo":"2017/03/10/20170310141240_9444.jpeg","goods_name":"双汇大肉块香肠","goods_guige":"30g*8支/袋","goods_num":1,"point_price":null,"goods_price":960,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":null},{"rec_id":1568,"order_id":103630,"goods_id":2432,"photo":"2017/03/10/20170310142017_8156.jpeg","goods_name":"南德调味料","goods_guige":"200g/袋","goods_num":1,"point_price":null,"goods_price":500,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 08:46:32","created_at":"2017-03-11 08:46:32","note":null}]}]
         */

        private int total;
        private int per_page;
        private int current_page;
        private int last_page;
        private String next_page_url;
        private Object prev_page_url;
        private int from;
        private int to;
        private List<DataBeanX> data;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPer_page() {
            return per_page;
        }

        public void setPer_page(int per_page) {
            this.per_page = per_page;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getLast_page() {
            return last_page;
        }

        public void setLast_page(int last_page) {
            this.last_page = last_page;
        }

        public String getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(String next_page_url) {
            this.next_page_url = next_page_url;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public List<DataBeanX> getData() {
            return data;
        }

        public void setData(List<DataBeanX> data) {
            this.data = data;
        }

        public static class DataBeanX {
            /**
             * order_id : 103666
             * shop_id : 567
             * user_id : 2260
             * ride_id : null
             * ride_status : null
             * consignee : null
             * mobile : null
             * address : null
             * total_amount : 200
             * order_amount : 200
             * order_status : 已发货
             * pay_type : 人民币
             * pay_status : 0
             * pay_time : 2017-03-25 17:14:05
             * shipping_time : null
             * ok_time : null
             * deleted_at : null
             * updated_at : 2017-03-25 17:14:05
             * created_at : 2017-03-11 10:49:35
             * note : null
             * order_type : 普通订单
             * wuliu_type : 同城配送
             * verify_code : 23456
             * goodsid : null
             * points_price : null
             * pgoodsid : null
             * ps_time : 2017-03-11 12:49:35
             * city_cost : null
             * yunbei_cost : null
             * yb_num : null
             * freight : 0
             * ride_info : null
             * jiedan_time : null
             * shop_name : 云贝网官方店—南阳
             * data : [{"rec_id":1607,"order_id":103666,"goods_id":2615,"photo":"2017/03/10/20170310194217_6926.jpeg","goods_name":"乡巴佬鸡爪","goods_guige":"35g/袋","goods_num":2,"point_price":null,"goods_price":100,"cost_price":0,"deleted_at":null,"updated_at":"2017-03-11 10:49:35","created_at":"2017-03-11 10:49:35","note":null}]
             */

            private int order_id;
            private int shop_id;
            private int user_id;
            private Object ride_id;
            private Object ride_status;
            private Object consignee;
            private Object mobile;
            private Object address;
            private int total_amount;
            private int order_amount;
            private String order_status;
            private String pay_type;
            private int pay_status;
            private String pay_time;
            private Object shipping_time;
            private Object ok_time;
            private Object deleted_at;
            private String updated_at;
            private String created_at;
            private Object note;
            private String order_type;
            private String wuliu_type;
            private String verify_code;
            private Object goodsid;
            private Object points_price;
            private Object pgoodsid;
            private String ps_time;
            private Object city_cost;
            private Object yunbei_cost;
            private Object yb_num;
            private int freight;
            private Object ride_info;
            private Object jiedan_time;
            private String shop_name;
            private List<DataBean> data;

            public int getOrder_id() {
                return order_id;
            }

            public void setOrder_id(int order_id) {
                this.order_id = order_id;
            }

            public int getShop_id() {
                return shop_id;
            }

            public void setShop_id(int shop_id) {
                this.shop_id = shop_id;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public Object getRide_id() {
                return ride_id;
            }

            public void setRide_id(Object ride_id) {
                this.ride_id = ride_id;
            }

            public Object getRide_status() {
                return ride_status;
            }

            public void setRide_status(Object ride_status) {
                this.ride_status = ride_status;
            }

            public Object getConsignee() {
                return consignee;
            }

            public void setConsignee(Object consignee) {
                this.consignee = consignee;
            }

            public Object getMobile() {
                return mobile;
            }

            public void setMobile(Object mobile) {
                this.mobile = mobile;
            }

            public Object getAddress() {
                return address;
            }

            public void setAddress(Object address) {
                this.address = address;
            }

            public int getTotal_amount() {
                return total_amount;
            }

            public void setTotal_amount(int total_amount) {
                this.total_amount = total_amount;
            }

            public int getOrder_amount() {
                return order_amount;
            }

            public void setOrder_amount(int order_amount) {
                this.order_amount = order_amount;
            }

            public String getOrder_status() {
                return order_status;
            }

            public void setOrder_status(String order_status) {
                this.order_status = order_status;
            }

            public String getPay_type() {
                return pay_type;
            }

            public void setPay_type(String pay_type) {
                this.pay_type = pay_type;
            }

            public int getPay_status() {
                return pay_status;
            }

            public void setPay_status(int pay_status) {
                this.pay_status = pay_status;
            }

            public String getPay_time() {
                return pay_time;
            }

            public void setPay_time(String pay_time) {
                this.pay_time = pay_time;
            }

            public Object getShipping_time() {
                return shipping_time;
            }

            public void setShipping_time(Object shipping_time) {
                this.shipping_time = shipping_time;
            }

            public Object getOk_time() {
                return ok_time;
            }

            public void setOk_time(Object ok_time) {
                this.ok_time = ok_time;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public Object getNote() {
                return note;
            }

            public void setNote(Object note) {
                this.note = note;
            }

            public String getOrder_type() {
                return order_type;
            }

            public void setOrder_type(String order_type) {
                this.order_type = order_type;
            }

            public String getWuliu_type() {
                return wuliu_type;
            }

            public void setWuliu_type(String wuliu_type) {
                this.wuliu_type = wuliu_type;
            }

            public String getVerify_code() {
                return verify_code;
            }

            public void setVerify_code(String verify_code) {
                this.verify_code = verify_code;
            }

            public Object getGoodsid() {
                return goodsid;
            }

            public void setGoodsid(Object goodsid) {
                this.goodsid = goodsid;
            }

            public Object getPoints_price() {
                return points_price;
            }

            public void setPoints_price(Object points_price) {
                this.points_price = points_price;
            }

            public Object getPgoodsid() {
                return pgoodsid;
            }

            public void setPgoodsid(Object pgoodsid) {
                this.pgoodsid = pgoodsid;
            }

            public String getPs_time() {
                return ps_time;
            }

            public void setPs_time(String ps_time) {
                this.ps_time = ps_time;
            }

            public Object getCity_cost() {
                return city_cost;
            }

            public void setCity_cost(Object city_cost) {
                this.city_cost = city_cost;
            }

            public Object getYunbei_cost() {
                return yunbei_cost;
            }

            public void setYunbei_cost(Object yunbei_cost) {
                this.yunbei_cost = yunbei_cost;
            }

            public Object getYb_num() {
                return yb_num;
            }

            public void setYb_num(Object yb_num) {
                this.yb_num = yb_num;
            }

            public int getFreight() {
                return freight;
            }

            public void setFreight(int freight) {
                this.freight = freight;
            }

            public Object getRide_info() {
                return ride_info;
            }

            public void setRide_info(Object ride_info) {
                this.ride_info = ride_info;
            }

            public Object getJiedan_time() {
                return jiedan_time;
            }

            public void setJiedan_time(Object jiedan_time) {
                this.jiedan_time = jiedan_time;
            }

            public String getShop_name() {
                return shop_name;
            }

            public void setShop_name(String shop_name) {
                this.shop_name = shop_name;
            }

            public List<DataBean> getData() {
                return data;
            }

            public void setData(List<DataBean> data) {
                this.data = data;
            }

            public static class DataBean {
                /**
                 * rec_id : 1607
                 * order_id : 103666
                 * goods_id : 2615
                 * photo : 2017/03/10/20170310194217_6926.jpeg
                 * goods_name : 乡巴佬鸡爪
                 * goods_guige : 35g/袋
                 * goods_num : 2
                 * point_price : null
                 * goods_price : 100
                 * cost_price : 0
                 * deleted_at : null
                 * updated_at : 2017-03-11 10:49:35
                 * created_at : 2017-03-11 10:49:35
                 * note : null
                 */

                private int rec_id;
                private int order_id;
                private int goods_id;
                private String photo;
                private String goods_name;
                private String goods_guige;
                private int goods_num;
                private Object point_price;
                private int goods_price;
                private int cost_price;
                private Object deleted_at;
                private String updated_at;
                private String created_at;
                private Object note;

                public int getRec_id() {
                    return rec_id;
                }

                public void setRec_id(int rec_id) {
                    this.rec_id = rec_id;
                }

                public int getOrder_id() {
                    return order_id;
                }

                public void setOrder_id(int order_id) {
                    this.order_id = order_id;
                }

                public int getGoods_id() {
                    return goods_id;
                }

                public void setGoods_id(int goods_id) {
                    this.goods_id = goods_id;
                }

                public String getPhoto() {
                    return photo;
                }

                public void setPhoto(String photo) {
                    this.photo = photo;
                }

                public String getGoods_name() {
                    return goods_name;
                }

                public void setGoods_name(String goods_name) {
                    this.goods_name = goods_name;
                }

                public String getGoods_guige() {
                    return goods_guige;
                }

                public void setGoods_guige(String goods_guige) {
                    this.goods_guige = goods_guige;
                }

                public int getGoods_num() {
                    return goods_num;
                }

                public void setGoods_num(int goods_num) {
                    this.goods_num = goods_num;
                }

                public Object getPoint_price() {
                    return point_price;
                }

                public void setPoint_price(Object point_price) {
                    this.point_price = point_price;
                }

                public int getGoods_price() {
                    return goods_price;
                }

                public void setGoods_price(int goods_price) {
                    this.goods_price = goods_price;
                }

                public int getCost_price() {
                    return cost_price;
                }

                public void setCost_price(int cost_price) {
                    this.cost_price = cost_price;
                }

                public Object getDeleted_at() {
                    return deleted_at;
                }

                public void setDeleted_at(Object deleted_at) {
                    this.deleted_at = deleted_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public Object getNote() {
                    return note;
                }

                public void setNote(Object note) {
                    this.note = note;
                }
            }
        }
    }
}
