package com.liminyunbao.iyunbeishop.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Administrator on 2017/4/26 0026.
 */

public class AboutBitMap {
    /**
     * 图片压缩  三种方法
     */
    /**
     * 方法1  质量压缩
     * @param beforBitmap
     * @return
     */
    public static Bitmap compressImage(Bitmap beforBitmap) {
        // 可以捕获内存缓冲区的数据，转换成字节数组。
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if (beforBitmap != null) {
            // 第一个参数：图片压缩的格式；第二个参数：压缩的比率；第三个参数：压缩的数据存放到bos中
            beforBitmap.compress(Bitmap.CompressFormat.JPEG, 40, bos);
            int options = 40;
            // 循环判断压缩后的图片是否是大于100kb,如果大于，就继续压缩，否则就不压缩
            while (bos.toByteArray().length / 1024 > 500) {
                bos.reset();// 置为空
                // 压缩options%
                beforBitmap.compress(Bitmap.CompressFormat.JPEG, options, bos);
                // 每次都减少10
                options -= 10;
            }
            // 从bos中将数据读出来 存放到ByteArrayInputStream中
            ByteArrayInputStream bis = new ByteArrayInputStream(
                    bos.toByteArray());
            // 将数据转换成图片
            Bitmap afterBitmap = BitmapFactory.decodeStream(bis);
            return afterBitmap;
        }
        return null;
    }

//    /*
//     * 图片压缩方法02：获得缩略图
//     */
//    public Bitmap getThumbnail(int id) {
//        // 获得原图
//        Bitmap beforeBitmap = BitmapFactory.decodeResource(
//                mContext.getResources(), id);
//        // 宽
//        int w = mContext.getResources()
//                .getDimensionPixelOffset(R.dimen.image_w);
//        // 高
//        int h = mContext.getResources().getDimensionPixelSize(R.dimen.image_h);
//
//        // 获得缩略图
//        Bitmap afterBitmap = ThumbnailUtils
//                .extractThumbnail(beforeBitmap, w, h);
//        return afterBitmap;
//
//    }

    /**
     * 图片压缩03
     *
     *
     * @param newWidth
     *      图片指定的宽度
     * @param newHeight
     *      图片指定的高度
     * @return
     */
    public static Bitmap compressBitmap(Bitmap beforeBitmap, double newWidth, double newHeight) {
        //根据路径 获得原图
      //  Bitmap beforeBitmap = BitmapFactory.decodeFile(filePath);
        // 图片原有的宽度和高度
        float beforeWidth = beforeBitmap.getWidth();
        float beforeHeight = beforeBitmap.getHeight();
        // 计算宽高缩放率
        float scaleWidth = 0;
        float scaleHeight = 0;
        if (beforeWidth > beforeHeight) {
            scaleWidth = ((float) newWidth) / beforeWidth;
            //scaleHeight = ((float) newHeight) / beforeHeight;
            scaleHeight=scaleWidth;
        } else {
            scaleWidth = ((float) newWidth) / beforeWidth;
         //   scaleHeight = ((float) newHeight) / beforeWidth;

            scaleHeight=scaleWidth;
        }

        // 矩阵对象
        Matrix matrix = new Matrix();
        // 缩放图片动作 缩放比例
        matrix.postScale(scaleWidth, scaleHeight);

        // 创建一个新的Bitmap 从原始图像剪切图像
        Bitmap afterBitmap = Bitmap.createBitmap(beforeBitmap, 0, 0,
                (int) beforeWidth, (int) beforeHeight, matrix, true);

        return afterBitmap;
    }

    ///////////////////////////////=========将图片处理为base64 并上传============/////////////////////////////////////////////////

    /**
     * 通过base32  将图片处理为base64字符串
     * @param bit
     * @return
     */
    public static String Bitmap2StrByBase64(Bitmap bit){

        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        bit.compress(Bitmap.CompressFormat.PNG, 40, bos);//参数100表示不压缩
        byte[] bytes=bos.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);

    }


    public static String bitmapToBase64(Bitmap bitmap) {

        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                baos.flush();
                baos.close();

                byte[] bitmapBytes = baos.toByteArray();
                result = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }



}
