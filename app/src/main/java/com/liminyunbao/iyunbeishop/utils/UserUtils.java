package com.liminyunbao.iyunbeishop.utils;

import android.content.Context;

/**
 * Created by Administrator on 2017/6/2 0002.
 */

public class UserUtils {

    public static String UserToken(Context context){
       String usertoken= (String) SharePUtile.get(context,"user_token","");
        return usertoken;
    }
    public static String Userid(Context context){
        String usertoken= (String) SharePUtile.get(context,"user_id","");
        return usertoken;
    }
    public static String Shopid(Context context){
        String usertoken= (String) SharePUtile.get(context,"shop_id","");
        return usertoken;
    }
}
