package com.liminyunbao.iyunbeishop.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.liminyunbao.iyunbeishop.activity.SignInActivity;
import com.liminyunbao.iyunbeishop.R;

/**
 * Created by Administrator on 2017/5/11 0011.
 */

public class TokenUtils {

    public static final  String  SYSTEM_EXIT="com.liminyunbao.iyunbeishop";
    private static Dialog mDialog;

    /**
     * 检查user_token
     */
    public static void TackUserToken(Activity content){
        showPassOrdis(content);
    }

    /**
     *
     * @param
     */
    private static void showPassOrdis(final Activity conten){
        mDialog=new Dialog(conten, R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(30,0,30,0);
        params.width =WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate2 = LayoutInflater.from(conten).inflate(R.layout.item_warning_dialog, null);
        ImageView back= (ImageView) inflate2.findViewById(R.id.img_waring_daiog);
        Button btn= (Button) inflate2.findViewById(R.id.btn_waring);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharePUtile.put(conten,"user_id","");
                SharePUtile.put(conten, "user_token","");
                Intent intent2=new Intent();
                intent2.setAction(SYSTEM_EXIT);
                conten. sendBroadcast(intent2);
                Intent intent=new Intent(conten.getApplicationContext(), SignInActivity.class);
                conten.startActivity(intent);
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharePUtile.put(conten,"user_id","");
                SharePUtile.put(conten, "user_token","");
                Intent intent2=new Intent();
                intent2.setAction(SYSTEM_EXIT);
                conten. sendBroadcast(intent2);
                Intent intent=new Intent(conten, SignInActivity.class);
                conten.startActivity(intent);
            }
        });
        mDialog.setContentView(inflate2);
    }

}
