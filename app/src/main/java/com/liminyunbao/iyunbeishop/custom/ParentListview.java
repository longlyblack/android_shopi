package com.liminyunbao.iyunbeishop.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by Administrator on 2017/4/1.
 */

public class ParentListview extends ListView {
    public ParentListview(Context context) {
        super(context);
    }

    public ParentListview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ParentListview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
