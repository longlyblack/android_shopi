package com.liminyunbao.iyunbeishop.custom;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by pengbo on 2017/2/20.
 */

public abstract class BaseLazyFragment extends Fragment {

    protected abstract int getContentViewLayoutID();
    protected abstract void initViewsAndEvents(View view);
    private boolean isFirstVisible = true;
    private boolean isFirstInvisible = true;
    private boolean isPrepared;
    protected abstract void onFirstUserVisible();
    protected abstract void onUserVisible();
    private void onFirstUserInvisible() { }
    protected abstract void onUserInvisible();
    protected abstract void DetoryViewAndThing();
    private NetworkChangeReceiver mNetworkChangeReceiver;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(getContentViewLayoutID()!=0) {
            return inflater.inflate(getContentViewLayoutID(), null);
        }else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }




    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewsAndEvents(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initPrepare();
    }
    private synchronized void initPrepare() {
        if (isPrepared) {
            onFirstUserVisible();
        } else {
            isPrepared = true;
        }
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser){
            if(isFirstVisible){
                isFirstVisible=false;
                initPrepare();
            }else{
                onUserVisible();
            }
        }else{
            if(isFirstInvisible){
                isFirstInvisible=false;
                onFirstUserInvisible();
            }else {
                onUserInvisible();
            }
        }
    }

    @Override
    public void onDestroy() {
        DetoryViewAndThing();
        super.onDestroy();

    }
    class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null && info.isAvailable()) {

            } else {

            }
        }
    }

    /**
     * 做两个方法让子类集成，
     * 然而，有什么用呢？
     */
    protected abstract void HaveNet();
    protected abstract void GoneNet();

}
