package com.liminyunbao.iyunbeishop.http;

/**
 * Created by Administrator on 2017/4/8 0008.
 */

public interface HttpAPI {
     String API="http://3shop.iyunbei.net/api/v1/"; //测试接口
    //  String API="http://2shop.iyunbei.net/api/v1/"; //预上线环境接口
  //String API="http://shop.iyunbei.net/api/v1/";  //正式环境
   String LOGIN=API+"Package/login";//登录
   String LOGOUT=API+"Package/logout";//退出登录
   String CHOICE_SHOP=API+"Package/shoplist";//店铺选择
    /*服务类*/
   String RECORD=API+"Package/yanzheng";//验证记录
   String XIAOFEIYZ=API+"Package/code";//消费验证
   String MONEY=API+"Package/money";//资金管理/可用余额
   String MONEYINFOR=API+"Package/moneyinfo";
   String ORDERLIST=API+"Package/orderlistt";//普通订单列表
   String DETAILS=API+"Package/details";//普通流水
   String YOUHUI=API+"Package/discount";//优惠买单
   String YESTERDAY=API+"Package/yanzheng";//昨天
   String WEEK=API+"Package/yanzheng";//昨天
   String MOUNTH=API+"Package/yanzheng";//近一月
   String THREEMOUNT=API+"Package/yanzheng";//近3个月
   String APPLY=API+"Package/apply";//申请记录
   String INFO=API+"Package/todayinfo";//今日订单数  交易金额；
   String WITHDRAWALS=API+"Package/tixian";//是否绑定微信
   String QUETXIAN=API+"Package/querentixian";//确认提现
   String BINDING=API+"Package/bangding";//微信绑定
    /* 零售类 */
   String TODAYINFOR=API+"Retail/todayinfo";//今日收入
   String INFORMATION=API+"Package/information";//店铺管理
   String CHANGE_SHOP_NAME  =API+"Package/savename";//修改店铺名
   String CHANGE_ADDRES=API+"Package/addre";//修改地址
   String EVALUATION=API+"Retail/evaluation";//评价管理
   String RECEIVE=API+"Retail/received";//商家回复
   String BADREVIEW=API+"Retail/badreview";//差评
   String NOTREVIEW=API+"Retail/notreview";//未回复
   String REVENUE=API+"Retail/revenue";//营业额订单数
   String TREND=API+"Retail/trend";//年周月趋势图
   String SALE=API+"Retail/sale";//出售中
   String SALEOVER=API+"Retail/saleover";//已售罄
   String  WAREHOUSE=API+"Retail/warehouse";//仓库中
   String DELE=API+"Retail/delegoods";//删除商品
   String DOWN=API+"Retail/offshelf";//商品下架
   String FENLEI=API+"Retail/fenlei";//商品分类
   String PHOTO="http://img1.iyunbei.net/p.php";//上传分类
   String ADDGOODS=API+"Retail/addgoods";//添加商品
   String ORDER=API+"Retail/orderlist";//订单
   String ACCEPT=API+"Retail/shopjiedan";//商家接单
   String REFUSE=API+"Retail/shoprefusejiedan";//商家拒接接单
   String TACKORDER=API+"Retail/shopjiedan";//接单
   String SENDCORD=API+"Package/send";//发送验证码
   String CATEGORY=API+"Package/maincategory";//主营分类列表
   String REGIST=API+"Package/register";//注册
   String CREATSHOP=API+"Package/createshop";//创建店铺
   String Agreement="http://static.iyunbei.net/view/agreement/shopgreement.html";//入驻公约
   String Check="http://static.iyunbei.net/view/agreement/disputeRule.html";//交易纠纷处理规则
   String ChangName=API+"Package/savepeoname";//修该联系人
   String SHANGPINFEN=API+"Retail/fenzu";//商品分租
   String ABLECREATE=API+"Package/ablecreate";//是否充值过
   String CHANGMOBLIE=API+"Package/savemobile";//修改联系人号码
   String FORGETPWCORD=API+"Retail/sent";//修改密码验证码
   String CHANGPW =API+"Retail/savepwd";//获取code进行下一步
   String SUREPW=API+"Retail/surepwd";//确认修改
   String ADDGROUP=API+"Retail/teamadd";//分组添加
   String GROUPLIST=API+"Retail/teamshow";//分组列表
   String DELETGROUP=API+"Retail/deleteam";//分组删除
   String GROUPCHANGENAME=API+"Retail/changeteam";//分组更名
   String GROUPDETALIS=API+"Retail/teaminfo";//分组详情
   String SEARCHGOODSB=API+"Retail/teamsel";//分组商品查询
   String EDITGROUTGOODS=API+"Retail/teamedit";//编辑分组商品
   String TOWGROUP=API+"Retail/fenzuinfo";//耳机分组界面
   String SHANGPINLIST=API+"Retail/goodslist";//商品列表管理
   String SHANGPINXIANGQIANGADD=API+"Retail/goodsinfo";//商品管理详情
   String SAVEING=API+"Package/saveimg";//设置店招
   String  DELETGOOS=API+"Retail/goodsdel"; //商品详情删除
   String TUIJIAN=API+"Retail/ecommend";//推荐
   String OTHERGOODS=API+"Retail/othergoods";
   String Base64="data:image/jpeg;base64,";
   String MEALLITE=API+"Package/meallist";//推荐套餐列表
   String ADDMEAL=API+"Package/addmeal";//推荐套餐添加
   String MEALINFO=API+"Package/mealinfo";//推荐套餐详情
   String HEAD="http://img1.iyunbei.net/300x300/attachs/";//图片请求头
   String MEALLDELET=API+"Package/delmeal";//推荐套餐删除
   String MeallUPOD=API+"Package/setmeal";//推荐套餐上下架
   String RECOMMENDLIST=API+"Retail/recommendlist";//橱窗推荐
   String SHARE2="http://www.iyunbei.net/mobile/shop2/detail.html?shop_id=";//分享地址服务类
   String SHARE1="http://www.iyunbei.net/mobile/shop1/detail.html?shop_id=";//分享地址零售类
   String SHAREFAULT="https://static.iyunbei.net/res/images/mobile/shop1/shop.jpg";//默认分享图片
   String OKREFOND=API+"Retail/okrefund";//确认退款
   String FREIGHT=API+"Package/setfreight";//运费设置
   String CARDLIST=API+"Retail/cardlist";//读取卡券
}

