package com.liminyunbao.iyunbeishop;

/**
 * Created by Administrator on 2017/6/9 0009.
 */

public class GetPicture {
    public String pic_id;
    public String photo;

    public GetPicture(String cateid,String img){
        photo=img;
        pic_id=cateid;
    }

    public String getPic_ic() {
        return pic_id;
    }

    public void setPic_ic(String pic_ic) {
        this.pic_id = pic_ic;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
