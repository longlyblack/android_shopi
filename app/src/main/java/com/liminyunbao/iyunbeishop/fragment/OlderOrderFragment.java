package com.liminyunbao.iyunbeishop.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;

import com.liminyunbao.iyunbeishop.adapter.DialogOldoredAdapter;
import com.liminyunbao.iyunbeishop.adapter.OldorderLiseviewAdapter;
import com.liminyunbao.iyunbeishop.custom.ChildListView;
import com.liminyunbao.iyunbeishop.interFace.ChickBack;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/17.
 * 食堂已处理订单
 */

public class OlderOrderFragment extends Fragment {
    protected View rootView;
    protected ListView mListOldorderFram;


    private OldorderLiseviewAdapter mLiseviewAdapter;
    private View mHead;
    private Dialog mDialog;
    private DialogOldoredAdapter mDialogOldoredAdapter;
    private String foodname[]={"麻辣鸡丝","唐僧肉","麻婆豆腐"};
    private String bian[] = {"1号", "2号", "3号"};
    private List<String> mStrings;
    private List<String>mList;
    private ChildListView mChildListView;

    public static OlderOrderFragment newInstance() {

        Bundle args = new Bundle();

        OlderOrderFragment fragment = new OlderOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_old_order_fram, null, false);
        initView(rootView);
        initData();
        mListOldorderFram.setDividerHeight(0);
     //给listview  添加头部 但是在4.4版本上不能实现  会出现 类型转换异常  修改 使用listview 多布局  待修改
        mHead = View.inflate(getContext(), R.layout.item_headd,null);

        mListOldorderFram.addHeaderView(mHead);
        return rootView;
    }

    private void initData() {
        mStrings = new ArrayList<>();
        mList=new ArrayList<>();
        for (int i = 0; i < bian.length; i++) {
            mStrings.add(bian[i]);
            mList.add(foodname[i]);
        }
        mLiseviewAdapter = new OldorderLiseviewAdapter(new ChickBack() {
            @Override
            public void Back(Button button) {
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(mList);
                    }
                });

            }
        });

        mLiseviewAdapter.setList(mStrings);
        mListOldorderFram.setAdapter(mLiseviewAdapter);
        mLiseviewAdapter.notifyDataSetChanged();
    }
    private void showDialog(List<String> list){
        mDialogOldoredAdapter=new DialogOldoredAdapter();
        mDialog=new Dialog(getContext(),R.style.CustomDialog);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        params.width =WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate1 = LayoutInflater.from(getContext()).inflate(R.layout.item_dialog_oldorder, null);
        mChildListView = (ChildListView) inflate1.findViewById(R.id.listv_dialog);
        mChildListView.setDividerHeight(0);
        mChildListView.setHaveScrollbar(false);
        mDialogOldoredAdapter.setList(list);
        mChildListView.setAdapter(mDialogOldoredAdapter);
        mDialogOldoredAdapter.notifyDataSetChanged();
        mDialog.setContentView(inflate1);//设置view
    }


    private void initView(View rootView) {

        mListOldorderFram = (ListView) rootView.findViewById(R.id.list_oldorder_fram);

    }
}
