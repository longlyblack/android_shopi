package com.liminyunbao.iyunbeishop.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.bean.TrendBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.MPChartHelper;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/3/30.
 */

public class MonthFragment extends Fragment {
    protected View rootView;
    protected LineChart mLineChart;
    private List<String> xAxisValues;
    private List<Float> yAxisValues;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_month_fram, null, false);
        initView(rootView);
       // initData();
        initHttp();
      //  MPChartHelper.setLineChart(mLineChart,xAxisValues,yAxisValues,"营收统计（月）",true);
        return rootView;
    }

    private void initHttp(){
        String  userid = (String) SharePUtile.get(getActivity(), "user_id", "");
        String  user_token = (String) SharePUtile.get(getActivity(), "user_token", "");
        String shopid = (String) SharePUtile.get(getActivity(), "shop_id", "");
        OkGo.post(HttpAPI.TREND)
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("shopid",shopid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("趋势数据",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Gson gson=new Gson();
                                TrendBean trendBean = gson.fromJson(s, TrendBean.class);
                                List<TrendBean.DataBean.MonthBean> month = trendBean.getData().getMonth();
                                if(month.size()!=0){
                                    addData(month);
                                    mLineChart.setVisibility(View.VISIBLE);

                                }else {

                                    return;
                                }
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
    private void addData(List<TrendBean.DataBean.MonthBean>  month){
        yAxisValues = new ArrayList<>();
        xAxisValues = new ArrayList<>();
        if(month.size()==1){
            for(int i=0;i<month.size();i++){
                float weight=Float.parseFloat(month.get(i).getTotal());
                String time =month.get(i).getShijian();
               // String day=time.substring(5,10);
                // Log.e("这是截取的",weight+"");
                yAxisValues.add(weight);
                xAxisValues.add(time);
            }
            yAxisValues.add(0, 0f );
            xAxisValues.add(0,"0");
        }else {
            for(int i=0;i<month.size();i++){
                float weight=Float.parseFloat(month.get(i).getTotal());
                String time =month.get(i).getShijian();
               // String day=time.substring(5,10);
                // Log.e("这是截取的",weight+"");
                if(i==0){
                    xAxisValues.add(time);
                }else if(i==month.size()-1){
                    xAxisValues.add(time);
                }else {
                    xAxisValues.add("");
                }
                yAxisValues.add(weight);

            }
        }
        MPChartHelper.setLineChart(mLineChart,xAxisValues,yAxisValues,"营收统计（月）",true);
    }


/*    private void initData() {
        xAxisValues = new ArrayList<>();
        yAxisValues = new ArrayList<>();
        for (int i = 1; i < 5; ++i) {
            xAxisValues.add(String.valueOf(i));
            yAxisValues.add((float) (Math.random() * 700 + 20));
        }
    }*/

    private void initView(View rootView) {
        mLineChart = (LineChart) rootView.findViewById(R.id.lineChart);
    }
}
