package com.liminyunbao.iyunbeishop.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.activity.CommodityActivity;
import com.liminyunbao.iyunbeishop.activity.EvaluationActivity;
import com.liminyunbao.iyunbeishop.activity.MarkeManagerActivity;
import com.liminyunbao.iyunbeishop.activity.SettlementActivity;
import com.liminyunbao.iyunbeishop.activity.StatisticsActivity;
import com.liminyunbao.iyunbeishop.activity.StoreActivity;
import com.liminyunbao.iyunbeishop.activity.VerificationRemberActivity;
import com.liminyunbao.iyunbeishop.adapter.SaveHomGridAdapter;
import com.liminyunbao.iyunbeishop.bean.XFeiMaYanzhengBean;
import com.liminyunbao.iyunbeishop.custom.AutoHorizontalScrollTextView;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.IntentUtils;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.uuzuche.lib_zxing.activity.CaptureActivity;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/4/1.
 */

public class SaverHomFragment extends Fragment  {
    public static final int REQUEST_CODE = 111;
    protected View rootView;
    protected TextView mTvShopnameSaveHomepageFram;
    protected EditText mEdYanzhengSaveHomepageFram;
    protected AutoHorizontalScrollTextView mTvMessageSaveHompageFram;
    protected GridView mGdMenuSaveHomepageFram;
    protected ImageView mImgSaoma;
    protected ImageView mImgYanzheng;
    protected RelativeLayout mRlMessage;
    protected LinearLayout mLlSaoma;
    protected LinearLayout mLlYanzheng;
    private SaveHomGridAdapter mSaveHomGridAdapter;
    private List<String> mList;
    private String imgs[] = {"商品管理", "营销管理", "评价管理", "资金管理", "营收分析", "店铺管理"};
    private CharSequence temp;
    private Dialog mDialog;
    protected ImageView mImgCancel;
    protected ImageView mImgWPass;
    protected TextView mTvWPass;
    protected TextView mTextShopname;
    protected TextView mTvWShaoName;
    protected Button mBtnQueding;
    private String user_token;
    private String type = "error";

    private static final int PERMISSION_CORD = 10;

    public static SaverHomFragment newInstance(String shopname, int version) {
        Bundle args = new Bundle();
        args.putString("shopname", shopname);
        args.putInt("version", version);
        SaverHomFragment fragment = new SaverHomFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.item_save_home_fram, null, false);
        initView(rootView);
     /*   sdf = getActivity().getSharedPreferences("iyunbei", Activity.MODE_PRIVATE);
        Log.e("这是验证ma",sdf.getString("shop_id",""));*/
        Bundle arguments = getArguments();
        mTvShopnameSaveHomepageFram.setText(arguments.getString("shopname"));
        //  vsesion=arguments.getInt("version");
        initData();
        EdtextChange();
        mLlSaoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //判断版本是否是6.0以上
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    //判断是否授权
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {   //PackageManager.PERMISSION_GRANTED 可以往下执行 PERMISSION_DENIED 表示没有授权
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_CORD);
                    } else {
                        Zxing();
                    }
                } else {
                    Zxing();
                }
            }
        });
        mLlYanzheng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.goTo(getContext(), VerificationRemberActivity.class);
            }
        });
        return rootView;
    }

    private void initData() {
        user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        mList = new ArrayList<>();
        mSaveHomGridAdapter = new SaveHomGridAdapter();
        for (int i = 0; i < imgs.length; i++) {
            mList.add(imgs[i]);
        }
        mSaveHomGridAdapter.setList(mList);
        mGdMenuSaveHomepageFram.setAdapter(mSaveHomGridAdapter);
        mSaveHomGridAdapter.notifyDataSetChanged();

        mGdMenuSaveHomepageFram.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 3) {
                    IntentUtils.goTo(parent.getContext(), SettlementActivity.class);
                    // SaveZiJinLiuShuiActivity.class
                } else if (position == 0) {
                    //
                    Intent intent = new Intent(getContext(), CommodityActivity.class);
                    intent.putExtra("lei", "2");
                    startActivity(intent);
                } else if (position == 5) {
                    IntentUtils.goTo(parent.getContext(), StoreActivity.class);
                } else if (position == 2) {
                    IntentUtils.goTo(parent.getContext(), EvaluationActivity.class);
                } else if (position == 4) {
                    IntentUtils.goTo(parent.getContext(), StatisticsActivity.class);
                } else if (position == 1) {
                    //此处是跳转营销管理
                   // IntentUtils.goTo(parent.getContext(), MarkeManagerActivity.class);
                    Intent intent =new Intent(getContext(),MarkeManagerActivity.class);
                    intent.putExtra("WTUIJ","2");
                    startActivity(intent);
                }
            }
        });
    }
  /*  public void showCamer(){
        PermissionUtils.requestPermission(getActivity(),PermissionUtils.CODE_CAMERA,mPermissionGrant);
    }*/

    private void EdtextChange() {
        mEdYanzhengSaveHomepageFram.setCursorVisible(false);
        mEdYanzhengSaveHomepageFram.setHintTextColor(getResources().getColor(R.color.colorHlin));
        mEdYanzhengSaveHomepageFram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEdYanzhengSaveHomepageFram.setCursorVisible(true);
            }
        });
        mEdYanzhengSaveHomepageFram.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                temp = s;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                //判断输入的数字长度判断
                if (temp.length() == 8) {
                    initDate(mEdYanzhengSaveHomepageFram.getText().toString());
                }
            }
        });
    }

    private void Zxing() {
        Intent intent = new Intent(getContext(), CaptureActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    private void initDate(String code) {
        String userid = (String) SharePUtile.get(getContext(), "user_id", "");
        String shopid = (String) SharePUtile.get(getContext(), "shop_id", "");
        OkGo.post(HttpAPI.XIAOFEIYZ)
                .connTimeOut(10000)
                .params("user_id", userid)
                .params("user_token", user_token)
                .params("shopid", shopid)
                .params("code", code)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Gson gson = new Gson();
                        XFeiMaYanzhengBean xFeiMaYanzheng = gson.fromJson(s, XFeiMaYanzhengBean.class);
                        String status = xFeiMaYanzheng.getStatus();
                        int data = xFeiMaYanzheng.getData();
                        String masg = xFeiMaYanzheng.getMsg();
                        if (status.equals("1")) {
                            ShowDialog(data, masg);
                        } else if (status.equals("0")) {
                            ShowDialog(data, masg);
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e("我是返回的onError", e.getMessage());
                    }
                });
    }

    private void initView(View rootView) {
        mTvShopnameSaveHomepageFram = (TextView) rootView.findViewById(R.id.tv_shopname_save_homepage_fram);
        mEdYanzhengSaveHomepageFram = (EditText) rootView.findViewById(R.id.ed_yanzheng_save_homepage_fram);
        mTvMessageSaveHompageFram = (AutoHorizontalScrollTextView) rootView.findViewById(R.id.tv_message_save_hompage_fram);
        mGdMenuSaveHomepageFram = (GridView) rootView.findViewById(R.id.gd_menu_save_homepage_fram);
        mImgSaoma = (ImageView) rootView.findViewById(R.id.img_saoma);
        mImgYanzheng = (ImageView) rootView.findViewById(R.id.img_yanzheng);
        mRlMessage = (RelativeLayout) rootView.findViewById(R.id.rl_message);
        mLlSaoma = (LinearLayout) rootView.findViewById(R.id.ll_saoma);
        mLlYanzheng = (LinearLayout) rootView.findViewById(R.id.ll_yanzheng);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CORD) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Zxing();
            } else {
                Perssion();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void Perssion() {
        //权限被拒绝后 弹出提醒
        new AlertDialog.Builder(getContext())
                .setMessage("请设置权限，否则无法正常使用！")
                .setPositiveButton("去设置", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        getActivity().startActivity(intent);
                    }
                })
                .setNegativeButton("取消", null)
                .create()
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            //处理扫描结果（在界面上显示）
            if (null != data) {
                Bundle bundle = data.getExtras();
                if (bundle == null) {
                    return;
                }
                if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
                    String result = bundle.getString(CodeUtils.RESULT_STRING);
                    initDate(result);
                } else if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_FAILED) {
                    Toast.makeText(getContext(), "解析二维码失败", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    /**
     * 扫码成功后弹出的dialog
     */
    private void ShowDialog(int data, String masg) {
        mDialog = new Dialog(getActivity(), R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(30, 0, 30, 0);
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate1 = LayoutInflater.from(getActivity()).inflate(R.layout.item_saoma_dialog, null);
        mImgCancel = (ImageView) inflate1.findViewById(R.id.img_cancel);
        mImgWPass = (ImageView) inflate1.findViewById(R.id.img_w_pass);
        mTvWPass = (TextView) inflate1.findViewById(R.id.tv_w_pass);
        mTextShopname = (TextView) inflate1.findViewById(R.id.text_shopname);
        mTvWShaoName = (TextView) inflate1.findViewById(R.id.tv_w_shao_name);
        mBtnQueding = (Button) inflate1.findViewById(R.id.btn_queding);
        if (data == 1) {
            Bitmap mbit = BitmapFactory.decodeResource(getResources(), R.mipmap.tpass);
            mImgWPass.setImageBitmap(mbit);
            mTvWPass.setText("验证成功！");
            mTvWShaoName.setText(masg);
        } else {
            Bitmap mbit = BitmapFactory.decodeResource(getResources(), R.mipmap.tunpass);
            mImgWPass.setImageBitmap(mbit);
            mTvWPass.setText("验证失败！");
            mTextShopname.setVisibility(View.GONE);
            mTvWShaoName.setText(masg);
        }
        mImgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mBtnQueding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setContentView(inflate1);
    }

}
