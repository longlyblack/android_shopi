package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.liminyunbao.iyunbeishop.adapter.ConsumptionListAdapter;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/20.
 * 待消费
 */

public class ConsumptionFragent extends Fragment {

    protected View rootView;
    protected ListView mListConsumptionFram;
    private List<String >mList;
    private String hao[]={"1234560","1234570","1234580","55662","884121"};
    private ConsumptionListAdapter mConsumptionListAdapter;

    public static ConsumptionFragent newInstance() {

        Bundle args = new Bundle();

        ConsumptionFragent fragment = new ConsumptionFragent();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_consumption_fram, null, false);
        initView(rootView);
        mListConsumptionFram.setDividerHeight(0);
        initData();
        return rootView;
    }
    private void initData(){
        mList=new ArrayList<>();
        mConsumptionListAdapter=new ConsumptionListAdapter();
        for(int i=0;i<hao.length;i++){
            mList.add(hao[i]);
        }
        mConsumptionListAdapter.setList(mList);
        mListConsumptionFram.setAdapter(mConsumptionListAdapter);
        mConsumptionListAdapter.notifyDataSetChanged();
    }

    private void initView(View rootView) {
        mListConsumptionFram = (ListView) rootView.findViewById(R.id.list_consumption_fram);
    }
}
