package com.liminyunbao.iyunbeishop.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.liminyunbao.iyunbeishop.adapter.BeaseAddnewListAdapter;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/27.
 */

public class BeaseAddNewFragment extends Fragment {
    protected View rootView;
    protected ListView mListvAddnewChildLsit;
    private String names[]={"农夫大盘鸡","农夫三拳","确实","有点点点的","甜甜啊","三圈甜","农夫大盘鸡","农夫三拳","确实",};

    private BeaseAddnewListAdapter mAdapter;
    private List<String > mStrings;
    private Map<Integer, Boolean> isselect;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_beaseaddnew_fragment, null, false);
        initView(rootView);
        intiData(container.getContext());
        mListvAddnewChildLsit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*LiseAdapter.ViewHolder holder = (LiseAdapter.ViewHolder) view.getTag();
                holder.mBox.toggle();
                mLiseAdapter.isSelect.put(position, holder.mBox.isChecked());*/
            }
        });


        return rootView;
    }
    private void intiData(Context context){
        mStrings=new ArrayList<>();
        isselect=new HashMap<>();
        for(int i=0;i<names.length;i++){
            mStrings.add(names[i]);
        }
        mAdapter=new BeaseAddnewListAdapter(context,mStrings,isselect);
        mListvAddnewChildLsit.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }

    private void initView(View rootView) {
        mListvAddnewChildLsit = (ListView) rootView.findViewById(R.id.listv_addnew_child_lsit);

    }
}
