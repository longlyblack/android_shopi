package com.liminyunbao.iyunbeishop.fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.activity.GeneralorderActivity;
import com.liminyunbao.iyunbeishop.activity.SavesOrderActivity;
import com.liminyunbao.iyunbeishop.adapter.OrderGridAdapter;
import com.liminyunbao.iyunbeishop.bean.InFoBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.IntentUtils;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/3/15.
 * 订单
 */

public class OrderFragment extends BaseLazyFragment {
    protected View rootView;
    protected TextView mTvDingdannum;
    protected TextView mTvJiaoyie;
    protected GridView mGvMenuOrderFram;
    private String  names[]={"普通订单","堂食订单","预约管理","外卖订单"};
    private List<String> mStrings;
    private OrderGridAdapter mOrderGridAdapter;

    public static OrderFragment newInstance() {

        Bundle args = new Bundle();

        OrderFragment fragment = new OrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_fragment_order;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        mTvDingdannum = (TextView) view.findViewById(R.id.tv_dingdannum);
        mTvJiaoyie = (TextView) view.findViewById(R.id.tv_jiaoyie);
        mGvMenuOrderFram = (GridView) view.findViewById(R.id.gv_menu_order_fram);
    }

    @Override
    protected void onFirstUserVisible() {
        initData();
        intentActivity();http();
    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {
        http();
    }

    @Override
    protected void GoneNet() {

    }

    private  void http(){
       String user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        String  userid = (String) SharePUtile.get(getContext(), "user_id", "");
        OkGo.post(HttpAPI.INFO)
                .connTimeOut(10000)
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("shopid",shopid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {

                        Log.e("今日额",s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String statute=jsonObject.getString("status");
                            if(statute.equals("1")){
                                Gson gson=new Gson();
                                InFoBean inFoBean = gson.fromJson(s, InFoBean.class);
                                if(inFoBean.getStatus().equals("1")){
                                    mTvDingdannum.setText(""+inFoBean.getData().getDingdanshu());
                                    mTvJiaoyie.setText("￥"+inFoBean.getData().getJiaoyie());
                                }
                            }else if(statute.equals("0")){
                                Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                            }else if(statute.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getContext(),"链接超时!",Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void intentActivity(){

        mGvMenuOrderFram.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    IntentUtils.goTo(getActivity(), GeneralorderActivity.class);
                }else if(position==1){
                    Toast.makeText(getContext(),"暂未开通！",Toast.LENGTH_SHORT).show();
                   // Toast.makeText(getContext(),"暂未开通！",Toast.LENGTH_SHORT).show();
                }else if(position==2){
                 // IntentUtils.goTo(getActivity(), DistributionListActivity.class);
                    Toast.makeText(getContext(),"暂未开通！",Toast.LENGTH_SHORT).show();
                }else if(position==3){
                   // IntentUtils.goTo(getActivity(), CapitalFlowActivity.class);
                    IntentUtils.goTo(getActivity(), SavesOrderActivity.class);
                }
            }
        });

    }
    private void initData(){
        mStrings=new ArrayList<>();
        mOrderGridAdapter=new OrderGridAdapter();
        for(int i=0;i<names.length;i++){
            mStrings.add(names[i]);
        }
        mOrderGridAdapter.setStrings(mStrings);
        mGvMenuOrderFram.setAdapter(mOrderGridAdapter);
        mOrderGridAdapter.notifyDataSetChanged();
    }
}
