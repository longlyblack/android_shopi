package com.liminyunbao.iyunbeishop.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.activity.ShopDetails;
import com.liminyunbao.iyunbeishop.adapter.WiteSenAdapter;
import com.liminyunbao.iyunbeishop.bean.WaitingListBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;
import com.umeng.socialize.utils.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/3/30.
 * 待接单
 */

public class WiteSenFragment extends BaseLazyFragment {

    protected View rootView;
    protected PullToRefreshListView mListvWitesenFram;
    private List<WaitingListBean.DataBean.OrderMsg> mList;
    private List<WaitingListBean.DataBean.OrderMsg > AllList=new ArrayList<>();
    private WiteSenAdapter mWiteSenAdapter;

    private Dialog mDialog;
    private String shopid;
    private int page=1;
    private String user_token;
    private int last_page=0;
    private String userid;
    private   WaitingListBean.DataBean.OrderMsg orderMsg;
    private KyLoadingBuilder builder;
    public void setList(List<WaitingListBean.DataBean.OrderMsg> list) {
        mList = list;
    }

    public static WiteSenFragment newInstance() {

        Bundle args = new Bundle();

        WiteSenFragment fragment = new WiteSenFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_witesen_fram;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        mListvWitesenFram = (PullToRefreshListView) view.findViewById(R.id.listv_witesen_fram);
    }

    @Override
    protected void onFirstUserVisible() {
       userid = (String) SharePUtile.get(getContext(), "user_id", "");
        user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        pullrefish();
        initData();
        mListvWitesenFram.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                orderMsg = AllList.get(position-1);
                Intent intent=new Intent(getContext(), ShopDetails.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("ordermsg",orderMsg);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onUserVisible() {
        AllList.clear();
        page=1;
        initData();
    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {
       // initData();
    }

    @Override
    protected void GoneNet() {

    }

    private void pullrefish(){
        mListvWitesenFram.setMode(PullToRefreshBase.Mode.BOTH);
        mListvWitesenFram.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                AllList.clear();
                page=1;
                initData();
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

                if(page!=last_page){
                    page++;
                    initData();
                }else{
                    mListvWitesenFram.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvWitesenFram.onRefreshComplete();
                        }
                    }, 1000);
                }
            }
        });
        mWiteSenAdapter=new WiteSenAdapter();
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvWitesenFram.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mWiteSenAdapter.notifyDataSetChanged();
    }
    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(getContext());
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭

    }
    private void initData(){

        mList=new ArrayList<>();
        OkGo.post(HttpAPI.ORDER)
                .connTimeOut(10000)
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("shopid",shopid)
                .params("page",page+"")
                .params("cmd","1")
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);

                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {

                        Log.e("待完成",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                JSONObject data = jsonObject.getJSONObject("data");
                                last_page=data.getInt("last_page");
                                Gson gson=new Gson();
                                WaitingListBean waitingListBean = gson.fromJson(s, WaitingListBean.class);
                                mList = waitingListBean.getData().getData();
                                AllList.addAll(mList);
                                mWiteSenAdapter.setList(AllList);
                                mListvWitesenFram.setAdapter(mWiteSenAdapter);
                                mWiteSenAdapter.notifyDataSetChanged();
                                mListvWitesenFram.onRefreshComplete();
                            }else if(status.equals("0")){
                                mListvWitesenFram.onRefreshComplete();
                                Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);

                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getContext(),"链接超时！",Toast.LENGTH_SHORT).show();
                    }
                });

        mWiteSenAdapter.setSetOnlesion(new WiteSenAdapter.setOnlesion() {
            @Override
            public void setBtnLisen(Button button, final String orderid) {
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tackOrder(orderid);
                    }
                });
            }
        });
    }

    /**
     * 接单
     */

    private void tackOrder(String order){
        OkGo.post(HttpAPI.TACKORDER)
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("orderid",order)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("接单",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            int status = jsonObject.getInt("status");
                            if(status==1){
                                String msg = jsonObject.getString("msg");
                                Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
                                AllList.clear();
                                page=1;
                                initData();
                            }else if(status==0){
                                String msg = jsonObject.getString("msg");
                                Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
    /**
     * 之前的接单备注Dialog
     */
    private void ShowDialog(){
        mDialog=new Dialog(getActivity(),R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(30,0,30,0);
        params.width =WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate1 = LayoutInflater.from(getActivity()).inflate(R.layout.item_beizhu_dialog, null);
        ImageView closeh= (ImageView) inflate1.findViewById(R.id.img_beizhu_closs);
        closeh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setContentView(inflate1);
    }
}
