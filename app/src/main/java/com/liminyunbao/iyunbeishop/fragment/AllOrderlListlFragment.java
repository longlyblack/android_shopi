package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.liminyunbao.iyunbeishop.adapter.AllOrderListAdapter;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/20.
 *  订单列表 全部
 */

public class AllOrderlListlFragment extends Fragment {
    protected View rootView;
    protected ListView mListConsumptionFram;
    private String hao[]={"15566320","885523570","1268550","11455455","19936","1896335"};
    private AllOrderListAdapter mAllOrderListAdapter;
    private List<String> mList;

    public static AllOrderlListlFragment newInstance() {

        Bundle args = new Bundle();

        AllOrderlListlFragment fragment = new AllOrderlListlFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_consumption_fram, null, false);
        initView(rootView);
        mListConsumptionFram.setDividerHeight(0);
        initData();
        mListConsumptionFram.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        return rootView;
    }
    private void initData(){
        mList=new ArrayList<>();
        mAllOrderListAdapter=new AllOrderListAdapter();
        for(int i=0;i<hao.length;i++){
            mList.add(hao[i]);

        }
        mList.add(hao.length,null);
      //  mList.add(hao.length,null);
        mAllOrderListAdapter.setList(mList);
        mListConsumptionFram.setAdapter(mAllOrderListAdapter);
        mAllOrderListAdapter.notifyDataSetChanged();


    }

    private void initView(View rootView) {
        mListConsumptionFram = (ListView) rootView.findViewById(R.id.list_consumption_fram);
    }
}
