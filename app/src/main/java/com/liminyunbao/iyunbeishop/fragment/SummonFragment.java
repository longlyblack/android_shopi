package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.view.View;

import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;

/**
 * Created by SensYang on 2017/07/03 11:43
 */

public class SummonFragment extends BaseLazyFragment {

    /**
     * 传入一个view视图，相当于oncreateView方法
     */
    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_summon;
    }


    @Override
    protected void initViewsAndEvents(View view) {
    }

    @Override
    protected void onFirstUserVisible() {

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }
    public static SummonFragment newInstance() {
        Bundle args = new Bundle();
        SummonFragment f = new SummonFragment();
        f.setArguments(args);
        return f;
    }
}
