package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.liminyunbao.iyunbeishop.R;

/**
 * Created by Administrator on 2017/3/21.
 * 配送订单 的退款
 */

public class DeliveryRefundFragment extends Fragment {
    protected View rootView;
    protected ListView mListConsumptionFram;

    public static DeliveryRefundFragment newInstance() {

        Bundle args = new Bundle();

        DeliveryRefundFragment fragment = new DeliveryRefundFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_consumption_fram, null, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mListConsumptionFram = (ListView) rootView.findViewById(R.id.list_consumption_fram);
    }
}
