package com.liminyunbao.iyunbeishop.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.activity.ShopDetails;
import com.liminyunbao.iyunbeishop.adapter.FaHuoZhuangTaiAdapter;
import com.liminyunbao.iyunbeishop.bean.WaitingListBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;
import com.umeng.socialize.utils.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/3/30.
 * 完成
 */

public class SuccessFragment extends BaseLazyFragment {
    protected View rootView;
    protected PullToRefreshListView mListvSuccess;
    private List<WaitingListBean.DataBean.OrderMsg> mList;
    private FaHuoZhuangTaiAdapter mFaHuoZhuangTaiAdapter;
    private List<WaitingListBean.DataBean.OrderMsg> AllLsit=new ArrayList<>();
    private int page=1;
    private int last_page=0;
    private String shopid;
    private String user_token;
    private String userid;
    private WaitingListBean.DataBean.OrderMsg orderMsg;

    private KyLoadingBuilder builder;
    public static SuccessFragment newInstance() {

        Bundle args = new Bundle();
        SuccessFragment fragment = new SuccessFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_success_fram;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        mListvSuccess = (PullToRefreshListView) view.findViewById(R.id.listv_success);
    }

    @Override
    protected void onFirstUserVisible() {
        userid = (String) SharePUtile.get(getContext(), "user_id", "");
        user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        pullrefish();
        initData();
        mListvSuccess.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                orderMsg = AllLsit.get(position-1);
                Intent intent=new Intent(getContext(), ShopDetails.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("ordermsg",orderMsg);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onUserVisible() {
        AllLsit.clear();
        page=1;
        initData();
    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(getContext());
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭

    }
    private void initData(){

        OkGo.post(HttpAPI.ORDER)
                .connTimeOut(10000)
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("shopid",shopid)
                .params("page",page+"")
                .params("cmd","3")
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);

                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("已完成",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                JSONObject data = jsonObject.getJSONObject("data");
                                last_page = data.getInt("last_page");
                                Gson gson=new Gson();
                                WaitingListBean waitingListBean = gson.fromJson(s, WaitingListBean.class);
                                mList= waitingListBean.getData().getData();
                                AllLsit.addAll(mList);
                                mFaHuoZhuangTaiAdapter.setList(AllLsit);
                                mListvSuccess.setAdapter(mFaHuoZhuangTaiAdapter);
                                mFaHuoZhuangTaiAdapter.notifyDataSetChanged();
                                mFaHuoZhuangTaiAdapter.setRefund(new FaHuoZhuangTaiAdapter.Refund() {
                                    @Override
                                    public void setRefund(Button mrefund,String orderid) {
                                        /**
                                         * 此处对退款处理
                                         */
                                    }
                                });
                                mListvSuccess.onRefreshComplete();
                            }else if(status.equals("0")){
                                Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                mListvSuccess.onRefreshComplete();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);

                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getContext(),"链接超时！",Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void pullrefish(){
        mListvSuccess.setMode(PullToRefreshBase.Mode.BOTH);
        mListvSuccess.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                AllLsit.clear();
                page=1;
                initData();
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

                if(page!=last_page){
                    page++;
                    initData();
                }else{
                    mListvSuccess.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvSuccess.onRefreshComplete();
                        }
                    }, 1000);
                }
            }
        });
        mFaHuoZhuangTaiAdapter=new FaHuoZhuangTaiAdapter();
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvSuccess.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mFaHuoZhuangTaiAdapter.notifyDataSetChanged();
    }
}
