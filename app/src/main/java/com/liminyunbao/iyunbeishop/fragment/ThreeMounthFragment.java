package com.liminyunbao.iyunbeishop.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.activity.OrderDetailsActivity;
import com.liminyunbao.iyunbeishop.adapter.YanZhengAdapter;
import com.liminyunbao.iyunbeishop.bean.PuTongListBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/4/10 0010.
 */

public class ThreeMounthFragment extends Fragment {
    protected View rootView;
    protected PullToRefreshListView mListvYanzhengThreemouont;

    private List<PuTongListBean.DataBean.OrderDmsg> mDateBeen;
    private List<PuTongListBean.DataBean.OrderDmsg> mALLBeen=new ArrayList<>();
    private YanZhengAdapter mYanZhengAdapter;
    private boolean isup=false;
    private int page=1;
    private int last_page=0;


    private KyLoadingBuilder builder;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_threemount_fram, null, false);

        initView(rootView);
        Aimoi();
        initData();
        pullfresh();


        mListvYanzhengThreemouont.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PuTongListBean.DataBean.OrderDmsg dataBeanX ;
                Intent intent = new Intent(getContext(), OrderDetailsActivity.class);
              /*  if(!isup){
                    dataBeanX = mDateBeen.get(position - 1);
                }else{*/
                    dataBeanX=mALLBeen.get(position-1);

                List<PuTongListBean.DataBean.OrderDmsg.ShopData> data = dataBeanX.getData();
                intent.putExtra("code", dataBeanX.getVerify_code() + "");
                intent.putExtra("shopname", dataBeanX.getShop_name());
                intent.putExtra("status", dataBeanX.getOrder_status() + "");//状态
                intent.putExtra("zongja", dataBeanX.getTotal_amount() + "");//总价
                intent.putExtra("zonge", dataBeanX.getOrder_amount() + "");//总额
                intent.putExtra("youhui", Float.parseFloat(dataBeanX.getOrder_amount()) -Float.parseFloat(dataBeanX.getTotal_amount())  + "");//优惠
                intent.putExtra("oreder", dataBeanX.getOrder_id() + "");//订单号
                intent.putExtra("time", dataBeanX.getCreated_at() + "");//下单时间
                intent.putExtra("data", (Serializable) data);
                startActivity(intent);


            }
        });

        return rootView;
    }

    private void pullfresh() {
        mListvYanzhengThreemouont.setMode(PullToRefreshBase.Mode.BOTH);
        mListvYanzhengThreemouont.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                //开始下拉  我们做Http请求
                Aimoi();
                mALLBeen.clear();
               page=1;

                initData();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                Aimoi();
             if(page!=last_page){
                 page++;
                 initData();
             }else{
                 mListvYanzhengThreemouont.postDelayed(new Runnable() {
                     @Override
                     public void run() {
                         mListvYanzhengThreemouont.onRefreshComplete();
                     }
                 },1000);
             }

            }
        });
        mYanZhengAdapter = new YanZhengAdapter();
        mListvYanzhengThreemouont.setAdapter(mYanZhengAdapter);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate = new Date(System.currentTimeMillis());
        String time = format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvYanzhengThreemouont.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mYanZhengAdapter.notifyDataSetChanged();


    }
    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(getContext());
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }
    private void initData(){
        String  userid = (String) SharePUtile.get(getContext(), "user_id", "");
        String  user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        OkGo.post(HttpAPI.THREEMOUNT)
                .tag(this)
                .connTimeOut(10000)
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("shopid", shopid)
                .params("cmd", "5")
                .params("page",page+"")
                .execute(new StringCallback() {
                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Gson gson = new Gson();
                                Log.e("近3个月的", s);
                                PuTongListBean puTongListBean = gson.fromJson(s, PuTongListBean.class);
                                puTongListBean.getData().getLast_page();
                                mDateBeen = puTongListBean.getData().getData();
                                mALLBeen.addAll(mDateBeen);
                                mYanZhengAdapter.setStrings(mALLBeen);
                                mYanZhengAdapter.notifyDataSetChanged();
                                mListvYanzhengThreemouont.onRefreshComplete();
                            }else  if(status.equals("0")){
                                Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                mListvYanzhengThreemouont.onRefreshComplete();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                }
                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getContext(),"链接超时!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);
                        builder.show();
                    }
                });
    }
    private void initView(View rootView) {
  mListvYanzhengThreemouont = (PullToRefreshListView) rootView.findViewById(R.id.listv_yanzheng_threemouont);
    }
}
