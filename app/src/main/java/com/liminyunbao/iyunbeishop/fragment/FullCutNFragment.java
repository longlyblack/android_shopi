package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.CardPullAlladapterHang;
import com.liminyunbao.iyunbeishop.bean.FullWhatBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/6/30/030.
 */

public class FullCutNFragment extends BaseLazyFragment {
    protected View rootView;
    protected PullToRefreshListView listvFullCutHang;
    private CardPullAlladapterHang mAdapter;
    private  List<FullWhatBean.DataBean> mALLdata;

    public static FullCutNFragment newInstance(String type) {

        Bundle args = new Bundle();

        args.putString("discount_type",type);
        FullCutNFragment fragment = new FullCutNFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_full_cut_hang;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        listvFullCutHang = (PullToRefreshListView) view.findViewById(R.id.listv_full_cut_hang);
    }

    @Override
    protected void onFirstUserVisible() {
        Bundle arguments = getArguments();
        String discount_type = arguments.getString("discount_type");
        mALLdata=new ArrayList<>();
        pullfresh(discount_type);
        gethttp(discount_type);
    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    private void gethttp(String type){

        OkGo.post(HttpAPI.CARDLIST)
                .params("shopid", UserUtils.Shopid(getContext()))
                .params("user_id", UserUtils.Userid(getContext()))
                .params("user_token", UserUtils.UserToken(getContext()))
                .params("discount_type",type)
                .params("closed","1")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("满减N",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Gson gson =new Gson();
                                FullWhatBean fullWhatBean = gson.fromJson(s, FullWhatBean.class);
                                List<FullWhatBean.DataBean> data = fullWhatBean.getData();
                                mALLdata.addAll(data);
                                mAdapter.setmData(mALLdata);
                                listvFullCutHang.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();
                                listvFullCutHang.onRefreshComplete();

                            }else if(status.equals("0")){

                                ToastUtil.showShort(getContext(),"暂无数据");

                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


    }

    private void pullfresh(final String type){

        listvFullCutHang.setMode(PullToRefreshBase.Mode.BOTH);
        listvFullCutHang.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

              /*  ALLdataBean.clear();
                page=1;
                initHttp();*/
                gethttp(type);
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
/*
                if(page!=last_page){
                    page++;
                    initHttp();
                }else{
                    mListvSaleoverFram.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvSaleoverFram.onRefreshComplete();
                        }
                    }, 1000);
                }*/

                listvFullCutHang.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listvFullCutHang.onRefreshComplete();
                    }
                },1000);

            }
        });

        mAdapter=new CardPullAlladapterHang();
        listvFullCutHang.setAdapter(mAdapter);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = listvFullCutHang.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mAdapter.notifyDataSetChanged();


    }





}
