package com.liminyunbao.iyunbeishop.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.liminyunbao.iyunbeishop.adapter.AddnewGrdiViewCommodityAdapter;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/25.
 */

public class AddnewCommodityFragment extends Fragment {
    protected View rootView;
    public static final String TAG = "MyFragment";
    protected GridView mGvAddnewAdapter;
    protected FrameLayout mFlChildAddnewFram;
    protected LinearLayout mRlAddnewCommoditAdapter;
    private String leimu[] = {"精选", "巧克力", "曲奇", "糕点", "膨化", "饼干"};
    private List<String> mStrings;
    private  BeaseAddNewFragment beaseAddNewFragment;
    private AddnewGrdiViewCommodityAdapter mGrdiViewCommodityAdapter;
    private int selectorPosition;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_addnewa_dapter, null, false);
        initView(rootView);
        Bundle arguments = getArguments();
        //  mTextAddd.setText(arguments.getString(TAG));
        initData();


        mGvAddnewAdapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mGrdiViewCommodityAdapter.changeState(position);
                selectorPosition=position;
                for(int i=0;i<leimu.length;i++){
                    FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                    beaseAddNewFragment = new BeaseAddNewFragment();
                    fragmentTransaction.replace(R.id.fl_child_addnew_fram, beaseAddNewFragment);
                    fragmentTransaction.commit();
                }

            }
        });
        addFragment();
        return rootView;
    }
    private void addFragment(){
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        beaseAddNewFragment = new BeaseAddNewFragment();
        fragmentTransaction.replace(R.id.fl_child_addnew_fram, beaseAddNewFragment);
        fragmentTransaction.commit();

    }

    private void initData() {
        mStrings = new ArrayList<>();
        mGrdiViewCommodityAdapter = new AddnewGrdiViewCommodityAdapter();

        for (int i = 0; i < leimu.length; i++) {
            mStrings.add(leimu[i]);
        }
        mGrdiViewCommodityAdapter.setStrings(mStrings);
        mGvAddnewAdapter.setAdapter(mGrdiViewCommodityAdapter);
        mGrdiViewCommodityAdapter.notifyDataSetChanged();


    }

    private void initView(View rootView) {
        mGvAddnewAdapter = (GridView) rootView.findViewById(R.id.gv_addnew_adapter);
        mFlChildAddnewFram = (FrameLayout) rootView.findViewById(R.id.fl_child_addnew_fram);
        mRlAddnewCommoditAdapter = (LinearLayout) rootView.findViewById(R.id.rl_addnewCommodit_adapter);

    }
}
