package com.liminyunbao.iyunbeishop.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.activity.AddNewShopActivity;
import com.liminyunbao.iyunbeishop.activity.SaverAddnewShopActivity;
import com.liminyunbao.iyunbeishop.adapter.SaleOverAndWareHAdapter;
import com.liminyunbao.iyunbeishop.bean.SaleBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.custom.PinchImageView;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/3/24.
 * 已售罄
 */

public class SoldOutFragment extends BaseLazyFragment{
    protected View rootView;
    protected PullToRefreshListView mListvSaleoverFram;
    private SaleBean.MassageBean.GoodsList dataBe;
    private int page=1;
    private int last_page=0;
    private SaleOverAndWareHAdapter mAdapter;
    private  List<SaleBean.MassageBean.GoodsList> dataBean;
    private  List<SaleBean.MassageBean.GoodsList> ALLdataBean=new ArrayList<>();
    private  List<SaleBean.MassageBean.GoodsList> pandataBean=new ArrayList<>();
    private Dialog mDialog;
    private PinchImageView bigimg;
    private int ADDCORE=4;
    private KyLoadingBuilder builder;
    private String leixing ;
    public static SoldOutFragment newInstance(String lei) {

        Bundle args = new Bundle();
        args.putString("l",lei);
        SoldOutFragment fragment = new SoldOutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_soldout_fram;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        mListvSaleoverFram = (PullToRefreshListView) view.findViewById(R.id.listv_saleover_fram);
    }

    @Override
    protected void onFirstUserVisible() {

        Bundle arguments = getArguments();
        leixing= arguments.getString("l");
        initHttp();
        pullfresh();
        mListvSaleoverFram.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dataBe=ALLdataBean.get(position-1);
                if(leixing.equals("1")){
                    Intent intent=new Intent(getContext(),AddNewShopActivity.class);
                    intent.putExtra("type","salout");
                    intent.putExtra("goodsid",dataBe.getBase_goods_id()+"");
                    startActivityForResult(intent,ADDCORE);
                }else if(leixing.equals("2")){
                    Intent intent=new Intent(getContext(),SaverAddnewShopActivity.class);
                    intent.putExtra("type","salout");
                    intent.putExtra("goodsid",dataBe.getBase_goods_id()+"");
                    startActivityForResult(intent,ADDCORE);
                }

  /* Bundle bundle = new Bundle();
                bundle.putSerializable("data", dataBe);
                intent.putExtras(bundle);
               */
            }
        });
        mAdapter.setOnSaleoutWarehListener(

                new SaleOverAndWareHAdapter.onSaleoutWarehListener() {
            @Override
            public void onSaleoutWarehListener(ImageView img, final String url) {
                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showphoto(url);
                    }
                });
            }
        });
    }
    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(getContext());
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭

    }
    /**
     * 显示大图
     * @param img
     */
    private void showphoto(String img){
        mDialog=new Dialog(getActivity(),R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(0,0,0,0);
        params.width =WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate2 = LayoutInflater.from(getActivity()).inflate(R.layout.item_dialog_imageview, null);
        bigimg= (PinchImageView) inflate2.findViewById(R.id.big_img);
        Glide.with(getContext()).load(img).into(bigimg);
        mDialog.setContentView(inflate2);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==ADDCORE){
            String refresh = data.getStringExtra("refresh");
            if(refresh.equals("1")){
                ALLdataBean.clear();
                page=1;
                initHttp();
            }
        }
    }

    @Override
    protected void onUserVisible() {
        ALLdataBean.clear();
        page=1;
            initHttp();
    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    private void pullfresh(){
        mListvSaleoverFram.setMode(PullToRefreshBase.Mode.BOTH);
        mListvSaleoverFram.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

                ALLdataBean.clear();
                page=1;
                initHttp();
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

                if(page!=last_page){
                    page++;
                    initHttp();
                }else{
                    mListvSaleoverFram.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvSaleoverFram.onRefreshComplete();
                        }
                    }, 1000);
                }

            }
        });

        mAdapter=new SaleOverAndWareHAdapter(getContext());
        mListvSaleoverFram.setAdapter(mAdapter);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvSaleoverFram.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mAdapter.notifyDataSetChanged();
    }
    private void initHttp(){
        String  user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        String  userid = (String) SharePUtile.get(getContext(), "user_id", "");

        OkGo.post(HttpAPI.SHANGPINLIST)
                .connTimeOut(10000)
                .params("cmd","2")
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("shopid",shopid)
                .params("page",page+"")
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);

                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("手腕",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                JSONObject data = jsonObject.getJSONObject("data");
                                last_page=data.getInt("last_page");
                                Gson gson=new Gson();
                                SaleBean saleBean = gson.fromJson(s, SaleBean.class);
                                dataBean=saleBean.getData().getData();
                                ALLdataBean.addAll(dataBean);
                                mAdapter.setList(ALLdataBean);
                                mAdapter.notifyDataSetChanged();
                                mListvSaleoverFram.onRefreshComplete();
                            }else if(status.equals("0")){
                                Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                mListvSaleoverFram.onRefreshComplete();
                                mAdapter.notifyDataSetChanged();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getContext(),"请求超时！",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);

                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("我是出售罄","哈哈");
    }
}
