package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.adapter.NoReplyAdapter;
import com.liminyunbao.iyunbeishop.bean.EvauationBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.interFace.ReplayHuiF;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/3/23.
 */

public class NoReplyFragment extends BaseLazyFragment {

    protected View rootView;
    protected PullToRefreshListView mListvAllevauation;
    private int page=1;
    private int last_page=0;
    private NoReplyAdapter mAdapter;
    private String userid;
    private List<EvauationBean.DataBeanX.DataBean>mDataBeen;
    private List<EvauationBean.DataBeanX.DataBean> AllData=new ArrayList<>();

    private KyLoadingBuilder builder;
    public static NoReplyFragment newInstance() {

        Bundle args = new Bundle();
        NoReplyFragment fragment = new NoReplyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_noreplay_evaluation_fram;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        mListvAllevauation = (PullToRefreshListView) view.findViewById(R.id.listv_allevauation);
    }

    @Override
    protected void onFirstUserVisible() {
       userid = (String) SharePUtile.get(getContext(), "user_id", "");
        Aimoi();
        pullreifish();
        initdata();
    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(getContext());
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭

    }
    private void pullreifish(){
        mListvAllevauation.setMode(PullToRefreshBase.Mode.BOTH);
        mListvAllevauation.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                AllData.clear();
                Aimoi();
                page=1;
                initdata();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                Aimoi();
                if(page!=last_page){
                    page++;
                    initdata();
                }else{
                    mListvAllevauation.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvAllevauation.onRefreshComplete();
                        }
                    }, 1000);
                }

            }

        });
        mAdapter = new NoReplyAdapter(getContext(), new ReplayHuiF() {
            @Override
            public void Replahuif(final RelativeLayout huifu, final RelativeLayout saver, final Integer porsion) {
                Button rep= (Button) huifu.findViewById(R.id.btn_nop_huifu);
                Button save= (Button) saver.findViewById(R.id.btn_nop_save_anser);
                final EditText content= (EditText) saver.findViewById(R.id.tv_nop_insert_text);
                rep.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        huifu.setVisibility(View.GONE);
                        saver.setVisibility(View.VISIBLE);
                    }
                });
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
                        String  user_token = (String) SharePUtile.get(getContext(), "user_token", "");
                        int order_id = AllData.get(porsion).getOrder_id();
                        OkGo.post(HttpAPI.RECEIVE)
                                .connTimeOut(10000)
                                .params("user_id",userid)
                                .params("shopid",shopid)
                                .params("user_token",user_token)
                                .params("order_id",order_id)
                                .params("content",content.getText().toString())
                                .execute(new StringCallback() {
                                    @Override
                                    public void onSuccess(String s, Call call, Response response) {
                                        try {
                                            JSONObject jsong=new JSONObject(s);
                                            String status = jsong.getString("status");
                                            if(status.equals("1")){
                                                initdata();
                                                content.setText(null);
                                            }else if (status.equals("0")){
                                                Toast.makeText(getContext(),"回复失败！",Toast.LENGTH_SHORT).show();
                                            }else if(status.equals("2")){
                                                TokenUtils.TackUserToken(getActivity());
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    @Override
                                    public void onError(Call call, Response response, Exception e) {
                                        super.onError(call, response, e);
                                        Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                                    }
                                });


                    }
                });
            }
        });
        mListvAllevauation.setAdapter(mAdapter);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvAllevauation.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mAdapter.notifyDataSetChanged();

    }

    private void  initdata(){
        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        String  user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        OkGo.post(HttpAPI.NOTREVIEW)
                .connTimeOut(10000)
                .params("user_id",userid)
                .params("shopid",shopid)
                .params("user_token",user_token)
                .params("page",page+"")
                    .execute(new StringCallback() {
                        @Override
                        public void onBefore(BaseRequest request) {
                            super.onBefore(request);
                            builder.show();
                        }

                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                String status = jsonObject.getString("status");
                                if(status.equals("1")){
                                    Gson  gson=new Gson();
                                    EvauationBean evauationBean = gson.fromJson(s, EvauationBean.class);
                                    mDataBeen=evauationBean.getData().getData();
                                    AllData.addAll(mDataBeen);
                                    mAdapter.setDataBeen(AllData);
                                    mAdapter.notifyDataSetChanged();
                                    mListvAllevauation.onRefreshComplete();
                                }else if(status.equals("0")){
                                    Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                    mListvAllevauation.onRefreshComplete();
                                }else if(status.equals("2")){
                                    TokenUtils.TackUserToken(getActivity());
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Call call, Response response, Exception e) {
                            super.onError(call, response, e);
                            Toast.makeText(getContext(),"请求超时!",Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onAfter(String s, Exception e) {
                            super.onAfter(s, e);
                            builder.dismiss();
                        }
                    });



    }

}
