package com.liminyunbao.iyunbeishop.fragment;

import android.app.Dialog;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.activity.CommodityActivity;
import com.liminyunbao.iyunbeishop.activity.EvaluationActivity;
import com.liminyunbao.iyunbeishop.activity.MarkeManagerActivity;
import com.liminyunbao.iyunbeishop.activity.SettlementActivity;
import com.liminyunbao.iyunbeishop.activity.StatisticsActivity;
import com.liminyunbao.iyunbeishop.activity.StoreActivity;
import com.liminyunbao.iyunbeishop.adapter.HompagerGridAdapter;
import com.liminyunbao.iyunbeishop.custom.AutoHorizontalScrollTextView;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.IntentUtils;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/3/15.
 * 首页
 */

public class HomePageFragment extends Fragment {
    protected View rootView;
    // protected EditText mEdYanzhengHomepageFram;
    protected GridView mGdMenuHomepageFram;
    protected AutoHorizontalScrollTextView mTvMessageHompageFram;
    protected TextView mTvShopnameHomepageFram;
    protected TextView mTvIncomeHomepage;
    protected TextView mTvOrdernumbHomepage;
    protected TextView mTvShopnumHomepage;
    protected TextView mTvSeenumHomepage;
    private CharSequence temp;
    private String imgs[] = {"商品管理", "营销管理", "评价管理", "资金管理", "营收统计", "店铺管理"};
    private Dialog mDialog;
    private EditText mEditText_pickup;
    private Button mButton_pickop;
    private View mImage;
    private View mClose;
    private Button mSure;
    private Button mNext;
    private TextView mPass;
    private ImageView mWhatPass;
    private List<String> mList;
    private HompagerGridAdapter mHompagerGridAdapter;
    public static HomePageFragment newInstance(String shopname) {

        Bundle args = new Bundle();
        args.putString("shopname", shopname);
        HomePageFragment fragment = new HomePageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static HomePageFragment newInstance() {
        
        Bundle args = new Bundle();
        HomePageFragment fragment = new HomePageFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_fragment_homepage, null, false);
        initView(rootView);
        initData();
        todayHttp();
       Bundle arguments = getArguments();
        mTvShopnameHomepageFram.setText(arguments.getString("shopname"));
      /*  mTvMessageHompageFram.setText("能死的我的脚下是上天给予他们的恩赐家具哈哈");
        mTvMessageHompageFram.setVisibility(View.GONE);*/
        return rootView;
    }

    private void todayHttp(){
        String user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        String userid = (String) SharePUtile.get(getContext(), "user_id", "");
        OkGo.post(HttpAPI.TODAYINFOR)
                .params("user_token",user_token)
                .params("shopid",shopid)
                .params("user_id",userid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status=  jsonObject.getString("status");
                            JSONObject data=jsonObject.getJSONObject("data");
                            if(status.equals("1")){
                                String goodtotal=data.getString("goodtotal");
                                String ordertotal=data.getString("ordertotal");
                                String pricetotal=data.getString("pricetotal");
                                String liulantotal=data.getString("liulantotal");
                                mTvIncomeHomepage.setText(pricetotal);
                                mTvOrdernumbHomepage.setText(ordertotal);
                                mTvShopnumHomepage.setText(goodtotal);
                                mTvSeenumHomepage.setText(liulantotal);
                        }else if(status.equals("0")){
                                Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
    private void initData() {
        mList = new ArrayList<>();
        mHompagerGridAdapter = new HompagerGridAdapter();
        for (int i = 0; i < imgs.length; i++) {
            mList.add(imgs[i]);
        }
        mHompagerGridAdapter.setList(mList);
        mGdMenuHomepageFram.setAdapter(mHompagerGridAdapter);
        mHompagerGridAdapter.notifyDataSetChanged();
        mGdMenuHomepageFram.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              if(position==0){
                  //IntentUtils.goTo(parent.getContext(), CommodityActivity.class);

                  Intent intent=new Intent(getContext(),CommodityActivity.class);
                  intent.putExtra("lei","1");
                  startActivity(intent);
              } else if(position==1){
                   // ShowDialog();
                 // IntentUtils.goTo(getActivity(), SavesOrderActivity.class);

                  Intent intent=new Intent(getContext(), MarkeManagerActivity.class);
                  intent.putExtra("WTUIJ","1");
                  startActivity(intent);

                }else if(position==2){
                    IntentUtils.goTo(parent.getContext(), EvaluationActivity.class);
                }else if(position==3){
                  IntentUtils.goTo(parent.getContext(), SettlementActivity.class);
              }else if(position==4){
                  IntentUtils.goTo(parent.getContext(), StatisticsActivity.class);
              }else if(position==5){
                  IntentUtils.goTo(parent.getContext(), StoreActivity.class);
              }
            }
        });
    }

    private void ShowDialog(){
        mDialog=new Dialog(getActivity(),R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(30,0,30,0);
        params.width =WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate1 = LayoutInflater.from(getActivity()).inflate(R.layout.item_pickup_dailog, null);
        mEditText_pickup= (EditText) inflate1.findViewById(R.id.edit_pickup_dialog);
        mButton_pickop= (Button) inflate1.findViewById(R.id.btn_sure_pick_dialog);
        mImage=inflate1.findViewById(R.id.img_close_pickup_dailog);
        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mButton_pickop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hexiao();
                mDialog.dismiss();
            }
        });
        mDialog.setContentView(inflate1);

    }

    private void hexiao(){
        String user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        String  userid = (String) SharePUtile.get(getContext(), "user_id", "");
        OkGo.post(HttpAPI.XIAOFEIYZ)
                .params("user_token",user_token)
                .params("shopid",shopid)
                .params("user_id",userid)
                .params("code",mEditText_pickup.getText().toString())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                String data =jsonObject.getString("data");
                                String msg=jsonObject.getString("msg");
                              showPassOrdis(data,msg);
                              Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
                            }else if(status.equals("0")){
                                String data =jsonObject.getString("data");
                                String msg=jsonObject.getString("msg");
                                showPassOrdis(data,msg);
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void showPassOrdis(String data,String mag){
        mDialog=new Dialog(getActivity(),R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(30,0,30,0);
        params.width =WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate2 = LayoutInflater.from(getActivity()).inflate(R.layout.item_pass_dialog, null);
        mClose=inflate2.findViewById(R.id.img_closer_pass_daiog);
        mSure= (Button) inflate2.findViewById(R.id.btn_sure_pass_ro_dispass);
        mNext= (Button) inflate2.findViewById(R.id.btn_next_pass_or_dispass);
        mPass= (TextView) inflate2.findViewById(R.id.tv_pass_or_dispass);
        mWhatPass= (ImageView) inflate2.findViewById(R.id.img_pass_or_dispass);

        if(data.equals("1")){
            mPass.setText(mag);
            mPass.setTextSize(15);
            Bitmap mbit= BitmapFactory.decodeResource(getResources(),R.mipmap.pass);
            mWhatPass.setImageBitmap(mbit);
            mNext.setText(R.string.next);
        }else  if(data.equals("2")){
            mPass.setText(mag);
            mPass.setTextSize(15);
            Bitmap mbit= BitmapFactory.decodeResource(getResources(),R.mipmap.dispass);
            mWhatPass.setImageBitmap(mbit);
            mNext.setText(R.string.again);
        }else  if(data.equals("3")){
            mPass.setText(mag);
            mPass.setTextSize(15);
            Bitmap mbit= BitmapFactory.decodeResource(getResources(),R.mipmap.dispass);
            mWhatPass.setImageBitmap(mbit);
            mNext.setText(R.string.again);
        }else  if(data.equals("4")){
            mPass.setText(mag);
            mPass.setTextSize(15);
            Bitmap mbit= BitmapFactory.decodeResource(getResources(),R.mipmap.dispass);
            mWhatPass.setImageBitmap(mbit);
            mNext.setText(R.string.again);
        }

        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                ShowDialog();
            }
        });
        mDialog.setContentView(inflate2);
    }

    private void EdtextChange() {
        mEditText_pickup.setCursorVisible(false);
        mButton_pickop.setHintTextColor(getResources().getColor(R.color.colorHlin));
        mEditText_pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        mEditText_pickup.setCursorVisible(true);
            }
        });
        mEditText_pickup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                temp = s;
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
               /* if (temp.length() == 8) {
                    //输入长度是8就可以直接请求网络
                }*/
            }
        });
    }
    private void initView(View rootView) {
        // mEdYanzhengHomepageFram = (EditText) rootView.findViewById(R.id.ed_yanzheng_homepage_fram);
        mGdMenuHomepageFram = (GridView) rootView.findViewById(R.id.gd_menu_homepage_fram);
        mTvMessageHompageFram = (AutoHorizontalScrollTextView) rootView.findViewById(R.id.tv_message_hompage_fram);
        mTvShopnameHomepageFram = (TextView) rootView.findViewById(R.id.tv_shopname_homepage_fram);
        mTvIncomeHomepage = (TextView) rootView.findViewById(R.id.tv_income_homepage);
        mTvOrdernumbHomepage = (TextView) rootView.findViewById(R.id.tv_ordernumb_homepage);
        mTvShopnumHomepage = (TextView) rootView.findViewById(R.id.tv_shopnum_homepage);
        mTvSeenumHomepage = (TextView) rootView.findViewById(R.id.tv_seenum_homepage);
    }
    @Override
    public void onResume() {
        super.onResume();

    }
}
