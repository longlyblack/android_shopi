package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.adapter.EvauationAdapter;
import com.liminyunbao.iyunbeishop.bean.EvauationBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.interFace.AllRePlay;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Administrator on 2017/3/23.
 */

public class BadEvaluationFragment extends BaseLazyFragment {
    protected View rootView;
    protected PullToRefreshListView mListvAllevauation;
    private int page=1;
    private  int last_page=0;
    private List<EvauationBean.DataBeanX.DataBean> mDataBeen;
    private List<EvauationBean.DataBeanX.DataBean> AllDataBeen=new ArrayList<>();
    private EvauationAdapter mAdapter;
    private KyLoadingBuilder builder;
    private String userid;
    public static BadEvaluationFragment newInstance() {

        Bundle args = new Bundle();

        BadEvaluationFragment fragment = new BadEvaluationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_bad_evaluation_fram;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        mListvAllevauation = (PullToRefreshListView) view.findViewById(R.id.listv_allevauation);

    }

    @Override
    protected void onFirstUserVisible() {
         userid = (String) SharePUtile.get(getContext(), "user_id", "");
        Aimoi();
        initdata();
        pullfresh();

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    private void pullfresh(){
        mListvAllevauation.setMode(PullToRefreshBase.Mode.BOTH);
        mListvAllevauation.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                Aimoi();
                AllDataBeen.clear();
                page=1;
                initdata();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                Aimoi();
                if(page!=last_page){
                    page++;
                    initdata();
                }else{
                    mListvAllevauation.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvAllevauation.onRefreshComplete();
                        }
                    }, 1000);
                }

            }

        });
        mAdapter = new EvauationAdapter(getContext(), new AllRePlay() {
            @Override
            public void allRepla(final RelativeLayout huifu, final RelativeLayout saver, final LinearLayout huiRong, String replay, final Integer porsion) {
                Button hui= (Button) huifu.findViewById(R.id.btn_huifu);
                Button save= (Button) saver.findViewById(R.id.btn_save_anser);
                final EditText content= (EditText) saver.findViewById(R.id.tv_insert_text);

                if(replay==null){
                    huifu.setVisibility(View.VISIBLE);
                    saver.setVisibility(View.GONE);
                    huiRong.setVisibility(View.GONE);
                }else if(replay!=null){
                    huifu.setVisibility(View.GONE);
                    saver.setVisibility(View.GONE);
                    huiRong.setVisibility(View.VISIBLE);
                }
                hui.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        huifu.setVisibility(View.GONE);
                        saver.setVisibility(View.VISIBLE);
                        huiRong.setVisibility(View.GONE);
                    }
                });
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
                        String user_token = (String) SharePUtile.get(getContext(), "user_token", "");
                        int order_id = AllDataBeen.get(porsion).getOrder_id();
                        OkGo.post(HttpAPI.RECEIVE)
                                .connTimeOut(10000)
                                .params("user_id",userid)
                                .params("user_token",user_token)
                                .params("shopid",shopid)
                                .params("order_id",order_id+"")
                                .params("content",content.getText().toString())
                                .execute(new StringCallback() {
                                    @Override
                                    public void onSuccess(String s, Call call, Response response) {
                                        try {
                                            JSONObject jsong=new JSONObject(s);
                                            String status = jsong.getString("status");
                                            if(status.equals("1")){
                                                JSONObject data = jsong.getJSONObject("data");
                                                String reply = data.getString("reply");
                                                long reply_time = data.getLong("reply_time");
                                                huifu.setVisibility(View.GONE);
                                                saver.setVisibility(View.GONE);
                                                huiRong.setVisibility(View.VISIBLE);
                                                AllDataBeen.get(porsion).setReply(reply);
                                                AllDataBeen.get(porsion).setReply_time((int) reply_time);
                                                content.setText("");
                                                mAdapter.notifyDataSetChanged();
                                            }else if (status.equals("0")){
                                                Toast.makeText(getContext(),"回复失败！",Toast.LENGTH_SHORT).show();
                                            }else if(status.equals("2")){
                                                TokenUtils.TackUserToken(getActivity());
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    @Override
                                    public void onError(Call call, Response response, Exception e) {
                                        super.onError(call, response, e);
                                        Toast.makeText(getContext(),"请求超时！",Toast.LENGTH_SHORT).show();
                                    }
                                });


                        InputMethodManager imm = (InputMethodManager)getContext(). getSystemService(INPUT_METHOD_SERVICE); imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                });
            }
        });
        mListvAllevauation.setAdapter(mAdapter);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvAllevauation.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mAdapter.notifyDataSetChanged();


    }
    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(getContext());
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭

    }
    private void initdata(){
        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        String  user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        OkGo.post(HttpAPI.BADREVIEW)
                .connTimeOut(10000)
                .params("user_id",userid)
                .params("shopid",shopid)
                .params("user_token",user_token)
                .params("page",page+"")
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);
                        builder.show();
                    }
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("这是差评",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                last_page=jsonObject.getJSONObject("data").getInt("last_page");
                                Gson gson=new Gson();
                                EvauationBean evauationBean = gson.fromJson(s, EvauationBean.class);
                                mDataBeen=evauationBean.getData().getData();
                                AllDataBeen.addAll(mDataBeen);
                                mAdapter.setMhuifu(AllDataBeen);
                                mAdapter.notifyDataSetChanged();
                                mListvAllevauation.onRefreshComplete();
                            }else if(status.equals("0")){
                                Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                mListvAllevauation.onRefreshComplete();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getContext(),"请求超时!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);
                        builder.dismiss();
                    }
                });


    }


}
