package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.liminyunbao.iyunbeishop.adapter.RefundOrderListAdapter;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/20.
 * 订单列表  退款
 */

public class RefundFragment extends Fragment {
    protected View rootView;
    protected ListView mListConsumptionFram;
    private List<String > mList;
    private String hao[]={"1234560","1234570","1234580","55524","444551"};
    private RefundOrderListAdapter mRefundOrderListAdapter;
    public static RefundFragment newInstance() {

        Bundle args = new Bundle();

        RefundFragment fragment = new RefundFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_consumption_fram, null, false);
        initView(rootView);
        initData();
        mListConsumptionFram.setDividerHeight(0);
        return rootView;
    }
    private  void initData(){
        mList=new ArrayList<>();
        mRefundOrderListAdapter=new RefundOrderListAdapter();
        for(int i=0;i<hao.length;i++){
            mList.add(hao[i]);

        }
        mRefundOrderListAdapter.setList(mList);
        mListConsumptionFram.setAdapter(mRefundOrderListAdapter);
        mRefundOrderListAdapter.notifyDataSetChanged();



    }

    private void initView(View rootView) {
        mListConsumptionFram = (ListView) rootView.findViewById(R.id.list_consumption_fram);
    }
}
