package com.liminyunbao.iyunbeishop.fragment;



import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.adapter.OrderListNewPagerAapter;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/30.
 * 新订单列表
 */

public class OrderListNewFragment extends BaseLazyFragment {

    protected View rootView;
    protected SlidingTabLayout mSTabOrderlistNewFram;
    protected ViewPager mVpOrdlisitNewFram;
    private List<Fragment> mFragments;

    private OrderListNewPagerAapter mPagerAapter;


    public static OrderListNewFragment newInstance() {

        Bundle args = new Bundle();

        OrderListNewFragment fragment = new OrderListNewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_orderlist_new_fram;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        mSTabOrderlistNewFram = (SlidingTabLayout) view.findViewById(R.id.sTab_orderlist_new_fram);
        mVpOrdlisitNewFram = (ViewPager) view.findViewById(R.id.vp_ordlisit_new_fram);
    }

    @Override
    protected void onFirstUserVisible() {
        initFragment();
    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    private void initFragment(){
        mFragments=new ArrayList<>();
        mPagerAapter=new OrderListNewPagerAapter(getActivity().getSupportFragmentManager());
        mFragments.add(WiteSenFragment.newInstance());
        mFragments.add(UnSuccessFragment.newInstance());
        mFragments.add(SuccessFragment.newInstance());
        mPagerAapter.setFragments(mFragments);
        mVpOrdlisitNewFram.setAdapter(mPagerAapter);
        mSTabOrderlistNewFram.setViewPager(mVpOrdlisitNewFram);
        mVpOrdlisitNewFram.setOffscreenPageLimit(3);

    }
}
