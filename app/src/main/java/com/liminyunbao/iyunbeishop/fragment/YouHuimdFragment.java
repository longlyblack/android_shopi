package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.adapter.YouHuiAdapter;
import com.liminyunbao.iyunbeishop.bean.YouHuiMaiDianBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/4/6 0006.
 */

/**
 * 资金流水——优惠买单
 */

public class YouHuimdFragment extends BaseLazyFragment {


    protected View rootView;
    protected PullToRefreshListView mListvYouhuimd;
    private List<YouHuiMaiDianBean.DataBeanX.DataBean> mDataBeen;
    private List<YouHuiMaiDianBean.DataBeanX.DataBean> mAllBean=new ArrayList<>();
    private YouHuiAdapter mYouHuiAdapter;
    private KyLoadingBuilder builder;

    private String userid;
    private int page=1;
    private int last_page=0;

    public static YouHuimdFragment newInstance() {

        Bundle args = new Bundle();

        YouHuimdFragment fragment = new YouHuimdFragment();
        fragment.setArguments(args);
        return fragment;
    }
    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(getContext());
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }
    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_youhuils_fram;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        mListvYouhuimd = (PullToRefreshListView) view.findViewById(R.id.listv_youhuimd);
    }

    @Override
    protected void onFirstUserVisible() {
        userid = (String) SharePUtile.get(getContext(), "user_id", "");
        Aimoi();
        initdata();
        pullfresh();

    }
      private void pullfresh(){
          mListvYouhuimd.setMode(PullToRefreshBase.Mode.BOTH);
          mListvYouhuimd.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                //开始下拉  我们做Http请求
                Aimoi();
                mAllBean.clear();
                page=1;
                initdata();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

                Aimoi();
                if(page!=last_page){
                    page++;
                    initdata();
                }else{
                    mListvYouhuimd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mListvYouhuimd.onRefreshComplete();
                        }
                    },1000);
                }


            }
        });
          mYouHuiAdapter=new YouHuiAdapter();
          mListvYouhuimd.setAdapter(mYouHuiAdapter);
         SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         Date curDate    =   new Date(System.currentTimeMillis());
         String time=format.format(curDate);
         ILoadingLayout loadingLayoutProxy = mListvYouhuimd.getLoadingLayoutProxy();
         loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
         loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
         loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
         loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
         loadingLayoutProxy.setReleaseLabel("松手开始刷新");
          mYouHuiAdapter.notifyDataSetChanged();

    }
    private void initdata(){
        String  user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        mDataBeen=new ArrayList<>();
            OkGo.post(HttpAPI.YOUHUI)
                    .params("user_id",userid)
                    .params("user_token",user_token)
                    .params("shopid", shopid)
                    .params("page", page + "")
                    .execute(new StringCallback() {
                        @Override
                        public void onBefore(BaseRequest request) {
                            super.onBefore(request);
                            builder.show();
                        }

                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                String status = jsonObject.getString("status");
                                if(status.equals("1")){
                                    Gson gson = new Gson();
                                    YouHuiMaiDianBean youHuiMaiDian = gson.fromJson(s, YouHuiMaiDianBean.class);
                                    last_page = youHuiMaiDian.getData().getLast_page();
                                    mDataBeen = youHuiMaiDian.getData().getData();
                                    mAllBean.addAll(mDataBeen);
                                    mYouHuiAdapter.setStrings(mAllBean);
                                    mYouHuiAdapter.notifyDataSetChanged();
                                    mListvYouhuimd.onRefreshComplete();
                                }else if(status.equals("0")){
                                    Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                    mListvYouhuimd.onRefreshComplete();
                                }else if(status.equals("2")){
                                    TokenUtils.TackUserToken(getActivity());
                                }

                            } catch (JSONException e) {
                            }
                        }
                        @Override
                        public void onAfter(String s, Exception e) {
                            super.onAfter(s, e);
                            builder.dismiss();
                        }
                    });
    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

}
