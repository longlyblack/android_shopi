package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.liminyunbao.iyunbeishop.adapter.ListOrderAdapter;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/17.
 * 食堂新订单
 */

public class NewOrderFragment extends Fragment {

    protected View rootView;
    protected ListView mListvNewOrder;
    private String bian[]={"3号","4号","5号",};
    private List<String> mStrings;
    private ListOrderAdapter mListOrderAdapter;

    public static NewOrderFragment newInstance() {

        Bundle args = new Bundle();

        NewOrderFragment fragment = new NewOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.item_new_order_fram, null, false);
        initView(rootView);
        initData();
        mListvNewOrder.setDividerHeight(0);
        return rootView;
    }

    private void initData(){
        mStrings=new ArrayList<>();
        mListOrderAdapter=new ListOrderAdapter();
        for(int i=0;i<bian.length;i++){
            mStrings.add(bian[i]);
        }
        mListOrderAdapter.setStrings(mStrings);

        mListvNewOrder.setAdapter(mListOrderAdapter);

        mListOrderAdapter.notifyDataSetChanged();

    }
    private void initView(View rootView) {
        mListvNewOrder = (ListView) rootView.findViewById(R.id.listv_new_order);
    }
}
