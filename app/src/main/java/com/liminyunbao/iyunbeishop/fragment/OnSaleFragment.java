package com.liminyunbao.iyunbeishop.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.activity.AddNewShopActivity;
import com.liminyunbao.iyunbeishop.activity.SaverAddnewShopActivity;
import com.liminyunbao.iyunbeishop.adapter.OnSaleAdapter;
import com.liminyunbao.iyunbeishop.bean.SaleBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.custom.PinchImageView;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.interFace.BackImg;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/3/24.
 * 出售中
 */

public class OnSaleFragment extends BaseLazyFragment implements View.OnClickListener {
    protected View rootView;
    protected PullToRefreshListView mListvOnsaleFram;
    protected ImageView mImgCloseEditDailog;
    protected TextView mTvEditShopname;
    protected EditText mEdiShopprice;
    protected EditText mEdiShopStock;
    protected Button mBtnCancel;
    protected Button mBtnPreservation;
    protected Button mBtnCommodityAddShop;
    private PopupWindow mPopupWindow;
    private   List<SaleBean.MassageBean.GoodsList> mList;
    private   List<SaleBean.MassageBean.GoodsList> mAllList=new ArrayList<>();
    private OnSaleAdapter mOnSaleAdapter;
   // private Map<Integer, ImageView> mMap;
    private Map<Integer, RelativeLayout> mMap;
    private Map<Integer, Map<Integer, View>> mMapMap;
    private Map<Integer ,Integer> mMaprecommen;
    private TextView mDown;
    private TextView mEdit;
    private TextView mDelet;
    private TextView tuijian;
    private Dialog mDialog;
    private SaleBean.MassageBean.GoodsList dataBean;
    private int last_page=0;
    private int page=1;

    private int ADDNEWCODE=6;
    private int ADDCODE=3;
    private PinchImageView bigimg;
    private String shopid;
    private String user_token;
    private String userid;

    private String leixing;//判断是服务类还是零售类
    private KyLoadingBuilder builder;
    public static OnSaleFragment newInstance( String lei) {
        Bundle args = new Bundle();

        args.putString("l",lei);
        OnSaleFragment fragment = new OnSaleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_onsale_fram;
    }

    @Override
    protected void initViewsAndEvents(View view) {
        mListvOnsaleFram = (PullToRefreshListView) view.findViewById(R.id.listv_onsale_fram);
        mBtnCommodityAddShop = (Button) view.findViewById(R.id.btn_commodity_add_shop);
        mBtnCommodityAddShop.setOnClickListener(OnSaleFragment.this);
    }

    @Override
    protected void onFirstUserVisible() {
        Bundle arguments = getArguments();
        leixing = arguments.getString("l");
        Aimoi();
        userid = (String) SharePUtile.get(getContext(), "user_id", "");
        shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        user_token = (String) SharePUtile.get(getContext(), "user_token", "");
      //  int typew = (int) SharePUtile.get(getContext(), "shop_typ", 0);
        mMapMap = new HashMap<>();
        mList = new ArrayList<>();
        mMap = new HashMap<>();
        mMaprecommen=new HashMap<>();
        initData();
        pullfresh();
    }
    @Override
    protected void onUserVisible() {
        mAllList.clear();
        page=1;
        initData();
    }
    @Override
    protected void onUserInvisible() {

    }
    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    private void pullfresh(){
        mListvOnsaleFram.setMode(PullToRefreshBase.Mode.BOTH);
        mListvOnsaleFram.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mAllList.clear();
                page=1;
                initData();
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

                if(page!=last_page){
                    page++;
                    initData();
                }else{
                    mListvOnsaleFram.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvOnsaleFram.onRefreshComplete();
                        }
                    }, 1000);
                }
            }
        });
     /*   mOnSaleAdapter = new OnSaleAdapter(getContext(),new BackImg() {
            @Override
            public void BackSrc(ImageView imageView, final int positon) {

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Showpopuwind(mMap.get(positon), positon);

                    }
                });
            }
        });*/
        mOnSaleAdapter=new OnSaleAdapter(getContext(), new BackImg() {
            @Override
            public void BackSrc(RelativeLayout imageView, final int positon) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // Showpopuwind(mMap.get(positon), positon);
                        RelativeLayout menu = mMap.get(positon);
                        ImageView img = (ImageView) menu.findViewById(R.id.img_conndity_menu);
                        Showpopuwind(img, positon);

                    }
                });
            }
        });
        mOnSaleAdapter.setListener(new OnSaleAdapter.onListener() {
            @Override
            public void OnListener(RelativeLayout imageView, int psion, Map<Integer, View> viewMap, ImageView photo, String url, Integer is_recommend) {
                mMap.put(psion, imageView);
                mMapMap.put(psion, viewMap);
                mMaprecommen.put(psion,is_recommend);
            }
        });

       /* mOnSaleAdapter.setListener(new OnSaleAdapter.onListener() {
            @Override
            public void OnListener(ImageView imageView, int psion, Map<Integer, View> viewMap, ImageView photo, final String url) {
                mMap.put(psion, imageView);
                mMapMap.put(psion, viewMap);
                photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //showphoto(url);
                    }
                });
            }
        });*/

        mListvOnsaleFram.setAdapter(mOnSaleAdapter);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvOnsaleFram.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mOnSaleAdapter.notifyDataSetChanged();
    }
    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(getContext());
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }
    private void initData() {
        Log.e("出售中",""+userid+"哈哈"+shopid+"哈哈"+user_token);
        OkGo.post(HttpAPI.SHANGPINLIST)
                .connTimeOut(10000)
                .params("cmd","1")
                .params("user_id",userid)
                .params("shopid",shopid)
                .params("user_token",user_token)
                .params("page",page+"")
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);
                    }
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("出售中",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                JSONObject data = jsonObject.getJSONObject("data");
                                last_page=data.getInt("last_page");
                                Gson gson=new Gson();
                                SaleBean saleBean = gson.fromJson(s, SaleBean.class);
                                mList = saleBean.getData().getData();
                                mAllList.addAll(mList);
                                mOnSaleAdapter.setList(mAllList);
                                mListvOnsaleFram.setAdapter(mOnSaleAdapter);
                                mOnSaleAdapter.notifyDataSetChanged();
                                mListvOnsaleFram.onRefreshComplete();
                            }else if(status.equals("0")){
                                Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                mListvOnsaleFram.onRefreshComplete();
                                mOnSaleAdapter.notifyDataSetChanged();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getContext(),"请求超时！",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);

                    }
                });

        mListvOnsaleFram.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dataBean= mAllList.get(position-1);

                if(leixing.equals("1")){
                    Intent intent=new Intent(getContext(),AddNewShopActivity.class);
                    intent.putExtra("type","onsale");
                    intent.putExtra("goodsid", dataBean.getBase_goods_id()+"");
                    startActivityForResult(intent,ADDCODE);
                }else if(leixing.equals("2")){
                    Intent intent=new Intent(getContext(),SaverAddnewShopActivity.class);
                    intent.putExtra("type","onsale");
                    intent.putExtra("goodsid", dataBean.getBase_goods_id()+"");
                    startActivityForResult(intent,ADDCODE);
                }


                 /*  Bundle bundle = new Bundle();
                bundle.putSerializable("data", dataBean);
                intent.putExtras(bundle);
                ;*/
            }
        });

    }

    /**
     * 显示大图
     * @param img
     */
    private void showphoto(String img){
        mDialog=new Dialog(getActivity(),R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(0,0,0,0);
        params.width =WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate2 = LayoutInflater.from(getActivity()).inflate(R.layout.item_dialog_imageview, null);
        bigimg= (PinchImageView) inflate2.findViewById(R.id.big_img);
        Glide.with(getContext()).load(img).into(bigimg);
        mDialog.setContentView(inflate2);
    }


    private void Showpopuwind(ImageView imageView, final int position) {
        Integer integer = mMaprecommen.get(position);
        dataBean= mAllList.get(position);
        mPopupWindow = new PopupWindow();
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.item_popupwindow_menu, null);
        mPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        mPopupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        mPopupWindow.setContentView(inflate);
        mPopupWindow.setTouchable(true);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        mPopupWindow.showAsDropDown(imageView, 0,0);
        mDown = (TextView) inflate.findViewById(R.id.tv_down_menu);
        mEdit = (TextView) inflate.findViewById(R.id.tv_edit_menu);
        mDelet = (TextView) inflate.findViewById(R.id.tv_deldet_menu);
        tuijian= (TextView) inflate.findViewById(R.id.tv_tuijan);
        if(integer==1){
            tuijian.setText("取消推荐");
            Tuijian("1");
        }else if(integer==0){
            tuijian.setText("推荐");
            Tuijian("0");
        }

        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  showDialog(mMapMap.get(position), position);
                Intent intent=new Intent(getContext(),AddNewShopActivity.class);
                /*Bundle bundle = new Bundle();
                bundle.putSerializable("data", dataBean);
                intent.putExtras(bundle);
                intent.putExtra("type","onsale");*/
                intent.putExtra("type","onsale");
                intent.putExtra("goodsid", dataBean.getBase_goods_id()+"");
                startActivityForResult(intent,ADDCODE);
                mPopupWindow.dismiss();
            }
        });
        mDelet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OkGo.post(HttpAPI.DELE)
                        .params("user_id",userid)
                        .params("user_token",user_token)
                        .params("shopid",shopid)
                        .params("base_goods_id",dataBean.getBase_goods_id()+"")
                        .execute(new StringCallback() {
                            @Override
                            public void onError(Call call, Response response, Exception e) {
                                super.onError(call, response, e);

                            }

                            @Override
                            public void onSuccess(String s, Call call, Response response) {
                                Log.e("删除",s+"userid:"+userid+"usertoken"+user_token);
                                try {
                                    JSONObject jsonObject=new JSONObject(s);
                                    String status = jsonObject.getString("status");
                                    if(status.equals("1")){
                                        mAllList.clear();
                                        initData();
                                        Toast.makeText(getContext(),"删除成功",Toast.LENGTH_SHORT).show();
                                        mPopupWindow.dismiss();



                                    }else if(status.equals("0")){
                                        Toast.makeText(getContext(),"删除失败",Toast.LENGTH_SHORT).show();
                                        mPopupWindow.dismiss();
                                    }else if(status.equals("2")){
                                        TokenUtils.TackUserToken(getActivity());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            }
        });
        mDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OkGo.post(HttpAPI.DOWN)
                        .params("user_id",userid)
                        .params("shopid",shopid)
                        .params("user_token",user_token)
                        .params("base_goods_id",dataBean.getBase_goods_id()+"")
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(String s, Call call, Response response) {
                                try {
                                    JSONObject jsonObject=new JSONObject(s);
                                    String status = jsonObject.getString("status");
                                    if(status.equals("1")){
                                        Toast.makeText(getContext(),"下架成功",Toast.LENGTH_SHORT).show();
                                        mPopupWindow.dismiss();
                                        mAllList.clear();
                                        initData();
                                    }else if(status.equals("0")){
                                        Toast.makeText(getContext(),"下架失败",Toast.LENGTH_SHORT).show();
                                        mPopupWindow.dismiss();
                                    }else if(status.equals("2")){
                                        TokenUtils.TackUserToken(getActivity());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            }
        });
    }

    private void Tuijian(final String cmd){

        Log.e("推荐",UserUtils.Userid(getContext())+"\n"+UserUtils.UserToken(getContext())+"\n"+dataBean.getBase_goods_id()+"");
        tuijian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OkGo.post(HttpAPI.TUIJIAN)
                        .params("shopid",UserUtils.Shopid(getContext()))
                        .params("user_id",UserUtils.Userid(getContext()))
                        .params("user_token", UserUtils.UserToken(getContext()))
                        .params("goods_agent_id",dataBean.getBase_goods_id()+"")
                        .params("cmd",cmd)
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(String s, Call call, Response response) {
                                Log.e("推荐",s);
                                try {
                                    JSONObject jsonObject=new JSONObject(s);
                                    String status = jsonObject.getString("status");
                                    if(status.equals("1")){
                                        String msg = jsonObject.getString("msg");
                                        ToastUtil.showShort(getContext(),msg);
                                        mAllList.clear();
                                        initData();
                                        mPopupWindow.dismiss();
                                    }else if(status.equals("0")){
                                        String msg = jsonObject.getString("msg");
                                        ToastUtil.showShort(getContext(),msg);

                                    }else if(status.equals("2")){
                                        TokenUtils.TackUserToken(getActivity());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(Call call, Response response, Exception e) {
                                super.onError(call, response, e);
                                ToastUtil.showShort(getContext(),e.getMessage().toString());
                            }
                        });
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_commodity_add_shop) {
            if(leixing.equals("1")){
                Intent intent=new Intent(getContext(),AddNewShopActivity.class);
                intent.putExtra("type","addnew");
                startActivityForResult(intent,ADDNEWCODE);
            }else if(leixing .equals("2")){
                Intent intent=new Intent(getContext(),SaverAddnewShopActivity.class);
                intent.putExtra("type","addnew");
                startActivityForResult(intent,ADDNEWCODE);
            }

        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==ADDCODE){
            String refresh = data.getStringExtra("refresh");
            if(refresh.equals("1")){
                mAllList.clear();
                page=1;
                initData();
            }
        }else if(resultCode==ADDNEWCODE){
            String refresh = data.getStringExtra("refresh");
            if(refresh.equals("1")){
                mAllList.clear();
                page=1;
                initData();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
