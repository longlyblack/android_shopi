package com.liminyunbao.iyunbeishop.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.adapter.PuTongLSAdapter;
import com.liminyunbao.iyunbeishop.bean.PuTongLiuShuiBean;
import com.liminyunbao.iyunbeishop.custom.BaseLazyFragment;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/4/6 0006.
 */

/**
 * 资金流水---->普通流水
 */
public class PuTonglsFragment extends BaseLazyFragment {


    protected View rootView;
    protected PullToRefreshListView mListvPutonglsFram;
    private List<PuTongLiuShuiBean.DataBeanX.DataBean> mDataBeen;
    private List<PuTongLiuShuiBean.DataBeanX.DataBean> mAllBean=new ArrayList<>();
  //  protected ListView mListvPutonglsFram;
    private PuTongLSAdapter mPuTongLSAdapter;
    private int last_page=0;

    private KyLoadingBuilder builder;
    private int page=1;
    private String userid;


    public static PuTonglsFragment newInstance() {

        Bundle args = new Bundle();

        PuTonglsFragment fragment = new PuTonglsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.item_putongls_fram;

    }
    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(getContext());
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }
    @Override
    protected void initViewsAndEvents(View view) {
       // mListvPutonglsFram = (ListView) view.findViewById(R.id.listv_putongls_fram);
        mListvPutonglsFram = (PullToRefreshListView) view.findViewById(R.id.listv_putongls_fram);
        //初始化 控件
    }

    @Override
    protected void onFirstUserVisible() {
        userid = (String) SharePUtile.get(getContext(), "user_id", "");
        Aimoi();
        initData();
        pullfresh();
        //请求数据
    }
    private void pullfresh(){
        mListvPutonglsFram.setMode(PullToRefreshBase.Mode.BOTH);
        mListvPutonglsFram.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                //开始下拉  我们做Http请求
                Aimoi();
                mAllBean.clear();
                page=1;
                initData();
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                Aimoi();
                if(page!=last_page){
                    initData();
                    page++;
                }else{
                    mListvPutonglsFram.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mListvPutonglsFram.onRefreshComplete();
                        }
                    },1000);
                }


            }
        });
        mPuTongLSAdapter=new PuTongLSAdapter();
        mListvPutonglsFram.setAdapter(mPuTongLSAdapter);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvPutonglsFram.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
    }
    private void initData(){
        mDataBeen=new ArrayList<>();
        String user_token = (String) SharePUtile.get(getContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getContext(),"shop_id","");
        OkGo.post(HttpAPI.DETAILS)
                .params("user_id",userid)
                .params("shopid",shopid)
                .params("user_token",user_token)
                .params("page",page+"")
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);
                        builder.show();
                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("普通流水",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Gson gson=new Gson();
                                PuTongLiuShuiBean puTongLiuShui = gson.fromJson(s, PuTongLiuShuiBean.class);
                                last_page= puTongLiuShui.getData().getLast_page();
                                mDataBeen = puTongLiuShui.getData().getData();
                                mAllBean.addAll(mDataBeen);
                                mPuTongLSAdapter.setStrings(mAllBean);
                                mPuTongLSAdapter.notifyDataSetChanged();
                                mListvPutonglsFram.onRefreshComplete();
                            }else if(status.equals("0")){
                                Toast.makeText(getContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                mListvPutonglsFram.onRefreshComplete();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);
                        builder.dismiss();
                    }
                });

    }
    @Override
    protected void onUserVisible() {

        //加载动画 广播
    }

    @Override
    protected void onUserInvisible() {

        //暂停动画广播
    }

    @Override
    protected void DetoryViewAndThing() {

    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }


}
