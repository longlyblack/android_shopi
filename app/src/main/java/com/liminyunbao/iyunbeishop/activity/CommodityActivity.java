package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.adapter.CommodityPagerAdapter;
import com.liminyunbao.iyunbeishop.fragment.OnSaleFragment;
import com.liminyunbao.iyunbeishop.fragment.SoldOutFragment;
import com.liminyunbao.iyunbeishop.fragment.WarehouseFragment;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品管理
 */
public class CommodityActivity extends BaseActivity implements View.OnClickListener {

    protected SlidingTabLayout mSTabCommodity;
    protected ViewPager mVpCommodity;
    protected TabLayout mTabCommodity;
    protected ImageView mImgCommodityBack;
    protected ImageView mImgAddshopBack;
    protected RelativeLayout mRlBack;
    protected TextView mTvGroupf;
    private List<Fragment> mLsit;
    private CommodityPagerAdapter mCommodityAdapter;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(), "你进入没有网的异次元！", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_commodity);
        initView();
        Intent intent = getIntent();
        String lei = intent.getStringExtra("lei");
        if(lei.equals("1")){
            initFragment("1");
        }else if(lei.equals("2")){
            initFragment("2");
        }

    }

    private void initFragment(String leixing) {
        mLsit = new ArrayList<>();
        mCommodityAdapter = new CommodityPagerAdapter(getSupportFragmentManager());
        mLsit.add(OnSaleFragment.newInstance(leixing));
        mLsit.add(SoldOutFragment.newInstance(leixing));
        mLsit.add(WarehouseFragment.newInstance(leixing));
        mCommodityAdapter.setFragments(mLsit);
        mVpCommodity.setAdapter(mCommodityAdapter);
        mSTabCommodity.setViewPager(mVpCommodity);
       /* mTabCommodity.setupWithViewPager(mVpCommodity);
        mTabCommodity.setTabMode(TabLayout.MODE_FIXED);*/
        mVpCommodity.setOffscreenPageLimit(3);


    }

    private void initView() {
        mSTabCommodity = (SlidingTabLayout) findViewById(R.id.sTab_commodity);
        /// mTabCommodity= (TabLayout) findViewById(R.id.tab_commodity);
        mVpCommodity = (ViewPager) findViewById(R.id.vp_commodity);
        mImgAddshopBack = (ImageView) findViewById(R.id.img_addshop_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(CommodityActivity.this);
        mTvGroupf = (TextView) findViewById(R.id.tv_groupf);
        mTvGroupf.setOnClickListener(CommodityActivity.this);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.rl_back) {
            finish();
        } else if (view.getId() == R.id.tv_groupf) {
            Intent intent = new Intent(CommodityActivity.this, GroupManagementActivity.class);
            startActivity(intent);
        }
    }
}
