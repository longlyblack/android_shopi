package com.liminyunbao.iyunbeishop.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.GetPicture;
import com.liminyunbao.iyunbeishop.adapter.HorelistAdapter;
import com.liminyunbao.iyunbeishop.bean.AnyEventType;
import com.liminyunbao.iyunbeishop.bean.MealDetailsBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.PictureUtil;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.AboutBitMap;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.liminyunbao.iyunbeishop.view.HorizontalListView;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.liminyunbao.iyunbeishop.view.SelectPicPopupWindow;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 推荐新增
 */
public class AddPackageActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgAddpackageBack;
    protected EditText mEditNameAddpackage;
    protected EditText mEditDescribeAddpackage;
    protected ImageView mImgAddpAddpackage;
    protected HorizontalListView mHorlistPicAddpackage;
    protected EditText mEditOldPricAddpackage;
    protected EditText mEditNewPricAddpackage;
    protected ImageView mImgTuwenAddpackage;
    protected TextView mTvTuwenAddpackage;
    protected RelativeLayout mRlTuwenAddpackage;
    protected ImageView mImgXuzhiAddpackage;
    protected TextView mTvXuzhiAddpackage;
    protected RelativeLayout mLrXuzhiAddpackage;
    protected ImageView mImgDescribeAddpackage;
    protected TextView mTvDescribeAddpackage;
    protected RelativeLayout mLrDescribeAddpackage;
    protected Button mBtnInAddpackage;
    protected Button mBtnShelvesAddpackage;
    private String mapurl;
    private KyLoadingBuilder builder;
    private int picnum=0;
    private  int ADDPXUNZHI=111;
    private  int ADDPTUWEN=112;
    private int ADDPDESCRIBE=113;

    private String  notes;//须知
    private String  detail;//商品描述
    private String goods_agend_id;
  /*  private String  nowpric;//现价
    private String oldpric;//原价
    */

  private  MealDetailsBean.GoodsMsgBean data;//这是goodsmassage;



    private List<String> imgs;//这是存放轮播图uri
    private List<String> deleimg;//这是  删除轮播图  id
    private List<String> upPhoto;//添加的新的uri

    private List<String > imgid;//存放请求的轮播图id

    private List<MealDetailsBean.GoodsMsgBean.TuwenBean>pic;//这是图文详情的
    private List<GetPicture> mGetPictures;
    private List<MealDetailsBean.GoodsMsgBean.ImgBean> img;
    private HorelistAdapter  mHorelistAdapter;

    private String deletuwen;
    private String tuwen;//图文
    private String lunbo;//轮播
    private String deleimgs;
    private Gson mGson;
    private boolean flag=false;
    private int sun=5;
    private ProgressDialog progressDialog;
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_add_package);
        initView();
        if (null == progressDialog) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("执行中……");
        }
        imgs=new ArrayList<>();
        pic=new ArrayList<>();
        imgid=new ArrayList<>();
        mGson=new Gson();
        deleimg=new ArrayList<>();
        Aimoi();
        mGetPictures=new ArrayList<>();
        Intent intent = getIntent();
        String goodid = intent.getStringExtra("goodid");

        if(goodid.equals("0")){
            goods_agend_id=0+"";
            mHorelistAdapter=new HorelistAdapter(imgs,getApplicationContext());
            mHorlistPicAddpackage.setAdapter(mHorelistAdapter);
            mHorelistAdapter.notifyDataSetChanged();
        }else {
            inithttp(goodid);
        }

        EventBus.getDefault().register(this);


        mHorlistPicAddpackage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                imgs.remove(position);
                if(position>imgid.size()){
                    deleimg.add(imgid.get(position));
                }
                mGetPictures.remove(position);
                picnum--;
                mHorelistAdapter.notifyDataSetChanged();

            }
        });


    }

    /**
     * 当goodid不位0的时候数据请求
     */

    private void inithttp(final String goodid){

        OkGo.post(HttpAPI.MEALINFO)
                .params("shopid",UserUtils.Shopid(getApplicationContext()))
                .params("user_id",UserUtils.Userid(getApplicationContext()))
                .params("user_token",UserUtils.UserToken(getApplicationContext()))
                .params("goods_id",goodid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("测试",""+s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Gson gson=new Gson();
                                MealDetailsBean mealDetailsBean = gson.fromJson(s, MealDetailsBean.class);
                                data = mealDetailsBean.getData();
                                notes=  data.getBuyknow();
                                detail  =data.getGoods_remark();
                                mEditNameAddpackage.setText(data.getGoods_name());
                                mEditNameAddpackage.setSelection(data.getGoods_name().length());
                                mEditDescribeAddpackage.setText(data.getMiaoshu());
                                mEditDescribeAddpackage.setSelection(data.getMiaoshu().length());
                                mEditNewPricAddpackage.setText(data.getGoods_price());
                                mEditNewPricAddpackage.setSelection(data.getGoods_price().length());
                                mEditOldPricAddpackage.setText(data.getMarket_price());
                                mEditOldPricAddpackage.setSelection(data.getMarket_price().length());
                                pic= data.getTuwen();
                                goods_agend_id=data.getGoods_agent_id()+"";
                                img = data.getImg();
                                if(img.size()>0){
                                    for(int i =0;i<img.size();i++){
                                        imgs.add(img.get(i).getPhoto());
                                        imgid.add(img.get(i).getPic_id()+"");
                                        mGetPictures.add(new GetPicture(img.get(i).getPic_id()+"",img.get(i).getPhoto()));
                                    }
                                }
                                mHorelistAdapter=new HorelistAdapter(imgs,getApplicationContext());
                                mHorlistPicAddpackage.setAdapter(mHorelistAdapter);
                                mHorelistAdapter.notifyDataSetChanged();

                            }else if(status.equals("0")){

                                ToastUtil.showShort(getApplicationContext(),"暂无数据");
                            }else if(status.equals("2")){

                                TokenUtils.TackUserToken(AddPackageActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * 判空  并将需要上传的参数转化json
     */

    private void  cinspect(){

        flag=false;
        deleimgs=mGson.toJson(deleimg);
        lunbo= mGson.toJson(mGetPictures);
        if(deletuwen==null){
            deletuwen="[]";
        }
        if(deleimgs==null){
            deleimgs="[]";
        }
        if(TextUtils.isEmpty(notes)||notes==null){
            notes="0";
        }
        if(TextUtils.isEmpty(detail)||detail==null){
            detail="0";
        }
        if(lunbo==null){
            lunbo="[]";
        }
        if(tuwen==null){
            tuwen="[]";
        }

        if(imgs.size()==0){
            ToastUtil.showShort(getApplicationContext(),"请选择照片");
            flag=true;
        }

      /*  if(TextUtils.isEmpty(mEditDescribeAddpackage.getText())){
            ToastUtil.showShort(getApplicationContext(),"请输入商品描述");
            flag=true;

        }*/

        if(TextUtils.isEmpty(mEditOldPricAddpackage.getText())){
            ToastUtil.showShort(getApplicationContext(),"请输入商品原价");
            flag=true;
        }else if(!TextUtils.isEmpty(mEditOldPricAddpackage.getText())){
            double i = Double.parseDouble(mEditOldPricAddpackage.getText().toString());
            if(i==0){
                ToastUtil.showShort(getApplicationContext(),"输入商品原价不能为0");
                flag=true;
            }
        }

        if(TextUtils.isEmpty(mEditNewPricAddpackage.getText())){
            ToastUtil.showShort(getApplicationContext(),"请输入商品现价");
            flag=true;
        }else if(!TextUtils.isEmpty(mEditNewPricAddpackage.getText())){
            double i1 = Double.parseDouble(mEditNewPricAddpackage.getText().toString());
            if(i1==0){
                ToastUtil.showShort(getApplicationContext(),"输入商品现价不能为0");
                flag=true;
            }
        }

        if(TextUtils.isEmpty(mEditNameAddpackage.getText())){
            ToastUtil.showShort(getApplicationContext(),"请输入商品名称");
            flag=true;
        }


    }




    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(AddPackageActivity.this);
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("上传中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_addpackage_back) {
            finish();

        } else if (view.getId() == R.id.img_addp_addpackage) {
            if(picnum!=3){
                tackPhoto();
            }else{
                ToastUtil.showShort(getApplicationContext(),"只能上传3张照片");
            }

        } else if (view.getId() == R.id.rl_tuwen_addpackage) {
            Intent intent = new Intent(AddPackageActivity.this, PicTuijanActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("photo", (Serializable)pic);
            intent.putExtras(b);
            startActivityForResult(intent, ADDPTUWEN);
        } else if (view.getId() == R.id.lr_xuzhi_addpackage) {
            Intent intent = new Intent(AddPackageActivity.this, GoodsDescribeActivity.class);
                 intent.putExtra("peng", "3");
                intent.putExtra("xuzhi", notes);
            startActivityForResult(intent, ADDPXUNZHI);
        } else if (view.getId() == R.id.lr_describe_addpackage) {
            Intent intent = new Intent(AddPackageActivity.this, GoodsDescribeActivity.class);
            intent.putExtra("peng", "4");
                intent.putExtra("descreibe", detail);
            startActivityForResult(intent, ADDPDESCRIBE);
        } else if (view.getId() == R.id.btn_in_addpackage) {
            Warehouse();
        } else if (view.getId() == R.id.btn_shelves_addpackage) {
            showgood();
        }
    }


    /**
     * 上架
     */
    private void showgood(){
        cinspect();

        Log.e("彭","jieshao:"+mEditDescribeAddpackage.getText().toString()+"\n"
        +"market_price:"+mEditOldPricAddpackage.getText().toString()+"\n"+"name:"+mEditNameAddpackage.getText().toString()+"\n" +
                "notes:"+notes+"\n"+"shop_price:"+mEditNewPricAddpackage.getText().toString()+"\n"+"detail:"+detail+"\n" +
                "base_goods_id:"+goods_agend_id+"\n"+"lunbo:"+lunbo+"\n"+"deleimg:"+deleimgs+"\n"+"photo:"+tuwen+"\n"+"delephoto:"+deletuwen);


        if(!flag) {
            OkGo.post(HttpAPI.ADDMEAL)
                    .params("shopid", UserUtils.Shopid(getApplicationContext()))
                    .params("user_id", UserUtils.Userid(getApplicationContext()))
                    .params("user_token", UserUtils.UserToken(getApplicationContext()))
                    .params("jieshao", "")// 商品介绍
                    .params("market_price", mEditOldPricAddpackage.getText().toString())//原价
                    .params("name", mEditNameAddpackage.getText().toString())//商品名
                    .params("notes", notes)//购买须知
                    .params("shop_price", mEditNewPricAddpackage.getText().toString())//现价
                    .params("status", "1")//上架
                    .params("detail", detail)//详情描述
                    .params("base_goods_id", goods_agend_id)//新增还是编辑
                    .params("img", lunbo)//轮播图
                    .params("deleimg", deleimgs)//删除轮播图
                    .params("photo", tuwen)//图文
                    .params("delephoto", deletuwen)//删除图文
                    .execute(new StringCallback() {

                        @Override
                        public void onBefore(BaseRequest request) {
                            super.onBefore(request);
                            progressDialog.show();
                        }

                        @Override
                        public void onAfter(String s, Exception e) {
                            super.onAfter(s, e);
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                String status = jsonObject.getString("status");
                                if (status.equals("1")) {
                                    String msg = jsonObject.getString("msg");
                                    ToastUtil.showShort(getApplicationContext(), msg);
                                    finish();
                                } else if (status.equals("0")) {
                                    ToastUtil.showShort(getApplicationContext(), "暂无数据");
                                } else if (status.equals("2")) {
                                    TokenUtils.TackUserToken(AddPackageActivity.this);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Call call, Response response, Exception e) {
                            super.onError(call, response, e);
                            ToastUtil.showShort(getApplicationContext(), e.getMessage().toString());
                        }
                    });
        }

    }


    /**
     * 放入仓库
     */
    private void Warehouse(){
        cinspect();

        if(!flag){
            OkGo.post(HttpAPI.ADDMEAL)
                    .params("shopid",UserUtils.Shopid(getApplicationContext()))
                    .params("user_id",UserUtils.Userid(getApplicationContext()))
                    .params("user_token",UserUtils.UserToken(getApplicationContext()))
                    .params("jieshao",mEditDescribeAddpackage.getText().toString())// 商品介绍
                    .params("market_price",mEditOldPricAddpackage.getText().toString())//原价
                    .params("name",mEditNameAddpackage.getText().toString())//商品名
                    .params("notes",notes)//购买须知
                    .params("shop_price",mEditNewPricAddpackage.getText().toString())//现价
                    .params("status","2")//仓库
                    .params("detail",detail)//详情描述
                    .params("base_goods_id",goods_agend_id)//新增还是编辑
                    .params("img",lunbo)//轮播图
                    .params("deleimg",deleimgs)//删除轮播图
                    .params("photo",tuwen)//图文
                    .params("delephoto",deletuwen)//删除图文
                    .execute(new StringCallback() {

                        @Override
                        public void onBefore(BaseRequest request) {
                            super.onBefore(request);
                            progressDialog.show();
                        }

                        @Override
                        public void onAfter(String s, Exception e) {
                            super.onAfter(s, e);
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                String status = jsonObject.getString("status");
                                if(status.equals("1")){
                                    String msg = jsonObject.getString("msg");
                                    ToastUtil.showShort(getApplicationContext(),msg);
                                    finish();
                                }else if (status.equals("0")){
                                    ToastUtil.showShort(getApplicationContext(),"暂无数据");
                                }else if(status.equals("2")){
                                    TokenUtils.TackUserToken(AddPackageActivity.this);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Call call, Response response, Exception e) {
                            super.onError(call, response, e);
                            ToastUtil.showShort(getApplicationContext(),e.getMessage().toString());
                        }
                    });

        }

    }






    /**
     * 选择照片或者拍照
     */
    SelectPicPopupWindow menuWindow;
    private void tackPhoto() {
        menuWindow = new SelectPicPopupWindow(this, itemsOnClick);
        menuWindow.showAtLocation(findViewById(R.id.xiangji),
                Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);

    }
    private View.OnClickListener itemsOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            menuWindow.dismiss();
            switch (v.getId()) {
                case R.id.takePhotoBtn: //选择相机拍照

                    AndPermission.with(AddPackageActivity.this)
                            .requestCode(100)
                            .permission(Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE)
                            .callback(listener)
                            .start();
                    break;
                case R.id.pickPhotoBtn:     //选择相册
                    openPic();
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * 权限申请回调
     */
    private int REQUEST_CODE_SETTING = 300;
    private PermissionListener listener = new PermissionListener() {
        @Override
        public void onSucceed(int requestCode, List<String> grantedPermissions) {
            // 权限申请成功回调。
            // 这里的requestCode就是申请时设置的requestCode。
            // 和onActivityResult()的requestCode一样，用来区分多个不同的请求。
            if (requestCode == 100) {
                // TODO ...
                openCamera();
            }
        }

        @Override
        public void onFailed(int requestCode, List<String> deniedPermissions) {
            // 权限申请失败回调。
            if (requestCode == 100) {
                // TODO ...
                // 第一种：用默认的提示语。
                AndPermission.defaultSettingDialog(AddPackageActivity.this, REQUEST_CODE_SETTING).show();
            }
        }
    };


    /**
     * 调用相机     相机拍照之后 一般都是一个  时间.jpg 文件
     */
    private File mFile;
    /**
     * 定义三种状态
     */
    private static final int REQUESTCODE_PIC = 1;//相册
    private static final int REQUESTCODE_CAM = 2;//相机

    private void openCamera() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            if (!file.exists()) {
                file.mkdirs();
            }
            mFile = new File(file, System.currentTimeMillis() + ".jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mFile));
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            startActivityForResult(intent, REQUESTCODE_CAM);
        } else {
            Toast.makeText(this, "请确认已经插入SD卡", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 打开相册
     */
    private void openPic() {
        Intent picIntent = new Intent(Intent.ACTION_PICK, null);
        picIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(picIntent, REQUESTCODE_PIC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {//返回码是可用的
            switch (requestCode) {
                case REQUESTCODE_CAM:
                    final String absolutePath = mFile.getAbsolutePath();
                    final Bitmap bitmap = PictureUtil.compressSizeImage(absolutePath);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            setImageToView(bitmap);
                        }
                    }).start();

                    break;
                case REQUESTCODE_PIC:
                    Uri uri = data.getData();
                    final String s = parsePicturePath(getBaseContext(), uri);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            setImageToView(PictureUtil.compressSizeImage(s));
                        }
                    }).start();
                    break;
            }

        }  else if(resultCode==ADDPXUNZHI){

            if (data.getStringExtra("bo").length() != 0) {
                notes = data.getStringExtra("bo");
            } else {
                notes = "0";
            }


        }else if(resultCode==ADDPTUWEN){
            String pic = data.getStringExtra("pic");
            String delepic = data.getStringExtra("delephoto");

            if(pic!=null){

                tuwen=pic;
            }else {
                tuwen="[]";
            }

            if(delepic!=null){
                deletuwen=delepic;
            }else {
                deletuwen="[]";
            }

        }else if(resultCode==ADDPDESCRIBE){


            if (data.getStringExtra("bo").length() != 0) {

                detail = data.getStringExtra("bo");
            } else {
                detail = "0";
            }
        }





        /*else if (resultCode == XUZHI) {
            if (data.getStringExtra("bo").length() != 0) {
                notes = data.getStringExtra("bo");
            } else {
                notes = "0";
            }
        } else if (resultCode == DESCRIBE) {
            if (data.getStringExtra("bo").length() != 0) {
                details = data.getStringExtra("bo");
            } else {
                details = "0";
            }
        } else if (resultCode == PICTURE) {

            String pic = data.getStringExtra("pic");
            String delepic = data.getStringExtra("delephoto");
            if (pic != null) {
                picture = pic;
            }
            if (delepic != null) {
                shanchu = delepic;
            }
            Log.e("上传", pic);*/



    }

    /**
     * 图片上传
     */
    private void setImageToView(Bitmap bitmap) {
        EventBus.getDefault().post(new AnyEventType("addpackage"));
        Bitmap nbitmap = bitmap;
        mapurl = AboutBitMap.bitmapToBase64(nbitmap);
        OkGo.post(HttpAPI.PHOTO)
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("base64", HttpAPI.Base64 + mapurl)
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);
                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        String substring = s.substring(1, s.length());
                    /*    getUrl = substring;
                        mUriString.add(getUrl);
                        mpicban.add(new GetPicture("0", getUrl));
                        upUriString.add(getUrl);
                        mHorelistAdapter.notifyDataSetChanged();*/
                        imgs.add(substring);
                        mGetPictures.add(new GetPicture("0",substring));
                        mHorelistAdapter.notifyDataSetChanged();
                        picnum++;
                        EventBus.getDefault().post(new AnyEventType("addsucess"));
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);

                    }
                });
    }

    /**
     * 解析获取图片库图片Uri物理路径
     *
     * @param context
     * @param uri
     * @return
     */
    @SuppressLint("NewApi")
    public String parsePicturePath(Context context, Uri uri) {

        if (null == context || uri == null)
            return null;

        boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentUri
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageDocumentsUri
            if (isExternalStorageDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                String[] splits = docId.split(":");
                String type = splits[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + File.separator + splits[1];
                }
            }
            // DownloadsDocumentsUri
            else if (isDownloadsDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaDocumentsUri
            else if (isMediaDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                String[] split = docId.split(":");
                String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                String selection = "_id=?";
                String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosContentUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;

    }

    private String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        String column = "_data";
        String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            try {
                if (cursor != null)
                    cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;

    }

    private boolean isExternalStorageDocumentsUri(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocumentsUri(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocumentsUri(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosContentUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private void initView() {
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mImgAddpackageBack.setOnClickListener(AddPackageActivity.this);
        mEditNameAddpackage = (EditText) findViewById(R.id.edit_name_addpackage);
        mEditDescribeAddpackage = (EditText) findViewById(R.id.edit_describe_addpackage);
        mImgAddpAddpackage = (ImageView) findViewById(R.id.img_addp_addpackage);
        mImgAddpAddpackage.setOnClickListener(AddPackageActivity.this);
        mHorlistPicAddpackage = (HorizontalListView) findViewById(R.id.horlist_pic_addpackage);
        mEditOldPricAddpackage = (EditText) findViewById(R.id.edit_old_pric_addpackage);
        mEditNewPricAddpackage = (EditText) findViewById(R.id.edit_new_pric_addpackage);
        mImgTuwenAddpackage = (ImageView) findViewById(R.id.img_tuwen_addpackage);
        mTvTuwenAddpackage = (TextView) findViewById(R.id.tv_tuwen_addpackage);
        mRlTuwenAddpackage = (RelativeLayout) findViewById(R.id.rl_tuwen_addpackage);
        mRlTuwenAddpackage.setOnClickListener(AddPackageActivity.this);
        mImgXuzhiAddpackage = (ImageView) findViewById(R.id.img_xuzhi_addpackage);
        mTvXuzhiAddpackage = (TextView) findViewById(R.id.tv_xuzhi_addpackage);
        mLrXuzhiAddpackage = (RelativeLayout) findViewById(R.id.lr_xuzhi_addpackage);
        mLrXuzhiAddpackage.setOnClickListener(AddPackageActivity.this);
        mImgDescribeAddpackage = (ImageView) findViewById(R.id.img_describe_addpackage);
        mTvDescribeAddpackage = (TextView) findViewById(R.id.tv_describe_addpackage);
        mLrDescribeAddpackage = (RelativeLayout) findViewById(R.id.lr_describe_addpackage);
        mLrDescribeAddpackage.setOnClickListener(AddPackageActivity.this);
        mBtnInAddpackage = (Button) findViewById(R.id.btn_in_addpackage);
        mBtnInAddpackage.setOnClickListener(AddPackageActivity.this);
        mBtnShelvesAddpackage = (Button) findViewById(R.id.btn_shelves_addpackage);
        mBtnShelvesAddpackage.setOnClickListener(AddPackageActivity.this);
    }

    /**
     * EventBus功能部分
     * 此处主要用到的功能为：在NewTaskService中接收到新订单之后，那边会发送一个通知，然后这边会通知新任务界面去刷新界面
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final AnyEventType bean) {
        //这里接受到消息
        if (bean.getS().equals("addpackage")) {
            if (builder == null) {
                Aimoi();
                builder.show();
            } else {
                builder.show();
            }

        } else {
            //progressDialog.dismiss();
            builder.dismiss();
            builder = null;
        }

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
