package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.adapter.MingXiAdapter;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

public class CapitalFlowActivity extends BaseActivity implements View.OnClickListener {

    protected TextView mTvTetileName;
    protected ListView mListvCapitalflo;
    protected ImageView mImgCapitalBack;
    protected LinearLayout mActivityCapitalFlow;
    private List<String> mList;
 private MingXiAdapter mMingXiAdapter;

    private String name[] = {"支付宝", "云贝费率", "微信支付", "QQ支付"};

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_capital_flow);
        initView();

        initData();

    }

    private void initData() {
        mList = new ArrayList<>();
        mMingXiAdapter = new MingXiAdapter();
        for (int i = 0; i < name.length; i++) {
            mList.add(name[i]);
        }
        mMingXiAdapter.setStrings(mList);
        mListvCapitalflo.setAdapter(mMingXiAdapter);
        mMingXiAdapter.notifyDataSetChanged();


    }

    private void initView() {
        mTvTetileName = (TextView) findViewById(R.id.tv_tetile_name);
        mListvCapitalflo = (ListView) findViewById(R.id.listv_Capitalflo);
        mImgCapitalBack = (ImageView) findViewById(R.id.img_capital_back);
        mImgCapitalBack.setOnClickListener(CapitalFlowActivity.this);
        mActivityCapitalFlow = (LinearLayout) findViewById(R.id.activity_capital_flow);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_capital_back) {
            finish();

        }
    }
}
