package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.adapter.PuTongDdAdapter;
import com.liminyunbao.iyunbeishop.bean.PuTongListBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 普通订单 列表
 */
public class GeneralorderActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgPutonglistBack;
    protected PullToRefreshListView mListvPtol;
    private List<PuTongListBean.DataBean.OrderDmsg> mDateBeen;
    private List<PuTongListBean.DataBean.OrderDmsg> mALLdataBean=new ArrayList<>();
    private PuTongDdAdapter mPuTongDdAdapter;
    private int last_page=0;
    private int page=1;
    private boolean isup=false;
    private KyLoadingBuilder builder;
    @Override
    protected void HaveNet() {

    }
    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_pu_torder_list);
        initView();
        initData();
        pullfresh();
        mListvPtol.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(GeneralorderActivity.this, OrderDetailsActivity.class);
                PuTongListBean.DataBean.OrderDmsg dataBeanX;
                dataBeanX=mALLdataBean.get(position-1);
                List<PuTongListBean.DataBean.OrderDmsg.ShopData> data = dataBeanX.getData();
                // intent.putExtra("photo",dataBeanX.getPhoto());

                intent.putExtra("code", dataBeanX.getVerify_code() + "");
                intent.putExtra("shopname", dataBeanX.getShop_name());
                intent.putExtra("status", dataBeanX.getOrder_status() + "");//状态
                intent.putExtra("zongja", dataBeanX.getTotal_amount() + "");//总价
                intent.putExtra("zonge", dataBeanX.getOrder_amount() + "");//总额
                intent.putExtra("youhui",Float.parseFloat(dataBeanX.getOrder_amount())  -Float.parseFloat( dataBeanX.getTotal_amount()) + "");//优惠
                intent.putExtra("oreder", dataBeanX.getOrder_id() + "");//订单号
                intent.putExtra("time", dataBeanX.getCreated_at() + "");//下单时间
                intent.putExtra("data", (Serializable) data);
                startActivity(intent);
            }
        });
    }
    private void pullfresh(){

        mListvPtol.setMode(PullToRefreshBase.Mode.BOTH);
        mListvPtol.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                //开始下拉  我们做Http请求
                mALLdataBean.clear();
                page=1;
                initData();
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if(page!=last_page){
                    page++;
                    initData();
                }else{
                    mListvPtol.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvPtol.onRefreshComplete();
                        }
                    }, 1000);
                }

            }
        });
        mPuTongDdAdapter = new PuTongDdAdapter();
        mListvPtol.setAdapter(mPuTongDdAdapter);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvPtol.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mPuTongDdAdapter.notifyDataSetChanged();
    }

    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(this);
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }
    private void initData() {
        Aimoi();
       String user_token = (String) SharePUtile.get(getApplicationContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getBaseContext(),"shop_id","");
        String  userid = (String) SharePUtile.get(getBaseContext(), "user_id", "");
        mDateBeen = new ArrayList<>();
        OkGo.post(HttpAPI.ORDERLIST)
                .connTimeOut(10000)
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("shopid", shopid)
                .params("page",page+"")
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);
                       builder.show();
                    }
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("普通订单",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String statue=jsonObject.getString("status");
                            if(statue.equals("1")){
                                Gson gson = new Gson();
                                PuTongListBean puTongListBean = gson.fromJson(s, PuTongListBean.class);
                                mDateBeen = puTongListBean.getData().getData();
                                last_page=puTongListBean.getData().getLast_page();
                                mALLdataBean.addAll(mDateBeen);
                                mPuTongDdAdapter.setBeanXList(mALLdataBean);
                                mPuTongDdAdapter.notifyDataSetChanged();
                                mListvPtol.onRefreshComplete();
                            }else if(statue.equals("0")){
                                Toast.makeText(getApplicationContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                mListvPtol.onRefreshComplete();
                            }else if(statue.equals("2")){
                                TokenUtils.TackUserToken(GeneralorderActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);
                        builder.dismiss();
                    }
                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getApplicationContext(),"链接超时！",Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void initView() {
        mImgPutonglistBack = (ImageView) findViewById(R.id.img_putonglist_back);
        mImgPutonglistBack.setOnClickListener(GeneralorderActivity.this);
        mListvPtol = (PullToRefreshListView) findViewById(R.id.listv_ptol);
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_putonglist_back) {
            finish();
        }
    }
}
