package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

public class ChangeNameActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgChangenameBack;
    protected EditText mEdiNameChange;
    protected Button mBtnChangeNameSure;
    private String nickname;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

        Toast.makeText(getApplicationContext(),"",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_change_name);
        initView();
        Intent intent = getIntent();
        nickname = intent.getStringExtra("nickname");
        if(nickname.equals("null")){
            mEdiNameChange.setHint("待完善");
        }else{
            mEdiNameChange.setText(nickname);
        }

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_change_name_sure) {
            changename();
            Log.e("这是修该联系人", "wodianji le ");
        } else if (view.getId() == R.id.img_changename_back) {

            finish();
        }
    }

    private void changename() {
        if(mEdiNameChange.getText().toString()!=null){
            String userid = (String) SharePUtile.get(getApplicationContext(), "user_id", "");
            String user_token = (String) SharePUtile.get(getApplicationContext(), "user_token", "");
            String shopid = (String) SharePUtile.get(getApplicationContext(), "shop_id", "");
            OkGo.post(HttpAPI.ChangName)
                    .params("user_id", userid)
                    .params("user_token", user_token)
                    .params("shopid", shopid)
                    .params("nickname", mEdiNameChange.getText().toString())
                    .execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            Log.e("这是修该联系人", s);
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                String status = jsonObject.getString("status");
                                if(status.equals("1")){
                                    String msg = jsonObject.getString("msg");
                                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                                    finish();
                                }else if(status.equals("0")){
                                    String msg = jsonObject.getString("msg");
                                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                                }else if(status.equals("2")){
                                    TokenUtils.TackUserToken(ChangeNameActivity.this);
                                }

                            } catch (JSONException e) {

                            }
                        }
                    });
        }else {
            Toast.makeText(getApplicationContext(),"请填写要修改的联系人",Toast.LENGTH_SHORT).show();
        }

    }

    private void initView() {
        mImgChangenameBack = (ImageView) findViewById(R.id.img_changename_back);
        mImgChangenameBack.setOnClickListener(ChangeNameActivity.this);
        mEdiNameChange = (EditText) findViewById(R.id.edi_name_change);
        mBtnChangeNameSure = (Button) findViewById(R.id.btn_change_name_sure);
        mBtnChangeNameSure.setOnClickListener(ChangeNameActivity.this);
    }
}
