package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.ChildListAdapter;
import com.liminyunbao.iyunbeishop.bean.WaitingListBean;
import com.liminyunbao.iyunbeishop.custom.ChildListView;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.StringsUtils;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;


/**
 * 订单详情
 */
public class ShopDetails extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgShaodetailsBack;
    protected TextView mTvTitleName;
    protected ImageView mImgZhuangtai;
    protected TextView mTvPeisong;
    protected TextView mTvPeisongMessage;
    protected TextView mTvPeisongTime;
    protected TextView mTvGenSong;
    protected ImageView mImgHome;
    protected TextView mTvShaopNameShopDetails;
    protected ChildListView mListvChildLsitDetails;
    protected TextView mTextShangprice;
    protected TextView mTextYunfei;
    protected TextView mTextYouhui;
    protected TextView mTextZonge;
    protected TextView mTvShangpriceZongjia;
    protected TextView mTvPeisongfeiZongjia;
    protected TextView mTvYouhuiZongjia;
    protected TextView mTvZongeZongjia;
    protected TextView mTextPeiTime;
    protected TextView mTvPeoTime;
    protected TextView mTextPeiPeople;
    protected TextView mTvPeisongPeopel;
    protected TextView mTextShouhuiMessage;
    protected TextView mTvShouhuiMessage;
    protected TextView mTvShouhuiAddress;
    protected TextView mTextOrderNumPeisong;
    protected TextView mTvOrdernumPeiso;
    protected TextView mTextOrderTimePeisong;
    protected TextView mTvOrdertimePeiso;
    protected TextView mTextOrderZhuanPeisong;
    protected TextView mTvOrderzhuanPeiso;
    protected Button mBtnTackOrder;
    protected Button mBtnAnaccept;
    protected RelativeLayout mRlDaiJieDan;
    protected Button mBtnEvaluate;
    protected Button mBtnAgain;
    protected RelativeLayout mRlEvaluate;
    protected TextView mTextBeizhuMag;
    protected TextView mTvBeizhuMsg;
    protected Button mBtnOkrefond;
    protected RelativeLayout mRlOkrefond;

    private WaitingListBean.DataBean.OrderMsg orderMsg;
    private ChildListAdapter mChildListAdapter;
    private List<WaitingListBean.DataBean.OrderMsg.GoodsList> data;
    private List<WaitingListBean.DataBean.OrderMsg.GoodsList> goodsList;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(), "你进入没有网的异次元！", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_shop_details);
        initView();
        initData();
    }

    /**
     * 接受传递过来的数据
     */
    private void initData() {
        goodsList = new ArrayList<>();
        mChildListAdapter = new ChildListAdapter();
        Intent intent = getIntent();
        orderMsg = (WaitingListBean.DataBean.OrderMsg) intent.getSerializableExtra("ordermsg");
        data = orderMsg.getData();
        for (int i = 0; i < data.size(); i++) {
            if (i < 2) {

            }
            goodsList.add(data.get(i));
        }
        mChildListAdapter.setList(goodsList);
        mListvChildLsitDetails.setAdapter(mChildListAdapter);
        mChildListAdapter.notifyDataSetChanged();
        setData();
    }

    /**
     * 设置数据
     */
    private void setData() {
        float discount =  Float.parseFloat(orderMsg.getTotal_amount())+Float.parseFloat(orderMsg.getFreight())-Float.parseFloat(orderMsg.getOrder_amount()) ;
        mTvShangpriceZongjia.setText("￥" + orderMsg.getTotal_amount());
        mTvPeisongfeiZongjia.setText("￥" + orderMsg.getFreight());
        mTvYouhuiZongjia.setText("-￥" + discount);
        mTvZongeZongjia.setText("￥" + orderMsg.getOrder_amount());
        mTvPeoTime.setText(StringsUtils.replaceBlank(orderMsg.getPs_time()));
        mTvShouhuiMessage.setText(orderMsg.getConsignee() + " 先生/女士" + " " + orderMsg.getMobile());
        mTvShouhuiAddress.setText(orderMsg.getAddress());
        mTvPeisongPeopel.setText("云贝骑士/" + orderMsg.getRid_nickname());
        mTvOrdernumPeiso.setText(orderMsg.getOrder_id() + "");
        mTvOrdertimePeiso.setText(orderMsg.getCreated_at());
        // Glide.with(this).load("http://img1.iyunbei.net/300x300/attachs/"+orderMsg.getPhoto()).into(mImgHome);
        mTvShaopNameShopDetails.setText(orderMsg.getShop_name());
        int status = orderMsg.getOrder_status();



        SimpleDateFormat formatter = new SimpleDateFormat(" HH:mm ");
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = formatter.format(curDate);
        mTvBeizhuMsg.setText(orderMsg.getNote());
        mTvPeisongTime.setText(str);
        if (status == 0) {
            mTvOrderzhuanPeiso.setText("待付款");

        } else if (status == 1) {
            mTvOrderzhuanPeiso.setText("待发货");
            Bitmap mbit = BitmapFactory.decodeResource(getResources(), R.mipmap.wsend);
            mImgZhuangtai.setImageBitmap(mbit);
            mTvTitleName.setText("待发货");
            mTvPeisong.setText("等待骑士接货");
            mTvPeisongMessage.setText(" 您已接单等待骑士接货~");
        } else if (status == 2) {
            mTvOrderzhuanPeiso.setText("已发货");
            Bitmap mbit = BitmapFactory.decodeResource(getResources(), R.mipmap.sending);
            mImgZhuangtai.setImageBitmap(mbit);
            mTvTitleName.setText("已发货");
            mTvPeisong.setText(" 骑士配送中  ");
            mTvPeisongMessage.setText(" 骑士正在配送中~");
        } else if (status == 3) {
            mTvOrderzhuanPeiso.setText("待评价");
        } else if (status == 4) {
            mTvOrderzhuanPeiso.setText("已完成");
            Bitmap mbit = BitmapFactory.decodeResource(getResources(), R.mipmap.complete);
            mImgZhuangtai.setImageBitmap(mbit);
            mTvTitleName.setText("已完成");
            mTvPeisong.setText(" 订单已完成  ");
            mTvPeisongMessage.setText(" 该订单已结束交易达成~");
        } else if (status == 5) {
            mTvOrderzhuanPeiso.setText("已关闭");
        } else if (status == 6) {
            mTvOrderzhuanPeiso.setText("退款中");
        } else if (status == 7) {
            mTvOrderzhuanPeiso.setText("待接单");
            mTvPeisong.setText("等待商家接单");
            mTvPeisongMessage.setText(" 您还未接单请尽快接单~");
            mTvTitleName.setText("待接单");
            mRlDaiJieDan.setVisibility(View.VISIBLE);
            Bitmap mbit = BitmapFactory.decodeResource(getResources(), R.mipmap.received);
            mImgZhuangtai.setImageBitmap(mbit);
        } else if (status == 8) {
            mTvOrderzhuanPeiso.setText("拒收");
        } else if (status == 9) {
            mTvTitleName.setText("商家退款中");
            mTvOrderzhuanPeiso.setText("商家退款中");
            mRlOkrefond.setVisibility(View.VISIBLE);
        }
        mBtnOkrefond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okrefond(orderMsg.getOrder_id()+"");
            }
        });
        mBtnTackOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OkGo.post(HttpAPI.ACCEPT)
                        .params("orderid", orderMsg.getOrder_id() + "")
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(String s, Call call, Response response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    String status1 = jsonObject.getString("status");
                                    if (status1.equals("1")) {
                                        String msg = jsonObject.getString("msg");
                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                        finish();
                                    } else if (status1.equals("0")) {
                                        String msg = jsonObject.getString("msg");
                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            }
        });
        mBtnAnaccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OkGo.post(HttpAPI.REFUSE)
                        .params("orderid", orderMsg.getOrder_id() + "")
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(String s, Call call, Response response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    String status1 = jsonObject.getString("status");
                                    if (status1.equals("1")) {
                                        String msg = jsonObject.getString("msg");
                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                        finish();
                                    } else if (status1.equals("0")) {
                                        String msg = jsonObject.getString("msg");
                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            }
        });

    }

    private void okrefond(String orderid){
        OkGo.post(HttpAPI.OKREFOND)
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token",UserUtils.UserToken(getApplicationContext()))
                .params("shopid",UserUtils.Shopid(getApplicationContext()))
                .params("orderid",orderid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(),msg);
                            }else if(status.equals("0")){
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(),msg);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });



    }

    private void initView() {
        mImgShaodetailsBack = (ImageView) findViewById(R.id.img_shaodetails_back);
        mImgShaodetailsBack.setOnClickListener(ShopDetails.this);
        mTvTitleName = (TextView) findViewById(R.id.tv_title_name);
        mImgZhuangtai = (ImageView) findViewById(R.id.img_zhuangtai);
        mTvPeisong = (TextView) findViewById(R.id.tv_peisong);
        mTvPeisongMessage = (TextView) findViewById(R.id.tv_peisong_message);
        mTvPeisongTime = (TextView) findViewById(R.id.tv_peisong_time);
        mTvGenSong = (TextView) findViewById(R.id.tv_gen_song);
        mImgHome = (ImageView) findViewById(R.id.img_home);
        mTvShaopNameShopDetails = (TextView) findViewById(R.id.tv_shaop_name_shop_details);
        mListvChildLsitDetails = (ChildListView) findViewById(R.id.listv_child_lsit_details);
        mTextShangprice = (TextView) findViewById(R.id.text_shangprice);
        mTextYunfei = (TextView) findViewById(R.id.text_yunfei);
        mTextYouhui = (TextView) findViewById(R.id.text_youhui);
        mTextZonge = (TextView) findViewById(R.id.text_zonge);
        mTvShangpriceZongjia = (TextView) findViewById(R.id.tv_shangprice_zongjia);
        mTvPeisongfeiZongjia = (TextView) findViewById(R.id.tv_peisongfei_zongjia);
        mTvYouhuiZongjia = (TextView) findViewById(R.id.tv_youhui_zongjia);
        mTvZongeZongjia = (TextView) findViewById(R.id.tv_zonge_zongjia);
        mTextPeiTime = (TextView) findViewById(R.id.text_pei_time);
        mTvPeoTime = (TextView) findViewById(R.id.tv_peo_time);
        mTextPeiPeople = (TextView) findViewById(R.id.text_pei_people);
        mTvPeisongPeopel = (TextView) findViewById(R.id.tv_peisong_peopel);
        mTextShouhuiMessage = (TextView) findViewById(R.id.text_shouhui_message);
        mTvShouhuiMessage = (TextView) findViewById(R.id.tv_shouhui_message);
        mTvShouhuiAddress = (TextView) findViewById(R.id.tv_shouhui_address);
        mTextOrderNumPeisong = (TextView) findViewById(R.id.text_order_num_peisong);
        mTvOrdernumPeiso = (TextView) findViewById(R.id.tv_ordernum_peiso);
        mTextOrderTimePeisong = (TextView) findViewById(R.id.text_order_time_peisong);
        mTvOrdertimePeiso = (TextView) findViewById(R.id.tv_ordertime_peiso);
        mTextOrderZhuanPeisong = (TextView) findViewById(R.id.text_order_zhuan_peisong);
        mTvOrderzhuanPeiso = (TextView) findViewById(R.id.tv_orderzhuan_peiso);
        mBtnTackOrder = (Button) findViewById(R.id.btn_tack_order);
        mBtnAnaccept = (Button) findViewById(R.id.btn_anaccept);
        mRlDaiJieDan = (RelativeLayout) findViewById(R.id.rl_dai_jie_dan);
        mBtnEvaluate = (Button) findViewById(R.id.btn_evaluate);
        mBtnAgain = (Button) findViewById(R.id.btn_again);
        mRlEvaluate = (RelativeLayout) findViewById(R.id.rl_evaluate);
        mTextBeizhuMag = (TextView) findViewById(R.id.text_beizhu_mag);
        mTvBeizhuMsg = (TextView) findViewById(R.id.tv_beizhu_msg);
        mBtnOkrefond = (Button) findViewById(R.id.btn_okrefond);
        mRlOkrefond = (RelativeLayout) findViewById(R.id.rl_okrefond);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_shaodetails_back) {
            finish();
        }
    }
}
