package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.FullTitieAdapter;
import com.liminyunbao.iyunbeishop.fragment.FullCutFragment;
import com.liminyunbao.iyunbeishop.fragment.FullCutNFragment;

import java.util.ArrayList;
import java.util.List;

public class FullGiftActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgAddpackageBack;
    protected RelativeLayout mRlBack;
    protected Button mBtnCardAdd;
    protected SlidingTabLayout mSTabFullGift;
    protected ViewPager mVpFullGift;
    protected LinearLayout mActivityFullGift;
    private FullTitieAdapter fullTitieAdapterm;
    private List<Fragment> fragmentsm;

    public FullGiftActivity() {
    }

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_full_gift);
        initView();
        fullTitieAdapterm = new FullTitieAdapter(getSupportFragmentManager());
        fragmentsm = new ArrayList<>();
        fragmentsm.add(FullCutFragment.newInstance("3"));//3代表的而是礼品券
        fragmentsm.add(FullCutNFragment.newInstance("3"));
        fullTitieAdapterm.setFragments(fragmentsm);
        mVpFullGift.setAdapter(fullTitieAdapterm);
        mSTabFullGift.setViewPager(mVpFullGift);
        mVpFullGift.setOffscreenPageLimit(2);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {
            finish();
        } else if (view.getId() == R.id.btn_card_add) {
            Intent intent=new Intent(getApplicationContext(),AddGiftActivity.class);
            startActivity(intent);
        }
    }

    private void initView() {
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(FullGiftActivity.this);
        mBtnCardAdd = (Button) findViewById(R.id.btn_card_add);
        mBtnCardAdd.setOnClickListener(FullGiftActivity.this);
        mSTabFullGift = (SlidingTabLayout) findViewById(R.id.sTab_full_gift);
        mVpFullGift = (ViewPager) findViewById(R.id.vp_full_gift);
        mActivityFullGift = (LinearLayout) findViewById(R.id.activity_full_gift);
    }
}
