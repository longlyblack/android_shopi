package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.WindowDisplayAdapter;
import com.liminyunbao.iyunbeishop.bean.DisplayWindowBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class DisplayWindowActivity extends BaseActivity {

    protected ImageView mImgAddpackageBack;
    protected RelativeLayout mRlBack;
    protected TextView mTvNow;
    protected TextView mTvMax;
    protected ListView mListvWindow;
    protected TextView mText;
    protected RelativeLayout mRlWindowAddnew;

    private  List<DisplayWindowBean.DataBean> data;
    private WindowDisplayAdapter mWindowDisplayAdapter;
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_display_window);
        initView();
        initHttp();
        mImgAddpackageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * 数据请求
     */
    private void initHttp(){

        OkGo.post(HttpAPI.RECOMMENDLIST)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id",UserUtils.Userid(getApplicationContext()))
                .params("shopid",UserUtils.Shopid(getApplicationContext()))
                .params("user_token",UserUtils.UserToken(getApplicationContext()))
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject  jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Gson gson=new Gson();
                                DisplayWindowBean displayWindowBean = gson.fromJson(s, DisplayWindowBean.class);
                                data = displayWindowBean.getData();
                                mTvMax.setText("最多可推荐"+displayWindowBean.getMax()+"件商品");
                                mTvNow.setText("全部商品("+displayWindowBean.getNow()+")/");
                                mWindowDisplayAdapter=new WindowDisplayAdapter(DisplayWindowActivity.this,data);
                                mListvWindow.setAdapter(mWindowDisplayAdapter);
                                mWindowDisplayAdapter.notifyDataSetChanged();
                                mWindowDisplayAdapter.setGetllcancle(new WindowDisplayAdapter.getllcancle() {
                                    @Override
                                    public void getCancle(LinearLayout ll, final int position) {
                                        ll.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                cancle("1", position);
                                            }
                                        });
                                    }
                                });
                            }else if(status.equals("0")){
                                ToastUtil.showShort(getApplicationContext(),"暂无数据");
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(DisplayWindowActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

    }


    /**
     * 橱窗推荐取消
     */

    private void cancle(String cmd ,int position){
        OkGo.post(HttpAPI.TUIJIAN)
                .params("shopid",UserUtils.Shopid(getApplicationContext()))
                .params("user_id",UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("goods_agent_id",data.get(position).getGoods_agent_id()+"")
                .params("cmd",cmd)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        JSONObject jsonObject= null;
                        try {
                            jsonObject = new JSONObject(s);

                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(),msg);
                                initHttp();
                            }else if(status.equals("0")){
                                ToastUtil.showShort(getApplicationContext(),"操作失败");
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(DisplayWindowActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void initView() {
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mTvNow = (TextView) findViewById(R.id.tv_now);
        mTvMax = (TextView) findViewById(R.id.tv_max);
        mListvWindow = (ListView) findViewById(R.id.listv_window);
        mText = (TextView) findViewById(R.id.text);
        mRlWindowAddnew = (RelativeLayout) findViewById(R.id.rl_window_addnew);
    }
}
