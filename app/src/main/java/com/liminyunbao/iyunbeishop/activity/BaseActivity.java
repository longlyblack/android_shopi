package com.liminyunbao.iyunbeishop.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.liminyunbao.iyunbeishop.R;
import com.readystatesoftware.systembartint.SystemBarTintManager;

/**
 * 零售类主页
 * Git码云更新测试
 */

public abstract class BaseActivity extends AppCompatActivity {
    //广播action
    public static final  String  SYSTEM_EXIT="com.liminyunbao.iyunbeishop";

    //接收器
    private MyReceiver mMyReceiver;
    private NetworkChangeReceiver mNetworkChangeReceiver;
    protected abstract void HaveNet();
    protected abstract void GoneNet();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVerson();
        logOut();
        NetworkChange();
    }
    private  void NetworkChange(){

        mNetworkChangeReceiver = new NetworkChangeReceiver();
        IntentFilter mFilter= new IntentFilter();
        mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mNetworkChangeReceiver, mFilter);
    }
    private void logOut() {
        mMyReceiver=new MyReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(SYSTEM_EXIT);//这个是退出登录的Action
        this.registerReceiver(mMyReceiver, filter);

    }
    class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null && info.isAvailable()) {
                HaveNet();
                //有网
            } else {
                GoneNet();
            }
        }
    }
    private class MyReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();//关闭界面
        }
    }
/*
*
*
*   ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null && info.isAvailable()) {
                //有网的时候操作
            } else {

            }
* */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mMyReceiver);
        this.unregisterReceiver(mNetworkChangeReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void initVerson() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //透明导航栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            // 激活状态栏
            tintManager.setStatusBarTintEnabled(true);
            // enable navigation bar tint 激活导航栏
            tintManager.setNavigationBarTintEnabled(true);
            //设置系统栏设置颜色
            //tintManager.setTintColor(R.color.red);
            //给状态栏设置颜色
            tintManager.setStatusBarTintResource(R.color.colorDianPuB);
            //Apply the specified drawable or color resource to the system navigation bar.
            //给导航栏设置资源

        }

       /*
        需要退出程序的时候发送这条广播
        Intent intent = new Intent();
        intent.setAction(BaseActivity.SYSTEM_EXIT);
        sendBroadcast(intent);*/

    }


}
