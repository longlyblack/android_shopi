package com.liminyunbao.iyunbeishop.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.adapter.AddnewCommodityApdater;
import com.liminyunbao.iyunbeishop.fragment.AddnewCommodityFragment;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.IntentUtils;

import java.util.ArrayList;
import java.util.List;

public class AddNewCommodityActivity extends BaseActivity implements View.OnClickListener {

    protected ListView mAddnewList;
    protected FrameLayout mAddnewFram;
    protected ImageView mImgAddnewBack;
    protected ListView mListvAddnewList;
    protected FrameLayout mFlAddnewFram;
    protected Button mBtnAddnewQingdan;
    protected Button mBtnOkAddnew;
    private List<String> mStrings;
    private String titel[] = {"休闲零食", "酒水乳饮", "米面粮油", "美妆洗化", "生活用品"};
    private AddnewCommodityApdater mAddnewApdater;
    private AddnewCommodityFragment mAddnewFragment;
    public static int stra;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

        Toast.makeText(getApplicationContext(),"当前没有网络！",Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_add_new_commodity);
        // initVerson();
        initView();
        mAddnewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                stra = position;
                mAddnewApdater.notifyDataSetChanged();
                for (int i = 0; i < titel.length; i++) {
                    mAddnewFragment = new AddnewCommodityFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fl_addnew_fram, mAddnewFragment);
                    Bundle bundle = new Bundle();
                    bundle.putString(AddnewCommodityFragment.TAG, titel[stra]);
                    mAddnewFragment.setArguments(bundle);
                    fragmentTransaction.commit();
                }
            }
        });
        initData();
    }

    private void initData() {
        mStrings = new ArrayList<>();
        mAddnewApdater = new AddnewCommodityApdater();
        for (int i = 0; i < titel.length; i++) {
            mStrings.add(titel[i]);
        }
        mAddnewApdater.setStrings(mStrings);
        mAddnewList.setAdapter(mAddnewApdater);
        mAddnewApdater.notifyDataSetChanged();

        mAddnewFragment = new AddnewCommodityFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_addnew_fram, mAddnewFragment);
        Bundle bundle = new Bundle();
        bundle.putString(AddnewCommodityFragment.TAG, titel[stra]);
        mAddnewFragment.setArguments(bundle);
        fragmentTransaction.commit();

    }

    private void initView() {
        mAddnewList = (ListView) findViewById(R.id.listv_addnew_list);
        mAddnewFram = (FrameLayout) findViewById(R.id.fl_addnew_fram);
        mImgAddnewBack = (ImageView) findViewById(R.id.img_addnew_back);
        mImgAddnewBack.setOnClickListener(AddNewCommodityActivity.this);
        mListvAddnewList = (ListView) findViewById(R.id.listv_addnew_list);
        mFlAddnewFram = (FrameLayout) findViewById(R.id.fl_addnew_fram);
        mBtnAddnewQingdan = (Button) findViewById(R.id.btn_addnew_qingdan);
        mBtnAddnewQingdan.setOnClickListener(AddNewCommodityActivity.this);
        mBtnOkAddnew = (Button) findViewById(R.id.btn_ok_addnew);
        mBtnOkAddnew.setOnClickListener(AddNewCommodityActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_addnew_back) {
            finish();
        } else if (view.getId() == R.id.btn_addnew_qingdan) {
            IntentUtils.goTo(this,CommodityListActivity.class);
        } else if (view.getId() == R.id.btn_ok_addnew) {
            IntentUtils.goTo(this,CommodityListActivity.class);
        }
    }
}
