package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

public class FreightStringActivity extends BaseActivity implements View.OnClickListener {

    protected RelativeLayout mRlBack;
    protected EditText mEditPric;
    protected Button mBtnFreightSure;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_freight_string);
        Intent intent = getIntent();
        String freight = intent.getStringExtra("freight");

        initView();
        if(!TextUtils.isEmpty(freight)){
            mEditPric.setText(freight);
        }


    }

    private void setFreth() {

        if (!TextUtils.isEmpty(mEditPric.getText())) {

            double v = Double.parseDouble(mEditPric.getText().toString());
            if(v>=5&&v!=0){
                OkGo.post(HttpAPI.FREIGHT)
                        .params("shopid", UserUtils.Shopid(getApplicationContext()))
                        .params("user_id", UserUtils.Userid(getApplicationContext()))
                        .params("user_token", UserUtils.UserToken(getApplicationContext()))
                        .params("yb_free", mEditPric.getText().toString())
                        .execute(new StringCallback() {
                            @Override
                            public void onSuccess(String s, Call call, Response response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    String status = jsonObject.getString("status");

                                    if (status.equals("1")) {
                                        String msg = jsonObject.getString("msg");
                                        ToastUtil.showShort(getApplicationContext(), msg);
                                        finish();
                                    } else if (status.equals("2")) {
                                        TokenUtils.TackUserToken(FreightStringActivity.this);
                                    } else if (status.equals("0")) {
                                        String msg = jsonObject.getString("msg");
                                        ToastUtil.showShort(getApplicationContext(), msg);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(Call call, Response response, Exception e) {
                                super.onError(call, response, e);
                                ToastUtil.showShort(getApplicationContext(),e.getMessage().toString());
                            }
                        });
            }else {
                ToastUtil.showShort(getApplicationContext(),"输入金额不能小于5圆");
            }
            } else {
            ToastUtil.showShort(getApplicationContext(), "请输入运费");
        }



    }


    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(FreightStringActivity.this);
        mEditPric = (EditText) findViewById(R.id.edit_pric);
        mBtnFreightSure = (Button) findViewById(R.id.btn_freight_sure);
        mBtnFreightSure.setOnClickListener(FreightStringActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {

            finish();
        } else if (view.getId() == R.id.btn_freight_sure) {
            setFreth();
        }
    }
}
