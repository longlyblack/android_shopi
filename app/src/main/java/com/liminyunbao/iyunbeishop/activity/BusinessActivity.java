package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.flyco.tablayout.listener.CustomTabEntity;
import com.liminyunbao.iyunbeishop.adapter.BusinessPagerAdapter;
import com.liminyunbao.iyunbeishop.fragment.HomePageFragment;
import com.liminyunbao.iyunbeishop.fragment.OrderListNewFragment;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.fragment.SummonFragment;
import com.liminyunbao.iyunbeishop.utils.ExampleUtil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

/**
 * 首页(零售)
 */
public class BusinessActivity extends BaseActivity {

    protected RadioButton mRbHomePageBusiness;
    protected RadioButton mRbOrderBusiness;
    protected RadioGroup mRadioGroup;
    protected TabLayout mTabBusiness;
    private long firstTime = 0;
    protected ViewPager mVpBusiness;
    //protected CommonTabLayout mCommTabBusiness;
    protected LinearLayout mActivityBusiness;
    private List<Fragment> mFragments;
    private List<CustomTabEntity> mCustomTabEntities;
    private String titel[] = {"首页", "订单"};
    private BusinessPagerAdapter mPagerAdapter;
    private List<RadioButton> mRadioButtons;

    private  String shopname;
    private String shopid;
    private String areaid;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_business);
        Intent intent = getIntent();
        shopname = intent.getStringExtra("shopname");
        shopid=intent.getStringExtra("shopid");
        areaid=intent.getStringExtra("area_id");
        Log.e("shopid",shopid);
        initView();
        addFragment();
        setTag( shopid, areaid);
       /* mCommTabBusiness.showMsg(1, 300);
        mCommTabBusiness.setMsgMargin(1, -5, 10);
        MsgView rtv_2_3 = mCommTabBusiness.getMsgView(1);
        if (rtv_2_3 != null) {
            rtv_2_3.setBackgroundColor(Color.parseColor("#EA5656"));
        }*/
    }
    private void initView() {
        mVpBusiness = (ViewPager) findViewById(R.id.vp_business);
        //mCommTabBusiness = (CommonTabLayout) findViewById(R.id.commTab_business);
        mActivityBusiness = (LinearLayout) findViewById(R.id.activity_business);
        mTabBusiness = (TabLayout) findViewById(R.id.tab_business);
    }
    private void addFragment() {
        mFragments = new ArrayList<>();
        mCustomTabEntities = new ArrayList<>();
        mRadioButtons = new ArrayList<>();
        mRadioButtons.add(mRbHomePageBusiness);
        mRadioButtons.add(mRbOrderBusiness);
        /*for (int i = 0; i < 2; i++) {
            mCustomTabEntities.add(new TabEntity(titel[i], 0, 0));
        }*/
        mFragments.add(HomePageFragment.newInstance(shopname));
        mFragments.add(OrderListNewFragment.newInstance());
        mFragments.add(SummonFragment.newInstance());
        mPagerAdapter = new BusinessPagerAdapter(getSupportFragmentManager());
        mPagerAdapter.setFragments(mFragments);
        mVpBusiness.setAdapter(mPagerAdapter);
        mTabBusiness.setupWithViewPager(mVpBusiness);
        mTabBusiness.setTabMode(TabLayout.MODE_FIXED);
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            long secondTime = System.currentTimeMillis();
            if (secondTime - firstTime > 2000) {
                Toast.makeText(BusinessActivity.this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                firstTime = secondTime;
                return true;
            } else {
                Intent intent = new Intent();
                intent.setAction(BaseActivity.SYSTEM_EXIT);
                sendBroadcast(intent);
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        //super.onSaveInstanceState(outState, outPersistentState);
    }
    private static final int MSG_SET_ALIAS = 1001;
    private static final int MSG_SET_TAGS = 1002;


    private void setTag(String shopid,String quyvid) {
        // EditText tagEdit = (EditText) findViewById(R.id.et_tag);
        String tag = shopid+","+quyvid;

        // 检查 tag 的有效性
        if (TextUtils.isEmpty(tag)) {
           // Toast.makeText(BusinessActivity.this, R.string.error_tag_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        // ","隔开的多个 转换成 Set
        String[] sArray = tag.split(",");
        Set<String> tagSet = new LinkedHashSet<String>();
        for (String sTagItme : sArray) {
            if (!ExampleUtil.isValidTagAndAlias(sTagItme)) {
                //Toast.makeText(BusinessActivity.this, R.string.error_tag_gs_empty, Toast.LENGTH_SHORT).show();
                return;
            }
            tagSet.add(sTagItme);
        }
        //调用JPush API设置Tag
        mHandler.sendMessage(mHandler.obtainMessage(MSG_SET_TAGS, tagSet));

    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SET_ALIAS:
                   // Log.i("cesh", "Set alias in handler.");
                    //   JPushInterface.setAliasAndTags(getApplicationContext(), (String) msg.obj, null, mAliasCallback);
                    break;
                case MSG_SET_TAGS:
                    //Log.i("cesh", "Set tags in handler.");
                    JPushInterface.setAliasAndTags(getApplicationContext(), null, (Set<String>) msg.obj, mTagsCallback);
                    break;

                default:

            }
        }
    };

    private final TagAliasCallback mTagsCallback = new TagAliasCallback() {
        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs="";
            switch (code) {
                case 0:
                    //logs = "Set 标签 and 别名 success";
                  //  Log.i("ceshi", logs);
                    break;

                case 6002:
                  // logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
                  //  Log.i("ceshi", logs);
                    if (ExampleUtil.isConnected(getApplicationContext())) {
                        mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_TAGS, tags), 1000 * 60);
                    } else {
                   //     Log.i("ceshi", "No network");
                    }
                    break;

                default:
                   // logs = "Failed with errorCode = " + code;
                   // Log.i("ceshi", logs);
            }
          //  ExampleUtil.showToast(logs, getApplicationContext());
        }

    };

}
