package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;

public class RechargeActivity extends BaseActivity {

    protected ImageView mImgAddpackageBack;
    protected RelativeLayout mRlBack;
    protected EditText mEditThevouchersName;
    protected CheckBox mCbWeixin;
    protected LinearLayout mLlWeixin;
    protected View mVLine;
    protected CheckBox mCbAlipay;
    protected LinearLayout mLlAlipay;
    protected View mVLine2;
    protected CheckBox mCbBank;
    protected LinearLayout mLlBank;
    protected Button mBtnNowRecharge;
    protected LinearLayout mActivityRecharge;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_recharge);
        initView();
        mLlAlipay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                mCbAlipay.setChecked(true);
                mCbBank.setChecked(false);
                mCbWeixin.setChecked(false);

            }
        });
        mLlWeixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCbAlipay.setChecked(false);
                mCbBank.setChecked(false);
                mCbWeixin.setChecked(true);

            }
        });
        mLlBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCbAlipay.setChecked(false);
                mCbBank.setChecked(true);
                mCbWeixin.setChecked(false);

            }
        });
    }

    private void initView() {
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mEditThevouchersName = (EditText) findViewById(R.id.edit_thevouchers_name);
        mCbWeixin = (CheckBox) findViewById(R.id.cb_weixin);
        mLlWeixin = (LinearLayout) findViewById(R.id.ll_weixin);
        mVLine = (View) findViewById(R.id.v_line);
        mCbAlipay = (CheckBox) findViewById(R.id.cb_Alipay);
        mLlAlipay = (LinearLayout) findViewById(R.id.ll_Alipay);
        mVLine2 = (View) findViewById(R.id.v_line2);
        mCbBank = (CheckBox) findViewById(R.id.cb_bank);
        mLlBank = (LinearLayout) findViewById(R.id.ll_bank);
        mBtnNowRecharge = (Button) findViewById(R.id.btn_now_recharge);
        mActivityRecharge = (LinearLayout) findViewById(R.id.activity_recharge);
    }
}
