package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

public class AddGroupActivity extends BaseActivity implements View.OnClickListener {
    private static final int ADDNEWGROUP=1;
    protected RelativeLayout mRlBack;
    protected TextView mTextStorPhon;
    protected EditText mEdiSetPw;
    protected Button mBtnSignOut;
    private String addw;
    private  static final  int ADDGTOADD=2;
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_add_group);
        initView();
         Intent intent = getIntent();
         addw = intent.getStringExtra("addw");
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {
            if(addw.equals("1")){
                Intent intent=new Intent(AddGroupActivity.this,ChoiceGroupActivity.class);
                setResult(ADDNEWGROUP,intent);
                finish();
            }else if(addw.equals("2")){
                Intent intent=new Intent(AddGroupActivity.this,GroupManagementActivity.class);
                setResult(ADDGTOADD,intent);
                finish();
            }

        } else if (view.getId() == R.id.btn_sign_out) {
                saveGroup();
        }
    }
    /**
     * 保存分组
     */
    private void saveGroup(){

        if(TextUtils.isEmpty(mEdiSetPw.getText().toString())){
            ToastUtil.showShort(getApplicationContext(),"请填写分组名");
        }else {
            OkGo.post(HttpAPI.ADDGROUP)
                    .params("shopid", UserUtils.Shopid(getApplicationContext()))
                    .params("user_id",UserUtils.Userid(getApplicationContext()))
                    .params("user_token",UserUtils.UserToken(getApplicationContext()))
                    .params("name",mEdiSetPw.getText().toString())
                    .execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                String status = jsonObject.getString("status");
                                if(status.equals("1")){
                                    String msg = jsonObject.getString("msg");
                                    ToastUtil.showShort(getApplicationContext(),msg);
                                    if(addw.equals("1")){
                                        Intent intent=new Intent(AddGroupActivity.this,ChoiceGroupActivity.class);
                                        setResult(ADDNEWGROUP,intent);
                                        finish();
                                    }else if(addw.equals("2")){
                                        Intent intent=new Intent(AddGroupActivity.this,GroupManagementActivity.class);
                                        setResult(ADDGTOADD,intent);
                                        finish();
                                    }

                                }else if(status.equals("0")){
                                    ToastUtil.showShort(getApplicationContext(),"保存失败");
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Call call, Response response, Exception e) {
                            super.onError(call, response, e);
                            Log.e("保存分组",e.getMessage().toString());
                        }
                    });
        }
    }
    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(AddGroupActivity.this);
        mTextStorPhon = (TextView) findViewById(R.id.text_stor_phon);
        mEdiSetPw = (EditText) findViewById(R.id.edi_set_pw);
        mBtnSignOut = (Button) findViewById(R.id.btn_sign_out);
        mBtnSignOut.setOnClickListener(AddGroupActivity.this);
    }
}
