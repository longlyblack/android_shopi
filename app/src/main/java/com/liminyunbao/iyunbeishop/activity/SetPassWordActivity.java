package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

public class SetPassWordActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgGroupmanagementBack;
    protected TextView mTextStorPhon;
    protected EditText mEdiSetPw;
    protected TextView mTextStorPhon1;
    protected EditText mEdiSetPwAgain;
    protected Button mSaver;
    protected RelativeLayout mRlBack;

    private String mobile;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_set_pass_word);
        initView();
        Intent intent = getIntent();
        mobile = intent.getStringExtra("mobile");
        mSaver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sure();
            }
        });
    }

    /**
     * 确认修改密码
     */

    private void sure() {
        if (TextUtils.isEmpty(mEdiSetPw.getText().toString())) {
            Toast.makeText(getApplicationContext(), "请输入新的密码", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(mEdiSetPwAgain.getText().toString())) {
            Toast.makeText(getApplicationContext(), "请输入确认密码", Toast.LENGTH_SHORT).show();
        } else {
            OkGo.post(HttpAPI.SUREPW)
                    .params("mobile", mobile)
                    .params("pwd1", mEdiSetPw.getText().toString())
                    .params("pwd2", mEdiSetPwAgain.getText().toString())
                    .execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                String status = jsonObject.getString("status");
                                JSONObject data = jsonObject.getJSONObject("data");
                                String info = data.getString("info");
                                if (status.equals("0")) {
                                    Toast.makeText(getApplicationContext(), info, Toast.LENGTH_SHORT).show();
                                } else if (status.equals("1")) {
                                    Toast.makeText(getApplicationContext(), info, Toast.LENGTH_SHORT).show();
                                    Intent inten = new Intent(getApplicationContext(), SignInActivity.class);
                                    startActivity(inten);
                                    Intent intent = new Intent();
                                    intent.setAction(BaseActivity.SYSTEM_EXIT);
                                    sendBroadcast(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(Call call, Response response, Exception e) {
                            super.onError(call, response, e);
                            Log.e("baocun", e.getMessage().toString());
                        }
                    });
        }

    }

    private void initView() {
        mImgGroupmanagementBack = (ImageView) findViewById(R.id.img_groupmanagement_back);
        mImgGroupmanagementBack.setOnClickListener(SetPassWordActivity.this);
        mTextStorPhon = (TextView) findViewById(R.id.text_stor_phon);
        mEdiSetPw = (EditText) findViewById(R.id.edi_set_pw);
        mTextStorPhon1 = (TextView) findViewById(R.id.text_stor_phon1);
        mEdiSetPwAgain = (EditText) findViewById(R.id.edi_set_pw_again);
        mSaver = (Button) findViewById(R.id.saver);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(SetPassWordActivity.this);
    }

    @Override
    public void onClick(View view) {
      if (view.getId() == R.id.rl_back) {
            finish();
        }
    }
}
