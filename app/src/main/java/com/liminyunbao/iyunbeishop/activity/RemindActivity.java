package com.liminyunbao.iyunbeishop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.utils.SocializeUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

public class RemindActivity extends BaseActivity implements View.OnClickListener {
    protected ImageView mImgSelectAccountBack;
    protected Button mBtnBandweixin;
    private String openID;
    private String screenName;
    private String userid;
    private int statussCode = 6;
    private ProgressDialog dialog;
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_remind);
        initView();
        dialog = new ProgressDialog(this);
        dialog.setMessage("请稍等...");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UMShareAPI.get(this).release();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        UMShareAPI.get(this).onSaveInstanceState(outState);
    }


    private void bandWeix() {
        SHARE_MEDIA platform = SHARE_MEDIA.WEIXIN;
        UMShareAPI.get(this).getPlatformInfo(this, platform, new UMAuthListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                SocializeUtils.safeShowDialog(dialog);
                Log.e("onStart","微信调用开始");
            }

            @Override
            public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {
                Log.e("获取的信息", "用户名：" + map.get("screen_name"));
                Log.e("获取的信息", "用户openid" + map.get("openid"));
                screenName = map.get("screen_name");
                openID = map.get("openid");
                SharePUtile.put(getApplicationContext(), "openid", openID);
                SharePUtile.put(getApplicationContext(), "screenName", screenName);
                initWatBind(screenName, openID);
                SocializeUtils.safeCloseDialog(dialog);

            }

            @Override
            public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "请安装微信客户端", Toast.LENGTH_SHORT).show();
                SocializeUtils.safeCloseDialog(dialog);
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media, int i) {
                SocializeUtils.safeCloseDialog(dialog);
            }
        });
    }

    private void initWatBind(String screenName, String openID) {
        userid = (String) SharePUtile.get(getBaseContext(), "user_id", "");
        String shopid = (String) SharePUtile.get(getBaseContext(), "shop_id", "");
        String user_token = (String) SharePUtile.get(getBaseContext(), "user_token", "");
        OkGo.post(HttpAPI.BINDING)
                .params("user_id", userid)
                .params("user_token", user_token)
                .params("shopid", shopid)
                .params("openid", openID)
                .params("weixinname", screenName).execute(new StringCallback() {
            @Override
            public void onSuccess(String s, Call call, Response response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);
                    String status = jsonObject.getString("status");
                    String msg = jsonObject.getString("msg");
                    if (status.equals("2")) {
                        TokenUtils.TackUserToken(RemindActivity.this);
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("status", status);
                        intent.putExtra("msg", msg);
                        setResult(statussCode, intent);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_bandweixin) {
            bandWeix();
        }
    }

    private void initView() {
        mImgSelectAccountBack = (ImageView) findViewById(R.id.img_select_account_back);
        mBtnBandweixin = (Button) findViewById(R.id.btn_bandweixin);
        mBtnBandweixin.setOnClickListener(RemindActivity.this);
    }
}
