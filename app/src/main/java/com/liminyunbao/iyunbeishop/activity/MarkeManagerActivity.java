package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.liminyunbao.iyunbeishop.R;

/**
 * 营销管理
 */
public class MarkeManagerActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgAddpackageBack;
    protected RelativeLayout mRlBack;
    protected ImageView mImgActiv;
    protected ImageView mReght;
    protected ImageView mImgNewmsg;
    protected RelativeLayout mRlActiv;
    protected ImageView mImgRecommend;
    protected RelativeLayout mRlRecommend;
    protected ImageView mImgChuchuang;
    protected RelativeLayout mRlChuchuang;
    protected ImageView imgCardCoupons;
    protected RelativeLayout rlCardCoupons;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_marke_manager);
        initView();
        Intent intent = getIntent();
        String wtuij = intent.getStringExtra("WTUIJ");
        if (wtuij.equals("1")) {
            mRlChuchuang.setVisibility(View.VISIBLE);
            mRlRecommend.setVisibility(View.GONE);
        } else if (wtuij.equals("2")) {
            mRlChuchuang.setVisibility(View.GONE);
            mRlRecommend.setVisibility(View.VISIBLE);
        }


    }


    /***
     * 点击事件
     * @param view
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {

            finish();
        } else if (view.getId() == R.id.rl_activ) {


        } else if (view.getId() == R.id.rl_recommend) {
            Intent intent = new Intent(getApplicationContext(), PackageRecommendationActivity.class);
            startActivity(intent);

        } else if (view.getId() == R.id.rl_chuchuang) {

            Intent intent = new Intent(getApplicationContext(), DisplayWindowActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.rl_cardCoupons) {

            Intent intent =new Intent(getApplicationContext(),CardCouponsManagerActivity.class);
            startActivity(intent);
        }
    }

    private void initView() {
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(MarkeManagerActivity.this);
        mImgActiv = (ImageView) findViewById(R.id.img_activ);
        mReght = (ImageView) findViewById(R.id.reght);
        mImgNewmsg = (ImageView) findViewById(R.id.img_newmsg);
        mRlActiv = (RelativeLayout) findViewById(R.id.rl_activ);
        mRlActiv.setOnClickListener(MarkeManagerActivity.this);
        mImgRecommend = (ImageView) findViewById(R.id.img_recommend);
        mRlRecommend = (RelativeLayout) findViewById(R.id.rl_recommend);
        mRlRecommend.setOnClickListener(MarkeManagerActivity.this);
        mImgChuchuang = (ImageView) findViewById(R.id.img_chuchuang);
        mRlChuchuang = (RelativeLayout) findViewById(R.id.rl_chuchuang);
        mRlChuchuang.setOnClickListener(MarkeManagerActivity.this);
        imgCardCoupons = (ImageView) findViewById(R.id.img_cardCoupons);
        rlCardCoupons = (RelativeLayout) findViewById(R.id.rl_cardCoupons);
        rlCardCoupons.setOnClickListener(MarkeManagerActivity.this);
    }
}
