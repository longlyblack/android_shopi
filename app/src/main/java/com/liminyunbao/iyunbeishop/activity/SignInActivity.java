package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.bean.SiginInBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import okhttp3.Call;
import okhttp3.Response;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    protected EditText mEdiPhoneNum;
    protected EditText mEdiMima;
    protected Button mBtnSignin;
    protected Button mBunZhuce;
    protected TextView mTvForget;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_sign_in);
        initView();
        initVerson();
        if (SharePUtile.get(getBaseContext(), "user_id", "") != "" && SharePUtile.get(getBaseContext(), "user_token", "") != "") {
            Intent i = new Intent(SignInActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }

        mBtnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                httpq(mEdiPhoneNum.getText().toString(), mEdiMima.getText().toString());
               /* if(MobileUtils.isMobile(mEdiPhoneNum.getText().toString())){
                   //
                    Toast.makeText(getApplicationContext(), "手机正确！", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(), "手机号码格式不正确！", Toast.LENGTH_SHORT).show();
                }*/
            }
        });
    }


    private void httpq(String username, String pwd) {
        OkGo.post(HttpAPI.LOGIN)
                .tag(this)
                .connTimeOut(10000)
                .params("username", username)
                .params("pwd", pwd).execute(new StringCallback() {
            @Override
            public void onError(Call call, Response response, Exception e) {
                super.onError(call, response, e);
                Log.e("这是成功的",e.getMessage().toString());
            }

            @Override
            public void onSuccess(String s, Call call, Response response) {
                Log.e("这是成功的", s);
                Gson gson = new Gson();
                SiginInBean siginIn = gson.fromJson(s, SiginInBean.class);
                String status = siginIn.getStatus();
                if (status.equals("1")) {
                    SharePUtile.put(getBaseContext(), "shop_typ", 0);
                    SharePUtile.put(getBaseContext(), "selection", 0);
                    SharePUtile.put(getBaseContext(), "shopname", "");
                    SharePUtile.put(getBaseContext(), "isselection", false);
                    SharePUtile.put(getBaseContext(), "shop_id", "");
                    SharePUtile.put(getBaseContext(), "backtwo", 1);
                    SharePUtile.put(getBaseContext(), "user_id", siginIn.getData().getUser_id());
                    SharePUtile.put(getBaseContext(), "user_token", siginIn.getData().getUser_token());
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                } else if (status.equals("0")) {
                    Toast.makeText(getApplicationContext(), "手机号或密码错误！", Toast.LENGTH_SHORT).show();
                }

               /* try {
                    JSONObject jsonObject=new JSONObject(s);
                    String stauts = jsonObject.getString("status");
                    JSONObject data = jsonObject.getJSONObject("data");
                    String info = data.getString("info");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

*/

            }
        });
    }

    private void initVerson() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //透明导航栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            // 激活状态栏
            tintManager.setStatusBarTintEnabled(true);
            // enable navigation bar tint 激活导航栏
            tintManager.setNavigationBarTintEnabled(true);
            //设置系统栏设置颜色
            //tintManager.setTintColor(R.color.red);
            //给状态栏设置颜色
            tintManager.setStatusBarTintResource(R.color.colorDingLine);
            //Apply the specified drawable or color resource to the system navigation bar.
            //给导航栏设置资源
        }
    }

    private void initView() {
        mEdiPhoneNum = (EditText) findViewById(R.id.edi_phone_num);
        mEdiMima = (EditText) findViewById(R.id.edi_mima);
        mBtnSignin = (Button) findViewById(R.id.btn_signin);
        mBunZhuce = (Button) findViewById(R.id.bun_zhuce);
        mBunZhuce.setOnClickListener(SignInActivity.this);
        mTvForget = (TextView) findViewById(R.id.tv_forget);
        mTvForget.setOnClickListener(SignInActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.bun_zhuce) {
            Intent intent = new Intent(SignInActivity.this, RegisterActivity.class);
            startActivity(intent);

        } else if (view.getId() == R.id.tv_forget) {
            Intent intent=new Intent(SignInActivity.this,ForgetPwActivity.class);
            startActivity(intent);
        }
    }
}
