package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

/**
 * 卡券管理
 */
public class CardCouponsManagerActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView imgAddpackageBack;
    protected RelativeLayout rlBack;
    protected LinearLayout activityCardCouponsManager;
    protected ImageView imgFullCut;
    protected TextView tvFullCut;
    protected RelativeLayout rlFullCut;
    protected ImageView imgFullFolding;
    protected TextView tvFullFolding;
    protected RelativeLayout rlFullFolding;
    protected ImageView imgFullFree;
    protected TextView tvFullFree;
    protected RelativeLayout rlFullFree;
    protected ImageView imgGift;
    protected TextView tvCardCouponsManager;
    protected RelativeLayout rlGift;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_card_coupons_manager);
        initView();
    }
    private void initView() {
        imgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        rlBack = (RelativeLayout) findViewById(R.id.rl_back);
        rlBack.setOnClickListener(CardCouponsManagerActivity.this);
        activityCardCouponsManager = (LinearLayout) findViewById(R.id.activity_card_coupons_manager);
        imgFullCut = (ImageView) findViewById(R.id.img_full_cut);
        tvFullCut = (TextView) findViewById(R.id.tv_full_cut);
        rlFullCut = (RelativeLayout) findViewById(R.id.rl_full_cut);
        rlFullCut.setOnClickListener(CardCouponsManagerActivity.this);
        imgFullFolding = (ImageView) findViewById(R.id.img_full_folding);
        tvFullFolding = (TextView) findViewById(R.id.tv_full_folding);
        rlFullFolding = (RelativeLayout) findViewById(R.id.rl_full_folding);
        rlFullFolding.setOnClickListener(CardCouponsManagerActivity.this);
        imgFullFree = (ImageView) findViewById(R.id.img_full_free);
        tvFullFree = (TextView) findViewById(R.id.tv_full_free);
        rlFullFree = (RelativeLayout) findViewById(R.id.rl_full_free);
        rlFullFree.setOnClickListener(CardCouponsManagerActivity.this);
        imgGift = (ImageView) findViewById(R.id.img_gift);
        tvCardCouponsManager = (TextView) findViewById(R.id.tv_card_coupons_manager);
        rlGift = (RelativeLayout) findViewById(R.id.rl_gift);
        rlGift.setOnClickListener(CardCouponsManagerActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {
            finish();
        } else if (view.getId() == R.id.rl_full_cut) {
            //满减券
            Intent intent=new Intent(getApplicationContext(),FullCutActivity.class);
            startActivity(intent);

        } else if (view.getId() == R.id.rl_full_folding) {
            //满折
            Intent intent=new Intent(getApplicationContext(),FullFoldingActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.rl_full_free) {
            //满免
            Intent intent=new Intent(getApplicationContext(),FullFreeShoppingActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.rl_gift) {
            //礼品
            Intent intent=new Intent(getApplicationContext(),FullGiftActivity.class);
           // Intent intent=new Intent(getApplicationContext(),RechargeActivity.class);
            startActivity(intent);
        }
    }
}
