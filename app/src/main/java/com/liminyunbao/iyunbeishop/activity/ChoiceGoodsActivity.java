package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.ChoiceGoodsAdapter;
import com.liminyunbao.iyunbeishop.bean.SaleBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

public class ChoiceGoodsActivity extends BaseActivity {
    private final int CHOICEGOODS = 157;
    protected ImageView mImgAddpackageBack;
    protected RelativeLayout mRlBack;
    protected PullToRefreshListView mListvFullCutHang;
    protected LinearLayout mActivityChoiceGoods;
    private Map<Integer, Boolean> mIsSelectther;
    private int last_page = 0;
    private int page = 1;
    private List<SaleBean.MassageBean.GoodsList> mAlldatamsg;
    private ChoiceGoodsAdapter mAdapter;
    private String goddsid;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_choice_goods);
        initView();
        mAlldatamsg = new ArrayList<>();
         pullfresh();
        inithttp();
        mListvFullCutHang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
              int position=i-1;
                mAdapter.isSelected.put(position,true);
                goddsid=mAlldatamsg.get(position).getBase_goods_id()+"";
                String goodsname=mAlldatamsg.get(position).getTitle();
                Intent intent=new Intent();
                intent.putExtra("goodsid",goddsid);
                intent.putExtra("goodsname",goodsname);
                setResult(CHOICEGOODS,intent);
                finish();
            }
        });
    }

    private void inithttp() {
        OkGo.post(HttpAPI.SHANGPINLIST)
                .connTimeOut(10000)
                .params("cmd", "1")
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("page", page + "")
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);
                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("商品", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                JSONObject data = jsonObject.getJSONObject("data");
                                last_page = data.getInt("last_page");
                                Gson gson = new Gson();
                                SaleBean saleBean = gson.fromJson(s, SaleBean.class);
                                List<SaleBean.MassageBean.GoodsList> datamsg = saleBean.getData().getData();
                                mAlldatamsg.addAll(datamsg);
                                mIsSelectther = new HashMap<Integer, Boolean>();
                                mAdapter = new ChoiceGoodsAdapter(mAlldatamsg,getApplicationContext(),mIsSelectther);
                                mAdapter.setLists(mAlldatamsg);
                                mListvFullCutHang.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();
                                mListvFullCutHang.onRefreshComplete();
                            } else if (status.equals("0")) {
                                Toast.makeText(getApplicationContext(), "暂无数据！", Toast.LENGTH_SHORT).show();
                              mListvFullCutHang.onRefreshComplete();
                                mAdapter.notifyDataSetChanged();
                            } else if (status.equals("2")) {
                                TokenUtils.TackUserToken(ChoiceGoodsActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getApplicationContext(), "请求超时！", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);

                    }
                });

    }

    private void pullfresh() {

        mListvFullCutHang.setMode(PullToRefreshBase.Mode.BOTH);
        mListvFullCutHang.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

                mAlldatamsg.clear();
                page = 1;
                inithttp();

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (page != last_page) {
                    page++;
                    inithttp();
                } else {
                    mListvFullCutHang.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvFullCutHang.onRefreshComplete();
                        }
                    }, 1000);
                }

            }
        });


        mListvFullCutHang.setAdapter(mAdapter);
        mAdapter.setChickbox(new ChoiceGoodsAdapter.chickbox() {
            @Override
            public void ischickbox(CheckBox checkBox, final int posion) {
                                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                            @Override
                                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                if(b){
                                                    mAdapter.isSelected.put(posion,true);
                                                    goddsid=mAlldatamsg.get(posion).getBase_goods_id()+"";
                                                    String goodsname=mAlldatamsg.get(posion).getTitle();
                                                    Intent intent=new Intent();
                                                    intent.putExtra("goodsid",goddsid);
                                                    intent.putExtra("goodsname",goodsname);
                                                    setResult(CHOICEGOODS,intent);
                                                    finish();
                                                }
                                            }
                                        });
            }
        });
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate = new Date(System.currentTimeMillis());
        String time = format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvFullCutHang.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mAdapter.notifyDataSetChanged();

    }

    private void initView() {
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mListvFullCutHang = (PullToRefreshListView) findViewById(R.id.listv_full_cut_hang);
        mActivityChoiceGoods = (LinearLayout) findViewById(R.id.activity_choice_goods);

    }
}
