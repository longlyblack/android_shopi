package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

public class AddGiftActivity extends BaseActivity implements View.OnClickListener {

    private final  int  CHOICEGOODS= 157;
    protected ImageView mImgAddpackageBack;
    protected RelativeLayout mRlBack;
    protected ImageView mImgRight;
    protected TextView mTvSelecname;
    protected EditText mEditThevouchersName;
    protected CheckBox mCbLong;
    protected LinearLayout mLlLong;
    protected View mVLine;
    protected CheckBox mCbTerm;
    protected TextView mTvSelfetime;
    protected LinearLayout mLlTerm;
    protected EditText mEditStock;
    protected EditText mEditFreight;
    protected TextView mTvXuzhiNum;
    protected Button mBtnTemporary;
    protected Button mBtnImmediately;
    protected LinearLayout mLlSelectGoods;
    protected LinearLayout mActivityAddGift;
    private  String goodsid=null;

    /**
     * 添加礼品券
     */
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_add_gift);
        initView();
        mLlSelectGoods .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),ChoiceGoodsActivity.class);
                startActivityForResult(intent ,CHOICEGOODS);
            }
        });


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode ==CHOICEGOODS){
            goodsid = data.getStringExtra("goodsid");
            String goodsname = data.getStringExtra("goodsname");
            mTvSelecname.setText(goodsname);
        }



    }

    private void initView() {
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(AddGiftActivity.this);
        mImgRight = (ImageView) findViewById(R.id.img_right);
        mTvSelecname = (TextView) findViewById(R.id.tv_selecname);
        mEditThevouchersName = (EditText) findViewById(R.id.edit_thevouchers_name);
        mCbLong = (CheckBox) findViewById(R.id.cb_long);
        mLlLong = (LinearLayout) findViewById(R.id.ll_long);
        mVLine = (View) findViewById(R.id.v_line);
        mCbTerm = (CheckBox) findViewById(R.id.cb_term);
        mTvSelfetime = (TextView) findViewById(R.id.tv_selfetime);
        mLlTerm = (LinearLayout) findViewById(R.id.ll_term);
        mEditStock = (EditText) findViewById(R.id.edit_stock);
        mEditFreight = (EditText) findViewById(R.id.edit_freight);
        mTvXuzhiNum = (TextView) findViewById(R.id.tv_xuzhi_num);
        mBtnTemporary = (Button) findViewById(R.id.btn_temporary);
        mBtnImmediately = (Button) findViewById(R.id.btn_immediately);
        mLlSelectGoods = (LinearLayout) findViewById(R.id.ll_select_goods);
        mActivityAddGift = (LinearLayout) findViewById(R.id.activity_add_gift);
    }
}
