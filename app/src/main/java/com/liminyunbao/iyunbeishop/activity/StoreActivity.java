package com.liminyunbao.iyunbeishop.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.PictureUtil;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.bean.AnyEventType;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.AboutBitMap;
import com.liminyunbao.iyunbeishop.utils.GetVersionCode;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.liminyunbao.iyunbeishop.view.SelectPicPopupWindow;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.umeng.socialize.shareboard.ShareBoardConfig;
import com.umeng.socialize.shareboard.SnsPlatform;
import com.umeng.socialize.utils.ShareBoardlistener;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 店铺管理
 */

public class StoreActivity extends BaseActivity implements View.OnClickListener {
    protected static final int CHOOSE_PICTURE = 17;
    protected static final int TAKE_PICTURE = 16;
    private static final int PERMISSION_CORDD = 15;
    protected ImageView mImgStoreBack;
    protected ImageView mImgLl;
    protected TextView mTvStoreName;
    protected RelativeLayout mRlStoreName;
    protected TextView mTvLeimuName;
    protected RelativeLayout mRlLeimu;
    protected ImageView mImgLl3;
    protected TextView mTvPositionName;
    protected RelativeLayout mRlAddress;
    protected TextView mTvPersonName;
    protected RelativeLayout mRlPersonName;
    protected TextView mTvPersonPhone;
    protected RelativeLayout mRlPersonPohne;
    protected TextView mTvBandwx;
    protected RelativeLayout mRlBandWx;
    protected RelativeLayout mRlChangeStore;
    protected Button mBtnSignOut;
    protected ImageView mImgPeoplename;
    protected ImageView mImgPhone;
    protected TextView mVersionName;
    protected RelativeLayout mRlShaoppaizi;
    protected ImageView mImgDianzhao;
    protected ImageView mImgRigg;
    protected ImageView mImgShare;
    protected RelativeLayout mRlWxShare;
    protected LinearLayout mXiangji;
    protected ImageView mImgFreight;
    protected TextView mTvFreightName;
    protected RelativeLayout mRlFreight;
    private String user_token;
    private String userid;
    private String addres;
    private String mapaddr;
    private String nickname;
    private String shopname;
    private String account;
    private Dialog mDialog;
    private File currentImageFile;
    private String mapurl;
    private String getUrl = null;
    private String dianzhao;
    private  String yb_free;//
    String picHead = "data:image/jpeg;base64,"; //base 64图片字节头
    private KyLoadingBuilder builder;

    private String Shareurl;
    private String Shopurl = "https://static.iyunbei.net/res/images/mobile/shop1/shop.jpg";
    private String lng;
    private String lat;
    private ShareAction mShareAction;

    @Override
    protected void HaveNet() {
        HttpData();
    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(), "你进入没有网的异次元！", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorDianPuB));
        }
        super.setContentView(R.layout.activity_store);
        user_token = (String) SharePUtile.get(getApplicationContext(), "user_token", "");
        userid = (String) SharePUtile.get(getApplicationContext(), "user_id", "");
        initView();
        HttpData();
        Aimoi();
        EventBus.getDefault().register(this);
        //版本号
        mVersionName.setText("Beat." + GetVersionCode.getAPPVersionCodeFromAPP(getApplicationContext()));
        mShareAction = new ShareAction(StoreActivity.this)
                .setDisplayList(SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE)
                .setShareboardclickCallback(new ShareBoardlistener() {
                    @Override
                    public void onclick(SnsPlatform snsPlatform, SHARE_MEDIA share_media) {
                        UMWeb web = new UMWeb(Shareurl);
                        web.setTitle("我的云贝网小店-" + shopname);
                        web.setDescription("快来进店参观吧！");
                        web.setThumb(new UMImage(StoreActivity.this, Shopurl));
                        new ShareAction(StoreActivity.this).withMedia(web)
                                .setPlatform(share_media)
                                .setCallback(new UMShareListener() {
                                    @Override
                                    public void onStart(SHARE_MEDIA share_media) {

                                        //   ToastUtil.showShort(getApplicationContext(),"开始分享分享");
                                    }

                                    @Override
                                    public void onResult(SHARE_MEDIA share_media) {

                                        ToastUtil.showShort(getApplicationContext(), "分享成功");

                                    }

                                    @Override
                                    public void onError(SHARE_MEDIA share_media, Throwable throwable) {

                                        ToastUtil.showShort(getApplicationContext(), "分享失败");
                                    }

                                    @Override
                                    public void onCancel(SHARE_MEDIA share_media) {

                                        ToastUtil.showShort(getApplicationContext(), "取消分享");
                                    }
                                })
                                .share();
                    }
                });
        mRlWxShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareBoardConfig config = new ShareBoardConfig();
                config.setShareboardPostion(ShareBoardConfig.SHAREBOARD_POSITION_CENTER);
                config.setMenuItemBackgroundShape(ShareBoardConfig.BG_SHAPE_CIRCULAR); // 圆角背景
                mShareAction.open(config);
                config.setTitleVisibility(false); // 隐藏title*/
                config.setCancelButtonVisibility(false); // 隐藏取消按钮
            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_store_back) {

            finish();
        } else if (view.getId() == R.id.rl_store_name) {

            Intent intent = new Intent(getBaseContext(), StoreMessageActivity.class);
            intent.putExtra("shopname", shopname);
            startActivity(intent);
        } else if (view.getId() == R.id.rl_leimu) {

        } else if (view.getId() == R.id.rl_address) {
            Intent intent = new Intent(getBaseContext(), StroeAddressActivity.class);
            intent.putExtra("addr", addres);
            if (!TextUtils.isEmpty(mapaddr) && mapaddr != null) {
                intent.putExtra("mapaddr", mapaddr);
            } else {
                intent.putExtra("mapaddr", "");
            }
            if(lat!=null){
                intent.putExtra("lat", lat);
            }else {
                intent.putExtra("lat", "");
            }
            if(lng!=null){
                intent.putExtra("lng", lng);
            }else {
                intent.putExtra("lng", "");
            }
            startActivity(intent);
        } else if (view.getId() == R.id.rl_person_name) {
            Intent intent = new Intent(getBaseContext(), ChangeNameActivity.class);
            intent.putExtra("nickname", nickname);
            startActivity(intent);
        } else if (view.getId() == R.id.rl_person_pohne) {
            Intent intent = new Intent(getBaseContext(), ChangeContactsActivity.class);
            intent.putExtra("account", account);
            startActivity(intent);
        } else if (view.getId() == R.id.rl_band_wx) {

        } else if (view.getId() == R.id.rl_change_store) {
            SharePUtile.put(getBaseContext(), "isselection", false);
            SharePUtile.get(getBaseContext(), "backtwo", 0);
            Intent intent2 = new Intent();
            intent2.setAction(BaseActivity.SYSTEM_EXIT);
            sendBroadcast(intent2);
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.btn_sign_out) {
            Logout();

        } else if (view.getId() == R.id.rl_shaoppaizi) {
            //店铺入招
            wahteGetpress();
        } else if (view.getId() == R.id.rl_freight) {

            Intent intent=new Intent(getApplicationContext(),FreightStringActivity.class);
            intent.putExtra("freight",yb_free);
            startActivity(intent);
        }
    }

    private void wahteGetpress() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permission = new ArrayList<>();
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.CAMERA);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (!permission.isEmpty()) {
                String[] permissons = permission.toArray(new String[permission.size()]);
                ActivityCompat.requestPermissions(this, permissons, PERMISSION_CORDD);
            } else {
                tackPhoto();
                //tackPhoto();
            }
        } else {

            tackPhoto();
            // tackPhoto();
        }
    }


    ////////////////////////////下面为相机和图片选择拍照功能部分(Modify By YangTK 2017.06.17)///////////////////////////////
    /**
     * 选择照片或者拍照
     */
    SelectPicPopupWindow menuWindow;

    private void tackPhoto() {
        menuWindow = new SelectPicPopupWindow(this, itemsOnClick);
        menuWindow.showAtLocation(findViewById(R.id.xiangji),
                Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);

    }


    private View.OnClickListener itemsOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            menuWindow.dismiss();
            switch (v.getId()) {
                case R.id.takePhotoBtn:     //选择相机拍照
//
                    AndPermission.with(StoreActivity.this)
                            .requestCode(100)
                            .permission(Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE)
//                            .rationale(rationaleListener)
                            .callback(listener)
                            .start();

                    break;
                case R.id.pickPhotoBtn:     //选择相册
                    openPic();
                    break;
                default:
                    break;
            }
        }
    };

    private int REQUEST_CODE_SETTING = 300;
    private PermissionListener listener = new PermissionListener() {
        @Override
        public void onSucceed(int requestCode, List<String> grantedPermissions) {
            // 权限申请成功回调。

            // 这里的requestCode就是申请时设置的requestCode。
            // 和onActivityResult()的requestCode一样，用来区分多个不同的请求。
            if (requestCode == 100) {
                // TODO ...
                openCamera();
            }
        }

        @Override
        public void onFailed(int requestCode, List<String> deniedPermissions) {
            // 权限申请失败回调。
            if (requestCode == 100) {
                // TODO ...
                // 第一种：用默认的提示语。
                AndPermission.defaultSettingDialog(StoreActivity.this, REQUEST_CODE_SETTING).show();
            }
        }
    };


    /**
     * 调用相机     相机拍照之后 一般都是一个  时间.jpg 文件
     */
    private File mFile;
    /**
     * 定义三种状态
     */
    private static final int REQUESTCODE_PIC = 1;//相册
    private static final int REQUESTCODE_CAM = 2;//相机
    private static final int REQUESTCODE_CUT = 3;//图片裁剪

    private void openCamera() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            if (!file.exists()) {
                file.mkdirs();
            }
            mFile = new File(file, System.currentTimeMillis() + ".jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mFile));
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            startActivityForResult(intent, REQUESTCODE_CAM);
        } else {
            Toast.makeText(this, "请确认已经插入SD卡", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 打开相册
     */
    private void openPic() {
        Intent picIntent = new Intent(Intent.ACTION_PICK, null);
        picIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(picIntent, REQUESTCODE_PIC);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {//返回码是可用的
            switch (requestCode) {
                case REQUESTCODE_CAM:
                    EventBus.getDefault().post(new AnyEventType("店招"));
                    final String absolutePath = mFile.getAbsolutePath();
                    final Bitmap bitmap = PictureUtil.compressSizeImage(absolutePath);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            setImageToView(bitmap);
                        }
                    }).start();
                    break;
                case REQUESTCODE_PIC:
                    Uri uri = data.getData();
                    EventBus.getDefault().post(new AnyEventType("店招"));
                    final String s = parsePicturePath(getBaseContext(), uri);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            setImageToView(PictureUtil.compressSizeImage(s));
                        }
                    }).start();
                    break;
            }

        }


    }


    /**
     * 图片上传
     */
    private void setImageToView(Bitmap bitmap) {

        Bitmap nbitmap = bitmap;
        mapurl = AboutBitMap.Bitmap2StrByBase64(nbitmap);
        OkGo.post(HttpAPI.PHOTO)
                .params("user_token", user_token)
                .params("base64", picHead + mapurl)
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);

                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        String substring = s.substring(1, s.length());
                        getUrl = substring;
                        EventBus.getDefault().post(new AnyEventType("嗯"));
                        String uurl = "http://img1.iyunbei.net/300x300/attachs/" + getUrl;
                        dianzhao = uurl;
                        Glide.with(getApplicationContext()).load(uurl).into(mImgDianzhao);
                        postImg(getUrl);
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);
                    }
                });
    }

    //上传店招图片
    private void postImg(String url) {
        OkGo.post(HttpAPI.SAVEING)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("img", url)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("上传店招图片", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                String msg = jsonObject.getString("msg");
                                // ToastUtil.showShort(getApplicationContext(), msg);
                            } else if (status.equals("0")) {
                                ToastUtil.showShort(getApplicationContext(), "店铺入招图片上传失败");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        HttpData();
    }

    private void Logout() {

        OkGo.post(HttpAPI.LOGOUT)
                .connTimeOut(100000)
                .params("user_id", userid)
                .params("user_token", user_token)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                JSONObject data = jsonObject.getJSONObject("data");
                                String info = data.getString("info");
                                Toast.makeText(getApplicationContext(), info, Toast.LENGTH_SHORT).show();
                                SharePUtile.put(getBaseContext(), "isselection", false);
                                SharePUtile.put(getBaseContext(), "backtwo", 0);
                                SharePUtile.put(getBaseContext(), "user_id", "");
                                SharePUtile.put(getBaseContext(), "user_token", "");
                                Intent intent3 = new Intent();
                                intent3.setAction(BaseActivity.SYSTEM_EXIT);
                                sendBroadcast(intent3);
                                Intent intent = new Intent(getBaseContext(), SignInActivity.class);
                                startActivity(intent);

                            } else if (status.equals("0")) {
                                Toast.makeText(getApplicationContext(), "退出失败！", Toast.LENGTH_SHORT).show();
                            } else if (status.equals("2")) {
                                TokenUtils.TackUserToken(StoreActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void HttpData() {
        final String shopid = (String) SharePUtile.get(getBaseContext(), "shop_id", "");
        OkGo.post(HttpAPI.INFORMATION)
                .params("user_id", userid)
                .params("shopid", shopid)
                .params("user_token", user_token)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("店铺信息", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                JSONObject data1 = jsonObject.getJSONObject("data");
                                String bang = data1.getString("bang");
                                mTvBandwx.setText(bang);
                                JSONObject data2 = data1.getJSONObject("data");
                                addres = data2.getString("addr");
                                mapaddr = data2.getString("mapaddr") + "";
                                shopname = data2.getString("shop_name");
                                int shoptype = data2.getInt("cate_type");
                                String photo = data2.getString("photo") + "";
                                dianzhao = "http://img1.iyunbei.net/300x300/attachs/" + photo;


                                yb_free = data2.getString("yb_free");
                                if(!TextUtils.isEmpty(yb_free)){
                                    mTvFreightName.setText(yb_free);
                                }else {
                                    mTvFreightName.setText("");
                                }


                                if (!photo.equals("null")) {
                                    Shopurl = dianzhao;
                                    Log.e("wozhixing", Shopurl);
                                    Glide.with(getApplicationContext()).load("http://img1.iyunbei.net/300x300/attachs/" + photo).into(mImgDianzhao);
                                }
                                account = data2.getString("mobile");
                                if (account.equals("0")) {
                                    mTvPersonPhone.setText("待完善");
                                } else {
                                    mTvPersonPhone.setText(account);
                                }
                                nickname = data2.getString("nickname");
                                // String cate_name = data2.getString("cate_name");
                                  lng = data2.getString("lng");
                                  lat = data2.getString("lat");
                                if (nickname != null) {
                                    if (nickname.equals("null")) {
                                        mTvPersonName.setText("待完善");
                                    } else {
                                        mTvPersonName.setText(nickname);
                                    }
                                } else {
                                    mTvPersonName.setText("无");
                                }

                                mTvPositionName.setText(addres);
                                mTvStoreName.setText(shopname);
                                Log.e("shop_type", shoptype + "");
                                if (shoptype == 0) {
                                    mTvLeimuName.setText("零售类");
                                    Shareurl = "http://www.iyunbei.net/mobile/shop1/detail.html?shop_id=" + shopid;

                                } else if (shoptype == 1) {
                                    mTvLeimuName.setText("服务类");
                                    Log.e("服务类", "" + shopid);
                                    Shareurl = "http://www.iyunbei.net/mobile/shop2/detail.html?shop_id=" + shopid;
                                }
                            } else if (status.equals("2")) {
                                TokenUtils.TackUserToken(StoreActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final AnyEventType bean) {
        //这里接受到消息
        Log.e("haha", bean.getS());
        if (bean.getS().equals("店招")) {
         /*   if (null==progressDialog){
                progressDialog = new ProgressDialog(this);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("加载中……");
            }
            progressDialog.show();*/
            builder.show();

        } else if (bean.getS().equals("嗯")) {
            builder.dismiss();
        }

    }

    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(StoreActivity.this);
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("上传中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }

    /**
     * 权限提醒
     * 权限被拒绝后 弹出提醒
     */
    private void Perssion() {

        new AlertDialog.Builder(this)
                .setMessage("请设置权限，否则无法正常使用！")
                .setPositiveButton("去设置", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("取消", null)
                .create()
                .show();
    }

    private void initView() {
        mImgStoreBack = (ImageView) findViewById(R.id.img_store_back);
        mImgStoreBack.setOnClickListener(StoreActivity.this);
        mImgLl = (ImageView) findViewById(R.id.img_ll);
        mTvStoreName = (TextView) findViewById(R.id.tv_store_name);
        mRlStoreName = (RelativeLayout) findViewById(R.id.rl_store_name);
        mRlStoreName.setOnClickListener(StoreActivity.this);
        mTvLeimuName = (TextView) findViewById(R.id.tv_leimu_name);
        mRlLeimu = (RelativeLayout) findViewById(R.id.rl_leimu);
        mRlLeimu.setOnClickListener(StoreActivity.this);
        mImgLl3 = (ImageView) findViewById(R.id.img_ll3);
        mTvPositionName = (TextView) findViewById(R.id.tv_position_name);
        mRlAddress = (RelativeLayout) findViewById(R.id.rl_address);
        mRlAddress.setOnClickListener(StoreActivity.this);
        mTvPersonName = (TextView) findViewById(R.id.tv_person_name);
        mRlPersonName = (RelativeLayout) findViewById(R.id.rl_person_name);
        mRlPersonName.setOnClickListener(StoreActivity.this);
        mTvPersonPhone = (TextView) findViewById(R.id.tv_person_phone);
        mRlPersonPohne = (RelativeLayout) findViewById(R.id.rl_person_pohne);
        mRlPersonPohne.setOnClickListener(StoreActivity.this);
        mTvBandwx = (TextView) findViewById(R.id.tv_bandwx);
        mRlBandWx = (RelativeLayout) findViewById(R.id.rl_band_wx);
        mRlBandWx.setOnClickListener(StoreActivity.this);
        mRlChangeStore = (RelativeLayout) findViewById(R.id.rl_change_store);
        mRlChangeStore.setOnClickListener(StoreActivity.this);
        mBtnSignOut = (Button) findViewById(R.id.btn_sign_out);
        mBtnSignOut.setOnClickListener(StoreActivity.this);
        mImgPeoplename = (ImageView) findViewById(R.id.img_peoplename);
        mImgPhone = (ImageView) findViewById(R.id.img_phone);
        mVersionName = (TextView) findViewById(R.id.versionName);
        mRlShaoppaizi = (RelativeLayout) findViewById(R.id.rl_shaoppaizi);
        mRlShaoppaizi.setOnClickListener(StoreActivity.this);
        mImgDianzhao = (ImageView) findViewById(R.id.img_dianzhao);
        mImgRigg = (ImageView) findViewById(R.id.img_rigg);
        mImgShare = (ImageView) findViewById(R.id.img_share);
        mRlWxShare = (RelativeLayout) findViewById(R.id.rl_wx_share);
        mXiangji = (LinearLayout) findViewById(R.id.xiangji);
        mImgFreight = (ImageView) findViewById(R.id.img_freight);
        mTvFreightName = (TextView) findViewById(R.id.tv_freight_name);
        mRlFreight = (RelativeLayout) findViewById(R.id.rl_freight);
        mRlFreight.setOnClickListener(StoreActivity.this);
    }

    /**
     * 解析获取图片库图片Uri物理路径
     *
     * @param context
     * @param uri
     * @return
     */

    @SuppressLint("NewApi")
    public String parsePicturePath(Context context, Uri uri) {

        if (null == context || uri == null)
            return null;

        boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentUri
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageDocumentsUri
            if (isExternalStorageDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                String[] splits = docId.split(":");
                String type = splits[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + File.separator + splits[1];
                }
            }
            // DownloadsDocumentsUri
            else if (isDownloadsDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaDocumentsUri
            else if (isMediaDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                String[] split = docId.split(":");
                String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                String selection = "_id=?";
                String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosContentUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;

    }

    private String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        String column = "_data";
        String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            try {
                if (cursor != null)
                    cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;

    }

    private boolean isExternalStorageDocumentsUri(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocumentsUri(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocumentsUri(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosContentUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
}
