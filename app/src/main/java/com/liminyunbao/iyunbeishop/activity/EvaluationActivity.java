package com.liminyunbao.iyunbeishop.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.adapter.EvaluationPagerAdapter;
import com.liminyunbao.iyunbeishop.fragment.AllEvauationFragment;
import com.liminyunbao.iyunbeishop.fragment.BadEvaluationFragment;
import com.liminyunbao.iyunbeishop.fragment.NoReplyFragment;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

public class EvaluationActivity extends BaseActivity implements View.OnClickListener {


    protected ViewPager mVpEvaluation;
    protected LinearLayout mActivityEvaluation;
    protected ImageView mImgStatiticsBack;
    protected TabLayout mTabEvaluation;
    protected SlidingTabLayout mSTabEvaluation;
    private EvaluationPagerAdapter mAdapter;
    private List<String> mList;

    private List<Fragment> mFragment;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    /**
     * 评价管理
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_evaluation);
        initView();
        initFragment();
    }

    private void initFragment() {
        mFragment = new ArrayList<>();
        mAdapter = new EvaluationPagerAdapter(getSupportFragmentManager());
        mFragment.add(AllEvauationFragment.newInstance());
        mFragment.add(BadEvaluationFragment.newInstance());
        mFragment.add(NoReplyFragment.newInstance());
        mAdapter.setFragments(mFragment);
        mVpEvaluation.setAdapter(mAdapter);
        mSTabEvaluation.setViewPager(mVpEvaluation);
        mVpEvaluation.setOffscreenPageLimit(3);
       /* mTabEvaluation.setupWithViewPager(mVpEvaluation);
        mTabEvaluation.setTabMode(TabLayout.MODE_FIXED);*/
        mVpEvaluation.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                //隐藏软件盘
                View mv = getWindow().peekDecorView();
                if (mv != null){
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(mv.getWindowToken(), 0);
                }
            }
        });
    }

    private void initView() {
        mVpEvaluation = (ViewPager) findViewById(R.id.vp_evaluation);
        mActivityEvaluation = (LinearLayout) findViewById(R.id.activity_evaluation);
        mImgStatiticsBack = (ImageView) findViewById(R.id.img_Statitics_back);
        mImgStatiticsBack.setOnClickListener(EvaluationActivity.this);
        //mTabEvaluation = (TabLayout) findViewById(R.id.tab_evaluation);
        mSTabEvaluation = (SlidingTabLayout) findViewById(R.id.sTab_evaluation);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_Statitics_back) {
            finish();
        }
    }
}
