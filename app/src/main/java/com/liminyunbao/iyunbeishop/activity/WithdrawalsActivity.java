package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.bean.MoneyInforBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 提现申请
 */

public class WithdrawalsActivity extends BaseActivity {

    protected ImageView mImgWithdrawalsBack;
    protected TextView mTextKetixian;
    protected TextView mTvWithdrawalsMoney;
    protected EditText mEdiWithdrawalsMoney;
    protected TextView mEdiWithdrawalsAccount;
    protected Button mBtnSureWithdrawals;
    protected Button mBtnAllmoney;
    protected TextView mTvTubiao;
    protected RelativeLayout mRlTixian;
    protected TextView mXinghao;
    protected TextView mEdiWithdrawalsWannBand;
    protected RelativeLayout mRlWeixinTixinm;
    private String userid;

    private int weixinCode = 7;

    @Override
    protected void HaveNet() {
        httpData();
        wahtband();
    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(), "你进入没有网的异次元！", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_withdrawals);
        initView();
        userid = (String) SharePUtile.get(getBaseContext(), "user_id", "");
        httpData();
        wahtband();
        Click();
    }

    /**
     * 是否绑定微信
     */
    private void wahtband() {
        String shopid = (String) SharePUtile.get(getBaseContext(), "shop_id", "");
        String user_token = (String) SharePUtile.get(getBaseContext(), "user_token", "");
        OkGo.post(HttpAPI.WITHDRAWALS)
                .params("user_id", userid)
                .params("shopid", shopid)
                .params("user_token", user_token)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                String msg = jsonObject.getString("msg");
                                mEdiWithdrawalsAccount.setVisibility(View.GONE);
                                mEdiWithdrawalsWannBand.setVisibility(View.VISIBLE);
                                mEdiWithdrawalsWannBand.setText("您已绑定微信账户");
                                mXinghao.setVisibility(View.GONE);
                            } else if (status.equals("0")) {
                                mRlWeixinTixinm.setVisibility(View.VISIBLE);
                                mRlTixian.setVisibility(View.GONE);
                                String msg = jsonObject.getString("msg");
                                mEdiWithdrawalsAccount.setText("您暂未绑定结算账户");
                                mXinghao.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(getApplicationContext(), SelectAccountActivity.class);
                                        startActivityForResult(intent, weixinCode);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    /**
     * 可提现资金
     */
    private void httpData() {
        String user_token = (String) SharePUtile.get(getBaseContext(), "user_token", "");
        String shopid = (String) SharePUtile.get(getBaseContext(), "shop_id", "");
        OkGo.post(HttpAPI.MONEYINFOR)
                .tag(this)
                .params("user_id", userid)
                .params("user_token", user_token)
                .params("shopid", shopid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                MoneyInforBean moneyInfor = gson.fromJson(s, MoneyInforBean.class);
                                mTvWithdrawalsMoney.setText(moneyInfor.getDate() + "");
                            } else if (status.equals("0")) {
                                mTvWithdrawalsMoney.setText("0");
                            } else if (status.equals("2")) {
                                TokenUtils.TackUserToken(WithdrawalsActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == weixinCode) {
            String status = data.getStringExtra("status");
            String msg = data.getStringExtra("msg");
            if (status.equals("3")) {
                mXinghao.setVisibility(View.GONE);
                mEdiWithdrawalsAccount.setVisibility(View.GONE);
                mEdiWithdrawalsWannBand.setVisibility(View.VISIBLE);
                mEdiWithdrawalsWannBand.setText(msg);
            } else if (status.equals("4")) {
                mXinghao.setVisibility(View.VISIBLE);
                mEdiWithdrawalsAccount.setVisibility(View.VISIBLE);
                mEdiWithdrawalsAccount.setText(msg);
            } else if (status.equals("1")) {
                mXinghao.setVisibility(View.GONE);
                mEdiWithdrawalsAccount.setVisibility(View.GONE);
                mEdiWithdrawalsWannBand.setVisibility(View.VISIBLE);
                mEdiWithdrawalsWannBand.setText(msg);
            }
        }


    }

    private void Click() {

        mImgWithdrawalsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mEdiWithdrawalsMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEdiWithdrawalsMoney.setCursorVisible(true);
                mEdiWithdrawalsMoney.setHint("");
            }
        });
        mBtnSureWithdrawals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
                mEdiWithdrawalsMoney.setText("");
            }
        });

        mBtnAllmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEdiWithdrawalsMoney.setText(mTvWithdrawalsMoney.getText());
            }
        });
    }

    /**
     * 确认提现资金
     */
    private void initData() {
        String shopid = (String) SharePUtile.get(getBaseContext(), "shop_id", "");
        String usertoken = (String) SharePUtile.get(getBaseContext(), "user_token", "");
        if (mEdiWithdrawalsMoney.getText().toString() != null) {
            OkGo.post(HttpAPI.QUETXIAN)
                    .params("user_id", userid)
                    .params("shopid", shopid)
                    .params("jine", mEdiWithdrawalsMoney.getText().toString())
                    .params("user_token", usertoken)
                    .execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                String status = jsonObject.getString("status");
                                if (status.equals("1")) {
                                    Toast.makeText(getApplicationContext(), "提现成功!", Toast.LENGTH_SHORT).show();
                                    httpData();
                                } else if (status.equals("0")) {
                                    Toast.makeText(getApplicationContext(), "提现失败!", Toast.LENGTH_SHORT).show();
                                } else if (status.equals("2")) {
                                    TokenUtils.TackUserToken(WithdrawalsActivity.this);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "请输入提现金额!", Toast.LENGTH_SHORT).show();
        }
    }

    private void initView() {
        mImgWithdrawalsBack = (ImageView) findViewById(R.id.img_withdrawals_back);
        mTextKetixian = (TextView) findViewById(R.id.text_ketixian);
        mTvWithdrawalsMoney = (TextView) findViewById(R.id.tv_withdrawals_money);
        mEdiWithdrawalsMoney = (EditText) findViewById(R.id.edi_withdrawals_money);
        mEdiWithdrawalsAccount = (TextView) findViewById(R.id.edi_withdrawals_account);
        mBtnSureWithdrawals = (Button) findViewById(R.id.btn_sure_withdrawals);
        mBtnAllmoney = (Button) findViewById(R.id.btn_allmoney);
        mTvTubiao = (TextView) findViewById(R.id.tv_tubiao);
        mRlTixian = (RelativeLayout) findViewById(R.id.rl_tixian);
        mXinghao = (TextView) findViewById(R.id.xinghao);
        mEdiWithdrawalsWannBand = (TextView) findViewById(R.id.edi_withdrawals_wannBand);
        mRlWeixinTixinm = (RelativeLayout) findViewById(R.id.rl_weixin_tixinm);
    }

 /*   @Override
    public void setFlag(String status, String msg) {
        if (status.equals("3")) {
            mXinghao.setVisibility(View.GONE);
            mEdiWithdrawalsAccount.setText(msg);
        } else if (status.equals("4")) {
            mXinghao.setVisibility(View.VISIBLE);
            mEdiWithdrawalsAccount.setText(msg);
        } else if (status.equals("1")) {
            mXinghao.setVisibility(View.GONE);
            mEdiWithdrawalsAccount.setText(msg);
        } else if (status.equals("2")) {
            TokenUtils.TackUserToken(WithdrawalsActivity.this);
        }
    }*/
}
