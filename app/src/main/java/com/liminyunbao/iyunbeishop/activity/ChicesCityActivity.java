package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.adapter.SortAdapter;
import com.liminyunbao.iyunbeishop.bean.CityName;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.custom.SideBar;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ChicesCityActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgChicesBack;
    protected ImageView mImgAddpic;
    protected TextView mTvCity;
    protected ListView mList;
    protected TextView mDialog;
    protected SideBar mSidebar;
    private int CITYCODE=2;
    private List<CityName.RECORDSBean> mCityNames;
    private SortAdapter mSortAdapter;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_chices_city);
        initJson();
        initView();
        GetCity();
        LetterComparator lc = new LetterComparator();
        Collections.sort(mCityNames, lc);
        mSortAdapter = new SortAdapter(this, mCityNames);
        mList.setAdapter(mSortAdapter);
        mSortAdapter.notifyDataSetChanged();
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              //  Toast.makeText(getApplicationContext(), mCityNames.get(position).getName() + "lat" + mCityNames.get(position).getLat(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.putExtra("cityname", mCityNames.get(position).getName());
                intent.putExtra("latitudes", mCityNames.get(position).getLat());
                intent.putExtra("longitudes", mCityNames.get(position).getLng());
                setResult(CITYCODE, intent);
                finish();


            }
        });
        initEvents();
        mSidebar.setTextView(mDialog);
    }

    private void GetCity() {
        Intent intent = getIntent();
        String city = intent.getStringExtra("city");
        Log.e("城市", city);
        mTvCity.setText(city);
    }

    private void initView() {
        mImgChicesBack = (ImageView) findViewById(R.id.img_chices_back);
        mImgChicesBack.setOnClickListener(ChicesCityActivity.this);
        mImgAddpic = (ImageView) findViewById(R.id.img_addpic);
        mTvCity = (TextView) findViewById(R.id.tv_city);
        mList = (ListView) findViewById(R.id.list);
        mDialog = (TextView) findViewById(R.id.dialog);
        mSidebar = (SideBar) findViewById(R.id.sidebar);
    }

    private void initEvents() {
        //设置右侧触摸监听
        mSidebar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = mSortAdapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    mList.setSelection(position);
                }
            }
        });
    }

    /**
     * 读取json文件
     */

    private void initJson() {
        mCityNames = new ArrayList<>();
        try {
            InputStreamReader isr = new InputStreamReader(getAssets().open("dbo_city.json"), "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(isr);
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line);
            }
            bufferedReader.close();
            isr.close();
            Gson gson = new Gson();
            CityName cityName = gson.fromJson(builder.toString(), CityName.class);
            mCityNames = cityName.getRECORDS();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private class LetterComparator implements Comparator<CityName.RECORDSBean> {
        @Override
        public int compare(CityName.RECORDSBean o1, CityName.RECORDSBean o2) {
            return Collator.getInstance().compare(o1.getFirst_letter(),
                    o2.getFirst_letter());
        }
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_chices_back) {
            finish();
        }
    }
}
