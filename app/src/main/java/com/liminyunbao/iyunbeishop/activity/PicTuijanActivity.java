package com.liminyunbao.iyunbeishop.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.GetPicture;
import com.liminyunbao.iyunbeishop.PictureUtil;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.PictureDisecbeAdapter;
import com.liminyunbao.iyunbeishop.bean.AnyEventType;
import com.liminyunbao.iyunbeishop.bean.MealDetailsBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.AboutBitMap;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.liminyunbao.iyunbeishop.view.SelectPicPopupWindow;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class PicTuijanActivity extends BaseActivity implements View.OnClickListener{
    private KyLoadingBuilder builder;
    private List<String> mList;
    private  int ADDPTUWEN=112;
    String picHead = "data:image/jpeg;base64,"; //base 64图片字节头
    private List<String> mSstring;//删除图片的额的额list
    protected RelativeLayout mRlBack;
    protected TextView mTvTitle;
    protected ListView mListvPicdec;
    protected Button mBtnSave;
    private String mapurl;
    private Dialog mDialog;
    private static final int PERMISSION_CORDD = 15;
    private List<GetPicture> mGetPictures=new ArrayList<>();
    private PictureDisecbeAdapter mDisecbeAdapter;
    private   List<MealDetailsBean.GoodsMsgBean.TuwenBean> tuwen;
    private List<MealDetailsBean.GoodsMsgBean.TuwenBean> mBean;
    protected Button mBtnAddpic;
    private int picnum=0;//存放图片的额个数
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic_tuijan);
        initView();
        EventBus.getDefault().register(this);
        mList = new ArrayList<>();
        mSstring=new ArrayList<>();
        mBean=new ArrayList<>();
        Aimoi();
        mDisecbeAdapter = new PictureDisecbeAdapter(getApplicationContext());
        Intent intent = getIntent();
        mBean = (List<MealDetailsBean.GoodsMsgBean.TuwenBean>) intent.getSerializableExtra("photo");
        if(mBean!=null){

            Log.e("测试","我执行了");
            for(int i=0;i<mBean.size();i++){
                picnum++;
                mList.add(mBean.get(i).getPhoto());

            }
        }
        mDisecbeAdapter.setList(mList);
        mListvPicdec.setAdapter(mDisecbeAdapter);
        mListvPicdec.setDividerHeight(0);
        mDisecbeAdapter.notifyDataSetChanged();
        mListvPicdec.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                deletphoto(position);
            }
        });


    }
    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(PicTuijanActivity.this);
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("上传中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }

    private void wahteGetpress() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permission = new ArrayList<>();
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.CAMERA);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (!permission.isEmpty()) {
                String[] permissons = permission.toArray(new String[permission.size()]);
                ActivityCompat.requestPermissions(this, permissons, PERMISSION_CORDD);
            } else {
                dialgoShow();
                //tackPhoto();
            }
        } else {

            dialgoShow();
            // tackPhoto();
        }
    }



    ////////////////////////////下面为相机和图片选择拍照功能部分(Modify By YangTK 2017.06.17)///////////////////////////////
    /**
     * 选择照片或者拍照
     */
    SelectPicPopupWindow menuWindow;

    private void dialgoShow() {
        menuWindow = new SelectPicPopupWindow(this, itemsOnClick);
        menuWindow.showAtLocation(findViewById(R.id.xiangji),
                Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);

    }


    private View.OnClickListener itemsOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            menuWindow.dismiss();
            switch (v.getId()) {
                case R.id.takePhotoBtn:     //选择相机拍照
//                    if ()
                    AndPermission.with(PicTuijanActivity.this)
                            .requestCode(100)
                            .permission(Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE)
//                            .rationale(rationaleListener)
                            .callback(listener)
                            .start();

                    break;
                case R.id.pickPhotoBtn:     //选择相册
                    openPic();
                    break;
                default:
                    break;
            }
        }
    };

    private int REQUEST_CODE_SETTING = 300;
    private PermissionListener listener = new PermissionListener() {
        @Override
        public void onSucceed(int requestCode, List<String> grantedPermissions) {
            // 权限申请成功回调。

            // 这里的requestCode就是申请时设置的requestCode。
            // 和onActivityResult()的requestCode一样，用来区分多个不同的请求。
            if (requestCode == 100) {
                // TODO ...
                openCamera();
            }
        }

        @Override
        public void onFailed(int requestCode, List<String> deniedPermissions) {
            // 权限申请失败回调。
            if (requestCode == 100) {
                // TODO ...
                // 第一种：用默认的提示语。
                AndPermission.defaultSettingDialog(PicTuijanActivity.this, REQUEST_CODE_SETTING).show();
            }
        }
    };


    /**
     * 调用相机     相机拍照之后 一般都是一个  时间.jpg 文件
     */
    private File mFile;
    /**
     * 定义三种状态
     */
    private static final int REQUESTCODE_PIC = 1;//相册
    private static final int REQUESTCODE_CAM = 2;//相机
    private static final int REQUESTCODE_CUT = 3;//图片裁剪

    private void openCamera() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            if (!file.exists()) {
                file.mkdirs();
            }
            mFile = new File(file, System.currentTimeMillis() + ".jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mFile));
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            startActivityForResult(intent, REQUESTCODE_CAM);
        } else {
            Toast.makeText(this, "请确认已经插入SD卡", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 打开相册
     */
    private void openPic() {
        Intent picIntent = new Intent(Intent.ACTION_PICK, null);
        picIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(picIntent, REQUESTCODE_PIC);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {//返回码是可用的
            switch (requestCode) {
                case REQUESTCODE_CAM:

                    final String absolutePath = mFile.getAbsolutePath();
                    final Bitmap bitmap = PictureUtil.compressSizeImage(absolutePath);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            setImageToView(bitmap);
                        }
                    }).start();
                    break;
                case REQUESTCODE_PIC:
                    Uri uri = data.getData();

                    final String s = parsePicturePath(getBaseContext(), uri);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            setImageToView(PictureUtil.compressSizeImage(s));
                        }
                    }).start();
                    break;
            }
        }
    }
    /**
     * EventBus功能部分
     * 此处主要用到的功能为：在NewTaskService中接收到新订单之后，那边会发送一个通知，然后这边会通知新任务界面去刷新界面
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final AnyEventType bean) {
        //这里接受到消息
        Log.e("haha",bean.getS());
        if(bean.getS().equals("推荐")){
            if(builder==null){
                Aimoi();
                builder.show();
            }else{
                builder.show();
            }
        }else {
            builder.dismiss();
            builder=null;
        }

    }
    /**
     * 图片上传
     */
    private void setImageToView(Bitmap bitmap) {
        EventBus.getDefault().post(new AnyEventType("推荐"));
        Bitmap nbitmap = bitmap;
        mapurl = AboutBitMap.bitmapToBase64(nbitmap);
        OkGo.post(HttpAPI.PHOTO)
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("base64", picHead + mapurl)
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);

                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        String substring = s.substring(1, s.length());
                        // mSstring.add(substring);
                        mList.add(substring);
                        mDisecbeAdapter.notifyDataSetChanged();
                        picnum++;
                        mGetPictures.add(new GetPicture("0",substring));
                        EventBus.getDefault().post(new AnyEventType("测试完成"));

                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);
                    }
                });
    }




    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(PicTuijanActivity.this);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mListvPicdec = (ListView) findViewById(R.id.listv_picdec);
        mBtnSave = (Button) findViewById(R.id.btn_save);
        mBtnSave.setOnClickListener(PicTuijanActivity.this);
        mBtnAddpic = (Button) findViewById(R.id.btn_addpic);
        mBtnAddpic.setOnClickListener(PicTuijanActivity.this);
    }


    /**
     * 解析获取图片库图片Uri物理路径
     *
     * @param context
     * @param uri
     * @return
     */

    @SuppressLint("NewApi")
    public String parsePicturePath(Context context, Uri uri) {

        if (null == context || uri == null)
            return null;

        boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentUri
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageDocumentsUri
            if (isExternalStorageDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                String[] splits = docId.split(":");
                String type = splits[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + File.separator + splits[1];
                }
            }
            // DownloadsDocumentsUri
            else if (isDownloadsDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaDocumentsUri
            else if (isMediaDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                String[] split = docId.split(":");
                String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                String selection = "_id=?";
                String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosContentUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;

    }

    private String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        String column = "_data";
        String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            try {
                if (cursor != null)
                    cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private boolean isExternalStorageDocumentsUri(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocumentsUri(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocumentsUri(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosContentUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_save) {

            if(mList.size()==5){
                ToastUtil.showShort(getApplicationContext(),"最多只能添加5张图片");
            }else {
                wahteGetpress();
            }


        } else if (view.getId() == R.id.btn_addpic) {
            if(picnum==mList.size()){
                    Gson gson=new Gson();
                    String s = gson.toJson(mSstring);
                    Intent intent=new Intent();
                    intent.putExtra("delephoto",s);
                    intent.putExtra("pic",gson.toJson(mGetPictures));
                    setResult(ADDPTUWEN,intent);
                    finish();
            }else {
                ToastUtil.showShort(getApplicationContext(),"图片后台上传中，请等两秒钟试一下");
            }

        } else if (view.getId() == R.id.rl_back) {
            finish();
        }
    }
    private void deletphoto(final int position){
        mDialog = new Dialog(this, R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(true);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(30, 0, 30,0);
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate = LayoutInflater.from(this).inflate(R.layout.item_save_tuwen_pic, null);
        Button cancel = (Button) inflate.findViewById(R.id.cancle);
        Button delet = (Button) inflate.findViewById(R.id.delet);

        delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mList.remove(position);
                if(mList.size()!=0){
                    mSstring.add(mBean.get(position).getPic_id()+"");
                }
                picnum--;
                mDisecbeAdapter.notifyDataSetChanged();
                mDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setContentView(inflate);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
