package com.liminyunbao.iyunbeishop.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.adapter.StatisticsAdapter;
import com.liminyunbao.iyunbeishop.fragment.MonthFragment;
import com.liminyunbao.iyunbeishop.fragment.WeekChartFragment;
import com.liminyunbao.iyunbeishop.fragment.YearFragment;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 营收统计
 */
public class StatisticsActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgStatiticsBack;
    protected FrameLayout mFlStatistic;
    protected PullToRefreshListView mListvStatistics;
    protected RadioButton mWeek;
    protected RadioButton mMonth;
    protected RadioButton mYear;
    protected TextView mTvToday;
    protected TextView mTvTodayOrder;
    protected TextView mTvYeseToday;
    protected TextView mTvYeseOrder;
    protected TextView mTvTotal;
    protected TextView mTvTotalOrder;
    private StatisticsAdapter mAdapter;
    private List<String> mStrings;
    private String data[] = {"今天", "昨天", "前天", "很久"};


    @Override
    protected void HaveNet() {
        inithttp();
    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_statistics);
        initView();
        //initData();
        inithttp();
        mWeek.setChecked(true);
        initFragnemt();
    }

    private void initFragnemt() {
        WeekChartFragment lineChartFragment = new WeekChartFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_statistic, lineChartFragment);
        fragmentTransaction.commit();

    }

    private void inithttp() {
        String  userid = (String) SharePUtile.get(this, "user_id", "");
        String shopid = (String) SharePUtile.get(this, "shop_id", "");
        String  user_token = (String) SharePUtile.get(this, "user_token", "");
        OkGo.post(HttpAPI.REVENUE).params("shopid", shopid)
                .params("user_token",user_token)
                .params("user_id",userid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                JSONObject data=jsonObject.getJSONObject("data");
                                String todaymoney = data.getString("todaymoney");
                                String todayorder = data.getString("todayorder");
                                String yesterdaymoney = data.getString("yesterdaymoney");
                                String yesterdayorder = data.getString("yesterdayorder");
                                String totalmoney = data.getString("totalmoney");
                                String totalorder = data.getString("totalorder");
                                mTvToday.setText(todaymoney+"元");
                                mTvTodayOrder.setText(todayorder);
                                mTvYeseToday.setText(yesterdaymoney+"元");
                                mTvYeseOrder.setText(yesterdayorder);
                                mTvTotal.setText(totalmoney+"元");
                                mTvTotalOrder.setText(totalorder);
                            } else if (status.equals("0")) {
                                Toast.makeText(StatisticsActivity.this, "请求失败！", Toast.LENGTH_SHORT).show();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(StatisticsActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(StatisticsActivity.this,"链接超时！", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_Statitics_back) {
            finish();
        } else if (view.getId() == R.id.week) {
            WeekChartFragment lineChartFragment = new WeekChartFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_statistic, lineChartFragment);
            fragmentTransaction.commit();
        } else if (view.getId() == R.id.month) {
            MonthFragment monthFragment = new MonthFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_statistic, monthFragment);
            fragmentTransaction.commit();

        } else if (view.getId() == R.id.year) {
            YearFragment yearFragment = new YearFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_statistic, yearFragment);
            fragmentTransaction.commit();
        }
    }

    private void initView() {
        mImgStatiticsBack = (ImageView) findViewById(R.id.img_Statitics_back);
        mImgStatiticsBack.setOnClickListener(StatisticsActivity.this);
        mFlStatistic = (FrameLayout) findViewById(R.id.fl_statistic);
       // mListvStatistics = (PullToRefreshListView) findViewById(R.id.listv_statistics);
        mWeek = (RadioButton) findViewById(R.id.week);
        mWeek.setOnClickListener(StatisticsActivity.this);
        mMonth = (RadioButton) findViewById(R.id.month);
        mMonth.setOnClickListener(StatisticsActivity.this);
        mYear = (RadioButton) findViewById(R.id.year);
        mYear.setOnClickListener(StatisticsActivity.this);
        mTvToday = (TextView) findViewById(R.id.tv_today);
        mTvTodayOrder = (TextView) findViewById(R.id.tv_today_order);
        mTvYeseToday = (TextView) findViewById(R.id.tv_yese_today);
        mTvYeseOrder = (TextView) findViewById(R.id.tv_yese_order);
        mTvTotal = (TextView) findViewById(R.id.tv_total);
        mTvTotalOrder = (TextView) findViewById(R.id.tv_total_order);
    }
}
