package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

public class GoodsDescribeActivity extends BaseActivity implements View.OnClickListener {

    protected RelativeLayout mRlBack;
    protected TextView mTvTitle;
    protected EditText mEditDescribe;
    private static final int XUZHI = 13;
    private static final int DESCRIBE = 14;
    private static final int  ADDPDESCRIBE=113;
    private static final int   ADDPXUNZHI=111;

    protected Button mBtnsure;
    private String peng;
    private String describe;
    private static int type=0;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_goods_describe);
        initView();
        Intent intent = getIntent();
         peng = intent.getStringExtra("peng");

        if (peng.equals("1")) {
            mTvTitle.setText("购买须知");
            String xuzhi = intent.getStringExtra("xuzhi");
            if(!TextUtils.isEmpty(xuzhi)){
                mEditDescribe.setText(xuzhi);
            }
            type=XUZHI;
        } else if (peng.equals("2")) {
            mTvTitle.setText("详情描述");
            String descreibe = intent.getStringExtra("descreibe");
            if(!TextUtils.isEmpty(descreibe)){
                mEditDescribe.setText(descreibe);
            }

            type=DESCRIBE;
        }else if(peng.equals("3")){
            mTvTitle.setText("购买须知");
            String xuzhi = intent.getStringExtra("xuzhi");
            if(!TextUtils.isEmpty(xuzhi)){
                mEditDescribe.setText(xuzhi);
            }
            type=ADDPXUNZHI;

        }else if(peng.equals("4")){
            mTvTitle.setText("详情描述");
            String descreibe = intent.getStringExtra("descreibe");
            if(!TextUtils.isEmpty(descreibe)){
                mEditDescribe.setText(descreibe);
            }
            type=ADDPDESCRIBE;
        }


    }

    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(GoodsDescribeActivity.this);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEditDescribe = (EditText) findViewById(R.id.edit_describe);
        mBtnsure = (Button) findViewById(R.id.btnsure);
        mBtnsure.setOnClickListener(GoodsDescribeActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {

            finish();
        } else if (view.getId() == R.id.btnsure) {
            describe = mEditDescribe.getText().toString();
            Log.e("全部参数",describe);
            Intent intent1 = new Intent();
            intent1.putExtra("bo", describe);
            setResult(type, intent1);
            finish();
        }
    }
}
