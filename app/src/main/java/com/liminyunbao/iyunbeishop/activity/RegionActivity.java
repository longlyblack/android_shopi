package com.liminyunbao.iyunbeishop.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.CityInfo;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeOption;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiDetailSearchOption;
import com.baidu.mapapi.search.poi.PoiIndoorResult;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionResult;
import com.baidu.mapapi.search.sug.SuggestionSearch;
import com.baidu.mapapi.search.sug.SuggestionSearchOption;
import com.liminyunbao.iyunbeishop.adapter.RegiongetAddsAdapter;
import com.liminyunbao.iyunbeishop.R;
import com.umeng.socialize.utils.Log;

import java.util.ArrayList;
import java.util.List;

import mapapi.overlayutil.PoiOverlay;

/**
 * 地图选择位置
 */
public class  RegionActivity extends BaseActivity implements View.OnClickListener {

    protected MapView mBmapView;
    protected ImageView mImgRegionBack;
    protected ListView mListvRegion;
    protected TextView mTvCityName;
    protected AutoCompleteTextView mSearch;
    private LocationClient mLocationClient;
    private Mylocation mMylocation = new Mylocation();
    private BaiduMap mBaiduMap;
    private LocationClientOption option;
    private boolean isfrist = true;
    private MyLocationConfiguration.LocationMode mLocationMode;
    // 经纬度
    private double latitude = 0.0;
    private double Longitude = 0.0;
 /*   private int mCurrentDirection = 0;
    private MyLocationData locData;*/
    private GeoCoder geoCoder;
    private LatLng latLng;
    private List<PoiInfo> infolist = new ArrayList<>();

    private int RECOD = 1;
    private int CITYCODE=2;
    private final int REGISTCORD=2;
    private RegiongetAddsAdapter mAdapter;
    private double lats;
    private double lngs;
    private String City;
    private SuggestionSearch mSuggestionSearch = null;
    private List<String> suggest;
    private ArrayAdapter<String> sugAdapter = null;
    private PoiSearch poiSearch;
    private String cityname;
    private final int RECOIDLOCA=100;
    private String isRegist;
    private int flag=50;

    @Override
    protected void HaveNet() {

        initView();
        geoCoder = GeoCoder.newInstance();
        poiSearch = PoiSearch.newInstance();
        mBaiduMap = mBmapView.getMap();
        mBaiduMap.setMyLocationEnabled(true);
        permission();
        BackData();
        SuggeSearch();//建议搜索模块
        initlistener();
    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  SDKInitializer.initialize(getApplicationContext());
        super.setContentView(R.layout.activity_region);
        initView();
        Intent intent = getIntent();
        isRegist = intent.getStringExtra("isRegist");
        geoCoder = GeoCoder.newInstance();
        poiSearch = PoiSearch.newInstance();
        mBaiduMap = mBmapView.getMap();
        mBaiduMap.setMyLocationEnabled(true);
        permission();
        BackData();
        SuggeSearch();//建议搜索模块
        initlistener();
    }

    /**
     *当前位置poi信息  ----地理位置反编码
     */
    private void geocodem() {
        // 创建地理编码检索实例
        geoCoder = GeoCoder.newInstance();
        OnGetGeoCoderResultListener listener = new OnGetGeoCoderResultListener() {
            // 反地理编码查询结果回调函数
            @Override
            public void onGetReverseGeoCodeResult(ReverseGeoCodeResult result) {
                if (result == null
                        || result.error != SearchResult.ERRORNO.NO_ERROR) {
                    // 没有检测到结果
                }
                infolist = result.getPoiList();
                mAdapter = new RegiongetAddsAdapter();
                mAdapter.setPoiInfos(infolist);
                mListvRegion.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
            // 地理编码查询结果回调函数
            @Override
            public void onGetGeoCodeResult(GeoCodeResult result) {
                if (result == null
                        || result.error != SearchResult.ERRORNO.NO_ERROR) {
                    // 没有检测到结果
                }
            }
        };
        // 设置地理编码检索监听者
        geoCoder.setOnGetGeoCodeResultListener(listener);
        geoCoder.reverseGeoCode(new ReverseGeoCodeOption().location(latLng));
    }
    private void initlistener(){
        mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {
            @Override
            public void onMapStatusChangeStart(MapStatus mapStatus) {
            }
            @Override
            public void onMapStatusChange(MapStatus mapStatus) {

            }

            @Override
            public void onMapStatusChangeFinish(MapStatus mapStatus) {
                LatLng target = mapStatus.target;
                geoCoder.setOnGetGeoCodeResultListener(new OnGetGeoCoderResultListener() {
                    @Override
                    public void onGetReverseGeoCodeResult(ReverseGeoCodeResult reverseGeoCodeResult) {
                        if (reverseGeoCodeResult == null || reverseGeoCodeResult.error != SearchResult.ERRORNO.NO_ERROR) {
                            Toast.makeText(RegionActivity.this, "抱歉，未能找到结果", Toast.LENGTH_SHORT)
                                    .show();
                            return;
                        }
                        infolist = reverseGeoCodeResult.getPoiList();
                        mAdapter = new RegiongetAddsAdapter();
                        mAdapter.setPoiInfos(infolist);
                        mListvRegion.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }
                    @Override
                    public void onGetGeoCodeResult(GeoCodeResult geoCodeResult) {
                    }
                });
                geoCoder.reverseGeoCode(new ReverseGeoCodeOption().location(target));
            }
        });
    }


    private void SuggeSearch() {
        // 初始化建议搜索模块，注册建议搜索事件监听
        mSuggestionSearch = SuggestionSearch.newInstance();
        OnGetSuggestionResultListener listener = new OnGetSuggestionResultListener() {
            @Override
            public void onGetSuggestionResult(SuggestionResult suggestionResult) {
                if (suggestionResult == null || suggestionResult.getAllSuggestions() == null) {
                    return;
                }
                suggest = new ArrayList<String>();
                for (SuggestionResult.SuggestionInfo info : suggestionResult.getAllSuggestions()) {
                    if (info.key != null) {
                        suggest.add(info.key);
                    }
                }
                sugAdapter = new ArrayAdapter<String>(RegionActivity.this, android.R.layout.simple_dropdown_item_1line, suggest);
                mSearch.setAdapter(sugAdapter);
                sugAdapter.notifyDataSetChanged();
            }
        };
        mSuggestionSearch.setOnGetSuggestionResultListener(listener);
        sugAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line);
        mSearch.setAdapter(sugAdapter);
        mSearch.setThreshold(1);
        mSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

             //  Toast.makeText(RegionActivity.this, suggest.get(position), Toast.LENGTH_SHORT).show();
                Log.e("这是城市city",City);
                poisercg(City, suggest.get(position));
               // Log.e("这是建议搜索",suggest.get(position));
            }
        });
        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() <= 0) {
                    return;
                }
                /**
                 * 使用建议搜索服务获取建议列表，结果在onSuggestionResult()中更新
                 */
                mSuggestionSearch
                        .requestSuggestion((new SuggestionSearchOption())
                                .keyword(s.toString()).city(City));
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    private void BackData() {
        /**
         * 返回位置信息 坐标
         */
        mListvRegion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PoiInfo poiInfo = infolist.get(position);
                if(isRegist.equals("1")){
                    Intent intent = new Intent();
                    intent.putExtra("poiname", poiInfo.name);
                    intent.putExtra("latitude", poiInfo.location.latitude + "");
                    intent.putExtra("longitude", poiInfo.location.longitude + "");
                    setResult(RECOD, intent);
                    finish();
                }else if(isRegist.equals("2")){
                    Intent intent = new Intent();
                    intent.putExtra("poiname", poiInfo.name);
                    intent.putExtra("latitude", poiInfo.location.latitude + "");
                    intent.putExtra("longitude", poiInfo.location.longitude + "");
                    setResult(REGISTCORD, intent);
                    finish();
                }


            }
        });

        mTvCityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChicesCityActivity.class);
                intent.putExtra("city", City);
                startActivityForResult(intent,CITYCODE);
            }
        });
    }

    private void poisercg(String city, String keyword) {
        //城市搜索 及地点覆盖物
        OnGetPoiSearchResultListener poiSearchListener = new OnGetPoiSearchResultListener() {
            // 获取poiResult
            @Override
            public void onGetPoiResult(PoiResult poiResult) {
                if (poiResult == null || poiResult.error == SearchResult.ERRORNO.RESULT_NOT_FOUND) {
                    Toast.makeText(RegionActivity.this, "未找到结果", Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                if (poiResult.error == SearchResult.ERRORNO.NO_ERROR) {
                    mBaiduMap.clear();
                    MyPoiOverlay poiOverlay = new MyPoiOverlay(mBaiduMap);  //实例化poiOverlay对象.
                    poiOverlay.setData(poiResult); //设置poiOverlay数据.
                    mBaiduMap.setOnMarkerClickListener(poiOverlay);//当标志物被点击时监听设置.
                    poiOverlay.addToMap();//将所有的poi添加到Map上.
                    poiOverlay.zoomToSpan();
                    List<PoiInfo> allPoi = poiResult.getAllPoi();
                    return;
                }
                if (poiResult.error == SearchResult.ERRORNO.AMBIGUOUS_KEYWORD) {
                    // 当输入关键字在本市没有找到，但在其他城市找到时，返回包含该关键字信息的城市列表
                    String strInfo = "在";
                    for (CityInfo cityInfo : poiResult.getSuggestCityList()) {
                        strInfo += cityInfo.city;
                        strInfo += ",";
                    }
                    strInfo += "找到结果";
                    Toast.makeText(RegionActivity.this, strInfo, Toast.LENGTH_LONG)
                            .show();
                }
            }
            // 当点击覆盖物的时候,查询详细的数据信息,作为回调返回数据信息
            @Override
            public void onGetPoiDetailResult(PoiDetailResult result) {
                if (result.error != SearchResult.ERRORNO.NO_ERROR) {
                    Toast.makeText(RegionActivity.this, "抱歉，未找到结果", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(RegionActivity.this, result.getName() + ": " + result.getAddress(), Toast.LENGTH_SHORT)
                            .show();
                    Intent intent = new Intent();
                    intent.putExtra("poiname",  result.getName());
                    intent.putExtra("latitude",  result.getLocation().latitude+"");
                    intent.putExtra("longitude", result.getLocation().longitude+"");
                    setResult(RECOD, intent);
                    finish();
                }
            }
            @Override
            public void onGetPoiIndoorResult(PoiIndoorResult poiIndoorResult) {
            }
        };
        PoiCitySearchOption poiCitySearchOption = new PoiCitySearchOption();
        poiCitySearchOption.keyword(keyword);
        poiCitySearchOption.city(city);
        poiCitySearchOption.pageNum(0);
        poiCitySearchOption.pageCapacity(15);
        poiSearch.searchInCity(poiCitySearchOption);
        poiSearch.setOnGetPoiSearchResultListener(poiSearchListener);

    }

    public void permission() {
        //判断版本是否是6.0以上
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permission = new ArrayList<>();
            //判断是否授权
            // int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //PackageManager.PERMISSION_GRANTED 可以往下执行 PERMISSION_DENIED 表示没有授权
                //  ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
                permission.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (!permission.isEmpty()) {
                String[] permissons = permission.toArray(new String[permission.size()]);
                ActivityCompat.requestPermissions(this, permissons, RECOIDLOCA);
            } else {
                initLocation();

            }
        } else {
            initLocation();

        }
    }

    /**
     *初始化定位
     */
    private void initLocation() {
        mLocationMode = MyLocationConfiguration.LocationMode.NORMAL;
        mLocationClient = new LocationClient(getApplicationContext());
        mLocationClient.registerLocationListener(mMylocation);
        option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        option.setOpenGps(true);
        option.setCoorType("bd09ll");
        int span=1000;
        option.setScanSpan(span);
      //option.setIsNeedAddress(true);
        option.setLocationNotify(true);
        option.setAddrType("all");
        option.setIsNeedLocationPoiList(true);
        option.setLocationNotify(true);
        mLocationClient.setLocOption(option);
        mLocationClient.start();

       // mLocationClient.requestLocation();
    }

    private void initView() {
        mBmapView = (MapView) findViewById(R.id.bmapView);
        mImgRegionBack = (ImageView) findViewById(R.id.img_region_back);
        mImgRegionBack.setOnClickListener(RegionActivity.this);
        mListvRegion = (ListView) findViewById(R.id.listv_region);
        mTvCityName = (TextView) findViewById(R.id.tv_city_name);
        mSearch = (AutoCompleteTextView) findViewById(R.id.search);
    }

    private void negiv(final BDLocation bdLocation) {
        latitude = bdLocation.getLatitude();
        Longitude = bdLocation.getLongitude();

        if (isfrist) {
            isfrist = false;
            latLng = new LatLng(latitude, Longitude);
            // MapStatusUpdate update = MapStatusUpdateFactory.newLatLng(latLng);
            MapStatus.Builder builder = new MapStatus.Builder();
            builder.target(latLng).zoom(18.0f);
            //  mBaiduMap.animateMapStatus(update);
            //  update = MapStatusUpdateFactory.zoomTo(18f);
            mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
            //  mBaiduMap.animateMapStatus(update);
        }


        new Thread() {
            public void run() {
                //这儿是耗时操作，完成之后更新UI；
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //更新UI
                        mTvCityName.setText(bdLocation.getCity());
                    }
                });
            }
        }.start();
        City = bdLocation.getCity();
        PoiInfo info = new PoiInfo();
        info.address = bdLocation.getAddrStr();
        info.city = bdLocation.getCity();
        info.location = latLng;
        info.name = bdLocation.getAddrStr();
        Log.e("地址", bdLocation.getAddrStr()+"啊哈"+ bdLocation.getAddrStr());
        infolist.add(info);
        MyLocationData.Builder builder = new MyLocationData.Builder();
       // builder.direction(0); 这个是配合传感器  使用0-360度  箭头的方向  当为0的时候是指向北的
        builder.accuracy(bdLocation.getRadius());
        builder.latitude(bdLocation.getLatitude());
        builder.longitude(bdLocation.getLongitude());
        MyLocationData build = builder.build();
        mBaiduMap.setMyLocationData(build);
        geocodem();
      //  mBaiduMap.setMyLocationConfiguration(new MyLocationConfiguration(mLocationMode, true, null));  //这个是方向箭头
    }

    /**
     * 定位监听回调
     */
    public class Mylocation implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            // map view 销毁后不在处理新接收的位置
            if (bdLocation == null || mBmapView == null) {
                return;
            }
            Log.e("我执行了","喔喔");
            negiv(bdLocation);
        }

        @Override
        public void onConnectHotSpotMessage(String s, int i) {

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //获取所选择的城市列表的经纬度
        if(resultCode==CITYCODE){
            cityname=  data.getStringExtra("cityname");
            String lat=data.getStringExtra("latitudes");
            String lng=data.getStringExtra("longitudes");
            if(lat!=null&&lng!=null){
                lats =Double.parseDouble(lat);
                lngs=Double.parseDouble(lng);
                latLng = new LatLng(lats, lngs);
                MapStatusUpdate update = MapStatusUpdateFactory.newLatLng(latLng);
                mBaiduMap.setMapStatus(update);
                mTvCityName.setText(cityname);
                City=cityname;
                SuggeSearch();
            }else{
              return;
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RECOIDLOCA:
                if (grantResults.length > 0) {
                    for (int result : grantResults) {
                        if (result == PackageManager.PERMISSION_GRANTED) {
                            initLocation();
                        }else {
                            Toast.makeText(this, "没有授权！", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    //Toast.makeText(this, "未知错误！", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }
    private class MyPoiOverlay extends PoiOverlay {

        public MyPoiOverlay(BaiduMap baiduMap) {
            super(baiduMap);
        }

        @Override
        public boolean onPoiClick(int index) {
            super.onPoiClick(index);
            PoiInfo poi = getPoiResult().getAllPoi().get(index);
            poiSearch.searchPoiDetail((new PoiDetailSearchOption())
                    .poiUid(poi.uid));
            return true;
        }
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_region_back) {
            finish();
        }
    }
    @Override
    protected void onDestroy() {
        mBaiduMap.setMyLocationEnabled(false);
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mLocationClient.stop();
        mBmapView.onDestroy();
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mBmapView.onResume();
        super.onResume();

    }

    @Override
    protected void onPause() {
        mBmapView.onPause();
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理

    }
}
