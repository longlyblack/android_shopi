package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.adapter.ChoicegroupAdatper;
import com.liminyunbao.iyunbeishop.bean.TowGroupBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

public class ChoiceGroupActivity extends BaseActivity implements View.OnClickListener {
    private static final int  TOWGROUP=11;
    protected RelativeLayout mRlBack;
    protected TextView mTvAddgroup;
    protected ListView mListvGroup;
    protected Button mBtnFinish;


    private TowGroupBean.MsgBean mMsgBean;
    private ChoicegroupAdatper mChoicegroupAdatper;
    private Map<Integer, Boolean> mMap;
    private String base_goods_id;
    private int currentNum = -1;
    private Map<Integer,String> mStringMap;
    private List<String> mCateid;//选中分组的id
    private List<String> mWcateid=new ArrayList<>();//未选中商品的额id
    private Map<Integer ,String > mStatus;//存放刚开始的状态

    private List<String> mChicFalst;
    private List<String> mChicTure;

    private List<String>  chicTid=new ArrayList<>();
    private List<String>  chicFid=new ArrayList<>();

    public void setMap(Map<Integer, Boolean> map) {
        mMap = map;
    }
    private Map<Integer,TowGroupBean.MsgBean> mMsgBeanMap;
    private List<TowGroupBean.MsgBean> mList;
    private List<String> mNist;


    private Map<Integer, Boolean> mLastMap;

    private boolean flags=true;
    private static final int ADDNEWGROUP=1;
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_choice_group);
        initView();
        Intent intent = getIntent();
        base_goods_id = intent.getStringExtra("base_goods_id");
        mMap = new HashMap<>();
        mLastMap=new HashMap<>();
        mStringMap=new HashMap<>();//存放选中的cate_id;
        mCateid=new ArrayList<>();
        mMsgBeanMap=new HashMap<>();
        mNist=new ArrayList<>();
        mStatus=new HashMap<>();
        mChicTure=new ArrayList<>();
        mChicFalst=new ArrayList<>();




        HttpData();
        mListvGroup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (mMap.get(position)) {
                        mChoicegroupAdatper.isSelected.put(position, false);
                        TowGroupBean.MsgBean msgBean = mMsgBeanMap.get(position);
                        msgBean.setStatus("0");
                        mList.set(position,msgBean);
                }else{
                mChoicegroupAdatper.isSelected.put(position, true);
                TowGroupBean.MsgBean msgBean = mMsgBeanMap.get(position);
                msgBean.setStatus("1");
                mList.set(position,msgBean);
                }
                mChoicegroupAdatper.notifyDataSetChanged();

            }
        });


    }
    /**
     * http
     */
    private void HttpData() {

        OkGo.post(HttpAPI.TOWGROUP)
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("base_goods_id", base_goods_id)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("二级分组", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                TowGroupBean towGroupBean = gson.fromJson(s, TowGroupBean.class);
                                mList = towGroupBean.getMsg();
                                for (int i = 0; i<mList.size();i++){
                                    mNist.add(mList.get(i).getStatus());
                                }
                                mChoicegroupAdatper = new ChoicegroupAdatper(getApplicationContext(), mMap, mList);
                                mLastMap=mMap;
                                mListvGroup.setAdapter(mChoicegroupAdatper);
                                mChoicegroupAdatper.notifyDataSetChanged();
                                mChoicegroupAdatper.setChicboxkchick(new ChoicegroupAdatper.Chicboxkchick() {
                                    @Override
                                    public void setchick(CheckBox cb, final int position) {
                                        cb.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (mMap.get(position)) {

                                                    mChoicegroupAdatper.isSelected.put(position, false);
                                                    TowGroupBean.MsgBean msgBean = mMsgBeanMap.get(position);
                                                    msgBean.setStatus("0");
                                                    mList.set(position,msgBean);



                                                }else{
                                                    mChoicegroupAdatper.isSelected.put(position, true);
                                                    TowGroupBean.MsgBean msgBean = mMsgBeanMap.get(position);
                                                    msgBean.setStatus("1");
                                                    mList.set(position,msgBean);

                                                }
                                                mChoicegroupAdatper.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                });
                                mChoicegroupAdatper.setMasgben(new ChoicegroupAdatper.Masgben() {
                                    @Override
                                    public void setMasgStatus(TowGroupBean.MsgBean bean, Integer posion) {
                                        mMsgBeanMap.put(posion,bean);
                                    }
                                });
                            } else if (status.equals("0")) {
                                ToastUtil.showShort(getApplicationContext(), "暂无数据");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {

            finish();
        } else if (view.getId() == R.id.tv_addgroup) {

            if(mList.size()!=10){
                Intent intent = new Intent(ChoiceGroupActivity.this, AddGroupActivity.class);
                intent.putExtra("addw","1");
                startActivityForResult(intent,ADDNEWGROUP);
            }else {
                ToastUtil.showShort(getApplicationContext(),"最多只能创建10个分组");
            }
        } else if (view.getId() == R.id.btn_finish) {

            for(int i=0;i<mList.size();i++){
                String newlist = mList.get(i).getStatus();
                if(!newlist.equals(mNist.get(i))){
                    if(mList.get(i).getStatus().equals("1")){
                        mChicTure.add(mList.get(i).getCate_id()+"");
                    }else {
                        mChicFalst.add(mList.get(i).getCate_id()+"");
                    }
                }
            }
            Gson gson=new Gson();
            String s = gson.toJson(mChicTure);
            String wei=gson.toJson(mChicFalst);
            Log.e("开始的",s+"选中的数组");
            Log.e("开始的",wei+"未选中");

            Intent intent=new Intent();
                intent.putExtra("catelist","1");
                intent.putExtra("list",s);
                intent.putExtra("listw",wei);

                setResult(TOWGROUP,intent);
                finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==ADDNEWGROUP){
                HttpData();
        }
    }

    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(ChoiceGroupActivity.this);
        mTvAddgroup = (TextView) findViewById(R.id.tv_addgroup);
        mTvAddgroup.setOnClickListener(ChoiceGroupActivity.this);
        mListvGroup = (ListView) findViewById(R.id.listv_group);
        mBtnFinish = (Button) findViewById(R.id.btn_finish);
        mBtnFinish.setOnClickListener(ChoiceGroupActivity.this);
    }
}
