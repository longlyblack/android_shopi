package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.adapter.ZJLSpagerAdapter;
import com.liminyunbao.iyunbeishop.fragment.PuTonglsFragment;
import com.liminyunbao.iyunbeishop.fragment.YouHuimdFragment;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 服务类（资金流水）
 */
public class SaveZiJinLiuShuiActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgZjlsBack;
    protected SlidingTabLayout mSTabZjls;
    protected ViewPager mVpZjls;
    private List<Fragment> mFragments;
    private ZJLSpagerAdapter mZJLSpagerAdapter;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_save_zi_jin_liu_shui);
        initView();
        initFragment();
    }

    private void  initFragment(){
        mFragments=new ArrayList<>();
        mZJLSpagerAdapter=new ZJLSpagerAdapter(getSupportFragmentManager());
        mFragments.add(PuTonglsFragment.newInstance());
        mFragments.add(YouHuimdFragment.newInstance());
        mZJLSpagerAdapter.setFragments(mFragments);
        mVpZjls.setAdapter(mZJLSpagerAdapter);
        mSTabZjls.setViewPager(mVpZjls);

    }

    private void initView() {
        mImgZjlsBack = (ImageView) findViewById(R.id.img_zjls_back);
        mImgZjlsBack.setOnClickListener(SaveZiJinLiuShuiActivity.this);
        mSTabZjls = (SlidingTabLayout) findViewById(R.id.sTab_zjls);
        mVpZjls = (ViewPager) findViewById(R.id.vp_zjls);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_zjls_back) {
            finish();
        }
    }
}
