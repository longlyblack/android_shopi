package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.MobileUtils;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

public class ChangeContactsActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgChangecontactsBack;
    protected EditText mEdiNameChangecontacts;
    protected Button mBtnChangecontactsNameSure;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_change_contacts);
        initView();
        Intent intent = getIntent();
        String account = intent.getStringExtra("account");
        if(account.equals("0")){
            mEdiNameChangecontacts.setHint("待完善");
        }else {
            mEdiNameChangecontacts.setText(account);
        }
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_changecontacts_back) {
                  finish();
        } else if (view.getId() == R.id.btn_changecontacts_name_sure) {
            boolean mobile = MobileUtils.isMobile(mEdiNameChangecontacts.getText().toString());
            if(mobile){
                Changconacts();
            }else{
                Toast.makeText(getApplicationContext(),"请输入正确的手机号",Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void Changconacts(){

        if(mEdiNameChangecontacts.getText().toString()!=null){
            String userid = (String) SharePUtile.get(getApplicationContext(), "user_id", "");
            String user_token = (String) SharePUtile.get(getApplicationContext(), "user_token", "");
            String shopid = (String) SharePUtile.get(getApplicationContext(), "shop_id", "");
            Log.e("号码",userid+"用户to坑"+user_token+"商店id"+shopid+"xin"+mEdiNameChangecontacts.getText().toString());
            OkGo.post(HttpAPI.CHANGMOBLIE)
                    .params("user_id",userid)
                    .params("user_token",user_token)
                    .params("shopid",shopid)
                    .params("mobile",mEdiNameChangecontacts.getText().toString())
                    .execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            Log.e("号码",s);
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                String status = jsonObject.getString("status");
                                if(status.equals("1")){
                                    String mag = jsonObject.getString("msg");
                                    Toast.makeText(getApplicationContext(),mag,Toast.LENGTH_SHORT).show();
                                    finish();
                                }else if(status.equals("0")){
                                    String mag = jsonObject.getString("msg");
                                    Toast.makeText(getApplicationContext(),mag,Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(Call call, Response response, Exception e) {
                            super.onError(call, response, e);
                            Log.e("号码",e.getMessage().toString());
                        }
                    });


        }



    }


    private void initView() {
        mImgChangecontactsBack = (ImageView) findViewById(R.id.img_changecontacts_back);
        mImgChangecontactsBack.setOnClickListener(ChangeContactsActivity.this);
        mEdiNameChangecontacts = (EditText) findViewById(R.id.edi_name_changecontacts);
        mBtnChangecontactsNameSure = (Button) findViewById(R.id.btn_changecontacts_name_sure);
        mBtnChangecontactsNameSure.setOnClickListener(ChangeContactsActivity.this);
    }
}
