package com.liminyunbao.iyunbeishop.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.fragment.MounthFragment;
import com.liminyunbao.iyunbeishop.fragment.ThreeMounthFragment;
import com.liminyunbao.iyunbeishop.fragment.TodayFragment;
import com.liminyunbao.iyunbeishop.fragment.WeekFragment;
import com.liminyunbao.iyunbeishop.fragment.YeastDayFragment;
import com.liminyunbao.iyunbeishop.R;

/**
 * 验证记录
 */
public class VerificationRemberActivity extends BaseActivity implements View.OnClickListener {

    //protected ListView mListvYanzheng;
    protected ImageView mImgYanzhengBack;
    protected TextView mTvDay;
    protected ImageView mImgDate;
    protected TextView mTvTeday;
    protected TextView mTvYesteday;
    protected TextView mTvWeek;
    protected TextView mTvMonth;
    protected TextView mTvTheremonth;
    protected FrameLayout mFlYanzheng;
    private PopupWindow mPopupWindow;

    private  TodayFragment todayFragment;
    private YeastDayFragment yeastDayFragment;
    private WeekFragment weekFragment;
    private ThreeMounthFragment threeMounthFragment;
    private MounthFragment mounthFragment;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_yanzheng);
        initView();
        //httpData();
      /*  mListvYanzheng.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent=new Intent(VerificationRemberActivity.this,OrderDetailsActivity.class);
                startActivity(intent);

            }
        });*/
        initFragment();

    }
   /* private void httpData(){
        OkGo.post(HttpAPI.RECORD)
                .tag(this)
                .connTimeOut(10000).params("shopid","678").execute(new StringCallback() {
            @Override
            public void onSuccess(String s, Call call, Response response) {
                Log.e("这是验证记录",s);
                Gson gson =new Gson();
                YanZhengRembBean yanZhengRemb = gson.fromJson(s, YanZhengRembBean.class);
                mDateBeen=  yanZhengRemb.getDate().getData();
                mYanZhengAdapter = new YanZhengAdapter();
                mYanZhengAdapter.setStrings(mDateBeen);
                mListvYanzheng.setAdapter(mYanZhengAdapter);
                mYanZhengAdapter.notifyDataSetChanged();
            }
        });

    }*/

    private void initFragment() {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        todayFragment = new TodayFragment();
        fragmentTransaction.replace(R.id.fl_yanzheng,todayFragment);
        fragmentTransaction.commit();

    }



    private void Showpopuwind() {
        mPopupWindow = new PopupWindow();
        View inflate = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_date_popu, null);
        WindowManager.LayoutParams lp = this.getWindow()
                .getAttributes();
        lp.alpha = 0.4f;
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        this.getWindow().setAttributes(lp);
        mPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        mPopupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);

        mPopupWindow.setContentView(inflate);
        mPopupWindow.setTouchable(true);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        mPopupWindow.showAsDropDown(mImgDate, dp2px(getBaseContext(), -100), 0);
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {

            // 在dismiss中恢复透明度
            public void onDismiss() {
                WindowManager.LayoutParams lp = VerificationRemberActivity.this.getWindow()
                        .getAttributes();
                lp.alpha = 1f;
                VerificationRemberActivity.this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                VerificationRemberActivity.this.getWindow().setAttributes(lp);
            }

        });
        mTvTeday = (TextView) inflate.findViewById(R.id.tv_teday);
        mTvTeday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvDay.setText("今天");

               FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fl_yanzheng,todayFragment);
                fragmentTransaction.commit();
                mPopupWindow.dismiss();
            }
        });
        mTvYesteday = (TextView) inflate.findViewById(R.id.tv_yesteday);
        mTvYesteday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvDay.setText("昨天");
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                yeastDayFragment = new YeastDayFragment();
                fragmentTransaction.replace(R.id.fl_yanzheng,yeastDayFragment);
                fragmentTransaction.commit();
                mPopupWindow.dismiss();
            }
        });
        mTvWeek = (TextView) inflate.findViewById(R.id.tv_week);
        mTvWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvDay.setText("近一周");
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                weekFragment = new WeekFragment();
                fragmentTransaction.replace(R.id.fl_yanzheng,weekFragment);
                fragmentTransaction.commit();
                mPopupWindow.dismiss();
            }
        });
        mTvMonth = (TextView) inflate.findViewById(R.id.tv_month);
        mTvMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvDay.setText("近一个月");

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                mounthFragment = new MounthFragment();
                fragmentTransaction.replace(R.id.fl_yanzheng,mounthFragment);
                fragmentTransaction.commit();
                mPopupWindow.dismiss();
            }
        });
        mTvTheremonth = (TextView) inflate.findViewById(R.id.tv_theremonth);
        mTvTheremonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvDay.setText("近三个月");
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                threeMounthFragment = new ThreeMounthFragment();
                fragmentTransaction.replace(R.id.fl_yanzheng,threeMounthFragment);
                fragmentTransaction.commit();
                mPopupWindow.dismiss();
            }
        });
    }
    /**
     * dp转px
     */
    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal, context.getResources().getDisplayMetrics());
    }

    private void initView() {

        mImgYanzhengBack = (ImageView) findViewById(R.id.img_yanzheng_back);
        mImgYanzhengBack.setOnClickListener(VerificationRemberActivity.this);
        mTvDay = (TextView) findViewById(R.id.tv_day);
        mImgDate = (ImageView) findViewById(R.id.img_date);
        mImgDate.setOnClickListener(VerificationRemberActivity.this);
        mFlYanzheng = (FrameLayout) findViewById(R.id.fl_yanzheng);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_yanzheng_back) {
            finish();
        } else if (view.getId() == R.id.img_date) {
            Showpopuwind();
        }
    }
}
