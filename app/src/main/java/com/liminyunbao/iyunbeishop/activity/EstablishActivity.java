package com.liminyunbao.iyunbeishop.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.adapter.CategoryRightAdapter;
import com.liminyunbao.iyunbeishop.adapter.CategoyrLeftAdapter;
import com.liminyunbao.iyunbeishop.adapter.RegionqAdapter;
import com.liminyunbao.iyunbeishop.bean.CategoryBean;
import com.liminyunbao.iyunbeishop.bean.RegionqBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 创建店铺
 */
public class EstablishActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgEstablishBack;
    protected TextView mTvEstablishName;
    protected TextView mTextStorName;
    protected EditText mEdiStorName;
    protected TextView mTextCategoryName;
    protected TextView mEdiCategoryName;
    protected RelativeLayout mRlCategory;
    protected TextView mTextStorAddr;
    protected ImageView mImgStorPosition;
    protected TextView mTvStorAddr;
    protected RelativeLayout mRlStorAddr;
    protected TextView mTextStorXiangaddr;
    protected EditText mEdiStorXiangaddr;
    protected TextView mTextRead1;
    protected TextView mTextRead2;
    protected TextView mTextRead3;
    protected TextView mTextRead4;
    protected Button mBtnEstavlishSure;
    protected TextView mTextRegionName;
    protected TextView mEdiRegionName;
    protected RelativeLayout mRlRegion;
    private String name = null;
    private String lat;
    private String lng;
    private final int REGISTCORD = 2;
    private Dialog mDialog;
    private ListView right;
    private ListView left;
    private CategoyrLeftAdapter leftAdapter;
    private CategoryRightAdapter rightAdatper;
    private List<CategoryBean.TatilBean.KeyBean> key;
    private List<CategoryBean.TatilBean.KeyBean.DataBean> mRightData;
    private String category = null;

    private List<RegionqBean> mRegionqBeanList;
    private RegionqAdapter mRegionqAdapter;
    private String quyvid;
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(), "你进入没有网的异次元！", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_establish);
        initView();
       // initshuju();
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_Category) {
            //主营类目
            ShowDialog();

        } else if (view.getId() == R.id.btn_estavlish_sure) {
            initCreadshop();
        } else if (view.getId() == R.id.img_establish_back) {
            finish();
        } else if (view.getId() == R.id.rl_stor_addr) {
            Intent intent = new Intent(EstablishActivity.this, RegionActivity.class);
            intent.putExtra("isRegist", "2");
            startActivityForResult(intent, REGISTCORD);
        } else if (view.getId() == R.id.text_read2) {
            Intent intent = new Intent(EstablishActivity.this, AgreementActivity.class);
            intent.putExtra("style", "1");
            startActivity(intent);
        } else if (view.getId() == R.id.text_read4) {
            Intent intent = new Intent(EstablishActivity.this, AgreementActivity.class);
            intent.putExtra("style", "2");
            startActivity(intent);
        } else if (view.getId() == R.id.rl_region) {
            ShowRegio();
        }
    }

    private void initCreadshop() {
        String user_token = (String) SharePUtile.get(getApplicationContext(), "user_token", "");
        String user_id = (String) SharePUtile.get(getBaseContext(), "user_id", "");
        if (mEdiStorXiangaddr.getText().toString() != null && mEdiStorName.getText().toString() != null && category != null && lat != null && lng != null&&quyvid!=null) {
            OkGo.post(HttpAPI.CREATSHOP)
                    .params("shopname", mEdiStorName.getText().toString())
                    .params("category", category)
                    .params("user_id", user_id)
                    .params("area_id",quyvid)
                    .params("user_token", user_token)
                    .params("mapaddr",name)
                    .params("addr",  mEdiStorXiangaddr.getText().toString())
                    .params("lng", lng.substring(0, 10))
                    .params("lat", lat.substring(0, 10)).execute(new StringCallback() {
                @Override
                public void onSuccess(String s, Call call, Response response) {
                    Log.e("这是创建店铺",s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        String status = jsonObject.getString("status");
                        if (status.equals("1")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            String info = data.getString("info");
                            Toast.makeText(getApplicationContext(), info, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(EstablishActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else if (status.equals("0")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            String info = data.getString("info");
                            Toast.makeText(getApplicationContext(), info, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                    }
                }

                @Override
                public void onError(Call call, Response response, Exception e) {
                    super.onError(call, response, e);
                    Log.e("这是创建店铺",e.getMessage().toString());
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "信息不完整！", Toast.LENGTH_SHORT).show();
        }
    }
    private void  ShowRegio(){
        mRegionqAdapter=new RegionqAdapter();
        mRegionqBeanList=new ArrayList<>();
        mRegionqBeanList.add(new RegionqBean("南阳","4798"));
        mRegionqBeanList.add(new RegionqBean("方城","4802"));
        mDialog = new Dialog(this, R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(40, 0, 40, 0);
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate = LayoutInflater.from(this).inflate(R.layout.item_rgion_quy_dialog, null);
        ListView region= (ListView) inflate.findViewById(R.id.Listv_region);
        mRegionqAdapter.setRegionqBeen(mRegionqBeanList);
        region.setAdapter(mRegionqAdapter);
        mRegionqAdapter.notifyDataSetChanged();
        region.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                quyvid= mRegionqBeanList.get(position).getCituyid();
                String cityname = mRegionqBeanList.get(position).getCityname();
                mEdiRegionName.setText(cityname);
                mDialog.dismiss();
            }
        });
        mDialog.setContentView(inflate);
    }
    private void ShowDialog() {
        leftAdapter = new CategoyrLeftAdapter();
        mDialog = new Dialog(this, R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(40, 0, 40, 0);
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate = LayoutInflater.from(this).inflate(R.layout.item_category_dialog, null);
        right = (ListView) inflate.findViewById(R.id.listv_category_right);
        left = (ListView) inflate.findViewById(R.id.lsitv_category_left);
        initCategory();
        left.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mRightData = key.get(position).getData();
                leftAdapter.setSelectedPosition(position);
                leftAdapter.notifyDataSetChanged();
                initright(position);
            }
        });
        right.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mEdiCategoryName.setText(mRightData.get(position).getCate_name());
                category = mRightData.get(position).getCate_name();
                mDialog.dismiss();
            }
        });
        mDialog.setContentView(inflate);

    }

    private void initright(int position) {
        rightAdatper = new CategoryRightAdapter();
        rightAdatper.setDataBeen(mRightData);
        rightAdatper.setTitalPositon(position);
        right.setAdapter(rightAdatper);
        rightAdatper.notifyDataSetChanged();
    }


    private void initCategory() {
        OkGo.get(HttpAPI.CATEGORY)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                CategoryBean categoryBean = gson.fromJson(s, CategoryBean.class);
                                key = categoryBean.getData().getKey();
                                leftAdapter.setKey(key);
                                left.setAdapter(leftAdapter);
                                leftAdapter.notifyDataSetChanged();
                                leftAdapter.setSelectedPosition(0);
                                mRightData = key.get(0).getData();
                                initright(0);
                            } else if (status.equals("0")) {
                                Toast.makeText(getApplicationContext(), "暂无数据！", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == REGISTCORD) {
            name = data.getStringExtra("poiname");
            mTvStorAddr.setText(name);
            lat = data.getStringExtra("latitude");
            lng = data.getStringExtra("longitude");
            Log.e("这是位置", lat + "  " + lng + "  " + name);
        }
    }

    private void initView() {
        mImgEstablishBack = (ImageView) findViewById(R.id.img_establish_back);
        mImgEstablishBack.setOnClickListener(EstablishActivity.this);
        mTvEstablishName = (TextView) findViewById(R.id.tv_establish_name);
        mTextStorName = (TextView) findViewById(R.id.text_stor_name);
        mEdiStorName = (EditText) findViewById(R.id.edi_stor_name);
        mTextCategoryName = (TextView) findViewById(R.id.text_Category_name);
        mEdiCategoryName = (TextView) findViewById(R.id.edi_Category_name);
        mRlCategory = (RelativeLayout) findViewById(R.id.rl_Category);
        mRlCategory.setOnClickListener(EstablishActivity.this);
        mTextStorAddr = (TextView) findViewById(R.id.text_stor_addr);
        mImgStorPosition = (ImageView) findViewById(R.id.img_stor_position);
        mTvStorAddr = (TextView) findViewById(R.id.tv_stor_addr);
        mRlStorAddr = (RelativeLayout) findViewById(R.id.rl_stor_addr);
        mRlStorAddr.setOnClickListener(EstablishActivity.this);
        mTextStorXiangaddr = (TextView) findViewById(R.id.text_stor_xiangaddr);
        mEdiStorXiangaddr = (EditText) findViewById(R.id.edi_stor_xiangaddr);
        mTextRead1 = (TextView) findViewById(R.id.text_read1);
        mTextRead2 = (TextView) findViewById(R.id.text_read2);
        mTextRead2.setOnClickListener(EstablishActivity.this);
        mTextRead3 = (TextView) findViewById(R.id.text_read3);
        mTextRead4 = (TextView) findViewById(R.id.text_read4);
        mTextRead4.setOnClickListener(EstablishActivity.this);
        mBtnEstavlishSure = (Button) findViewById(R.id.btn_estavlish_sure);
        mBtnEstavlishSure.setOnClickListener(EstablishActivity.this);
        mTextRegionName = (TextView) findViewById(R.id.text_region_name);
        mEdiRegionName = (TextView) findViewById(R.id.edi_region_name);
        mRlRegion = (RelativeLayout) findViewById(R.id.rl_region);
        mRlRegion.setOnClickListener(EstablishActivity.this);
    }
}
