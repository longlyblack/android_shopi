package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

public class EditGroupActivity extends BaseActivity implements View.OnClickListener {

    protected RelativeLayout mRlBack;
    protected TextView mTextGroupname;
    protected EditText mEdiSetPw;
    protected ImageView mImgRight;
    protected TextView mTvShopnum;
    protected RelativeLayout mRlSeeGroup;
    protected Button mBtnSignOut;
    private String cateid;
    private String groupname;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_edit_group);
        initView();
        Intent intent = getIntent();
         groupname = intent.getStringExtra("groupname");
        cateid = intent.getStringExtra("cateid");
        String num = intent.getStringExtra("num");
        mEdiSetPw.setText(groupname);
        mTvShopnum.setText("共"+num+"件");
        mRlSeeGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(EditGroupActivity.this,ShopDetailsActivity.class);
                intent.putExtra("groupname",groupname);
                intent.putExtra("cateid",cateid);
                startActivity(intent);
            }
        });

    }

    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(EditGroupActivity.this);
        mTextGroupname = (TextView) findViewById(R.id.text_groupname);
        mEdiSetPw = (EditText) findViewById(R.id.edi_set_pw);
        mImgRight = (ImageView) findViewById(R.id.img_right);
        mTvShopnum = (TextView) findViewById(R.id.tv_shopnum);
        mRlSeeGroup = (RelativeLayout) findViewById(R.id.rl_see_group);
        mBtnSignOut = (Button) findViewById(R.id.btn_sign_out);
        mBtnSignOut.setOnClickListener(EditGroupActivity.this);
    }

    private void editGroupName(){
        OkGo.post(HttpAPI.GROUPCHANGENAME)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id",UserUtils.Userid(getApplicationContext()))
                .params("user_token",UserUtils.UserToken(getApplicationContext()))
                .params("cate_id",cateid)
                .params("name",mEdiSetPw.getText().toString())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(),msg);
                                finish();

                            }else if(status.equals("0")){
                                ToastUtil.showShort(getApplicationContext(),"修改失败");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {
            finish();
        } else if (view.getId() == R.id.btn_sign_out) {

            if(TextUtils.isEmpty(mEdiSetPw.getText().toString())){
                ToastUtil.showShort(getApplicationContext(),"请输入要修改的分组名");
            }else{
                editGroupName();
            }
        }
    }
}
