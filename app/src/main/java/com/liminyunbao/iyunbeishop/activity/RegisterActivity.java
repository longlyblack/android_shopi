package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgRegistBack;
    protected TextView mTvRegistName;
    protected TextView mTextStorPhon;
    protected EditText mEdiStorPhon;
    protected TextView mTextStorCord;
    protected EditText mEdiStorCord;
    protected Button mBtnGetcord;
    protected TextView mTextRead1;
    protected TextView mTextRead2;
    protected TextView mTextRead3;
    protected TextView mTextRead4;
    protected TextView mTextPawdCord;
    protected EditText mEdiPawdCord;
    protected Button mRegiset;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_register);
        initView();

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_regist_back) {
            finish();
        } else if (view.getId() == R.id.btn_getcord) {

            if(isMobile(mEdiStorPhon.getText().toString())){
                getCord();
                new CountDownTimer(60000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        String time = "(" + millisUntilFinished / 1000 + ")秒";
                        mBtnGetcord.setText(time);
                        mBtnGetcord.setClickable(false);
                    }

                    @Override
                    public void onFinish() {
                        mBtnGetcord.setText("重新获取");
                        mBtnGetcord.setClickable(true);
                    }
                }.start();
            }else{
                Toast.makeText(getApplicationContext(),"请输入正确的手机号！",Toast.LENGTH_SHORT).show();
            }
        } else if (view.getId() == R.id.regiset) {
          regitst();
        }
    }


    private void regitst(){

        if(mEdiStorPhon.getText().toString()!=null&&mEdiPawdCord.getText().toString()!=null&&mEdiStorCord.getText().toString()!=null){
            OkGo.post(HttpAPI.REGIST)
                    .params("mobile",mEdiStorPhon.getText().toString())
                    .params("pwd",mEdiPawdCord.getText().toString())
                    .params("code",mEdiStorCord.getText().toString())
                    .execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                String status = jsonObject.getString("status");
                                if(status.equals("1")){
                                    JSONObject data = jsonObject.getJSONObject("data");
                                    String info = data.getString("info");
                                    String user_token = data.getString("user_token");
                                    String user_id = data.getString("user_id");
                                    SharePUtile.put(getApplicationContext(),"user_token",user_token);
                                    SharePUtile.put(getBaseContext(), "user_id", user_id);
                                    Toast.makeText(getApplicationContext(),info,Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(RegisterActivity.this,OneOpenActivity.class);
                                    startActivity(intent);
                                }else if(status.equals("0")){
                                    JSONObject data = jsonObject.getJSONObject("data");
                                    String info = data.getString("info");
                                    Toast.makeText(getApplicationContext(),info,Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });


        }else {
            Toast.makeText(getApplicationContext(),"请输入手机号和密码！",Toast.LENGTH_SHORT).show();
        }

    }
    private void getCord(){

        OkGo.post(HttpAPI.SENDCORD)
                .params("mobile",mEdiStorPhon.getText().toString())
                .execute(new StringCallback() {
            @Override
            public void onSuccess(String s, Call call, Response response) {
                Log.e("这是验证码",s+"这是手机号"+mEdiStorPhon.getText().toString());
            }
        });
    }

    /**
     * 验证手机格式
     */
    public static boolean isMobile(String number) {
    /*
    移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188
    联通：130、131、132、152、155、156、185、186
    电信：133、153、180、189、（1349卫通）
    总结起来就是第一位必定为1，第二位必定为3或5或8，其他位置的可以为0-9
    */
       // String num = "[1][0358]\\d{9}";//"[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。

       String num="[1][123456798]\\d{9}";
        if (TextUtils.isEmpty(number)) {
            return false;
        } else {
            //matches():字符串是否在给定的正则表达式匹配
            return number.matches(num);
        }
    }
    private void initView() {
        mImgRegistBack = (ImageView) findViewById(R.id.img_regist_back);
        mImgRegistBack.setOnClickListener(RegisterActivity.this);
        mTvRegistName = (TextView) findViewById(R.id.tv_regist_name);
        mTextStorPhon = (TextView) findViewById(R.id.text_stor_phon);
        mEdiStorPhon = (EditText) findViewById(R.id.edi_stor_phon);
        mTextStorCord = (TextView) findViewById(R.id.text_stor_cord);
        mEdiStorCord = (EditText) findViewById(R.id.edi_stor_cord);
        mBtnGetcord = (Button) findViewById(R.id.btn_getcord);
        mBtnGetcord.setOnClickListener(RegisterActivity.this);
        mTextRead1 = (TextView) findViewById(R.id.text_read1);
        mTextRead2 = (TextView) findViewById(R.id.text_read2);
        mTextRead3 = (TextView) findViewById(R.id.text_read3);
        mTextRead4 = (TextView) findViewById(R.id.text_read4);
        mTextPawdCord = (TextView) findViewById(R.id.text_pawd_cord);
        mEdiPawdCord = (EditText) findViewById(R.id.edi_pawd_cord);
        mRegiset = (Button) findViewById(R.id.regiset);
        mRegiset.setOnClickListener(RegisterActivity.this);
    }
}
