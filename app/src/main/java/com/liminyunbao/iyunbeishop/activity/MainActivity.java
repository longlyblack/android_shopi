package com.liminyunbao.iyunbeishop.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.adapter.MainListAdapter;
import com.liminyunbao.iyunbeishop.bean.ShopListBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.Logger;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.jpush.android.api.JPushInterface;
import okhttp3.Call;
import okhttp3.Response;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    protected TextView mMainAddStor;
    protected LinearLayout mActivityMain;
    private long firstTime = 0;
    protected ImageView mImgTitleLlogMain;
    protected TextView mTxTatileMain;
    protected PullToRefreshListView mLsMain;
    private MainListAdapter mListAdapter;
    private List<ShopListBean.DataBean> mDataBeen;
    private List<ShopListBean.DataBean> mAllDataBeen=new ArrayList<>();
    private String user_token;
    private String userid;
    private int page=0;

    @Override
    protected void HaveNet() {

      //  htttData();
    }

    @Override
    protected void GoneNet() {
       // Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);
        user_token = (String) SharePUtile.get(getApplicationContext(), "user_token", "");
        userid = (String) SharePUtile.get(getBaseContext(), "user_id", "");
        initView();
        initVerson();
        whatSelection();
        pullfresh();

    }


    private void pullfresh(){
        mLsMain.setMode(PullToRefreshBase.Mode.BOTH);
        mLsMain.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                //开始下拉  我们做Http请求
                mAllDataBeen.clear();
                page=1;
                htttData();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mAllDataBeen.clear();
                    htttData();
                    mLsMain.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mLsMain .onRefreshComplete();
                        }
                    }, 1000);

            }

        });

        mListAdapter=new MainListAdapter();
        mLsMain.setAdapter(mListAdapter);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mLsMain.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mListAdapter.notifyDataSetChanged();

    }
    //是否有选中的店铺
    private void whatSelection() {
        boolean isselection = (boolean) SharePUtile.get(getBaseContext(), "isselection", false);
        if (isselection) {
            int type = (int) SharePUtile.get(getBaseContext(), "shop_typ", 0);
            String shopname = (String) SharePUtile.get(getBaseContext(), "shopname", "");
            String shopid = (String) SharePUtile.get(getBaseContext(), "shop_id", "");
            if (type == 0) {
                Intent intent = new Intent(getBaseContext(), BusinessActivity.class);
                intent.putExtra("shopname", shopname);
                intent.putExtra("shopid", shopid);
                startActivity(intent);
                finish();
            } else if (type == 1) {
                Intent intent = new Intent(getBaseContext(), BusinessSaverActivity.class);
                intent.putExtra("shopname", shopname);
                intent.putExtra("shopid", shopid);
                startActivity(intent);
                finish();
            }
        } else {
            htttData();
            SelectShop();
        }
    }
    private void SelectShop() {
        mLsMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mDataBeen.get(position-1).getShoptype() == 0) {
                    SharePUtile.get(getBaseContext(), "backtwo", 1);
                    SharePUtile.put(getBaseContext(), "isselection", true);
                    SharePUtile.put(getBaseContext(), "shop_typ", mDataBeen.get(position-1).getShoptype());
                    SharePUtile.put(getBaseContext(), "shopname", mDataBeen.get(position-1).getShop_name());
                    SharePUtile.put(getBaseContext(), "shop_id", mDataBeen.get(position-1).getShop_id() + "");
                    Intent intent = new Intent(getBaseContext(), BusinessActivity.class);
                    intent.putExtra("shopname", mDataBeen.get(position-1).getShop_name());
                    intent.putExtra("shopid", mDataBeen.get(position-1).getShop_id() + "");
                    intent.putExtra("area_id", mDataBeen.get(position-1).getArea_id() + "");
                    startActivity(intent);
                    finish();
                } else if (mDataBeen.get(position-1).getShoptype() == 1) {
                    SharePUtile.get(getBaseContext(), "backtwo", 1);
                    SharePUtile.put(getBaseContext(), "isselection", true);
                    SharePUtile.put(getBaseContext(), "shop_typ", mDataBeen.get(position-1).getShoptype());
                    SharePUtile.put(getBaseContext(), "shopname", mDataBeen.get(position-1).getShop_name());
                    SharePUtile.put(getBaseContext(), "shop_id", mDataBeen.get(position-1).getShop_id() + "");
                    Intent intent = new Intent(getBaseContext(), BusinessSaverActivity.class);
                    intent.putExtra("shopname", mDataBeen.get(position-1).getShop_name());
                    intent.putExtra("shopid", mDataBeen.get(position-1).getShop_id() + "");
                    intent.putExtra("area_id", mDataBeen.get(position-1).getArea_id() + "");
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void htttData() {
        mListAdapter = new MainListAdapter();
        OkGo.post(HttpAPI.CHOICE_SHOP)
                .params("user_token",user_token)
                .params("user_id", userid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                Log.e("这是店铺选择", s);
                                ShopListBean shopListBean = gson.fromJson(s, ShopListBean.class);
                                mDataBeen = shopListBean.getData();
                                mAllDataBeen.addAll(mDataBeen);
                                mListAdapter.setList(mAllDataBeen);
                                mLsMain.setAdapter(mListAdapter);
                                mListAdapter.notifyDataSetChanged();
                                mLsMain .onRefreshComplete();
                            }else if(status.equals("0")){
                                Intent intent=new Intent(MainActivity.this,OneOpenActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e("这是店铺选择", e.getMessage());
                    }
                });
    }
    private void initView() {
        mImgTitleLlogMain = (ImageView) findViewById(R.id.img_titleLlog_main);
        mTxTatileMain = (TextView) findViewById(R.id.tx_tatile_main);
        mLsMain = (PullToRefreshListView) findViewById(R.id.ls_mian);
        mMainAddStor = (TextView) findViewById(R.id.main_add_stor);
        mMainAddStor.setOnClickListener(MainActivity.this);
        mActivityMain = (LinearLayout) findViewById(R.id.activity_main);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            long secondTime = System.currentTimeMillis();
            if (secondTime - firstTime > 2000) {
                Toast.makeText(MainActivity.this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                firstTime = secondTime;
                return true;
            } else {
                Intent intent = new Intent();
                intent.setAction(BaseActivity.SYSTEM_EXIT);
                sendBroadcast(intent);
            }
        }
        return super.onKeyUp(keyCode, event);
    }
    private void initVerson() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //透明导航栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            // 激活状态栏
            tintManager.setStatusBarTintEnabled(true);
            // enable navigation bar tint 激活导航栏
            tintManager.setNavigationBarTintEnabled(true);
            //设置系统栏设置颜色
            //tintManager.setTintColor(R.color.red);
            //给状态栏设置颜色
            tintManager.setStatusBarTintResource(R.color.colorDianPuB);
            //Apply the specified drawable or color resource to the system navigation bar.
            //给导航栏设置资源
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.main_add_stor) {
            whatMoney();
        }
    }
    /**
     * 是否充值钱数 到创建店铺界面
     */
    private void whatMoney(){

        Log.e("这是是否充值user",user_token+" hah  "+userid);
        OkGo.post(HttpAPI.ABLECREATE)
                .params("user_token",user_token)
                .params("user_id",userid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("这是是否充值",s);
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Intent intent=new Intent(MainActivity.this,EstablishActivity.class);
                                startActivity(intent);
                            }else if (status.equals("0")){
                                String data = jsonObject.getString("data");
                                Toast.makeText(getApplicationContext(),data,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e("这是是cuwu",e.getMessage());
                    }
                });
    }
    public static boolean isForeground = false;
    public static final String MESSAGE_RECEIVED_ACTION = "com.example.jpushdemo.MESSAGE_RECEIVED_ACTION";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_EXTRAS = "extras";
    private MessageReceiver mMessageReceiver;
    public void registerMessageReceiver() {
        mMessageReceiver = new MessageReceiver();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction(MESSAGE_RECEIVED_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);
    }
    public class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
                Logger.d("用户点开了", "[MyReceiver] 用户点击打开了通知");

               /* //打开自定义的Activity
                Intent i = new Intent(context, Main2Activity.class);
                // i.putExtras(bundle);
                //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(i);*/
            }}
    }



}
