package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.adapter.DDxqChildAdapter;
import com.liminyunbao.iyunbeishop.bean.PuTongListBean;
import com.liminyunbao.iyunbeishop.custom.ChildListView;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * 订单详情
 */
public class OrderDetailsActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgDdxqBack;
    protected TextView mTvXfzhuangtai;
    protected TextView mTvDdxqHxm;
    protected TextView mTvShaopNameDdxq;
    protected ChildListView mListvDdxq;
    protected TextView mTextShangprice;
    protected TextView mTextYouhui;
    protected TextView mTextZonge;
    protected TextView mTvShangpriceZongjia;
    protected TextView mTvPeisongfeiZongjia;
    protected TextView mTvZongeZongjia;
    protected TextView mTextOrderNumDdxq;
    protected TextView mTvOrdernumDdxq;
    protected TextView mTextOrderTimeDdxq;
    protected TextView mTvOrdertimeDdxq;
    protected TextView mTextOrderZhuanDdxq;
    protected TextView mTvOrderzhuanDdxq;

    private DDxqChildAdapter mDDxqChildAdapter;
    private List<PuTongListBean.DataBean.OrderDmsg.ShopData> data;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_ding_dan_xq);
        initView();
        initdata();
    }

    private void initdata() {
        mDDxqChildAdapter = new DDxqChildAdapter();
        Intent intent = getIntent();
         data = (List<PuTongListBean.DataBean.OrderDmsg.ShopData>) intent.getSerializableExtra("data");
       // Log.e("hahaha",data.get(0).getGoods_name());
        Bundle extras = intent.getExtras();
        String code=extras.getString("code");
        String shopname=extras.getString("shopname");
        String zongjia = extras.getString("zongja");
        String zonge = extras.getString("zonge");
        String status = extras.getString("status");
        String youhui = extras.getString("youhui");
        String oreder = extras.getString("oreder");
        String time = extras.getString("time");



        mTvDdxqHxm.setText(code);
        mTvShaopNameDdxq.setText(shopname);
        mTvShangpriceZongjia.setText("￥"+zongjia);
        mTvOrdernumDdxq.setText(oreder);
        mTvOrdertimeDdxq.setText(time);
        mTvZongeZongjia.setText("￥"+zonge);
        mTvPeisongfeiZongjia.setText("￥"+youhui);


        if(status.equals("4")){
            mTvOrderzhuanDdxq.setText("已完成");
        }if(status.equals("3")){
            mTvOrderzhuanDdxq.setText("待评价");
        }
        // mDDxqChildAdapter.setStrings(text);

        mDDxqChildAdapter.setBeen(data);
        mListvDdxq.setAdapter(mDDxqChildAdapter);
        mDDxqChildAdapter.notifyDataSetChanged();


    }

    private void initView() {
        mImgDdxqBack = (ImageView) findViewById(R.id.img_ddxq_back);
        mImgDdxqBack.setOnClickListener(OrderDetailsActivity.this);
        mTvXfzhuangtai = (TextView) findViewById(R.id.tv_xfzhuangtai);
        mTvDdxqHxm = (TextView) findViewById(R.id.tv_ddxq_hxm);
        mTvShaopNameDdxq = (TextView) findViewById(R.id.tv_shaop_name_ddxq);
        mListvDdxq = (ChildListView) findViewById(R.id.listv_ddxq);
        mTextShangprice = (TextView) findViewById(R.id.text_shangprice);
        mTextYouhui = (TextView) findViewById(R.id.text_youhui);
        mTextZonge = (TextView) findViewById(R.id.text_zonge);
        mTvShangpriceZongjia = (TextView) findViewById(R.id.tv_shangprice_zongjia);
        mTvPeisongfeiZongjia = (TextView) findViewById(R.id.tv_peisongfei_zongjia);
        mTvZongeZongjia = (TextView) findViewById(R.id.tv_zonge_zongjia);
        mTextOrderNumDdxq = (TextView) findViewById(R.id.text_order_num_ddxq);
        mTvOrdernumDdxq = (TextView) findViewById(R.id.tv_ordernum_ddxq);
        mTextOrderTimeDdxq = (TextView) findViewById(R.id.text_order_time_ddxq);
        mTvOrdertimeDdxq = (TextView) findViewById(R.id.tv_ordertime_ddxq);
        mTextOrderZhuanDdxq = (TextView) findViewById(R.id.text_order_zhuan_ddxq);
        mTvOrderzhuanDdxq = (TextView) findViewById(R.id.tv_orderzhuan_ddxq);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_ddxq_back) {

            finish();
        }
    }
}
