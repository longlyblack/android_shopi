package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.liminyunbao.iyunbeishop.R;

public class SelectAccountActivity extends BaseActivity {

    protected ImageView mImgSelectAccountBack;
    protected CheckBox mChbWeixi;
    protected CheckBox mChbZhifubao;
    protected CheckBox mChbBank;
    protected Button mBtnNext;
    private int  statussCode=6;

    private int weixinCode=7;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_select_account);
        initView();
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   bandWeix();
                Intent intent=new Intent(getApplicationContext(),RemindActivity.class);
                startActivityForResult(intent,statussCode);
            }
        });
        mChbWeixi.setChecked(true);
        mChbWeixi.setClickable(false);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==statussCode){
            String status = data.getStringExtra("status");
            String msg = data.getStringExtra("msg");
            Intent intent=new Intent();
            intent.putExtra("status",status);
            intent.putExtra("msg",msg);
            setResult(weixinCode,intent);
            finish();

        }



    }

    private void initView() {
        mImgSelectAccountBack = (ImageView) findViewById(R.id.img_select_account_back);
        mChbWeixi = (CheckBox) findViewById(R.id.chb_weixi);
        mChbZhifubao = (CheckBox) findViewById(R.id.chb_zhifubao);
        mChbBank = (CheckBox) findViewById(R.id.chb_bank);
        mBtnNext = (Button) findViewById(R.id.btn_next);
    }
}
