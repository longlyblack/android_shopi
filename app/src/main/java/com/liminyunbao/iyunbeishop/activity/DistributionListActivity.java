package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.adapter.DistributionListFragmentAdapter;
import com.liminyunbao.iyunbeishop.fragment.AllDeliveryFragment;
import com.liminyunbao.iyunbeishop.fragment.DeliveryDaiFragment;
import com.liminyunbao.iyunbeishop.fragment.DeliveryRefundFragment;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 配送订单
 */
public class DistributionListActivity extends AppCompatActivity {

    protected SlidingTabLayout mSTabDistributionlist;
    protected ViewPager mVpDistributionlist;
    private List<Fragment>mFragments;

    private DistributionListFragmentAdapter mListFragmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_distribution);
        initView();
        initFragment();
    }
    private void initFragment(){
        mFragments=new ArrayList<>();

        mListFragmentAdapter=new DistributionListFragmentAdapter(getSupportFragmentManager());
        mFragments.add(DeliveryDaiFragment.newInstance());
        mFragments.add(AllDeliveryFragment.newInstance());
        mFragments.add(DeliveryRefundFragment.newInstance());
        mListFragmentAdapter.setFragments(mFragments);
        mVpDistributionlist.setAdapter(mListFragmentAdapter);
        mSTabDistributionlist.setViewPager(mVpDistributionlist);


    }

    private void initView() {
        mSTabDistributionlist = (SlidingTabLayout) findViewById(R.id.sTab_distributionlist);
        mVpDistributionlist = (ViewPager) findViewById(R.id.vp_distributionlist);
    }
}
