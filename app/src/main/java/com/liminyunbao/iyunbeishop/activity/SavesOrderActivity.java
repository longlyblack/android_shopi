package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.adapter.OrderListNewPagerAapter;
import com.liminyunbao.iyunbeishop.fragment.SuccessFragment;
import com.liminyunbao.iyunbeishop.fragment.UnSuccessFragment;
import com.liminyunbao.iyunbeishop.fragment.WiteSenFragment;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

public class SavesOrderActivity extends BaseActivity {

    protected RelativeLayout mRlBack;
    protected TextView mGroupName;
    protected SlidingTabLayout mSTabSaverOrderlist;
    protected ViewPager mVpSaverOrdlisit;
    protected View rootView;

    private List<Fragment> mFragments;

    private OrderListNewPagerAapter mPagerAapter;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_saves_order);
        initView();
        mFragments=new ArrayList<>();
        mPagerAapter=new OrderListNewPagerAapter(this.getSupportFragmentManager());
        mFragments.add(WiteSenFragment.newInstance());
        mFragments.add(UnSuccessFragment.newInstance());
        mFragments.add(SuccessFragment.newInstance());
        mPagerAapter.setFragments(mFragments);
        mVpSaverOrdlisit.setAdapter(mPagerAapter);
        mSTabSaverOrderlist.setViewPager(mVpSaverOrdlisit);
        mVpSaverOrdlisit.setOffscreenPageLimit(3);
        mRlBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mGroupName = (TextView) findViewById(R.id.group_name);
        mSTabSaverOrderlist = (SlidingTabLayout) findViewById(R.id.sTab_saver_orderlist);
        mVpSaverOrdlisit = (ViewPager) findViewById(R.id.vp_saver_ordlisit);
    }
}
