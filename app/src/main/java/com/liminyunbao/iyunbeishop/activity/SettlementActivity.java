package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.adapter.SettlementAdapter;
import com.liminyunbao.iyunbeishop.bean.MoneyInforBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.IntentUtils;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 结算管理  （资金）
 */

public class SettlementActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgSettlementBack;
    protected TextView mTvYve;
    protected ListView mListvSettlement;
    protected TextView mTvZijinLiushui;
    private String[] biaoti = {"结算申请", "结算记录"};
    private List<String> mList;
    private SettlementAdapter mAdapter;
    private String type = "ee";
    private  String userid;

    @Override
    protected void HaveNet() {
        httpData();
    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_settlement);
        initView();
         userid = (String) SharePUtile.get(getBaseContext(), "user_id", "");
        String  user_token = (String) SharePUtile.get(getBaseContext(), "user_token", "");
        Log.e("这是",user_token);
        initData();
        httpData();
        mListvSettlement.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                    IntentUtils.goTo(SettlementActivity.this, WithdrawalsActivity.class);
                } else if (position == 1) {

                    IntentUtils.goTo(SettlementActivity.this, DetailsActivity.class);
                }
            }
        });
    }
    /*private  void httpData(){
        String shopid = (String) SharePUtile.get(getBaseContext(), "shop_id", "");
        Log.e("这是是在资金管理的shopid",shopid);
        OkGo.post(HttpAPI.MONEY)
                .tag(this)
                .params("shopid",shopid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Log.e("资金管理",s);
                                Gson gson=new Gson();
                                MoneyInforBean moneyInfor = gson.fromJson(s, MoneyInforBean.class);
                                    mTvYve.setText(moneyInfor.getDate()+"");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e("资金管理onError",e.getMessage());
                    }
                });
    }*/
    private  void httpData(){
      String  user_token = (String) SharePUtile.get(getBaseContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getBaseContext(),"shop_id","");
        OkGo.post(HttpAPI.MONEY)
                .tag(this)
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("shopid",shopid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Log.e("资金管理",s);
                                Gson gson=new Gson();
                                MoneyInforBean moneyInfor = gson.fromJson(s, MoneyInforBean.class);
                                mTvYve.setText(moneyInfor.getDate()+"");
                            }else if(status.equals("0")){
                                Toast.makeText(getApplicationContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(SettlementActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Toast.makeText(getApplicationContext(),"链接超时！",Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initData() {
        mList = new ArrayList<>();
        for (int i = 0; i < biaoti.length; i++) {
            mList.add(biaoti[i]);
        }
        mAdapter = new SettlementAdapter();
        mAdapter.setList(mList);
        mListvSettlement.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void initView() {
        mImgSettlementBack = (ImageView) findViewById(R.id.img_settlement_back);
        mImgSettlementBack.setOnClickListener(SettlementActivity.this);
        mTvYve = (TextView) findViewById(R.id.tv_yve);
        mListvSettlement = (ListView) findViewById(R.id.listv_settlement);
        mTvZijinLiushui = (TextView) findViewById(R.id.tv_zijin_liushui);
        mTvZijinLiushui.setOnClickListener(SettlementActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_zijin_liushui) {
                IntentUtils.goTo(this, SaveZiJinLiuShuiActivity.class);
        } else if (view.getId() == R.id.img_settlement_back) {
            finish();
        }
    }
}
