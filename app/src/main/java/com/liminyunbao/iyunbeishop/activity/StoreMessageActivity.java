package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 修改店铺名
 */
public class StoreMessageActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgStoreMessageBack;
    protected EditText mEdiStoreChange;
    protected Button mBtnChangeSure;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_store_message);
        initView();
        mEdiStoreChange.setCursorVisible(false);
        String shopname = getIntent().getStringExtra("shopname");
        mEdiStoreChange.setText(shopname);
    }

    private void ChangeName(){
        String  userid = (String) SharePUtile.get(getApplicationContext(), "user_id", "");
        String  user_token = (String) SharePUtile.get(getApplicationContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getApplicationContext(),"shop_id","");
        if(mEdiStoreChange.getText().toString()!=null){

            OkGo.post(HttpAPI.CHANGE_SHOP_NAME)
                    .params("user_id",userid)
                    .params("user_token",user_token)
                    .params("shopid",shopid)
                    .params("name",mEdiStoreChange.getText().toString())
                    .execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                String status = jsonObject.getString("status");
                                String msg = jsonObject.getString("msg");
                                if(status.equals("1")){
                                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                                    finish();
                                }else if(status.equals("0")){
                                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                                }else if(status.equals("2")){
                                    TokenUtils.TackUserToken(StoreMessageActivity.this);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


        }else {
            Toast.makeText(getApplicationContext(),"请输入要修改的店名！",Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_store_message_back) {
            finish();
        } else if (view.getId() == R.id.edi_store_change) {
            mEdiStoreChange.setCursorVisible(true);
        } else if (view.getId() == R.id.btn_change_sure) {
            ChangeName();
        }
    }

    private void initView() {
        mImgStoreMessageBack = (ImageView) findViewById(R.id.img_store_message_back);
        mImgStoreMessageBack.setOnClickListener(StoreMessageActivity.this);
        mEdiStoreChange = (EditText) findViewById(R.id.edi_store_change);
        mEdiStoreChange.setOnClickListener(StoreMessageActivity.this);
        mBtnChangeSure = (Button) findViewById(R.id.btn_change_sure);
        mBtnChangeSure.setOnClickListener(StoreMessageActivity.this);
    }
}
