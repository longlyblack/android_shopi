package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.R;

public class OneOpenActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgOpenBack;
    protected ImageView mImgOpenShop;
    protected Button mBtnGetshop;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_one_open);
        initView();
    }

    private void initView() {
        mImgOpenBack = (ImageView) findViewById(R.id.img_open_back);
        mImgOpenBack.setOnClickListener(OneOpenActivity.this);
        mImgOpenShop = (ImageView) findViewById(R.id.img_open_shop);
        mBtnGetshop = (Button) findViewById(R.id.btn_getshop);
        mBtnGetshop.setOnClickListener(OneOpenActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_open_back) {

            finish();
        } else if (view.getId() == R.id.btn_getshop) {
            Intent intent=new Intent(OneOpenActivity.this,EstablishActivity.class);
            startActivity(intent);
        }
    }
}
