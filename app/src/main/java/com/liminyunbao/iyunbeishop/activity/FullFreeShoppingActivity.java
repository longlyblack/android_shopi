package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.FullTitieAdapter;
import com.liminyunbao.iyunbeishop.fragment.FullCutFragment;
import com.liminyunbao.iyunbeishop.fragment.FullCutNFragment;

import java.util.ArrayList;
import java.util.List;

public class FullFreeShoppingActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgAddpackageBack;
    protected RelativeLayout mRlBack;
    protected Button mBtnCardAdd;
    protected SlidingTabLayout mSTabFullFreeShopping;
    protected ViewPager mVpFullFreeShopping;
    protected LinearLayout mActivityFullFreeShopping;
    private FullTitieAdapter fullTitieAdapterm;
    private List<Fragment> fragmentsm;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_full_free_shopping);
        initView();
        fullTitieAdapterm = new FullTitieAdapter(getSupportFragmentManager());
        fragmentsm = new ArrayList<>();
        fragmentsm.add(FullCutFragment.newInstance("4"));//4代表的而是包邮券
        fragmentsm.add(FullCutNFragment.newInstance("4"));
        fullTitieAdapterm.setFragments(fragmentsm);

        mVpFullFreeShopping.setAdapter(fullTitieAdapterm);
        mSTabFullFreeShopping.setViewPager(mVpFullFreeShopping);
        mVpFullFreeShopping.setOffscreenPageLimit(2);


    }

    private void initView() {
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(FullFreeShoppingActivity.this);
        mBtnCardAdd = (Button) findViewById(R.id.btn_card_add);
        mBtnCardAdd.setOnClickListener(FullFreeShoppingActivity.this);
        mSTabFullFreeShopping = (SlidingTabLayout) findViewById(R.id.sTab_full_free_shopping);
        mVpFullFreeShopping = (ViewPager) findViewById(R.id.vp_full_free_shopping);
        mActivityFullFreeShopping = (LinearLayout) findViewById(R.id.activity_full_free_shopping);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {

            finish();
        } else if (view.getId() == R.id.btn_card_add) {
            Intent intent=new Intent(getApplicationContext(),AddFreeActivity.class);
            startActivity(intent);
        }
    }
}
