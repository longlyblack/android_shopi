package com.liminyunbao.iyunbeishop.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.liminyunbao.iyunbeishop.R;

public class AddFreeActivity extends BaseActivity {

    /**
     * 添加满免券
     */
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_free);
    }
}
