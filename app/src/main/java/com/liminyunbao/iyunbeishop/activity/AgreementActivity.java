package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;

/**
 * 云贝协议
 */
public class AgreementActivity extends BaseActivity implements View.OnClickListener {

    protected WebView mWebView;
    protected ImageView mImgArgBack;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_agreement);
        initView();
        mWebView.getSettings().setJavaScriptEnabled(true);
        //设置不用系统浏览器打开,直接显示在当前Webview
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        Intent intent = getIntent();
        String style = intent.getStringExtra("style");
        if(style.equals("1")){
            mWebView.loadUrl(HttpAPI.Agreement);
        }else if(style.equals("2")){
            mWebView.loadUrl(HttpAPI.Check);
        }

    }
    private void initView() {
        mWebView = (WebView) findViewById(R.id.webView);
        mImgArgBack = (ImageView) findViewById(R.id.img_arg_back);
        mImgArgBack.setOnClickListener(AgreementActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_arg_back) {

            finish();
        }
    }
}
