package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.FullTitieAdapter;
import com.liminyunbao.iyunbeishop.fragment.FullCutFragment;
import com.liminyunbao.iyunbeishop.fragment.FullCutNFragment;

import java.util.ArrayList;
import java.util.List;

public class FullFoldingActivity extends BaseActivity implements View.OnClickListener {


    protected SlidingTabLayout mSTabFullFolding;
    protected ViewPager mVpFullFolding;
    protected LinearLayout mActivityFullFolding;
    protected ImageView mImgAddpackageBack;
    protected RelativeLayout mRlBack;
    protected Button mBtnCardAdd;
    private FullTitieAdapter fullTitieAdapterm;
    private List<Fragment> fragmentsm;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_full_folding);
        initView();
        fullTitieAdapterm = new FullTitieAdapter(getSupportFragmentManager());
        fragmentsm = new ArrayList<>();
        fragmentsm.add(FullCutFragment.newInstance("2"));//2代表的而是满折券
        fragmentsm.add(FullCutNFragment.newInstance("2"));
        fullTitieAdapterm.setFragments(fragmentsm);
        mVpFullFolding.setAdapter(fullTitieAdapterm);
        mSTabFullFolding.setViewPager(mVpFullFolding);
        mVpFullFolding.setOffscreenPageLimit(2);
    }
    private void initView() {
        mSTabFullFolding = (SlidingTabLayout) findViewById(R.id.sTab_full_folding);
        mVpFullFolding = (ViewPager) findViewById(R.id.vp_full_folding);
        mActivityFullFolding = (LinearLayout) findViewById(R.id.activity_full_folding);
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(FullFoldingActivity.this);
        mBtnCardAdd = (Button) findViewById(R.id.btn_card_add);
        mBtnCardAdd.setOnClickListener(FullFoldingActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {

            finish();
        } else if (view.getId() == R.id.btn_card_add) {
            Intent intent=new Intent(getApplicationContext(),AddFoldingActivity.class);
            startActivity(intent);
        }
    }
}
