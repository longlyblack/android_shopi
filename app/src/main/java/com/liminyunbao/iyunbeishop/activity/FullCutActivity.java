package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.FullTitieAdapter;
import com.liminyunbao.iyunbeishop.fragment.FullCutFragment;
import com.liminyunbao.iyunbeishop.fragment.FullCutNFragment;

import java.util.ArrayList;
import java.util.List;

public class FullCutActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView imgAddpackageBack;
    protected RelativeLayout rlBack;
    protected Button btnCardAdd;
    protected SlidingTabLayout sTabFullCut;
    protected ViewPager vpFullCut;
    protected LinearLayout activityFullCut;
    private FullTitieAdapter fullTitieAdapterm;
    private List<Fragment> fragmentsm;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_full_cut);
        initView();
        fullTitieAdapterm = new FullTitieAdapter(getSupportFragmentManager());
        fragmentsm = new ArrayList<>();
        fragmentsm.add(FullCutFragment.newInstance("1"));//1代表的而是满减券
        fragmentsm.add(FullCutNFragment.newInstance("1"));
        fullTitieAdapterm.setFragments(fragmentsm);
        vpFullCut.setAdapter(fullTitieAdapterm);
        sTabFullCut.setViewPager(vpFullCut);
        vpFullCut.setOffscreenPageLimit(2);
    }

    private void initView() {
        imgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        rlBack = (RelativeLayout) findViewById(R.id.rl_back);
        rlBack.setOnClickListener(FullCutActivity.this);
        btnCardAdd = (Button) findViewById(R.id.btn_card_add);
        btnCardAdd.setOnClickListener(FullCutActivity.this);
        sTabFullCut = (SlidingTabLayout) findViewById(R.id.sTab_full_cut);
        vpFullCut = (ViewPager) findViewById(R.id.vp_full_cut);
        activityFullCut = (LinearLayout) findViewById(R.id.activity_full_cut);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {
            finish();
        } else if (view.getId() == R.id.btn_card_add) {
            Intent intent=new Intent(getApplicationContext(),AddCutActivity.class);
            startActivity(intent);
        }
    }
}
