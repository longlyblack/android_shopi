package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.adapter.CommodituyListAdapter;
import com.liminyunbao.iyunbeishop.adapter.OthergoodsAdapter;
import com.liminyunbao.iyunbeishop.bean.OhterGoodsBean;
import com.liminyunbao.iyunbeishop.bean.SearchGoodsBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;


/**
 * 商品清单
 */

public class CommodityListActivity extends BaseActivity implements View.OnClickListener {


    protected ListView mListvCommoditylist;
    protected RelativeLayout mRlBack;
    protected TextView mGroupName;
    protected TextView mTvFinishXunzen;
    protected EditText mEdiSearchGoods;
    protected ImageView mImgSearch;
    protected ListView mListvOtherGoods;
    private List<SearchGoodsBean.MsgBean> mStrings;
    private CommodituyListAdapter mListAdapter;
    private Map<Integer, Boolean> mIsSelect;
    private Map<Integer, Boolean> mIsSelectther;
    private Map<Integer, String> mStringMap;
    private List<String> mList;
    private List<String> mCateid;
    private int currentNum = -1;
    private String cateid;

    private String s;//选中商品的id

    private List<OhterGoodsBean.MsgBean.DataBean> data;

    private OthergoodsAdapter mOthergoodsAdapter;

    private  int num = 0;
    private int snum=0;
    private int snums=0;
    private int Asnum=0;
    private boolean isFrist=true;

    private List<String> chiced;

    private List<String> search;

    private static final  int PENGCHOICE=2;
    private boolean  falg=true;
    private KyLoadingBuilder builder;

    /*public void setIsSelectther(Map<Integer, Boolean> isSelectther) {
        mIsSelectther = isSelectther;
    }
    public void setIsSelect(Map<Integer, Boolean> isSelect) {
        mIsSelect = isSelect;
    }*/

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(), "你进入没有网的异次元！", Toast.LENGTH_SHORT).show();
    }
    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(CommodityListActivity.this);
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("上传中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_commodity_list);
        initView();
        mList = new ArrayList<>();
        mStringMap = new HashMap<>();//存放选中商品的id
        mCateid = new ArrayList<>();
        chiced=new ArrayList<>();//存放选中的id
        search=new ArrayList<>();
        Aimoi();
        Intent intent = getIntent();
        String groupname = intent.getStringExtra("groupname");
        cateid = intent.getStringExtra("cateid");
        mGroupName.setText(groupname);
        otherGoods();
        searchEdtextChange();
        mListvOtherGoods.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             /*   if (currentNum != position) { //选中*/
                    if (mIsSelectther.get(position)) {
                        mOthergoodsAdapter.isSelected.put(position, false);
                       // mList.set(position,mStrings.get(position).getBase_goods_id()+"");
                        mList.set(position,"1");
                        Asnum--;
                        currentNum = position;
                        getOthernum();
                    } else {
                        mOthergoodsAdapter.isSelected.put(position, true);
                        mList.set(position,data.get(position).getGoods_agent_id()+"");
                        currentNum = position;
                        getOthernum();
                    }

                 /*else if (currentNum == position) { //未选中
                    if (mIsSelectther.get(position)) {
                        mOthergoodsAdapter.isSelected.put(position, false);
                        mList.set(position,"1");
                        Asnum--;
                        currentNum = -1;
                        getOthernum();
                    } else {
                        mOthergoodsAdapter.isSelected.put(position, true);
                        mList.set(position,data.get(position).getGoods_agent_id()+"");
                        currentNum = -1;
                        getOthernum();
                    }

                }*/

                mOthergoodsAdapter.notifyDataSetChanged();
            }
        });

        /**
         * item 和chickbox 联系
         */
        mListvCommoditylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              /*  if (currentNum != position) { //选中*/
                    if (mIsSelect.get(position)) {
                        mListAdapter.isSelected.put(position, false);
                        mList.set(position,"1");
                        Asnum--;
                        getChicknum();
                        currentNum = position;
                    } else {
                        mListAdapter.isSelected.put(position, true);
                        mList.set(position,mStrings.get(position).getGoods_agent_id()+"");
                        currentNum = position;
                        getChicknum();
                    }

                /*else if (currentNum == position) { //未选中
                    if (mIsSelect.get(position)) {
                        mListAdapter.isSelected.put(position, false);
                        mList.set(position,"1");
                        Asnum--;
                        getChicknum();
                        currentNum = -1;
                    } else {
                        mListAdapter.isSelected.put(position, true);
                        mList.set(position,mStrings.get(position).getGoods_agent_id()+"");
                        getChicknum();
                        currentNum = -1;
                    }

                }*/

                mListAdapter.notifyDataSetChanged();
            }
        });

                wancheng();


    }

    private void searchEdtextChange() {
        mEdiSearchGoods.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //  Log.e("搜索",s.toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //ShopGrougoods();
                mListvOtherGoods.setVisibility(View.GONE);
                mListvCommoditylist.setVisibility(View.VISIBLE);
                Log.e("搜索", s.toString());
                ShopGrougoods(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    /**
     * 其他商品的额东西
     */
    private void otherGoods() {
        OkGo.post(HttpAPI.OTHERGOODS)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("cate_id", cateid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {

                        Log.e("其他",s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                OhterGoodsBean ohterGoodsBean = gson.fromJson(s, OhterGoodsBean.class);
                                data = ohterGoodsBean.getMsg().getData();
                                mIsSelectther = new HashMap<Integer, Boolean>();
                                mOthergoodsAdapter = new OthergoodsAdapter(data, getApplicationContext(), mIsSelectther);
                                mListvOtherGoods.setAdapter(mOthergoodsAdapter);
                                mOthergoodsAdapter.notifyDataSetChanged();

                                for(int i=0;i<data.size();i++){
                                    mList.add("1");
                                }

                                mOthergoodsAdapter.setChickbox(new OthergoodsAdapter.chickbox() {
                                    @Override
                                    public void ischickbox(CheckBox checkBox, final int position) {
                                        checkBox.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                            /*    if (currentNum != position) { //选中*/
                                                    if (mIsSelectther.get(position)) {
                                                        mOthergoodsAdapter.isSelected.put(position, false);
                                                        // mList.set(position,mStrings.get(position).getBase_goods_id()+"");
                                                        mList.set(position,"1");
                                                        Asnum--;
                                                        currentNum = position;
                                                        getOthernum();
                                                    } else {
                                                        mOthergoodsAdapter.isSelected.put(position, true);
                                                        mList.set(position,data.get(position).getGoods_agent_id()+"");
                                                        currentNum = position;
                                                        getOthernum();
                                                    }

                                                /*else if (currentNum == position) { //未选中
                                                    if (mIsSelectther.get(position)) {
                                                        mOthergoodsAdapter.isSelected.put(position, false);
                                                        mList.set(position,"1");
                                                        Asnum--;
                                                        currentNum = -1;
                                                        getOthernum();
                                                    } else {
                                                        mOthergoodsAdapter.isSelected.put(position, true);
                                                        mList.set(position,data.get(position).getGoods_agent_id()+"");
                                                        currentNum = -1;
                                                        getOthernum();
                                                    }

                                                }*/

                                                mOthergoodsAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                });

                            } else if (status.equals("0")) {
                                ToastUtil.showShort(getApplicationContext(), "暂无数据");
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    /**
     * 搜索的商品
     *
     * @param name
     */
    private void ShopGrougoods(String name) {
        Log.e("搜索", "执行了");
        OkGo.post(HttpAPI.SEARCHGOODSB)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("cate_id", cateid)
                .params("name", name)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("分组商品查询", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                SearchGoodsBean searchGoodsBean = gson.fromJson(s, SearchGoodsBean.class);
                                mStrings = searchGoodsBean.getMsg();
                                mIsSelect = new HashMap<>();
                                mListAdapter = new CommodituyListAdapter(getApplicationContext(), mStrings, mIsSelect);
                                mListvCommoditylist.setAdapter(mListAdapter);
                                mListAdapter.notifyDataSetChanged();
                                for(int i=0;i<mStrings.size();i++){
                                    mCateid.add("1");
                                }
                                mListAdapter.setChickbox(new CommodituyListAdapter.chickbox() {
                                    @Override
                                    public void ischickbox(CheckBox checkBox, final int position) {
                                        checkBox.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                               /* if (mIsSelect.get(posion)) {
                                                    mIsSelect.put(posion, false);
                                                    setIsSelect(mIsSelect);
                                                    getChicknum();
                                                } else {
                                                    mIsSelect.put(posion, true);
                                                    setIsSelect(mIsSelect);
                                                    getChicknum();
                                                }*/
                                             /*   if (currentNum != position) { //选中*/
                                                    if (mIsSelect.get(position)) {
                                                        mListAdapter.isSelected.put(position, false);
                                                        mList.set(position,"1");
                                                        Asnum--;
                                                        getChicknum();
                                                        currentNum = position;
                                                    } else {
                                                        mListAdapter.isSelected.put(position, true);
                                                        mList.set(position,mStrings.get(position).getGoods_agent_id()+"");
                                                        currentNum = position;
                                                        getChicknum();
                                                    }

                                                 /*else if (currentNum == position) { //未选中
                                                    if (mIsSelect.get(position)) {
                                                        mListAdapter.isSelected.put(position, false);
                                                        mList.set(position,"1");
                                                        Asnum--;
                                                        getChicknum();
                                                        currentNum = -1;
                                                    } else {
                                                        mListAdapter.isSelected.put(position, true);
                                                        mList.set(position,mStrings.get(position).getGoods_agent_id()+"");
                                                        getChicknum();
                                                        currentNum = -1;
                                                    }

                                                }*/

                                                mListAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                });
                            } else if (status.equals("0")) {
                                ToastUtil.showShort(getApplicationContext(), "搜索失败");
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(CommodityListActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e("分组商品查询", e.getMessage().toString());
                    }
                });
    }



    private void getOthernum(){
        num=0;
        snum=0;
        for(int i=0;i<mIsSelectther.size();i++){
            if(mIsSelectther.get(i)){
                num ++;
               // mList.add(data.get(i).getGoods_agent_id() + "");
            }
        }
        snum=num;
        Asnum=snum+snums;
        mTvFinishXunzen.setText("完成(" + Asnum + ")");
    }

    /**
     * 商品选中的个数
     */
    private void getChicknum() {
        num=0;
        snums=0;
        for (int i = 0; i < mIsSelect.size(); i++) {
            if (mIsSelect.get(i)) {
                num ++;
                // mStringMap.put(i,mStrings.get(i).getBase_goods_id()+"");
               // mList.add(mStrings.get(i).getGoods_agent_id() + "");
            }
        }
        snums=num;
        Asnum=snum+snums;

        mTvFinishXunzen.setText("完成(" + Asnum + ")");
       // complete();
    }

    /**
     * 选中的商品id的形式
     */
    private void complete() {
        Gson g = new Gson();

        for(int i=0;i<mList.size();i++){
            if(!mList.get(i).equals("1")){
                chiced.add(mList.get(i));
            }

        }
        for(int i=0;i<mCateid.size();i++){
            if(!mCateid.get(i).equals("1")){
                chiced.add(mCateid.get(i));
            }

        }
        s = g.toJson(chiced);
        Log.e("嗯嗯", s);
    }

    /**
     * 完成选择
     */

    private void wancheng() {

        mTvFinishXunzen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(num==0){
                    ToastUtil.showShort(getApplicationContext(),"选择商品数不能为空");
                }else {
                    if(falg){
                        complete();
                        OkGo.post(HttpAPI.EDITGROUTGOODS)
                                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                                .params("user_id", UserUtils.Userid(getApplicationContext()))
                                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                                .params("list", s)
                                .params("cate_id", cateid)
                                .execute(new StringCallback() {
                                    @Override
                                    public void onBefore(BaseRequest request) {
                                        super.onBefore(request);
                                        builder.show();
                                    }
                                    @Override
                                    public void onSuccess(String s, Call call, Response response) {

                                        Log.e("嗯嗯", s);
                                        try {
                                            JSONObject jsonObject = new JSONObject(s);
                                            String status = jsonObject.getString("status");
                                            if (status.equals("1")) {
                                                falg=false;
                                                String msg = jsonObject.getString("msg");
                                                ToastUtil.showShort(getApplicationContext(),msg);
                                                Intent intent=new Intent(getApplicationContext(),ShopDetailsActivity.class);
                                                setResult(PENGCHOICE,intent);
                                                finish();
                                            } else if (status.equals("0")) {

                                            }else if(status.equals("2")){
                                                TokenUtils.TackUserToken(CommodityListActivity.this);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onAfter(String s, Exception e) {
                                        super.onAfter(s, e);
                                        builder.dismiss();
                                    }

                                    @Override
                                    public void onError(Call call, Response response, Exception e) {
                                        super.onError(call, response, e);
                                        Log.e("编辑分组商品", e.toString());
                                        builder.dismiss();
                                    }
                                });
                    }

                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {
            finish();
        }
    }

    private void initView() {
        mListvCommoditylist = (ListView) findViewById(R.id.listv_commoditylist);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(CommodityListActivity.this);
        mGroupName = (TextView) findViewById(R.id.group_name);
        mTvFinishXunzen = (TextView) findViewById(R.id.tv_finish_xunzen);
        mEdiSearchGoods = (EditText) findViewById(R.id.edi_search_goods);
        mImgSearch = (ImageView) findViewById(R.id.img_search);
        mListvOtherGoods = (ListView) findViewById(R.id.listv_other_goods);
    }


}
