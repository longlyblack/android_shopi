package com.liminyunbao.iyunbeishop.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.adapter.ShopDetailsAdapter;
import com.liminyunbao.iyunbeishop.bean.GroupDetalBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class ShopDetailsActivity extends BaseActivity implements View.OnClickListener {
    private static final  int PENGDETAIL=1;
    private static final  int PENGCHOICE=2;
    protected RelativeLayout mRlBack;
    protected TextView mGroupName;
    protected Button mBtnShopDetailAdd;
    //  protected SwipeMenuRecyclerView mRecyclewhatgoods;

    protected PullToRefreshListView mListvShopDetail;
    //protected VerticalSwipeRefreshLayout mSwipeRefresh;
    private List<GroupDetalBean.MsgBean.DataBean> data;
    private List<GroupDetalBean.MsgBean.DataBean> AllData;
    private boolean mIsLoading;
    private String cateid;

    private int page = 1;
    private int last_page = 0;
   // private RecycleshopAdapter mRecycleshopAdapter;
   private Dialog mDialog;
    protected LinearLayoutManager mLayoutManager;
    protected int lastVisibleItem = 0;
    private List<GroupDetalBean.MsgBean> mList = new ArrayList<>();
    private ShopDetailsAdapter mShopDetailsAdapter;
    private String groupname;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_shop_details2);
        initView();
        AllData = new ArrayList<>();
        Intent intent = getIntent();
        groupname = intent.getStringExtra("groupname");
        cateid = intent.getStringExtra("cateid");
        mGroupName.setText(groupname);
        pullrefish();
        httpgoos();
       mListvShopDetail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                deletphoto(position);
            }
        });
    }
    private void deletphoto(final int position){
        mDialog = new Dialog(this, R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(true);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(30, 0, 30,0);
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate = LayoutInflater.from(this).inflate(R.layout.item_detail_shop, null);
        Button cancel = (Button) inflate.findViewById(R.id.cancle);
        Button delet = (Button) inflate.findViewById(R.id.delet);

        delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String goodid= AllData.get(position-1).getGoods_agent_id()+"";
                String cate = AllData.get(position-1).getCate_id()+"";
                Log.e("测试"," 点击了"+(position-1));
                Log.e("测试"," cateid:"+cate+"goodid:"+goodid);
                deteitem(cate, goodid,position);
                mDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setContentView(inflate);

    }

    private void pullrefish() {
        mListvShopDetail.setMode(PullToRefreshBase.Mode.BOTH);
        mListvShopDetail.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                AllData.clear();
                page = 1;
                httpgoos();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

                if (page != last_page) {
                    page++;
                    httpgoos();
                } else {
                    mListvShopDetail.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListvShopDetail.onRefreshComplete();
                        }
                    }, 1000);
                }
            }
        });
        mShopDetailsAdapter = new ShopDetailsAdapter(this);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate = new Date(System.currentTimeMillis());
        String time = format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvShopDetail.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mShopDetailsAdapter.notifyDataSetChanged();
    }

    private void httpgoos() {
        OkGo.post(HttpAPI.GROUPDETALIS)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("cate_id", cateid)
                .params("page",page)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("商品详情", s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                GroupDetalBean groupDetalBean = gson.fromJson(s, GroupDetalBean.class);
                                last_page = groupDetalBean.getMsg().getLast_page();
                                data = groupDetalBean.getMsg().getData();
                                AllData.addAll(data);

                                for(int i=0;i<AllData.size();i++){
                                    Log.e("嗯嗯",AllData.get(i).getGoods_agent_id()+"");

                                }

                                mShopDetailsAdapter.setList(AllData);
                                mListvShopDetail.setAdapter(mShopDetailsAdapter);
                                mShopDetailsAdapter.notifyDataSetChanged();
                                mListvShopDetail.onRefreshComplete();
                            } else if (status.equals("0")) {
                                ToastUtil.showShort(getApplicationContext(), "暂无数据");
                            }

                        } catch (JSONException e) {


                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e("商品详情", e.getMessage().toString());
                    }
                });

    }


    /**
     * 删除条目
     */

    private void deteitem(String cate, String good, final int posin) {
        OkGo.post(HttpAPI.DELETGOOS)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("goods_agent_id", good)
                .params("cate_id", cate)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("删除", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                AllData.remove(posin-1);
                                mShopDetailsAdapter.notifyDataSetChanged();
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(), msg);
                            } else if (status.equals("0")) {
                                ToastUtil.showShort(getApplicationContext(), "删除失败");
                            } else if (status.equals("2")) {
                                JSONObject data = jsonObject.getJSONObject("data");
                                String info = data.getString("info");
                                ToastUtil.showShort(getApplicationContext(), info);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e("删除", e.getMessage().toString());
                    }
                });


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {
            finish();
        } else if (view.getId() == R.id.btn_shop_detail_add) {

            Intent intent = new Intent(ShopDetailsActivity.this, CommodityListActivity.class);
            intent.putExtra("groupname", groupname);
            intent.putExtra("cateid", cateid);
            startActivityForResult(intent,PENGCHOICE);
           // startActivity(intent);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==PENGCHOICE){
            AllData.clear();
            httpgoos();

        }
    }

    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(ShopDetailsActivity.this);
        mGroupName = (TextView) findViewById(R.id.group_name);
        mBtnShopDetailAdd = (Button) findViewById(R.id.btn_shop_detail_add);
        mBtnShopDetailAdd.setOnClickListener(ShopDetailsActivity.this);
        mListvShopDetail = (PullToRefreshListView) findViewById(R.id.listv_shopDetail);
    }

}
