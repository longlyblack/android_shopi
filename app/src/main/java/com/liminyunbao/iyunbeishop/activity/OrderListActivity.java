package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.flyco.tablayout.SlidingTabLayout;
import com.liminyunbao.iyunbeishop.adapter.OrderListAdapter;
import com.liminyunbao.iyunbeishop.fragment.AllOrderlListlFragment;
import com.liminyunbao.iyunbeishop.fragment.ConsumptionFragent;
import com.liminyunbao.iyunbeishop.fragment.RefundFragment;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单列表
 */
public class OrderListActivity extends BaseActivity {

    protected SlidingTabLayout mSTabOrderlist;
    protected ViewPager mVpOrderlist;
    private List<Fragment>mFragments;
    private OrderListAdapter mOrderListAdapter;


    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_order_list);
        initView();
        initVp();

    }
    private void initVp(){
        mOrderListAdapter=new OrderListAdapter(getSupportFragmentManager());
        mFragments=new ArrayList<>();
        mFragments.add(ConsumptionFragent.newInstance());
        mFragments.add(AllOrderlListlFragment.newInstance());
        mFragments.add(RefundFragment.newInstance());
        mOrderListAdapter.setFragments(mFragments);
        mVpOrderlist.setAdapter(mOrderListAdapter);
        mSTabOrderlist.setViewPager(mVpOrderlist);

    }

    private void initView() {
        mSTabOrderlist = (SlidingTabLayout) findViewById(R.id.sTab_orderlist);
        mVpOrderlist = (ViewPager) findViewById(R.id.vp_orderlist);

    }

}
