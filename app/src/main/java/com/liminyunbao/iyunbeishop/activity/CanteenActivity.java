package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.adapter.CanteenFragmentAdapter;
import com.liminyunbao.iyunbeishop.fragment.NewOrderFragment;
import com.liminyunbao.iyunbeishop.fragment.OlderOrderFragment;
import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;

public class CanteenActivity extends BaseActivity {


    /**
     * 食堂订单
     */
    protected TabLayout mTabCantten;
    protected ViewPager mVpCantten;
    protected LinearLayout mActivityCanteen;
    private CanteenFragmentAdapter mCanteenFragmentAdapter;
    private List<Fragment> mFragments;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_canteen);
        initView();
        initTv();
    }

    private void initTv(){
        mCanteenFragmentAdapter=new CanteenFragmentAdapter(getSupportFragmentManager());
        mFragments=new ArrayList<>();
        mFragments.add(NewOrderFragment.newInstance());
        mFragments.add(OlderOrderFragment.newInstance());
        mCanteenFragmentAdapter.setFragments(mFragments);
        mVpCantten.setAdapter(mCanteenFragmentAdapter);
        mTabCantten.setupWithViewPager(mVpCantten);
        mTabCantten.setTabMode(TabLayout.MODE_FIXED);

    }


    private void initView() {
        mTabCantten = (TabLayout) findViewById(R.id.tab_cantten);
        mVpCantten = (ViewPager) findViewById(R.id.vp_cantten);
        mActivityCanteen = (LinearLayout) findViewById(R.id.activity_canteen);
    }



}
