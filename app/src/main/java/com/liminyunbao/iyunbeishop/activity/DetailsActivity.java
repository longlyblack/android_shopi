package com.liminyunbao.iyunbeishop.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.liminyunbao.iyunbeishop.adapter.CapitalFlowAdapter;
import com.liminyunbao.iyunbeishop.bean.ApplyBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 交易明细（提现记录）
 */

public class DetailsActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgDetailsBack;
    protected PullToRefreshListView mListvDetails;
    // protected Pull mListvDetails;
    private KyLoadingBuilder builder;

    private CapitalFlowAdapter mAdapter;

    private List<ApplyBean.DataBeanX.DataBean> mDataBeen;
    private int page=1;
    private int last_page=0;

    private String userid;
    private List<ApplyBean.DataBeanX.DataBean> mAllBean=new ArrayList<>();

    @Override
    protected void HaveNet() {
    }
    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_details);
        Aimoi();
        initView();
        userid = (String) SharePUtile.get(getBaseContext(), "user_id", "");
        initData();
        pullfresh();
    }

    private void pullfresh(){
        mListvDetails.setMode(PullToRefreshBase.Mode.BOTH);
        mListvDetails.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                //开始下拉  我们做Http请求
                Aimoi();
                mAllBean.clear();
                page=1;
                initData();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                Aimoi();
                if(page!=last_page){
                    initData();
                    page++;
                }else{
                    mListvDetails.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mListvDetails .onRefreshComplete();
                        }
                    }, 1000);
                }

            }

        });

        mAdapter=new CapitalFlowAdapter();
        mListvDetails.setAdapter(mAdapter);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate    =   new Date(System.currentTimeMillis());
        String time=format.format(curDate);
        ILoadingLayout loadingLayoutProxy = mListvDetails.getLoadingLayoutProxy();
        loadingLayoutProxy.setRefreshingLabel("正在刷新..."); // 刷新时
        loadingLayoutProxy.setPullLabel("下拉刷新"); // 刚下拉时，显示的提示
        loadingLayoutProxy.setLoadingDrawable(getResources().getDrawable(R.mipmap.dxuanze));
        loadingLayoutProxy.setLastUpdatedLabel(time); //一般是上次刷新的时间
        loadingLayoutProxy.setReleaseLabel("松手开始刷新");
        mAdapter.notifyDataSetChanged();

    }

    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(this);
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("正在加载中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭

    }
    private void initData() {
        Aimoi();
        mDataBeen=new ArrayList<>();
        String user_token = (String) SharePUtile.get(getBaseContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getBaseContext(),"shop_id","");
        OkGo.post(HttpAPI.APPLY)
                .connTimeOut(100000)
                .params("user_id",userid)
                .params("user_token",user_token)
                .params("shopid", shopid)
                .params("page",""+page)
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);
                        builder.show();
                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                Gson gson = new Gson();
                                Log.e("申请记录",s);
                                ApplyBean applyBean = gson.fromJson(s, ApplyBean.class);
                                last_page=applyBean.getData().getLast_page();
                                mDataBeen = applyBean.getData().getData();
                                mAllBean.addAll(mDataBeen);
                                mAdapter.setList(mAllBean);
                                mAdapter.notifyDataSetChanged();
                                mListvDetails.onRefreshComplete();
                            }else if(status.equals("0")){
                                Toast.makeText(getApplicationContext(),"暂无数据！",Toast.LENGTH_SHORT).show();
                                mListvDetails.onRefreshComplete();
                            }else if(status.equals("2")){
                                TokenUtils.TackUserToken(DetailsActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);
                        builder.dismiss();
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);

                        Toast.makeText(getApplicationContext(),"链接超时！",Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void initView() {
        mImgDetailsBack = (ImageView) findViewById(R.id.img_details_back);
        mImgDetailsBack.setOnClickListener(DetailsActivity.this);
        // mListvDetails = (ListView) findViewById(R.id.listv_details);
        mListvDetails = (PullToRefreshListView) findViewById(R.id.listv_details);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_details_back) {
            finish();
        }
    }
}
