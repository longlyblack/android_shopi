package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 店铺地址
 */
public class StroeAddressActivity extends BaseActivity implements View.OnClickListener {

    protected ImageView mImgStoreaddressBack;
    protected TextView mTextAddress;
    protected ImageView mImgPosition;
    protected TextView mTvAddress;
    protected TextView mTvMenNum;
    protected EditText mEdiMenpaiNum;
    protected Button mBtnSaveAddress;
    protected RelativeLayout mRlPosition;
    private int RECORD=1;
    private String latitude;
    private String longitude;
    private String Addres;



    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {
        Toast.makeText(getApplicationContext(),"你进入没有网的异次元！",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_stroe_address);
        initView();

         Intent intent = getIntent();
        String addr = intent.getStringExtra("addr");
        String mapaddr = intent.getStringExtra("mapaddr");

        longitude = intent.getStringExtra("lng");
        latitude = intent.getStringExtra("lat");
        Log.e("ces",mapaddr+"   "+addr);
        if(mapaddr.equals("null")){
            mTvAddress.setText("请选择店铺地址");
        }else {
            mTvAddress.setText(mapaddr);
        }

        mEdiMenpaiNum.setText(addr);
        mEdiMenpaiNum.setCursorVisible(false);

    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_storeaddress_back) {
            finish();
        } else if (view.getId() == R.id.edi_menpai_num) {
            mEdiMenpaiNum.setCursorVisible(true);
        } else if (view.getId() == R.id.btn_save_address) {
            ChangAddres();

        } else if (view.getId() == R.id.rl_position) {
            Intent  intent=new Intent(this,RegionActivity.class);
            intent.putExtra("isRegist","1");
            startActivityForResult(intent,RECORD);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RECORD){
            mTvAddress.setText(data.getStringExtra("poiname"));
            Addres=data.getStringExtra("poiname");
            latitude=data.getStringExtra("latitude").substring(0,10);
            longitude=data.getStringExtra("longitude").substring(0,10);
        }
    }
    private void ChangAddres(){
        String  userid = (String) SharePUtile.get(getApplicationContext(), "user_id", "");
        String  user_token = (String) SharePUtile.get(getApplicationContext(), "user_token", "");
        String shopid= (String) SharePUtile.get(getApplicationContext(),"shop_id","");
        if(longitude!=null&&latitude!=null&&mEdiMenpaiNum.getText().toString()!=null){
            OkGo.post(HttpAPI.CHANGE_ADDRES)
                    .params("user_id",userid)
                    .params("shopid",shopid)
                    .params("user_token",user_token)
                    .params("mapaddr",Addres)
                    .params("addr",mEdiMenpaiNum.getText().toString())
                    .params("lng",longitude)
                    .params("lat",latitude)
                    .execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            Log.e("这是修改",s);
                            try {
                                JSONObject jsonObject=new JSONObject(s);
                                String status = jsonObject.getString("status");
                                String msg = jsonObject.getString("msg");
                                if(status.equals("1")){
                                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                                    finish();
                                }else if(status.equals("0")){
                                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                                }else if(status.equals("2")){
                                    TokenUtils.TackUserToken(StroeAddressActivity.this);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }else {
            Toast.makeText(getApplicationContext(),"信息不整！",Toast.LENGTH_SHORT).show();
        }
    }
    private void initView() {
        mImgStoreaddressBack = (ImageView) findViewById(R.id.img_storeaddress_back);
        mImgStoreaddressBack.setOnClickListener(StroeAddressActivity.this);
        mTextAddress = (TextView) findViewById(R.id.text_address);
        mImgPosition = (ImageView) findViewById(R.id.img_position);
        mTvAddress = (TextView) findViewById(R.id.tv_address);
        mTvMenNum = (TextView) findViewById(R.id.tv_men_num);
        mEdiMenpaiNum = (EditText) findViewById(R.id.edi_menpai_num);
        mEdiMenpaiNum.setOnClickListener(StroeAddressActivity.this);
        mBtnSaveAddress = (Button) findViewById(R.id.btn_save_address);
        mBtnSaveAddress.setOnClickListener(StroeAddressActivity.this);
        mRlPosition = (RelativeLayout) findViewById(R.id.rl_position);
        mRlPosition.setOnClickListener(StroeAddressActivity.this);
    }
}
