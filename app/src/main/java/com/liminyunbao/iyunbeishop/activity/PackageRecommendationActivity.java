package com.liminyunbao.iyunbeishop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.adapter.RecommentAdapter;
import com.liminyunbao.iyunbeishop.bean.PackageRecommendation;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 推荐套餐列表
 */
public class PackageRecommendationActivity extends BaseActivity {

    protected ImageView mImgAddpackageBack;
    protected RelativeLayout mRlBack;
    protected ListView mListvRecommendation;
    protected TextView mText;
    protected TextView mTvNow;
    protected TextView mTvMax;
    protected RelativeLayout mRlAddnew;
    private RecommentAdapter mRecommentAdapter;
    private List<PackageRecommendation.ShopMassageBean> mData;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_package_recommendation);
        initView();
        inithttp();
        mListvRecommendation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                PackageRecommendation.ShopMassageBean shopMassageBean = mData.get(position);
                Intent intent = new Intent(getApplicationContext(), AddPackageActivity.class);
                intent.putExtra("goodid", shopMassageBean.getGoods_agent_id()+"");
                startActivity(intent);


            }
        });
        mRlAddnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mData.size()!=5){
                    Intent intent = new Intent(getApplicationContext(), AddPackageActivity.class);
                    intent.putExtra("goodid", "0");
                    startActivity(intent);
                }else {
                    ToastUtil.showShort(getApplicationContext(),"最多能添加5种套餐");
                }

            }
        });
        mImgAddpackageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * 数据请求
     */

    private void inithttp() {
        OkGo.post(HttpAPI.MEALLITE)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .execute(new StringCallback() {


                    @Override
                    public void onSuccess(final String s, Call call, Response response) {
                        Log.e("彭博",s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                mData = new ArrayList<PackageRecommendation.ShopMassageBean>();
                                Gson gson = new Gson();
                                PackageRecommendation packageRecommendation = gson.fromJson(s, PackageRecommendation.class);
                                mData = packageRecommendation.getData();
                                mRecommentAdapter = new RecommentAdapter(getApplicationContext());
                                mRecommentAdapter.setList(mData);
                                mTvMax.setText("最多可推荐种套餐"+packageRecommendation.getMax()+"种套餐");
                                mTvNow.setText("全部商品("+packageRecommendation.getNow()+")/");
                                mListvRecommendation.setAdapter(mRecommentAdapter);
                                mRecommentAdapter.notifyDataSetChanged();
                                mRecommentAdapter.setUporDown(new RecommentAdapter.UporDown() {
                                    @Override
                                    public void Upord(final LinearLayout backg, final RelativeLayout shang, final RelativeLayout upord, RelativeLayout edit, RelativeLayout delet, final TextView textViewupord, final int goodid) {

                                        shang.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                upod(goodid, "1",textViewupord,shang,upord,backg);
                                            }
                                        });

                                        upord.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                upod(goodid, "0",textViewupord,shang,upord,backg);

                                              /*  String upod = textViewupord.getText().toString();
                                                if (upod.equals("上架")) {
                                                     upod(goodid, "0",textViewupord);

                                                } else if (upod.equals("下架")) {
                                                   upod(goodid, "1",textViewupord);
                                                }*/
                                            }
                                        });

                                        edit.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ToastUtil.showShort(getApplicationContext(), "编辑");
                                                Intent intent = new Intent(getApplicationContext(), AddPackageActivity.class);
                                                intent.putExtra("goodid", goodid+"");
                                                startActivity(intent);

                                            }
                                        });
                                        delet.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dele(goodid);
                                            }
                                        });

                                    }
                                });
                            } else if (status.equals("0")) {
                                ToastUtil.showShort(getApplicationContext(), "暂无数据");
                            } else if (status.equals("2")) {
                                TokenUtils.TackUserToken(PackageRecommendationActivity.this);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * 上下架
     */

    private void upod(int goodid, final String cmd, final TextView textView, final RelativeLayout shang, final RelativeLayout upord, final LinearLayout backg) {
        OkGo.post(HttpAPI.MeallUPOD)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("goods_id", goodid + "")
                .params("cmd", cmd)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                               /* if(cmd.equals("0")){
                                   // textView.setText("下架");
                                    upord.setVisibility(View.GONE);
                                    shang.setVisibility(View.VISIBLE);
                                    backg.setBackgroundResource(R.color.colorWhite);
                                }else if(cmd.equals("1")){
                                    //textView.setText("上架");

                                    upord.setVisibility(View.VISIBLE);
                                    shang.setVisibility(View.GONE);
                                    backg.setBackgroundResource(R.color.colorXiangqing);
                                }*/
                                inithttp();
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(), msg);
                            } else if (status.equals("0")) {
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(), msg);
                            } else if (status.equals("2")) {
                                TokenUtils.TackUserToken(PackageRecommendationActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        ToastUtil.showShort(getApplicationContext(), e.getMessage().toString());
                    }
                });

    }

    /**
     * 删除
     */
    private void dele(int goodid) {

        OkGo.post(HttpAPI.MEALLDELET)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("goods_id", goodid + "")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(), msg);
                                inithttp();

                            } else if (status.equals("0")) {
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(), msg);
                            } else if (status.equals("2")) {
                                TokenUtils.TackUserToken(PackageRecommendationActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        ToastUtil.showShort(getApplicationContext(), e.getMessage().toString());
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        inithttp();
    }

    private void initView() {
        mImgAddpackageBack = (ImageView) findViewById(R.id.img_addpackage_back);
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mListvRecommendation = (ListView) findViewById(R.id.listv_recommendation);
        mText = (TextView) findViewById(R.id.text);
        mTvNow = (TextView) findViewById(R.id.tv_now);
        mTvMax = (TextView) findViewById(R.id.tv_max);
        mRlAddnew = (RelativeLayout) findViewById(R.id.rl_addnew);
    }
}
