package com.liminyunbao.iyunbeishop.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.adapter.CategoryRightAdapter;
import com.liminyunbao.iyunbeishop.adapter.CategoyrLeftAdapter;
import com.liminyunbao.iyunbeishop.adapter.ClassficationAdapter;
import com.liminyunbao.iyunbeishop.adapter.HorelistAdapter;
import com.liminyunbao.iyunbeishop.adapter.RecViewAddshopAdapter;
import com.liminyunbao.iyunbeishop.bean.AnyEventType;
import com.liminyunbao.iyunbeishop.bean.CategoryBean;
import com.liminyunbao.iyunbeishop.bean.GoodsXiangqingBean;
import com.liminyunbao.iyunbeishop.bean.ShangPinGroup;
import com.liminyunbao.iyunbeishop.bean.XingHaoBean;
import com.liminyunbao.iyunbeishop.custom.ChildListView;
import com.liminyunbao.iyunbeishop.custom.PinchImageView;
import com.liminyunbao.iyunbeishop.GetPicture;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.PictureUtil;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.AboutBitMap;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.TokenUtils;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.liminyunbao.iyunbeishop.view.HorizontalListView;
import com.liminyunbao.iyunbeishop.view.KyLoadingBuilder;
import com.liminyunbao.iyunbeishop.view.SelectPicPopupWindow;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.request.BaseRequest;
import com.lzy.okgo.request.PostRequest;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class SaverAddnewShopActivity extends BaseActivity implements View.OnClickListener {
    private static final int PICTURE = 20;
    private static final int TUWEN = 12;
    private static final int XUZHI = 13;
    private static final int DESCRIBE = 14;
    private static final int TOWGROUP = 11;
    private static final int PERMISSION_CORDD = 5;
    private static final int  ADDPTUWEN=112;
    protected ImageView mImgAddshopBack;
    protected ImageView mImgShopPhoto;
    protected ImageView mImgAddPhoto;
    protected EditText mEditShopName;
    protected EditText mEditDescribe;
    protected EditText mEditOldPric;
    protected EditText mEditNowPric;
    protected EditText mEditStock;
    protected EditText mEditSpecifications;
    protected TextView mClassification;
    protected Button mBtnIn;
    protected Button mBtnShelves;
    protected TextView mShaopfication;
    protected ChildListView mChilisetAddGuige;
    protected Button mBtnAddnewGuige;
    protected TextView mTvGroupf;
    protected ImageView mImgTuwen;
    protected TextView mTvTuwen;
    protected RelativeLayout mRlTuwen;
    protected ImageView mImgXuzhi;
    protected TextView mTvXuzhi;
    protected RelativeLayout mLrXuzhi;
    // protected RecyclerView mRecvPic;
    protected ImageView mImgDescribe;
    protected TextView mTvDescribe;
    protected RelativeLayout mLrDescribe;
    protected HorizontalListView mHorlistPic;
    protected LinearLayout mXiangji;
    private Dialog mDialog;
    // private SaleBean.parmeter.DataBean dataBean;
    // private List<ClassFication.DataBean> data1;
    private ClassficationAdapter mAdapter;
    private ListView listView;
    private PinchImageView bigimg;
    private static Uri mUri;
    private String getUrl = null;
    private String mapurl;
    private String shopid;
    private int classId;
    private int catea_id;
    String picHead = "data:image/jpeg;base64,"; //base 64图片字节头
    private String type;
    private String base_goods_id;
    private int ADDCODE = 0;
    private String catename;
    private String user_token;
    private String userid;
    private ListView right;
    private ListView left;
    private CategoyrLeftAdapter leftAdapter;
    private CategoryRightAdapter rightAdatper;
    private List<CategoryBean.TatilBean.KeyBean> key;
    private List<CategoryBean.TatilBean.KeyBean.DataBean> mRightData;
    private String category = null;

    private List<String> deled_specid = new ArrayList<>();
    private List<ShangPinGroup.DataBean.MsgBean> msg;
    private int tyoe;

    private boolean chiic = true;
    private int spec_id;
    private TextView myself;
    private String personInfos;
    // private EditText edit4;
    private RecViewAddshopAdapter mRecadapter;
    private HorelistAdapter mHorelistAdapter;
    private String list = "[]";//这是选择后的cateid
    private String dislist = "[]";//这是未选中的cateid

    private File currentImageFile;
    private int size = 0;//这是型号的结合大小

    private List<EditText> mEditTexts;
    private List<Integer> group;
    private boolean show = true;
    private int sunm = 0;//商品库存总数
    private int prompt[] = {R.string.shopname, R.string.shopoldpric, R.string.shopnewpric, R.string.shopstor, R.string.shopguige};
    private boolean flag = true;
    private int shopclas[] = {R.string.shopfeilei, R.string.shopgrop};
   // private List<GoodsXiangqingBean.DataBean> data = new ArrayList<>();
    private List<String> mUriString = new ArrayList<>();
    ;//这是轮播图的集合图

    private int picnum = 0;//控制轮播图的参数
    private List<String> upUriString;//存放转换后返回的uri的集合
    private Gson gson;
    private String picture = null;
    private String notes = null;//购买须知
    private String details = null;//商品描述
    private List<GoodsXiangqingBean.DataBean.PictureBean> photo;//轮播图列表
    private List<String> mPicid = new ArrayList<>();//轮播图的id
    private String shanchu;//要删除的图文的pic_id
    private List<GetPicture> mpicban = new ArrayList<>();
    private List<GoodsXiangqingBean.DataBean.PhotoBean> tuwwen;//图文详情列表
    private KyLoadingBuilder builder;
    private List<String> groupid = new ArrayList<>();//存放分组id

    private List<XingHaoBean> mBeanList = new ArrayList<>();//存放不为空的型号类对象

    private boolean flagstor = true;
    private ProgressDialog progressDialog;

    private AddNewChildAdapter mAdapterguige;

    private GoodsXiangqingBean.DataBean data;
    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_saver_addnew_shop);
        initView();
        Aimoi();
        if (null == progressDialog) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("执行中……");
        }
        photo = new ArrayList<>();
        EventBus.getDefault().register(this);
        upUriString = new ArrayList<>();
        user_token = (String) SharePUtile.get(getApplicationContext(), "user_token", "");
        shopid = (String) SharePUtile.get(this, "shop_id", "");
        userid = (String) SharePUtile.get(getBaseContext(), "user_id", "");
        tyoe = (int) SharePUtile.get(getBaseContext(), "shop_typ", 0);
        mAdapter = new ClassficationAdapter();
        oldPrice();
        GetEditext();
        addData();
    }

    /**
     * 限制输入小数点到两位
     */
    private void oldPrice() {
        mEditOldPric.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = s.toString();
                int posDot = temp.indexOf(".");
                if (posDot <= 0) return;
                if (temp.length() - posDot - 1 > 2) {
                    s.delete(posDot + 3, posDot + 4);
                }
            }

        });

        mEditNowPric.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = s.toString();
                int posDot = temp.indexOf(".");
                if (posDot <= 0) return;
                if (temp.length() - posDot - 1 > 2) {
                    s.delete(posDot + 3, posDot + 4);
                }

            }
        });
    }

    /**
     * 作为提示初始化
     */
    private void GetEditext() {
        mEditTexts = new ArrayList<>();
        mEditTexts.add(mEditShopName);
        mEditTexts.add(mEditDescribe);
        mEditTexts.add(mEditOldPric);
        mEditTexts.add(mEditNowPric);
        mEditTexts.add(mEditStock);
        mEditTexts.add(mEditSpecifications);
        group = new ArrayList<>();
        group.add(classId);
        group.add(catea_id);
    }

    /**
     * 根据传过来的额type 判断是那个界面传过来的goodsid
     */

    private void addData() {
        Intent intent = this.getIntent();
        type = intent.getStringExtra("type");
        if (type.equals("onsale")) {
            Log.e("这是啥", "wozhixingle");
            base_goods_id = intent.getStringExtra("goodsid");
            getHttp(base_goods_id);
            ADDCODE = 3;
        } else if (type.equals("salout")) {
            base_goods_id = intent.getStringExtra("goodsid");
            getHttp(base_goods_id);
            ADDCODE = 4;
        } else if (type.equals("waresale")) {
            base_goods_id = intent.getStringExtra("goodsid");
            getHttp(base_goods_id);
            ADDCODE = 5;
        } else if (type.equals("addnew")) {
            ADDCODE = 6;
            base_goods_id = "0";
            mAdapterguige = new AddNewChildAdapter(this);
            mChilisetAddGuige.setAdapter(mAdapterguige);
            mBtnAddnewGuige.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAdapterguige.arr.add(new GoodsXiangqingBean.DataBean.XinghaolistBean(Integer.parseInt("0"), "", "", ""));
                    mAdapterguige.notifyDataSetChanged();
                }
            });
            mAdapterguige.setXinghaoEdit(new AddNewChildAdapter.XinghaoEdit() {
                @Override
                public void xingHaoEdite(final EditText editText1, final EditText editText2, final EditText editText3, final int position, ImageView delet) {
                         /*
            *
            * 限制输入小数点到后两位
            * */
                    editText2.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            String temp = s.toString();
                            int posDot = temp.indexOf(".");
                            if (posDot <= 0) return;
                            if (temp.length() - posDot - 1 > 2) {
                                s.delete(posDot + 3, posDot + 4);
                            }
                        }
                    });

                    editText1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                editText1.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        Log.e("第一个", s + "");
                                        if (mAdapterguige.arr.size() > 0) {

                                            mAdapterguige.arr.set(position, new GoodsXiangqingBean.DataBean.XinghaolistBean(mAdapterguige.arr.get(position).getSpec_id(), s + "", editText2.getText().toString(), editText3.getText().toString()));

                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                    }
                                });
                            }
                        }
                    });
                    editText2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {

                            if (hasFocus) {
                                editText2.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        Log.e("第二个", s + "");
                                        if (mAdapterguige.arr.size() > 0) {

                                            mAdapterguige.arr.set(position, new GoodsXiangqingBean.DataBean.XinghaolistBean(mAdapterguige.arr.get(position).getSpec_id(), editText1.getText().toString(), s + "", editText3.getText().toString()));
                          /*  if(!TextUtils.isEmpty(edit2.getText().toString())){
                            }*/
                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                    }
                                });
                            }
                        }
                    });
                    editText3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {

                            if (hasFocus) {
                                editText3.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        Log.e("第三个", s + "");
                                        if (mAdapterguige.arr.size() > 0) {
                                            mAdapterguige.arr.set(position, new GoodsXiangqingBean.DataBean.XinghaolistBean(mAdapterguige.arr.get(position).getSpec_id(), editText1.getText().toString(), editText2.getText().toString(), s + ""));
                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                    }
                                });
                            }
                        }
                    });
                    delet.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {
                            //： spec_id 放在数组中是吧？是的
                            //从集合中删除所删除项的EditText的内
                            spec_id = mAdapterguige.arr.get(position).getSpec_id();
                            if (spec_id != 0) {
                                deled_specid.add(spec_id + "");
                            }

                  /*  if (!Arrays.asList(upSpecIDArr).contains(spec_id + "")) {
                        String[] tempIDarr = upSpecIDArr;
                        upSpecIDArr = new String[tempIDarr.length + 1];
                        for (int i = 0; i < tempIDarr.length; i++)
                            upSpecIDArr[i] = tempIDarr[i];
                        if(spec_id!=0){
                            upSpecIDArr[upSpecIDArr.length - 1] = spec_id + "";
                        }else{
                        }
                    }*/
                            mAdapterguige.arr.remove(position);
                            mAdapterguige.notifyDataSetChanged();
                        }
                    });
                }
            });

            addmore();
        }

    }


    /**
     * 添加多图
     */
    private void addmore() {
        mHorelistAdapter = new HorelistAdapter(mUriString, this);
        mHorlistPic.setAdapter(mHorelistAdapter);
        mHorelistAdapter.notifyDataSetChanged();
        mHorlistPic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             /*   mUriString.remove(position);
                mpicban.remove(position);
                mPicid.add(photo.get(position).getPic_id()+"");
                mHorelistAdapter.notifyDataSetChanged();*/
                deletphoto(position);
            }
        });

      /*  //添加多图
        mRecadapter = new RecViewAddshopAdapter(this);
        mRecadapter.setBitmaps(mUriString);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecvPic.setLayoutManager(manager);
        mRecvPic.setAdapter(mRecadapter);
        mRecadapter.notifyDataSetChanged();
        mRecadapter.setOnItemClickListener(new RecViewAddshopAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int posion) {
            }
            @Override
            public void imgclick(ImageView imgm, int posion) {
            }
        });
*/
    }

    private void deletphoto(final int position) {
        mDialog = new Dialog(this, R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(true);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(30, 0, 30, 0);
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate = LayoutInflater.from(this).inflate(R.layout.item_save_tuwen_pic, null);
        Button cancel = (Button) inflate.findViewById(R.id.cancle);
        Button delet = (Button) inflate.findViewById(R.id.delet);

        delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUriString.remove(position);
                mpicban.remove(position);
                if (photo.size() > 0) {
                    mPicid.add(photo.get(position).getPic_id() + "");
                }
                picnum--;
                mHorelistAdapter.notifyDataSetChanged();
                mDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setContentView(inflate);

    }

    /**
     * 存放band的集合
     */

    private void getinl() {
        for (int i = 0; i < photo.size(); i++) {
            mpicban.add(new GetPicture(photo.get(i).getPic_id() + "", photo.get(i).getPhoto()));
        }

    }

    /**
     * 将band转成json
     */

    private void tojson() {
        gson = new Gson();
        getUrl = gson.toJson(mpicban);

    }

    /**
     * goods_id 不为空的时候进行数据请求
     */
    private void getHttp(String goodsid) {
        flag = false;//编辑商品的时候防止弹toast
        OkGo.post(HttpAPI.SHANGPINXIANGQIANGADD)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .params("base_goods_id", goodsid)
                .connTimeOut(10000)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("全部参数", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                GoodsXiangqingBean goodsXiangqingBean = gson.fromJson(s, GoodsXiangqingBean.class);
                                 data = goodsXiangqingBean.getData();
                               // SaverAddnewShopActivity.this.data = goodsXiangqingBean.getData().getData();
                                String buyknow = SaverAddnewShopActivity.this.data.getBuyknow();
                                notes = buyknow;
                                String goods_remark = SaverAddnewShopActivity.this.data.getGoods_remark();
                                details = goods_remark;
                                tuwwen =data.getPhoto();
                                List<GoodsXiangqingBean.DataBean.XinghaolistBean> xinghaolist = SaverAddnewShopActivity.this.data.getXinghaolist();
                                mAdapterguige = new AddNewChildAdapter(getApplicationContext());
                                mChilisetAddGuige.setAdapter(mAdapterguige);
                                mAdapterguige.setArr((ArrayList<GoodsXiangqingBean.DataBean.XinghaolistBean>) xinghaolist);

                                //添加型号
                                mBtnAddnewGuige.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mAdapterguige.arr.add(new GoodsXiangqingBean.DataBean.XinghaolistBean(Integer.parseInt("0"), "", "", ""));
                                        mAdapterguige.notifyDataSetChanged();
                                    }
                                });
                                //回调
                                mAdapterguige.setXinghaoEdit(new AddNewChildAdapter.XinghaoEdit() {
                                    @Override
                                    public void xingHaoEdite(final EditText editText1, final EditText editText2, final EditText editText3, final int position, ImageView delet) {
                         /*
            *
            * 限制输入小数点到后两位
            * */
                                        editText2.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {
                                                String temp = s.toString();
                                                int posDot = temp.indexOf(".");
                                                if (posDot <= 0) return;
                                                if (temp.length() - posDot - 1 > 2) {
                                                    s.delete(posDot + 3, posDot + 4);
                                                }
                                            }
                                        });

                                        editText1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                            @Override
                                            public void onFocusChange(View v, boolean hasFocus) {

                                                if (hasFocus) {
                                                    editText1.addTextChangedListener(new TextWatcher() {
                                                        @Override
                                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                        }

                                                        @Override
                                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                            if (mAdapterguige.arr.size() > 0) {
                                                                mAdapterguige.arr.set(position, new GoodsXiangqingBean.DataBean.XinghaolistBean(mAdapterguige.arr.get(position).getSpec_id(), s + "", editText2.getText().toString(), editText3.getText().toString()));
                                                            }
                                                        }

                                                        @Override
                                                        public void afterTextChanged(Editable s) {

                                                        }
                                                    });
                                                }
                                            }
                                        });
                                        editText2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                            @Override
                                            public void onFocusChange(View v, boolean hasFocus) {
                                                if (hasFocus) {
                                                    editText2.addTextChangedListener(new TextWatcher() {
                                                        @Override
                                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                        }

                                                        @Override
                                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                            if (mAdapterguige.arr.size() > 0) {
                                                                mAdapterguige.arr.set(position, new GoodsXiangqingBean.DataBean.XinghaolistBean(mAdapterguige.arr.get(position).getSpec_id(), editText1.getText().toString(), s + "", editText3.getText().toString()));
                                                            }
                                                        }

                                                        @Override
                                                        public void afterTextChanged(Editable s) {

                                                        }
                                                    });


                                                }

                                            }
                                        });
                                        editText3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                            @Override
                                            public void onFocusChange(View v, boolean hasFocus) {

                                                if (hasFocus) {
                                                    editText3.addTextChangedListener(new TextWatcher() {
                                                        @Override
                                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                        }

                                                        @Override
                                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                            if (mAdapterguige.arr.size() > 0) {
                                                                mAdapterguige.arr.set(position, new GoodsXiangqingBean.DataBean.XinghaolistBean(mAdapterguige.arr.get(position).getSpec_id(), editText1.getText().toString(), editText2.getText().toString(), s + ""));
                                                            }
                                                        }

                                                        @Override
                                                        public void afterTextChanged(Editable s) {

                                                        }
                                                    });
                                                }
                                            }
                                        });

                                        delet.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View arg0) {
                                                //： spec_id 放在数组中是吧？是的
                                                //从集合中删除所删除项的EditText的内
                                                spec_id = mAdapterguige.arr.get(position).getSpec_id();
                                                if (spec_id != 0) {
                                                    deled_specid.add(spec_id + "");
                                                }
                                                mAdapterguige.arr.remove(position);
                                                mAdapterguige.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                });

                                mEditShopName.setText(SaverAddnewShopActivity.this.data.getTitle());
                                mEditShopName.setSelection(SaverAddnewShopActivity.this.data.getTitle().length());
                                String simple_title = SaverAddnewShopActivity.this.data.getSimple_title();
                                if (simple_title != null) {
                                    mEditDescribe.setText(SaverAddnewShopActivity.this.data.getSimple_title());
                                    mEditDescribe.setSelection(SaverAddnewShopActivity.this.data.getSimple_title().length());
                                } else {
                                    mEditDescribe.setText("");
                                }
                                //getUrl = dataBean.getPhoto();
                                photo = SaverAddnewShopActivity.this.data.getPicture();
                                if (photo.size() > 0) {
                                    for (int i = 0; i < SaverAddnewShopActivity.this.photo.size(); i++) {
                                        picnum++;
                                        mUriString.add(photo.get(i).getPhoto());
                                        upUriString.add(SaverAddnewShopActivity.this.photo.get(i).getPhoto());
                                    }
                                }
                                addmore();//添加多图
                                getinl();//轮播图
                                mEditOldPric.setText(SaverAddnewShopActivity.this.data.getMarket_price() + "");
                                mEditNowPric.setText(SaverAddnewShopActivity.this.data.getGoods_price() + "");
                                mEditStock.setText(SaverAddnewShopActivity.this.data.getSunnum() + "");
                                mEditSpecifications.setText(SaverAddnewShopActivity.this.data.getGuige());
                              //  mClassification.setText(data.get(0).getCate_name());
                                //  classId = data.get(0).getCate_id();//获取分类数据
                                List<GoodsXiangqingBean.DataBean.CateaNameBean> catea_name = SaverAddnewShopActivity.this.data.getCatea_name();

                                if (catea_name.size() != 0) {
                                    mShaopfication.setText("已选择");
                                } else {
                                    mShaopfication.setText("选择分组");
                                }
                              /*  if (classId == 0) {
                                    mClassification.setText("暂无分类");
                                }*/
                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e("这是啥", e.getMessage().toString());
                    }
                });
    }

    private void Aimoi() {
        //加载动画
        builder = new KyLoadingBuilder(SaverAddnewShopActivity.this);
        builder.setIcon(R.mipmap.dxuanze);
        builder.setText("上传中...");
        builder.setOutsideTouchable(false);//点击空白区域是否关闭
        builder.setBackTouchable(true);//按返回键是否关闭
        //builder.dismiss();//关闭
    }

    private void initView() {
        mImgAddshopBack = (ImageView) findViewById(R.id.img_addshop_back);
        mImgAddshopBack.setOnClickListener(SaverAddnewShopActivity.this);
        mTvGroupf = (TextView) findViewById(R.id.tv_groupf);
        mTvGroupf.setOnClickListener(SaverAddnewShopActivity.this);
        mEditShopName = (EditText) findViewById(R.id.edit_shop_name);
        mEditDescribe = (EditText) findViewById(R.id.edit_describe);
        mImgAddPhoto = (ImageView) findViewById(R.id.img_add_photo);
        mImgAddPhoto.setOnClickListener(SaverAddnewShopActivity.this);
        mHorlistPic = (HorizontalListView) findViewById(R.id.horlist_pic);
        mShaopfication = (TextView) findViewById(R.id.shaopfication);
        mShaopfication.setOnClickListener(SaverAddnewShopActivity.this);
        mEditOldPric = (EditText) findViewById(R.id.edit_old_pric);
        mEditNowPric = (EditText) findViewById(R.id.edit_now_pric);
        mEditStock = (EditText) findViewById(R.id.edit_stock);
        mEditSpecifications = (EditText) findViewById(R.id.edit_specifications);
        mChilisetAddGuige = (ChildListView) findViewById(R.id.chiliset_add_guige);
        mBtnAddnewGuige = (Button) findViewById(R.id.btn_addnew_guige);
        mBtnAddnewGuige.setOnClickListener(SaverAddnewShopActivity.this);
        mImgTuwen = (ImageView) findViewById(R.id.img_tuwen);
        mTvTuwen = (TextView) findViewById(R.id.tv_tuwen);
        mRlTuwen = (RelativeLayout) findViewById(R.id.rl_tuwen);
        mRlTuwen.setOnClickListener(SaverAddnewShopActivity.this);
        mImgXuzhi = (ImageView) findViewById(R.id.img_xuzhi);
        mTvXuzhi = (TextView) findViewById(R.id.tv_xuzhi);
        mLrXuzhi = (RelativeLayout) findViewById(R.id.lr_xuzhi);
        mLrXuzhi.setOnClickListener(SaverAddnewShopActivity.this);
        mImgDescribe = (ImageView) findViewById(R.id.img_describe);
        mTvDescribe = (TextView) findViewById(R.id.tv_describe);
        mLrDescribe = (RelativeLayout) findViewById(R.id.lr_describe);
        mLrDescribe.setOnClickListener(SaverAddnewShopActivity.this);
        mBtnIn = (Button) findViewById(R.id.btn_in);
        mBtnIn.setOnClickListener(SaverAddnewShopActivity.this);
        mBtnShelves = (Button) findViewById(R.id.btn_shelves);
        mBtnShelves.setOnClickListener(SaverAddnewShopActivity.this);
        mXiangji = (LinearLayout) findViewById(R.id.xiangji);

    }

    /**
     * 选择分类弹出的dialog
     */
  /*  private void showDialog() {
        leftAdapter = new CategoyrLeftAdapter();
        mDialog = new Dialog(this, R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(40, 0, 40, 0);
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate = LayoutInflater.from(this).inflate(R.layout.item_category_dialog, null);
        right = (ListView) inflate.findViewById(R.id.listv_category_right);
        left = (ListView) inflate.findViewById(R.id.lsitv_category_left);
        initCategory();
        left.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mRightData = key.get(position).getData();
                leftAdapter.setSelectedPosition(position);
                leftAdapter.notifyDataSetChanged();
                initright(position);
            }
        });
        right.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mClassification.setText(mRightData.get(position).getCate_name());
                category = mRightData.get(position).getCate_name();
                classId = mRightData.get(position).getCate_id();
                mDialog.dismiss();
            }
        });
        mDialog.setContentView(inflate);
    }
*/
    /**
     * 分类数据 Http获取数据
     */
    /*private void initCategory() {
        OkGo.get(HttpAPI.FENLEI)
                .params("user_token", user_token)
                .params("user_id", userid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                CategoryBean categoryBean = gson.fromJson(s, CategoryBean.class);
                                key = categoryBean.getData().getKey();
                                leftAdapter.setKey(key);
                                left.setAdapter(leftAdapter);
                                leftAdapter.notifyDataSetChanged();
                                leftAdapter.setSelectedPosition(0);
                                mRightData = key.get(0).getData();
                                initright(0);
                            } else if (status.equals("0")) {
                                Toast.makeText(getApplicationContext(), "暂无数据！", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
*/
    /**
     * 右边list绑定
     *
     * @param position
     */
  /*  private void initright(int position) {
        rightAdatper = new CategoryRightAdapter();
        rightAdatper.setDataBeen(mRightData);
        rightAdatper.setTitalPositon(position);
        right.setAdapter(rightAdatper);
        rightAdatper.notifyDataSetChanged();
    }
*/

    /**
     * 图片上传
     */
    private void setImageToView(Bitmap bitmap) {
        EventBus.getDefault().post(new AnyEventType("上传"));
        Bitmap nbitmap = bitmap;
        mapurl = AboutBitMap.bitmapToBase64(nbitmap);
        OkGo.post(HttpAPI.PHOTO)
                .params("user_token", user_token)
                .params("base64", picHead + mapurl)
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);
                    }

                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        String substring = s.substring(1, s.length());
                        getUrl = substring;
                        mUriString.add(getUrl);
                        mpicban.add(new GetPicture("0", getUrl));
                        upUriString.add(getUrl);
                        mHorelistAdapter.notifyDataSetChanged();
                        picnum++;
                        EventBus.getDefault().post(new AnyEventType("上传完成"));
                    }

                    @Override
                    public void onAfter(String s, Exception e) {
                        super.onAfter(s, e);

                    }
                });
    }


    /**
     * 上架销售
     */
    private void shelves() throws JSONException {
        WitEditet();
        if (details == null || TextUtils.isEmpty(details)) {
            details = "0";
        }
        if (notes == null || TextUtils.isEmpty(notes)) {
            notes = "0";
        }
        if (picture == null) {
            picture = "[]";
        }
        if (shanchu == null) {
            shanchu = "[]";
        }

        if (flagstor == true) {
            Log.e("全部参数", "user_id：" + UserUtils.Userid(getApplicationContext()) + "\n" + "shopid:" + UserUtils.Shopid(getApplicationContext())
                    + "\n" + "user_token:" + UserUtils.UserToken(getApplicationContext()) + "\n" + "name:" + mEditShopName.getText().toString() + "\n" + "fenleiid:" + classId + "" + "\n"
                    + "shop_price:" + mEditNowPric.getText().toString() + "\n" + "market_price" + mEditOldPric.getText().toString() + "\n" + "sunnum:" + sunm + "" + "\n" + "guige:" + mEditSpecifications.getText().toString() +
                    "\n" + "base_goods_id" + base_goods_id + "\n" + "img:" + getUrl + "\n" + "xinghao" + personInfos + "\n" + "notes:" + notes + "\n" + "detail:" + details + "\n" + "photo:" + picture
                    + "\n" + "delephoto:" + shanchu + "\n" + "editzulist:" + list + "\n" + "delezulist:" + dislist + "\n" + "deleimg:" + new Gson().toJson(mPicid) + "\n" + "dele:" + new Gson().toJson(deled_specid));
            PostRequest params = OkGo.post(HttpAPI.ADDGOODS)
                    .params("user_id", UserUtils.Userid(getApplicationContext()))
                    .params("shopid", UserUtils.Shopid(getApplicationContext()))
                    .params("user_token", UserUtils.UserToken(getApplicationContext()))
                    .params("name", mEditShopName.getText().toString())
                    .params("jieshao", "")
                    .params("fenleiid", classId + "")
                    .params("shop_price", mEditNowPric.getText().toString())
                    .params("market_price", mEditOldPric.getText().toString())
                    .params("sunnum", sunm + "")
                    .params("guige", mEditSpecifications.getText().toString())
                    .params("base_goods_id", base_goods_id + "")
                    .params("status", "1")
                    .params("img", getUrl)//轮播图
                    .params("xinghao", personInfos)//型号
                    .params("notes", notes)//购买须知
                    .params("detail", details)//详情描述
                    .params("photo", picture)//图文列表
                    .params("delephoto", shanchu)//图文列表删除
                    .params("editzulist", list)//分组列表id
                    .params("delezulist", dislist)//分组删除列表
                    .params("deleimg", new Gson().toJson(mPicid))//轮播图删除
                    .params("dele", new Gson().toJson(deled_specid));//型号删除
            params.execute(new StringCallback() {

                @Override
                public void onBefore(BaseRequest request) {
                    super.onBefore(request);
                    progressDialog.show();
                }

                @Override
                public void onSuccess(String s, Call call, Response response) {
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        if (status.equals("1")) {
                            chiic = false;
                            mBtnShelves.setClickable(false);
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("refresh", "1");
                            setResult(ADDCODE, intent);
                            finish();
                        } else if (status.equals("0")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        } else if (status.equals("2")) {
                            TokenUtils.TackUserToken(SaverAddnewShopActivity.this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAfter(String s, Exception e) {
                    super.onAfter(s, e);
                    progressDialog.dismiss();
                }

                @Override
                public void onError(Call call, Response response, Exception e) {
                    super.onError(call, response, e);
                    Log.e("上架销售", e.getMessage().toString());
                    builder.dismiss();
                    ToastUtil.showShort(getApplicationContext(), e.getMessage().toString());
                }
            });
        } else {
            ToastUtil.showShort(getApplicationContext(), "库存不能为0切不能为空");
        }

    }

    /**
     * 判断那个Editext没有输入
     */
    private void WitEditet() {
        sunm = 0;
        //String classid = classId + "";
        int kucun = 0;//判断是等于这个；
        gson = new Gson();
       /* if (classid == null) {
            Toast.makeText(SaverAddnewShopActivity.this, "请选择分类", Toast.LENGTH_SHORT).show();
        }*/
        /*判断是否有东西没填 并提示*/
        for (int i = 0; i < mEditTexts.size(); i++) {
            if (TextUtils.isEmpty(mEditTexts.get(i).getText())) {
                Toast.makeText(getApplication(), prompt[i], Toast.LENGTH_SHORT).show();
            }

        }

        if (flag) {
            Toast.makeText(getApplication(), "请选择图片", Toast.LENGTH_SHORT).show();
        }
        size = mAdapterguige.arr.size();
        Log.e("型号长度", size + "");
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                String knum = mAdapterguige.arr.get(i).getNum();
                String name = mAdapterguige.arr.get(i).getName();
                String price = mAdapterguige.arr.get(i).getPrice();
                if (!TextUtils.isEmpty(knum) && !knum.equals("") && knum.length() > 0) {
                    if (Integer.parseInt(mAdapterguige.arr.get(i).getNum()) != 0 && knum.length() > 0) {
                        kucun++;
                        int num = Integer.parseInt(mAdapterguige.arr.get(i).getNum());
                        sunm = sunm + num;
                    }

                }
                if (!TextUtils.isEmpty(knum) && !knum.equals("") && !TextUtils.isEmpty(name) && !TextUtils.isEmpty(price)) {
                    mBeanList.add(new XingHaoBean(mAdapterguige.arr.get(i).getName(), mAdapterguige.arr.get(i).getPrice(), mAdapterguige.arr.get(i).getSpec_id() + "", mAdapterguige.arr.get(i).getNum()));
                }
            }
            if (kucun == size) {
                flagstor = true;
            } else if (kucun != size) {
                mBeanList.clear();
                flagstor = false;
            }

        } else if (size == 0) {

            if (mEditStock.getText().toString().length() != 0) {
                String s = mEditStock.getText().toString();
                sunm = Integer.parseInt(s);
            }

        }
        personInfos = gson.toJson(mBeanList);
        getUrl = gson.toJson(upUriString);
        Log.e("型号长度", "类" + personInfos);
        tojson();
    }

    ////////////////////////////下面为相机和图片选择拍照功能部分(Modify By YangTK 2017.06.17)///////////////////////////////
    /**
     * 选择照片或者拍照
     */
    SelectPicPopupWindow menuWindow;

    private void tackPhoto() {
        menuWindow = new SelectPicPopupWindow(this, itemsOnClick);
        menuWindow.showAtLocation(findViewById(R.id.xiangji),
                Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);

    }


    private View.OnClickListener itemsOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            menuWindow.dismiss();
            switch (v.getId()) {
                case R.id.takePhotoBtn:     //选择相机拍照
//                    if ()
                    AndPermission.with(SaverAddnewShopActivity.this)
                            .requestCode(100)
                            .permission(Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE)
//                            .rationale(rationaleListener)
                            .callback(listener)
                            .start();

                    break;
                case R.id.pickPhotoBtn:     //选择相册
                    openPic();
                    break;
                default:
                    break;
            }
        }
    };

    private int REQUEST_CODE_SETTING = 300;
    private PermissionListener listener = new PermissionListener() {
        @Override
        public void onSucceed(int requestCode, List<String> grantedPermissions) {
            // 权限申请成功回调。

            // 这里的requestCode就是申请时设置的requestCode。
            // 和onActivityResult()的requestCode一样，用来区分多个不同的请求。
            if (requestCode == 100) {
                // TODO ...
                openCamera();
            }
        }

        @Override
        public void onFailed(int requestCode, List<String> deniedPermissions) {
            // 权限申请失败回调。
            if (requestCode == 100) {
                // TODO ...
                // 第一种：用默认的提示语。
                AndPermission.defaultSettingDialog(SaverAddnewShopActivity.this, REQUEST_CODE_SETTING).show();
            }
        }
    };


    /**
     * 调用相机     相机拍照之后 一般都是一个  时间.jpg 文件
     */
    private File mFile;
    /**
     * 定义三种状态
     */
    private static final int REQUESTCODE_PIC = 1;//相册
    private static final int REQUESTCODE_CAM = 2;//相机
    private static final int REQUESTCODE_CUT = 3;//图片裁剪

    private void openCamera() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            if (!file.exists()) {
                file.mkdirs();
            }
            mFile = new File(file, System.currentTimeMillis() + ".jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mFile));
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            startActivityForResult(intent, REQUESTCODE_CAM);
        } else {
            Toast.makeText(this, "请确认已经插入SD卡", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 打开相册
     */
    private void openPic() {
        Intent picIntent = new Intent(Intent.ACTION_PICK, null);
        picIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(picIntent, REQUESTCODE_PIC);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {//返回码是可用的
            switch (requestCode) {
                case REQUESTCODE_CAM:

                    final String absolutePath = mFile.getAbsolutePath();
                   /* mUriString.add(absolutePath);
                    mHorelistAdapter.notifyDataSetChanged();*/
                    final Bitmap bitmap = PictureUtil.compressSizeImage(absolutePath);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            setImageToView(bitmap);
                        }
                    }).start();

                    break;
                case REQUESTCODE_PIC:
                    Uri uri = data.getData();

                    final String s = parsePicturePath(getBaseContext(), uri);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            setImageToView(PictureUtil.compressSizeImage(s));
                        }
                    }).start();
                    break;
            }

        } else if (resultCode == TOWGROUP) {
            String catelist = data.getStringExtra("catelist");
            list = data.getStringExtra("list");
            dislist = data.getStringExtra("listw");
            if (catelist.equals("1")) {
                mShaopfication.setText("已添加");
            }
        } else if (resultCode == XUZHI) {
            if (data.getStringExtra("bo").length() != 0) {
                notes = data.getStringExtra("bo");
            } else {
                notes = "0";
            }
        } else if (resultCode == DESCRIBE) {
            if (data.getStringExtra("bo").length() != 0) {
                details = data.getStringExtra("bo");
            } else {
                details = "0";
            }
        } else if (resultCode == ADDPTUWEN) {

            String pic = data.getStringExtra("pic");
            String delepic = data.getStringExtra("delephoto");
            if (pic != null) {
                picture = pic;
            }
            if (delepic != null) {
                shanchu = delepic;
            }
            Log.e("上传", pic);
        }

    }


////////////////////////////上面为相机和图片选择拍照功能部分(Modify By YangTK 2017.06.17)///////////////////////////////

    /**
     * 放入仓库
     */
    private void in() throws JSONException {
        WitEditet();
        if (details == null || TextUtils.isEmpty(details)) {
            details = "0";
        }
        if (notes == null || TextUtils.isEmpty(notes)) {
            notes = "0";
        }
        if (picture == null) {
            picture = "[]";
        }
        if (shanchu == null) {
            shanchu = "[]";
        }
        if (flagstor == true) {
            Log.e("全部参数", "user_id：" + UserUtils.Userid(getApplicationContext()) + "\n" + "shopid:" + UserUtils.Shopid(getApplicationContext())
                    + "\n" + "user_token:" + UserUtils.UserToken(getApplicationContext()) + "\n" + "name:" + mEditShopName.getText().toString() + "\n" + "fenleiid:" + classId + "" + "\n"
                    + "shop_price:" + mEditNowPric.getText().toString() + "\n" + "market_price" + mEditOldPric.getText().toString() + "\n" + "sunnum:" + sunm + "" + "\n" + "guige:" + mEditSpecifications.getText().toString() +
                    "\n" + "base_goods_id" + base_goods_id + "\n" + "img:" + getUrl + "\n" + "xinghao" + personInfos + "\n" + "notes:" + notes + "\n" + "detail:" + details + "\n" + "photo:" + picture
                    + "\n" + "delephoto:" + shanchu + "\n" + "editzulist:" + list + "\n" + "delezulist:" + dislist + "\n" + "deleimg:" + new Gson().toJson(mPicid) + "\n" + "dele:" + new Gson().toJson(upSpecIDArr));
            PostRequest params = OkGo.post(HttpAPI.ADDGOODS)
                    .params("user_id", UserUtils.Userid(getApplicationContext()))
                    .params("shopid", UserUtils.Shopid(getApplicationContext()))
                    .params("user_token", UserUtils.UserToken(getApplicationContext()))
                    .params("name", mEditShopName.getText().toString())
                    .params("jieshao","")
                    .params("fenleiid", classId + "")
                    .params("shop_price", mEditNowPric.getText().toString())
                    .params("market_price", mEditOldPric.getText().toString())
                    .params("sunnum", sunm + "")
                    .params("guige", mEditSpecifications.getText().toString())
                    .params("base_goods_id", base_goods_id + "")
                    .params("status", "2")
                    .params("img", getUrl)//轮播图
                    .params("xinghao", personInfos)//型号
                    .params("notes", notes)//购买须知
                    .params("detail", details)//详情描述
                    .params("photo", picture)//图文列表
                    .params("delephoto", shanchu)//图文列表删除
                    .params("editzulist", list)//分组列表id
                    .params("delezulist", dislist)//分组删除列表
                    .params("deleimg", new Gson().toJson(mPicid))//轮播图删除
                    .params("dele", new Gson().toJson(deled_specid));//型号删除
            params.execute(new StringCallback() {

                @Override
                public void onBefore(BaseRequest request) {
                    super.onBefore(request);
                    progressDialog.show();
                }

                @Override
                public void onSuccess(String s, Call call, Response response) {
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        if (status.equals("1")) {
                            chiic = false;
                            mBtnShelves.setClickable(false);
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("refresh", "1");
                            setResult(ADDCODE, intent);
                            finish();
                        } else if (status.equals("0")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        } else if (status.equals("2")) {
                            TokenUtils.TackUserToken(SaverAddnewShopActivity.this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAfter(String s, Exception e) {
                    super.onAfter(s, e);
                    progressDialog.dismiss();
                }

                @Override
                public void onError(Call call, Response response, Exception e) {
                    super.onError(call, response, e);
                    Log.e("上架销售", e.getMessage().toString());
                    builder.dismiss();
                    ToastUtil.showShort(getApplicationContext(), e.getMessage().toString());
                }
            });
        } else {
            ToastUtil.showShort(getApplicationContext(), "库存不能为0切不能为空");
        }
    }


    /**
     * EventBus功能部分
     * 此处主要用到的功能为：在NewTaskService中接收到新订单之后，那边会发送一个通知，然后这边会通知新任务界面去刷新界面
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final AnyEventType bean) {
        //这里接受到消息
        Log.e("haha", bean.getS());
        if (bean.getS().equals("上传")) {
         /*   if (null==progressDialog){
                progressDialog = new ProgressDialog(this);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("加载中……");
            }
            progressDialog.show();*/
            if (builder == null) {
                Aimoi();
                builder.show();
            } else {
                builder.show();
            }

        } else {
            //progressDialog.dismiss();
            builder.dismiss();
            builder = null;
        }

    }

    /**
     * 解析获取图片库图片Uri物理路径
     *
     * @param context
     * @param uri
     * @return
     */
    @SuppressLint("NewApi")
    public String parsePicturePath(Context context, Uri uri) {

        if (null == context || uri == null)
            return null;

        boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentUri
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageDocumentsUri
            if (isExternalStorageDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                String[] splits = docId.split(":");
                String type = splits[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + File.separator + splits[1];
                }
            }
            // DownloadsDocumentsUri
            else if (isDownloadsDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaDocumentsUri
            else if (isMediaDocumentsUri(uri)) {
                String docId = DocumentsContract.getDocumentId(uri);
                String[] split = docId.split(":");
                String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                String selection = "_id=?";
                String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosContentUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;

    }

    private String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        String column = "_data";
        String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            try {
                if (cursor != null)
                    cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;

    }

    private boolean isExternalStorageDocumentsUri(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocumentsUri(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocumentsUri(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosContentUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.img_addshop_back) {
            finish();
        } else if (view.getId() == R.id.img_add_photo) {
            flag = false;
            if (mUriString.size() == 3) {
                ToastUtil.showShort(getApplicationContext(), "最多只能添加3张照片");
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    List<String> permission = new ArrayList<>();
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        permission.add(Manifest.permission.CAMERA);
                    }
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    }
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        permission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                    }
                    if (!permission.isEmpty()) {
                        String[] permissons = permission.toArray(new String[permission.size()]);
                        ActivityCompat.requestPermissions(this, permissons, PERMISSION_CORDD);
                    } else {
                        tackPhoto();
                    }
                } else {
                    tackPhoto();
                }
            }
        } else if (view.getId() == R.id.rl_tuwen) {
            Intent intent = new Intent(SaverAddnewShopActivity.this, PictureDescrbeActivity.class);

            Bundle b = new Bundle();
            b.putSerializable("photo", (Serializable) tuwwen);
            intent.putExtras(b);
            intent.putExtra("addnew","2");
            startActivityForResult(intent, PICTURE);
        } else if (view.getId() == R.id.lr_xuzhi) {
            Intent intent = new Intent(SaverAddnewShopActivity.this, GoodsDescribeActivity.class);
            intent.putExtra("peng", "1");
            if(ADDCODE==6){
                intent.putExtra("xuzhi","");
            }else {
                if ( !TextUtils.isEmpty(data.getBuyknow())) {
                    intent.putExtra("xuzhi", data.getBuyknow());
                }
            }
            startActivityForResult(intent, XUZHI);
        } else if (view.getId() == R.id.lr_describe) {
            Intent intent = new Intent(SaverAddnewShopActivity.this, GoodsDescribeActivity.class);
            intent.putExtra("peng", "2");
            if(ADDCODE==6){
                intent.putExtra("descreibe", "");
            }else{
                String goods_remark = data.getGoods_remark();
                if (!TextUtils.isEmpty(goods_remark)) {
                    intent.putExtra("descreibe", goods_remark);
                }

            }
            startActivityForResult(intent, DESCRIBE);
        } else if (view.getId() == R.id.btn_in) {

            try {
                in();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (view.getId() == R.id.btn_shelves) {

            try {
                shelves();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (view.getId() == R.id.tv_groupf) {
            Intent intent = new Intent(SaverAddnewShopActivity.this, GroupManagementActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.shaopfication) {
            Intent intent = new Intent(SaverAddnewShopActivity.this, ChoiceGroupActivity.class);
            intent.putExtra("base_goods_id", base_goods_id);
            startActivityForResult(intent, TOWGROUP);
        }
    }
/*  if (view.getId() == R.id.img_addshop_back) {

        }  else if (view.getId() == R.id.img_add_photo) {
            flag = false;
            if (mUriString.size() == 3) {
                Toast.makeText(getApplicationContext(), "最多只能添加3张照片", Toast.LENGTH_SHORT).show();
            } else { //判断版本是否是6.0以上    //设置动态权限
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    List<String> permission = new ArrayList<>();
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        permission.add(Manifest.permission.CAMERA);
                    }
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    }
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        permission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                    }
                    if (!permission.isEmpty()) {
                        String[] permissons = permission.toArray(new String[permission.size()]);
                        ActivityCompat.requestPermissions(this, permissons, PERMISSION_CORDD);
                    } else {
                        tackPhoto();
                    }
                } else {
                    tackPhoto();
                }
            }
        } else if (view.getId() == R.id.btn_in) {

            try {
                in();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (view.getId() == R.id.btn_shelves) {
            Log.e("全部参数", "上架销售");
            try {
                if (chiic) {

                    shelves();
                }
                //shelves();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (view.getId() == R.id.shaopfication) {
            // showDialog2();
            Intent intent = new Intent(SaverAddnewShopActivity.this, ChoiceGroupActivity.class);
            intent.putExtra("base_goods_id", base_goods_id);
            startActivityForResult(intent, TOWGROUP);
        } else if (view.getId() == R.id.tv_groupf) {
            Intent intent = new Intent(SaverAddnewShopActivity.this, GroupManagementActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.rl_tuwen) {
            Intent intent = new Intent(SaverAddnewShopActivity.this, PictureDescrbeActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("photo", (Serializable) tuwwen);
            intent.putExtras(b);
            startActivityForResult(intent, PICTURE);
        } else if (view.getId() == R.id.lr_xuzhi) {
            Intent intent = new Intent(SaverAddnewShopActivity.this, GoodsDescribeActivity.class);
            intent.putExtra("peng", "1");
            if (data.size() != 0) {
                intent.putExtra("xuzhi", data.get(0).getBuyknow());
            }
            startActivityForResult(intent, XUZHI);
        } else if (view.getId() == R.id.lr_describe) {
            Intent intent = new Intent(SaverAddnewShopActivity.this, GoodsDescribeActivity.class);
            intent.putExtra("peng", "2");
            if (data.size() != 0) {
                String goods_remark = data.get(0).getGoods_remark();
                intent.putExtra("descreibe", goods_remark);
            }
            startActivityForResult(intent, DESCRIBE);
        }*/

    /**
     * 商品型号适配器
     */
    public static class AddNewChildAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater inflater;
        private int specid1;

        public ArrayList<GoodsXiangqingBean.DataBean.XinghaolistBean> arr;


        private XinghaoEdit mXinghaoEdit;

        public interface XinghaoEdit {
            void xingHaoEdite(EditText editText1, EditText editText2, EditText editText3, int position, ImageView delet);

        }

        public void setXinghaoEdit(XinghaoEdit xinghaoEdit) {
            mXinghaoEdit = xinghaoEdit;
        }

        public void setArr(ArrayList<GoodsXiangqingBean.DataBean.XinghaolistBean> arr) {
            this.arr = arr;
        }

        public AddNewChildAdapter(Context context) {
            super();
            this.context = context;
            inflater = LayoutInflater.from(context);
            arr = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return arr == null ? 0 : arr.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            AddNewShopActivity.AddNewChildAdapter.ViewHolder viewHolder = null;
            if (view == null || !(view.getTag() instanceof AddNewShopActivity.AddNewChildAdapter.ViewHolder)) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.addnew_list_item, null, false);
                viewHolder = new AddNewShopActivity.AddNewChildAdapter.ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                viewHolder = (AddNewShopActivity.AddNewChildAdapter.ViewHolder) view.getTag();
            }
            GoodsXiangqingBean.DataBean.XinghaolistBean xinghaolistBean = arr.get(position);
            viewHolder.mEdit.setText(xinghaolistBean.getName());
            viewHolder.mEdit2.setText(xinghaolistBean.getPrice());
            viewHolder.mEdit3.setText(xinghaolistBean.getNum());
            mXinghaoEdit.xingHaoEdite(viewHolder.mEdit, viewHolder.mEdit2, viewHolder.mEdit3, position, viewHolder.mImgDel);
            return view;
        }

        static class ViewHolder {
            protected TextView mSpecid;
            protected TextView mText1;
            protected EditText mEdit;
            protected TextView mText2;
            protected EditText mEdit2;
            protected TextView mText3;
            protected EditText mEdit3;
            protected ImageView mImgDel;

            ViewHolder(View rootView) {
                initView(rootView);
            }

            private void initView(View rootView) {
                mSpecid = (TextView) rootView.findViewById(R.id.specid);
                mText1 = (TextView) rootView.findViewById(R.id.text1);
                mEdit = (EditText) rootView.findViewById(R.id.edit);
                mText2 = (TextView) rootView.findViewById(R.id.text2);
                mEdit2 = (EditText) rootView.findViewById(R.id.edit2);
                mText3 = (TextView) rootView.findViewById(R.id.text3);
                mEdit3 = (EditText) rootView.findViewById(R.id.edit3);
                mImgDel = (ImageView) rootView.findViewById(R.id.img_del);
            }
        }


    }

    public String[] upSpecIDArr = new String[0];

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
