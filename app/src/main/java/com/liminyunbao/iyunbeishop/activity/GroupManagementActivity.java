package com.liminyunbao.iyunbeishop.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liminyunbao.iyunbeishop.adapter.GroupManagerRCAdapter;
import com.liminyunbao.iyunbeishop.bean.GroutListBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.ToastUtil;
import com.liminyunbao.iyunbeishop.utils.UserUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class GroupManagementActivity extends BaseActivity implements View.OnClickListener {

    protected RelativeLayout mRlBack;
    protected TextView mTvGroupadd;
    protected RecyclerView mRcGroutmanager;
    private Dialog mDialog;
    private List<GroutListBean.MsgBean> msglist;

    private  static final  int ADDGTOADD=2;

    private GroupManagerRCAdapter mGroupManagerRCAdapter;

    @Override
    protected void HaveNet() {

    }

    @Override
    protected void GoneNet() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_group_management);
        initView();
        GrouList();

    }

    /**
     * 点击事件
     */
    private void ischic() {
        mGroupManagerRCAdapter.setOnItemClickListener(new GroupManagerRCAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(RelativeLayout v, int posion) {
                Intent intent=new Intent(GroupManagementActivity.this,ShopDetailsActivity.class);
                intent.putExtra("groupname",msglist.get(posion).getCate_name());
                intent.putExtra("cateid",msglist.get(posion).getCate_id()+"");
               startActivity(intent);

            }

            @Override
            public void OnButtonDelet(Button btn, int posion)
            {

                int num = msglist.get(posion).getNum();
                if(num!=0){
                   ToastUtil.showShort(getApplicationContext(),"该分组有商品不能删除");
                }else{
                    dialgoShow(msglist.get(posion).getCate_id(),posion);
                }

            }

            @Override
            public void OnButtonEdit(Button edit, int posion) {
                Intent intent=new Intent(GroupManagementActivity.this,EditGroupActivity.class);
                intent.putExtra("groupname",msglist.get(posion).getCate_name());
                intent.putExtra("cateid",msglist.get(posion).getCate_id()+"");
                intent.putExtra("num",msglist.get(posion).getNum()+"");
                startActivity(intent);
            }

            @Override
            public void OnZitemClick(View v, int posion) {
                Intent intent=new Intent(GroupManagementActivity.this,ShopDetailsActivity.class);
                intent.putExtra("groupname",msglist.get(posion).getCate_name());
                intent.putExtra("cateid",msglist.get(posion).getCate_id()+"");
                startActivity(intent);

            }
        });

    }

    /**
     * 分组列表
     */
    private void GrouList() {
        msglist = new ArrayList<>();
        mGroupManagerRCAdapter = new GroupManagerRCAdapter();
        OkGo.post(HttpAPI.GROUPLIST)
                .params("shopid", UserUtils.Shopid(getApplicationContext()))
                .params("user_id", UserUtils.Userid(getApplicationContext()))
                .params("user_token", UserUtils.UserToken(getApplicationContext()))
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Log.e("分组列表", s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                Gson gson = new Gson();
                                GroutListBean groutListBean = gson.fromJson(s, GroutListBean.class);
                                msglist = groutListBean.getMsg();
                               /* LinearLayoutManager manager=new LinearLayoutManager(getApplicationContext());
                                manager.setOrientation(LinearLayoutManager.VERTICAL);*/
                                mRcGroutmanager.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));
                                mGroupManagerRCAdapter.setMsglist(msglist);
                                mRcGroutmanager.setAdapter(mGroupManagerRCAdapter);
                                mGroupManagerRCAdapter.notifyDataSetChanged();
                                ischic();
                            } else if (status.equals("0")) {
                                Toast.makeText(getApplicationContext(), "这是分组", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e("分组列表", e.getMessage().toString());
                    }
                });
    }

    /**
     * 点击删除分组
     */
    private void dialgoShow(final int cateid, final int posion) {
        mDialog = new Dialog(this, R.style.CustomDialog);
        mDialog.setCanceledOnTouchOutside(false);
        Window window = mDialog.getWindow();
        window.setGravity(Gravity.CENTER);
        mDialog.show();
        WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
        window.getDecorView().setPadding(40, 0, 40, 0);
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        View inflate = LayoutInflater.from(this).inflate(R.layout.item_delet_dialog_group, null);
        RelativeLayout close = (RelativeLayout) inflate.findViewById(R.id.rl_close);
        Button cancel = (Button) inflate.findViewById(R.id.btn_cancel);
        Button sure = (Button) inflate.findViewById(R.id.btn_sure);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deledGroup(cateid,posion);
            }
        });
        mDialog.setContentView(inflate);
    }


    private void deledGroup(int cateid, final int posion){
        OkGo.post(HttpAPI.DELETGROUP)
                .params("shopid",UserUtils.Shopid(getApplicationContext()))
                .params("user_id",UserUtils.Userid(getApplicationContext()))
                .params("user_token",UserUtils.UserToken(getApplicationContext()))
                .params("cate_id",cateid+"")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {

                        try {
                            JSONObject jsonObject=new JSONObject(s);
                            String status = jsonObject.getString("status");
                            if(status.equals("1")){
                                String msg = jsonObject.getString("msg");
                                ToastUtil.showShort(getApplicationContext(),msg);
                                mDialog.dismiss();
                                msglist.remove(posion);
                                mGroupManagerRCAdapter.notifyDataSetChanged();
                            }else if(status.equals("0")){
                                ToastUtil.showShort(getApplicationContext(),"删除失败");

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }



    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_back) {
            finish();

        } else if (view.getId() == R.id.tv_groupadd) {
            if(msglist.size()!=10){
                Intent intent = new Intent(GroupManagementActivity.this, AddGroupActivity.class);
                intent.putExtra("addw","2");
                startActivityForResult(intent,ADDGTOADD);
            }else {

                ToastUtil.showShort(getApplicationContext(),"最多只能创建10个分组");
            }


        }
    }

    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.rl_back);
        mRlBack.setOnClickListener(GroupManagementActivity.this);
        mTvGroupadd = (TextView) findViewById(R.id.tv_groupadd);
        mTvGroupadd.setOnClickListener(GroupManagementActivity.this);
        mRcGroutmanager = (RecyclerView) findViewById(R.id.rc_groutmanager);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==ADDGTOADD){
            msglist.clear();
            GrouList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        msglist.clear();
        GrouList();
    }
}
