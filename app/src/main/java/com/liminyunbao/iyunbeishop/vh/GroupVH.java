package com.liminyunbao.iyunbeishop.vh;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

/**
 * Created by Administrator on 2017/6/3 0003.
 */

public class GroupVH extends RecyclerView.ViewHolder {

    public  RelativeLayout details;
    public Button  delet;
    public Button edit;
    public TextView name;
    public TextView num;
    public GroupVH(View itemView) {
        super(itemView);
        details= (RelativeLayout) itemView.findViewById(R.id.rl_details_group);
        delet= (Button) itemView.findViewById(R.id.btn_delet);
        edit= (Button) itemView.findViewById(R.id.btn_edit);
        name= (TextView) itemView.findViewById(R.id.tv_groupname);
        num= (TextView) itemView.findViewById(R.id.tv_group_num);
    }
}
