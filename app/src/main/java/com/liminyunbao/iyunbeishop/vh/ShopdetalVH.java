package com.liminyunbao.iyunbeishop.vh;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

import org.w3c.dom.Text;

/**
 * Created by Administrator on 2017/6/5 0005.
 */

public class ShopdetalVH  extends RecyclerView.ViewHolder{
    public ImageView mShopimg;
    public TextView oldpric;
    public TextView nowpric;
    public TextView goodname;
    public TextView stock;
    public TextView selectnum;
    public TextView guige;


    public ShopdetalVH(View itemView) {
        super(itemView);
        goodname= (TextView) itemView.findViewById(R.id.tv_saleover_commodityname);
        mShopimg= (ImageView) itemView.findViewById(R.id.img_saleover_shoppic);
        oldpric=(TextView) itemView.findViewById(R.id.tv_saleover_obsolete);
        nowpric=(TextView) itemView.findViewById(R.id.tv_saleover_price);
        guige=(TextView) itemView.findViewById(R.id.tv_saleover_specifications);
        stock=(TextView) itemView.findViewById(R.id.tv_saleover_stock);
        selectnum=(TextView) itemView.findViewById(R.id.tv_saleover_salesvolume);

    }
}
