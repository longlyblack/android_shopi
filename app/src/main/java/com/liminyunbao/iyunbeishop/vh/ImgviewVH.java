package com.liminyunbao.iyunbeishop.vh;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.liminyunbao.iyunbeishop.R;

/**
 * Created by Administrator on 2017/6/3 0003.
 */

public class ImgviewVH extends RecyclerView.ViewHolder {

    public ImageView mImageView;
    public ImgviewVH(View itemView) {
        super(itemView);
        mImageView= (ImageView) itemView.findViewById(R.id.img_pictuer);
    }
}
