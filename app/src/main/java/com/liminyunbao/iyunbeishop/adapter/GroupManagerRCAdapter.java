package com.liminyunbao.iyunbeishop.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.liminyunbao.iyunbeishop.bean.GroutListBean;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.vh.GroupVH;

import java.util.List;

/**
 * Created by Administrator on 2017/6/3 0003.
 */

public class GroupManagerRCAdapter extends RecyclerView.Adapter<GroupVH>  {

    private List<GroutListBean.MsgBean> msglist;


    private OnItemClickListener mOnItemClickListener;




    public interface  OnItemClickListener{
        void OnItemClick(RelativeLayout v, int posion);
        void OnButtonDelet(Button btn, int posion);
        void OnButtonEdit(Button edit, int posion);
        void OnZitemClick(View v, int posion);
    }

    public void setMsglist(List<GroutListBean.MsgBean> msglist) {
        this.msglist = msglist;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public GroupVH onCreateViewHolder(final ViewGroup parent, int viewType) {
        final View inflate = View.inflate(parent.getContext(), R.layout.item_shop_guorp_manager, null);
        final GroupVH groupVH=new GroupVH(inflate);
        final RelativeLayout rel= (RelativeLayout) inflate.findViewById(R.id.rl_details_group);
        final Button  delet= (Button) inflate.findViewById(R.id.btn_delet);
        final Button edit= (Button) inflate.findViewById(R.id.btn_edit);
        inflate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.OnZitemClick(inflate, (Integer) v.getTag());
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.OnButtonEdit(edit, (Integer) v.getTag());
            }
        });
        delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.OnButtonDelet(delet, (Integer) v.getTag());
            }
        });
        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.OnItemClick(rel, (Integer) v.getTag());
            }
        });
        return groupVH;
    }

    @Override
    public void onBindViewHolder(GroupVH holder, int position) {
        GroutListBean.MsgBean msgBean = msglist.get(position);
        GroupVH groupVH=  holder;
        groupVH.name.setText(msgBean.getCate_name());
        groupVH.num.setText("共"+msgBean.getNum()+"件商品");
        groupVH.itemView.setTag(position);
        groupVH.details.setTag(position);
        groupVH.edit.setTag(position);
        groupVH.delet.setTag(position);
    }

    @Override
    public int getItemCount() {
        return msglist==null?0:msglist.size();
    }

}
