package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.bean.WaitingListBean;
import com.liminyunbao.iyunbeishop.custom.ChildListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/31.
 */

public class WiteSenAdapter extends BaseAdapter {
    private List<WaitingListBean.DataBean.OrderMsg> mList;
    private ChildListAdapter mChildListAdapter;

    public void setSetOnlesion(setOnlesion setOnlesion) {
        mSetOnlesion = setOnlesion;
    }

    private setOnlesion mSetOnlesion;

    public interface setOnlesion {
        void setBtnLisen(Button button, String orderid);
    }

    public void setList(List<WaitingListBean.DataBean.OrderMsg> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;


        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_ordernew_witesen, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        WaitingListBean.DataBean.OrderMsg orderMsg = mList.get(position);
        viewHolder.mTvOrderNumWitesen.setText("订单号：" + orderMsg.getOrder_id());
        viewHolder.mTvOrderTimeWitesen.setText(orderMsg.getCreated_at());
        mSetOnlesion.setBtnLisen(viewHolder.mBtnAccept, orderMsg.getOrder_id() + "");
        int order_status = orderMsg.getOrder_status();
        if (order_status == 0) {
            viewHolder.mTewiteOrder.setText("待付款");
        } else if (order_status == 1) {
            viewHolder.mTewiteOrder.setText("待发货");

        } else if (order_status == 2) {
            viewHolder.mTewiteOrder.setText("已发货");

        } else if (order_status == 3) {
            viewHolder.mTewiteOrder.setText("待评价");
        } else if (order_status == 4) {

            viewHolder.mTewiteOrder.setText("已完成");
        } else if (order_status == 5) {
            viewHolder.mTewiteOrder.setText("已关闭");

        } else if (order_status == 6) {
            viewHolder.mTewiteOrder.setText("退款中");

        } else if (order_status == 6) {
            viewHolder.mTewiteOrder.setText("待接单");
        }

        mChildListAdapter = new ChildListAdapter();
        List<WaitingListBean.DataBean.OrderMsg.GoodsList> data = orderMsg.getData();
        List<WaitingListBean.DataBean.OrderMsg.GoodsList> mdata = new ArrayList<>();
        int sum = 0;
        if (data.size() < 2) {

            viewHolder.mTvSeemore.setVisibility(View.GONE);
            viewHolder.mViewLine.setVisibility(View.VISIBLE);
            viewHolder.mImgBiaoqian.setVisibility(View.GONE);
        } else {
            viewHolder.mTvSeemore.setVisibility(View.VISIBLE);
            viewHolder.mImgBiaoqian.setVisibility(View.VISIBLE);
            viewHolder.mViewLine.setVisibility(View.GONE);

        }
        for (int i = 0; i < data.size(); i++) {
            sum = sum + data.get(i).getGoods_num();
            if (i < 2) {
                mdata.add(new WaitingListBean.DataBean.OrderMsg.GoodsList(data.get(i).getPhoto(),
                        data.get(i).getGoods_name(), data.get(i).getGoods_guige(), data.get(i).getGoods_num(), data.get(i).getGoods_price()));
            }
        }

        mChildListAdapter.setList(mdata);
        viewHolder.mChidlListvShopName.setAdapter(mChildListAdapter);
        setListViewHeightBasedOnChildren(viewHolder.mChidlListvShopName);
        mChildListAdapter.notifyDataSetChanged();
        viewHolder.mChidlListvShopName.setHaveScrollbar(false);
        viewHolder.mChidlListvShopName.setDividerHeight(0);
        viewHolder.mTvAllMoney.setText("￥" + orderMsg.getTotal_amount());
        viewHolder.mTvBuyNum.setText("（共" + sum + "件商品）");

        return view;

    }


    public void setListViewHeightBasedOnChildren(ListView listView) {
        // 获取ListView对应的Adapter
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
            // listAdapter.getCount()返回数据项的数目
            View listItem = listAdapter.getView(i, null, listView);
            // 计算子项View 的宽高
            listItem.measure(0, 0);
            // 统计所有子项的总高度
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    static class ViewHolder {
        protected TextView mTvOrderNumWitesen;
        protected TextView mTvOrderTimeWitesen;
        protected TextView mTewiteOrder;
        protected ChildListView mChidlListvShopName;
        protected View mViewLine;
        protected ImageView mImgBiaoqian;
        protected TextView mTvSeemore;
        protected TextView mTvAllEdu;
        protected TextView mTvAllMoney;
        protected TextView mTvBuyNum;
        protected Button mBtnAccept;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvOrderNumWitesen = (TextView) rootView.findViewById(R.id.tv_order_num_witesen);
            mTvOrderTimeWitesen = (TextView) rootView.findViewById(R.id.tv_order_time_witesen);
            mTewiteOrder = (TextView) rootView.findViewById(R.id.tewite_order);
            mChidlListvShopName = (ChildListView) rootView.findViewById(R.id.chidl_listv_shop_name);
            mViewLine = (View) rootView.findViewById(R.id.view_line);
            mImgBiaoqian = (ImageView) rootView.findViewById(R.id.img_biaoqian);
            mTvSeemore = (TextView) rootView.findViewById(R.id.tv_seemore);
            mTvAllEdu = (TextView) rootView.findViewById(R.id.tv_all_edu);
            mTvAllMoney = (TextView) rootView.findViewById(R.id.tv_all_money);
            mTvBuyNum = (TextView) rootView.findViewById(R.id.tv_buy_num);
            mBtnAccept = (Button) rootView.findViewById(R.id.btn_accept);
        }
    }
}
