package com.liminyunbao.iyunbeishop.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/4/6 0006.
 */

public class ZJLSpagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragments;
    private String title[]={"普通流水","优惠买单"};

    public void setFragments(List<Fragment> fragments) {
        mFragments = fragments;
    }

    public ZJLSpagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
