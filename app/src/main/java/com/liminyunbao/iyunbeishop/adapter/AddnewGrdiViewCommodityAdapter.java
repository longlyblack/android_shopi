package com.liminyunbao.iyunbeishop.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/27.
 */

public class AddnewGrdiViewCommodityAdapter extends BaseAdapter {
    private List<String> mStrings;
    private int selectorPosition = 0;



    /*public AddnewGrdiViewCommodityAdapter(Context context){
        mContext=context;


    }*/
    public void setStrings(List<String> strings) {

        mStrings = strings;
    }

    @Override
    public int getCount() {
        return mStrings == null ? 0 : mStrings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_addnewcommodity_grid_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mStrings.get(position);
        viewHolder.mTvAddnewGridAdapter.setText(s);

        if (selectorPosition == position) {
            viewHolder.mRlAddnewGridAdapter.setBackgroundResource(R.drawable.shap_addnew_back_select);
            viewHolder.mTvAddnewGridAdapter.setTextColor(Color.WHITE);
        } else {
            viewHolder.mRlAddnewGridAdapter.setBackgroundResource(R.drawable.shap_addnew_back_nomal);
            viewHolder.mTvAddnewGridAdapter.setTextColor(Color.BLACK);
        }
        return view;
    }

    public void changeState(int pos) {
        selectorPosition = pos;
        notifyDataSetChanged();

    }

    static class ViewHolder {
        protected TextView mTvAddnewGridAdapter;
        protected RelativeLayout mRlAddnewGridAdapter;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvAddnewGridAdapter = (TextView) rootView.findViewById(R.id.tv_addnew_grid_adapter);
            mRlAddnewGridAdapter = (RelativeLayout) rootView.findViewById(R.id.rl_addnew_grid_adapter);
        }
    }
}
