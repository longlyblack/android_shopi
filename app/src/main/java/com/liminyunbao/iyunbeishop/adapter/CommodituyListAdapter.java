package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.bean.SearchGoodsBean;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/28.
 */

public class CommodituyListAdapter extends BaseAdapter {
    private List<SearchGoodsBean.MsgBean> mStrings;
    public static Map<Integer, Boolean> isSelected;
    private Context mContext;
    private chickbox mChickbox;

    public interface chickbox {
        void ischickbox(CheckBox checkBox, int posion);
    }

    public void setChickbox(chickbox chickbox) {
        mChickbox = chickbox;
    }

    public static Map<Integer, Boolean> getIsSelected() {
        return isSelected;
    }

    public static void setIsSelected(Map<Integer, Boolean> isSelected) {
        CommodituyListAdapter.isSelected = isSelected;
    }

    public CommodituyListAdapter(Context context, List<SearchGoodsBean.MsgBean> strings, Map<Integer, Boolean> isselec) {
        mContext = context;
        mStrings = strings;
        isSelected = isselec;
        inidata();
    }

    private void inidata() {
        for (int i = 0; i < mStrings.size(); i++) {
            isSelected.put(i, false);
        }

    }

    @Override
    public int getCount() {
        return mStrings == null ? 0 : mStrings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_commodity_lsit_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        SearchGoodsBean.MsgBean msgBean = mStrings.get(position);
        viewHolder.mTvShaopname.setText(msgBean.getTitle());
        viewHolder.mTvPassPrceCommodlist.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        Glide.with(parent.getContext()).load("http://img1.iyunbei.net/180x180/attachs/" + msgBean.getPhoto()).into(viewHolder.mImgShopp);
        viewHolder.mTvPriceCommodlist.setText("￥"+msgBean.getGoods_price() + "");
        viewHolder.mTvPassPrceCommodlist.setText("￥"+msgBean.getMarket_price() + "");
        viewHolder.mTvStock.setText("库存:"+msgBean.getScore_num()+"");
        viewHolder.mTvSelecnum.setText("销售量:"+msgBean.getSold_num()+"");
        mChickbox.ischickbox(viewHolder.mCbSelect, position);
        viewHolder.mCbSelect.setChecked(getIsSelected().get(position));
        return view;
    }

    static class ViewHolder {
        protected CheckBox mCbSelect;
        protected ImageView mImgShopp;
        protected TextView mTvShaopname;
        protected TextView mTvPriceCommodlist;
        protected TextView mTvPassPrceCommodlist;
        protected TextView mTvGuigeCommlist;
        protected TextView mTvStock;
        protected TextView mTvSelecnum;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mCbSelect = (CheckBox) rootView.findViewById(R.id.cb_select);
            mImgShopp = (ImageView) rootView.findViewById(R.id.img_shopp);
            mTvShaopname = (TextView) rootView.findViewById(R.id.tv_shaopname);
            mTvPriceCommodlist = (TextView) rootView.findViewById(R.id.tv_price_commodlist);
            mTvPassPrceCommodlist = (TextView) rootView.findViewById(R.id.tv_pass_prce_commodlist);
            mTvGuigeCommlist = (TextView) rootView.findViewById(R.id.tv_guige_commlist);
            mTvStock = (TextView) rootView.findViewById(R.id.tv_stock);
            mTvSelecnum = (TextView) rootView.findViewById(R.id.tv_selecnum);
        }
    }
}
