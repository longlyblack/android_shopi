package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.ApplyBean;
import com.liminyunbao.iyunbeishop.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/3/22.
 */

public class CapitalFlowAdapter extends BaseAdapter {
    private List<ApplyBean.DataBeanX.DataBean> mList;
    private Context mContext;

   /* public CapitalFlowAdapter(Context context){
        mContext=context;
    }*/
    public void setList(List<ApplyBean.DataBeanX.DataBean> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_capitalflow, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        ApplyBean.DataBeanX.DataBean dataBean = mList.get(position);

   if(dataBean.getStatus()==0){
       viewHolder.mTvUsenameCapitalfow.setText("审核中！");
   }else if(dataBean.getStatus()==1){
       viewHolder.mTvUsenameCapitalfow.setText("提现成功！");
   }else if(dataBean.getStatus()==2){
       viewHolder.mTvUsenameCapitalfow.setText("提现失败！");
   }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        long time=new Long(dataBean.getCreate_time())*1000L;
        Date date = new Date(time);
        String d = format.format(date);
        viewHolder.mTvTimeCapitalflow.setText(d);
        viewHolder.mTvMoneyCpitalflow.setText("￥"+dataBean.getMoney());
        viewHolder.mTvYveCapitalflow.setText("");
        return view;
    }

    static class ViewHolder {
        protected TextView mTvUsenameCapitalfow;
        protected TextView mTvMoneyCpitalflow;
        protected TextView mTvTimeCapitalflow;
        protected TextView mTvYveCapitalflow;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvUsenameCapitalfow = (TextView) rootView.findViewById(R.id.tv_usename_capitalfow);
            mTvMoneyCpitalflow = (TextView) rootView.findViewById(R.id.tv_money_cpitalflow);
            mTvTimeCapitalflow = (TextView) rootView.findViewById(R.id.tv_time_capitalflow);
            mTvYveCapitalflow = (TextView) rootView.findViewById(R.id.tv_yve_capitalflow);
        }
    }
}
