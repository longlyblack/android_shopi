package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/16.
 */

public class HompagerGridAdapter extends BaseAdapter {
    private List<String> mList;
    private int ims[]={R.mipmap.commodity,R.mipmap.marketing,R.mipmap.evaluate,R.mipmap.settlement,R.mipmap.statistics,R.mipmap.shop};
    public void setList(List<String> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_hompage_fram, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mList.get(position);
        viewHolder.mTvMenuGrid.setText(s);
        viewHolder.mImgMenuGrid.setImageResource(ims[position]);
        return view;
    }

    static class ViewHolder {
        protected ImageView mImgMenuGrid;
        protected TextView mTvMenuGrid;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mImgMenuGrid = (ImageView) rootView.findViewById(R.id.img_menu_grid);
            mTvMenuGrid = (TextView) rootView.findViewById(R.id.tv_menu_grid);
        }
    }
}
