package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/18.
 */

public class DialogOldoredAdapter extends BaseAdapter {
    private List<String> mList;

    public void setList(List<String> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_liset_tabl, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mList.get(position);
        viewHolder.mTvFoodname.setText(s);
        return view;
    }

    static class ViewHolder {
        protected TextView mTvFoodname;
        protected TextView mZhu;
        protected TextView mTvNumb;
        protected TextView mTvResultMoney;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvFoodname = (TextView) rootView.findViewById(R.id.tv_foodname);
            mZhu = (TextView) rootView.findViewById(R.id.zhu);
            mTvNumb = (TextView) rootView.findViewById(R.id.tv_numb);
            mTvResultMoney = (TextView) rootView.findViewById(R.id.tv_result_money);
        }
    }
}
