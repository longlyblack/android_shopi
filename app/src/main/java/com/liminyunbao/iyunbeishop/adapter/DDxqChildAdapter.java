package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.bean.PuTongListBean;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/4/7 0007.
 */

public class DDxqChildAdapter extends BaseAdapter {
    private  List<PuTongListBean.DataBean.OrderDmsg.ShopData> mBeen;

    public void setBeen(List<PuTongListBean.DataBean.OrderDmsg.ShopData> been) {
        mBeen = been;
    }

    @Override
    public int getCount() {
        return mBeen == null ? 0 : mBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ddxq_childlsit, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        PuTongListBean.DataBean.OrderDmsg.ShopData shopData = mBeen.get(position);
        viewHolder.mTvShaopName.setText(shopData.getGoods_name());
        viewHolder.mTvDdxqGuige.setText(shopData.getGoods_guige());
        viewHolder.mTvDdxqPrice.setText("￥"+shopData.getGoods_price());
        Glide.with(parent.getContext()).load("http://img1.iyunbei.net/300x300/attachs/"+shopData.getPhoto()).into(viewHolder.mImgDdxqChildlist);
        return view;
    }

    static class ViewHolder {
        protected ImageView mImgDdxqChildlist;
        protected TextView mTvShaopName;
        protected TextView mTvDdxqGuige;
        protected TextView mTvDdxqPrice;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mImgDdxqChildlist = (ImageView) rootView.findViewById(R.id.img_ddxq_childlist);
            mTvShaopName = (TextView) rootView.findViewById(R.id.tv_shaop_name);
            mTvDdxqGuige = (TextView) rootView.findViewById(R.id.tv_ddxq_guige);
            mTvDdxqPrice = (TextView) rootView.findViewById(R.id.tv_ddxq_price);
        }
    }
}
