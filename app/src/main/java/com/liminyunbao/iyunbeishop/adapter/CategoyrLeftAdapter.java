package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.CategoryBean;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/5/10 0010.
 */

public class CategoyrLeftAdapter extends BaseAdapter {
    private List<CategoryBean.TatilBean.KeyBean> mKeyBeen;

    public void setKey(List<CategoryBean.TatilBean.KeyBean> key) {
        this.mKeyBeen = key;
    }

    @Override
    public int getCount() {
        return mKeyBeen == null ? 0 : mKeyBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_left_right, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        CategoryBean.TatilBean.KeyBean keyBean = mKeyBeen.get(position);

        List<CategoryBean.TatilBean.KeyBean.DataBean> data = keyBean.getData();
        viewHolder.mTextCategory.setText(keyBean.getName());
        //选中和没选中时，设置不同的颜色
        if (position == selectedPosition) {
            view.setBackgroundResource(R.color.colorXiangqing);

        } else {
            view.setBackgroundResource(R.color.colorWhite);

        }
        return view;
    }
    private int selectedPosition = -1;

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }
    static class ViewHolder {
        protected TextView mTextCategory;
        protected RelativeLayout mRlCategoryItem;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTextCategory = (TextView) rootView.findViewById(R.id.text_category);
            mRlCategoryItem = (RelativeLayout) rootView.findViewById(R.id.rl_category_item);
        }
    }
}
