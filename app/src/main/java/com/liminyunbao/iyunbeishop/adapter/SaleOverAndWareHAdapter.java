package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.bean.SaleBean;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;

import java.util.List;

/**
 * Created by Administrator on 2017/4/26 0026.
 */

public class SaleOverAndWareHAdapter extends BaseAdapter {
    private List<SaleBean.MassageBean.GoodsList> mList;

    private int tyoe;
    private Context mContext;
    public SaleOverAndWareHAdapter(Context context){
        mContext=context;
        tyoe= (int) SharePUtile.get(mContext, "shop_typ", 0);
    }
    private onSaleoutWarehListener mOnSaleoutWarehListener;
    public interface  onSaleoutWarehListener{
        void onSaleoutWarehListener(ImageView img,String url);
    }

    public void setOnSaleoutWarehListener(onSaleoutWarehListener onSaleoutWarehListener) {
        mOnSaleoutWarehListener = onSaleoutWarehListener;
    }

    public void setList(List<SaleBean.MassageBean.GoodsList> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_saleovee_warehouse_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        SaleBean.MassageBean.GoodsList dataBean = mList.get(position);
        viewHolder.mTvSaleoverCommodityname.setText(dataBean.getTitle());
        //viewHolder.mImgSaleoverShoppic
        String url="http://img1.iyunbei.net/180x180/attachs/"+dataBean.getPhoto();
        Glide.with(parent.getContext()).load(url).animate(R.anim.item_photo).into(viewHolder.mImgSaleoverShoppic);
        viewHolder.mTvSaleoverPrice.setText("￥"+dataBean.getGoods_price()+"");
        viewHolder.mTvSaleoverObsolete.setText("￥"+dataBean.getMarket_price()+"");
        viewHolder.mTvSaleoverObsolete.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG );
        viewHolder.mTvSaleoverStock.setText("库存:"+dataBean.getSunnum()+"");
        viewHolder.mTvSaleoverSalesvolume.setText("销售量:"+dataBean.getSold_num()+"");
        mOnSaleoutWarehListener.onSaleoutWarehListener(viewHolder.mImgSaleoverShoppic,url);
        viewHolder.mTvSaleoverStock.setVisibility(View.VISIBLE);
        String guige = dataBean.getGuige();

       // viewHolder.mTvSaleoverSpecifications.setText("规格:"+dataBean.getGuige()+"");
        if (guige != null) {
            viewHolder.mTvSaleoverSpecifications.setText("规格:" + dataBean.getGuige()+"");
        } else {
            viewHolder.mTvSaleoverSpecifications.setVisibility(View.GONE);
        }

        return view;
    }
    static class ViewHolder {
        protected ImageView mImgSaleoverShoppic;
        protected TextView mTvSaleoverCommodityname;
        protected TextView mTvSaleoverPrice;
        protected TextView mTvSaleoverObsolete;
        protected TextView mTvSaleoverSpecifications;
        protected TextView mTvSaleoverStock;
        protected TextView mTvSaleoverSalesvolume;
        ViewHolder(View rootView) {
            initView(rootView);
        }
        private void initView(View rootView) {
            mImgSaleoverShoppic = (ImageView) rootView.findViewById(R.id.img_saleover_shoppic);
            mTvSaleoverCommodityname = (TextView) rootView.findViewById(R.id.tv_saleover_commodityname);
            mTvSaleoverPrice = (TextView) rootView.findViewById(R.id.tv_saleover_price);
            mTvSaleoverObsolete = (TextView) rootView.findViewById(R.id.tv_saleover_obsolete);
            mTvSaleoverSpecifications = (TextView) rootView.findViewById(R.id.tv_saleover_specifications);
            mTvSaleoverStock = (TextView) rootView.findViewById(R.id.tv_saleover_stock);
            mTvSaleoverSalesvolume = (TextView) rootView.findViewById(R.id.tv_saleover_salesvolume);
        }
    }
}
