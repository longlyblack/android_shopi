package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/27.
 */

public class BeaseAddnewListAdapter extends BaseAdapter {
    private List<String> mStrings;
    /**
     * 用来保存选中状态和对应的位置，用于解决item的复用问题
     */
    public static Map<Integer,Boolean> isSelect;
    private Context mContext;
    /**
     * 用来保存之前选中状态的位置，用于加载更多数据时恢复已选中的位置
     */
    public static List<Integer> hasSelect=new ArrayList<>();

    public static Map<Integer, Boolean> getIsSelect() {
        return isSelect;
    }

    public static void setIsSelect(Map<Integer, Boolean> isSelect) {
        BeaseAddnewListAdapter.isSelect = isSelect;
    }

    public BeaseAddnewListAdapter(Context context, List<String> strings, Map<Integer,Boolean> isselect){
        mContext=context;
        mStrings=strings;
        isSelect=isselect;
        initData();

    }


    /**
     * 初始选中状态
     *
     * @param
     */
    private void initData() {
        //判断isSelected是否已经存在
        for (int i = 0; i < mStrings.size(); i++) {
            isSelect.put(i, false);
        }

    }
    @Override
    public int getCount() {
        return mStrings == null ? 0 : mStrings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_beasaddnew_lists, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mStrings.get(position);
        viewHolder.mTvShangpinName.setText(s);
        viewHolder.mTvPassPrice.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG );
        viewHolder.mCbAddnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isSelect.get(position)){
                    isSelect.put(position,false);
                    setIsSelect(isSelect);
                }else{
                    isSelect.put(position,true);
                    setIsSelect(isSelect);
                }
                notifyDataSetChanged();
            }
        });
        viewHolder.mCbAddnew.setChecked(getIsSelect().get(position));
        return view;
    }
    static class ViewHolder {
        protected ImageView mImgAddnewLists;
        protected TextView mTvShangpinName;
        protected TextView mTvGuige;
        protected TextView mTvPriaceAddnew;
        protected TextView mTvPassPrice;
        protected CheckBox mCbAddnew;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mImgAddnewLists = (ImageView) rootView.findViewById(R.id.img_addnew_lists);
            mTvShangpinName = (TextView) rootView.findViewById(R.id.tv_shangpin_name);
            mTvGuige = (TextView) rootView.findViewById(R.id.tv_guige);
            mTvPriaceAddnew = (TextView) rootView.findViewById(R.id.tv_priace_addnew);
            mTvPassPrice = (TextView) rootView.findViewById(R.id.tv_pass_price);
            mCbAddnew = (CheckBox) rootView.findViewById(R.id.cb_addnew);
        }
    }
}
