package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.RegionqBean;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/5/17 0017.
 */

public class RegionqAdapter extends BaseAdapter {

    private List<RegionqBean> mRegionqBeen;

    public void setRegionqBeen(List<RegionqBean> regionqBeen) {
        mRegionqBeen = regionqBeen;
    }

    @Override
    public int getCount() {
        return mRegionqBeen == null ? 0 : mRegionqBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_left_right, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        RegionqBean regionqBean = mRegionqBeen.get(position);
        viewHolder.mTextCategory.setText(regionqBean.getCityname());
        return view;
    }

    static class ViewHolder {
        protected TextView mTextCategory;
        protected RelativeLayout mRlCategoryItem;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTextCategory = (TextView) rootView.findViewById(R.id.text_category);
            mRlCategoryItem = (RelativeLayout) rootView.findViewById(R.id.rl_category_item);
        }
    }
}
