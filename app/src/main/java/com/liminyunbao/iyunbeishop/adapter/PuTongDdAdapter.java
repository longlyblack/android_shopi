package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.PuTongListBean;
import com.liminyunbao.iyunbeishop.custom.ChildListView;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/4/7 0007.
 */

public class PuTongDdAdapter extends BaseAdapter {
    private List<PuTongListBean.DataBean.OrderDmsg> mBeanXList;
    private  List<PuTongListBean.DataBean.OrderDmsg.ShopData>mBeen;
    private DDxqChildAdapter mDDxqChildAdapter;

    public void setBeanXList(List<PuTongListBean.DataBean.OrderDmsg> beanXList) {
        mBeanXList = beanXList;
    }

    @Override
    public int getCount() {
        return mBeanXList == null ? 0 : mBeanXList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ptol_child, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        PuTongListBean.DataBean.OrderDmsg orderDmsg = mBeanXList.get(position);
        viewHolder.mTvPtolOrder.setText("订单号："+orderDmsg.getOrder_id());
        viewHolder.mTvPtolTime.setText(orderDmsg.getCreated_at());
        if(orderDmsg.getOrder_status()==0){
            viewHolder.mTvPtolZhuangtai.setText("待付款");
        }else if (orderDmsg.getOrder_status()==1){
            viewHolder.mTvPtolZhuangtai.setText("待发货");
        }else if (orderDmsg.getOrder_status()==2){
            viewHolder.mTvPtolZhuangtai.setText("已发货");
        }else if (orderDmsg.getOrder_status()==3){
            viewHolder.mTvPtolZhuangtai.setText("待评价");
        }else if (orderDmsg.getOrder_status()==4){
            viewHolder.mTvPtolZhuangtai.setText("已完成");
        }else if (orderDmsg.getOrder_status()==5){
            viewHolder.mTvPtolZhuangtai.setText("已关闭");
        }else if (orderDmsg.getOrder_status()==6){
            viewHolder.mTvPtolZhuangtai.setText("退款中");
        }else if (orderDmsg.getOrder_status()==7){
            viewHolder.mTvPtolZhuangtai.setText("待接单");
        }
        viewHolder.mTvPayShifuPtol.setText(orderDmsg.getTotal_amount()+"");
        mBeen = orderDmsg.getData();
        mDDxqChildAdapter=new DDxqChildAdapter();
        mDDxqChildAdapter.setBeen(mBeen);
        viewHolder.mLsitPtolChild.setAdapter(mDDxqChildAdapter);
        setListViewHeightBasedOnChildren(viewHolder.mLsitPtolChild);
        mDDxqChildAdapter.notifyDataSetChanged();
        viewHolder.mLsitPtolChild.setHaveScrollbar(false);
        viewHolder.mLsitPtolChild.setDividerHeight(0);
        return view;
    }


    public void setListViewHeightBasedOnChildren(ListView listView) {
        // 获取ListView对应的Adapter
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
            // listAdapter.getCount()返回数据项的数目
            View listItem = listAdapter.getView(i, null, listView);
            // 计算子项View 的宽高
            listItem.measure(0, 0);
            // 统计所有子项的总高度
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    static class ViewHolder {
        protected TextView mTvPtolOrder;
        protected TextView mTvPtolTime;
        protected TextView mTvPtolZhuangtai;
        protected ChildListView mLsitPtolChild;
        protected TextView mTvPayShifuPtol;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvPtolOrder = (TextView) rootView.findViewById(R.id.tv_ptol_order);
            mTvPtolTime = (TextView) rootView.findViewById(R.id.tv_ptol_time);
            mTvPtolZhuangtai = (TextView) rootView.findViewById(R.id.tv_ptol_zhuangtai);
            mLsitPtolChild = (ChildListView) rootView.findViewById(R.id.lsit_ptol_child);
            mTvPayShifuPtol = (TextView) rootView.findViewById(R.id.tv_pay_shifu_ptol);
        }
    }
}
