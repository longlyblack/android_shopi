package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.ShangPinGroup;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/4/26 0026.
 */

public class ClassficationAdapter extends BaseAdapter {
    private List<ShangPinGroup.DataBean.MsgBean> mList;

    public void setList(List<ShangPinGroup.DataBean.MsgBean>list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_classfication_list, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        ShangPinGroup.DataBean.MsgBean msgBean = mList.get(position);
        viewHolder.mTvCalss.setText(msgBean.getCatea_name());
        return view;
    }

    static class ViewHolder {
        protected TextView mTvCalss;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvCalss = (TextView) rootView.findViewById(R.id.tv_calss);
        }
    }
}
