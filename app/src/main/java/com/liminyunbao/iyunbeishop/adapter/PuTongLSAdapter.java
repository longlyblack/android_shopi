package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.PuTongLiuShuiBean;
import com.liminyunbao.iyunbeishop.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/4/6 0006.
 */

public class PuTongLSAdapter extends BaseAdapter {
    private List<PuTongLiuShuiBean.DataBeanX.DataBean> mDataBeen;

    public void setStrings(List<PuTongLiuShuiBean.DataBeanX.DataBean> strings) {
        mDataBeen = strings;
    }

    @Override
    public int getCount() {
        return mDataBeen == null ? 0 : mDataBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_putong_list_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        //由于改动   变量名是时间的显示 intor   名是余额的显示 time
        PuTongLiuShuiBean.DataBeanX.DataBean dataBean = mDataBeen.get(position);
        viewHolder.mTvPutongMoney.setText("￥"+dataBean.getMoney());
        viewHolder.mTvPtorderTime.setText("订单号:"+dataBean.getOrder_id());
        viewHolder.mTvPutongName.setText(dataBean.getIntro());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Long time=new Long(dataBean.getCreate_time()*1000L);
        Date date = new Date(time);
        String d = format.format(date);
        viewHolder.mTvPutongYve.setText(d);
        return view;
    }

    static class ViewHolder {
        protected TextView mTvPutongName;
        protected TextView mTvPutongMoney;
        protected TextView mTvPtorderTime;
        protected TextView mTvPutongYve;
        ViewHolder(View rootView) {
            initView(rootView);
        }
        private void initView(View rootView) {
            mTvPutongName = (TextView) rootView.findViewById(R.id.tv_putong_name);
            mTvPutongMoney = (TextView) rootView.findViewById(R.id.tv_putong_money);
            mTvPtorderTime = (TextView) rootView.findViewById(R.id.tv_ptorder_time);
            mTvPutongYve = (TextView) rootView.findViewById(R.id.tv_putong_yve);
        }
    }
}
