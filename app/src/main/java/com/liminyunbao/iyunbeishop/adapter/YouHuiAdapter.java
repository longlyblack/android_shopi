package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.YouHuiMaiDianBean;
import com.liminyunbao.iyunbeishop.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/4/6 0006.
 */

public class YouHuiAdapter extends BaseAdapter {
    private List<YouHuiMaiDianBean.DataBeanX.DataBean> mDataBeen;

    public void setStrings(List<YouHuiMaiDianBean.DataBeanX.DataBean> strings) {
        mDataBeen = strings;
    }

    @Override
    public int getCount() {
        return mDataBeen == null ? 0 : mDataBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_youhui_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        YouHuiMaiDianBean.DataBeanX.DataBean dataBean = mDataBeen.get(position);
        viewHolder.mTvYouhuiMoney.setText("￥"+dataBean.getNeed_pay());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Long time=new Long(dataBean.getCreate_time())*1000L;
        Date date = new Date(time);
        String d = format.format(date);
        viewHolder.mTvYouhuiTime.setText(d);
        return view;
    }

    static class ViewHolder {
        protected TextView mTvYouhuiName;
        protected TextView mTvYouhuiTime;
        protected TextView mTvYouhuiMoney;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvYouhuiName = (TextView) rootView.findViewById(R.id.tv_youhui_name);
            mTvYouhuiTime = (TextView) rootView.findViewById(R.id.tv_youhui_time);
            mTvYouhuiMoney = (TextView) rootView.findViewById(R.id.tv_youhui_money);
        }
    }
}
