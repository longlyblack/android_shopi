package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/17.
 */

public class OrderGridAdapter extends BaseAdapter {
    private List<String> mStrings;
    private int imgs[]={R.mipmap.ptong,R.mipmap.tshi,R.mipmap.yyue,R.mipmap.wmai};

    public void setStrings(List<String> strings) {
        mStrings = strings;
    }

    @Override
    public int getCount() {
        return mStrings == null ? 0 : mStrings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_hompage_fram, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mStrings.get(position);
        viewHolder.mTvMenuGrid.setText(s);
        viewHolder.mImgMenuGrid.setImageResource(imgs[position]);
        return view;
    }

    static class ViewHolder {
        protected ImageView mImgMenuGrid;
        protected TextView mTvMenuGrid;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mImgMenuGrid = (ImageView) rootView.findViewById(R.id.img_menu_grid);
            mTvMenuGrid = (TextView) rootView.findViewById(R.id.tv_menu_grid);
        }
    }
}
