package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/20.
 */

public class AllOrderListAdapter extends BaseAdapter {
    private List<String> mList;
    final int TYPE_1=0;
    final int TYPE_2=1;
    //final int TYPE_3=2;
    public void setList(List<String> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if(position==mList.size()-1){
            return TYPE_2;
        }/*else if(position==mList.size()-1){
         //   return TYPE_3;
        }*/
        return TYPE_1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        int type=getItemViewType(position);
        ViewHolder viewHolder = null;
        ViewHolder2 viewHolser2=null;
        //ViewHolder3 viewHolder3=null;
        if (view == null ) {
            switch (type){
                case  TYPE_1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orderlist_typ1, null, false);
                viewHolder = new ViewHolder();
                viewHolder.mView=(View) view.findViewById(R.id.view);
                viewHolder.mTvOrderlist = (TextView) view.findViewById(R.id.tv_orderlist);
                viewHolder.mTvOrderNumbOrderlist = (TextView) view.findViewById(R.id.tv_orderNumb_orderlist);
                viewHolder.mTvStateOrderlist = (TextView) view.findViewById(R.id.tv_state_orderlist);
                viewHolder.mTvFoodnameOrderlist = (TextView) view.findViewById(R.id.tv_foodname_orderlist);
                viewHolder.mTvShiplayOrderlist = (TextView) view.findViewById(R.id.tv_shiplay_orderlist);
                viewHolder.mTvPaymoneyOrderlist = (TextView) view.findViewById(R.id.tv_paymoney_orderlist);
                viewHolder.mTvOrderTypeOrderlist = (TextView) view.findViewById(R.id.tv_orderType_orderlist);
                viewHolder.mTvTypeOrderlist = (TextView) view.findViewById(R.id.tv_type_orderlist);
                viewHolder.mTvOrdertiemOrderlist = (TextView) view.findViewById(R.id.tv_ordertiem_orderlist);
                viewHolder.mTvTimeOrderlist = (TextView) view.findViewById(R.id.tv_time_orderlist);
                view.setTag(viewHolder);
                    break;
                case  TYPE_2:
                    view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orderlist_typ2, null, false);
                    viewHolser2=new ViewHolder2();
                    viewHolser2. mTvOrderNumbOrderlist = (TextView) view.findViewById(R.id.tv_orderNumb_orderlist);
                    viewHolser2. mTvStateOrderlistType2 = (TextView) view.findViewById(R.id.tv_state_orderlist_type2);
                    viewHolser2.  mTvPaynameType2 = (TextView) view.findViewById(R.id.tv_payname_type2);
                    viewHolser2. mTvPaynameOrderlistType2 = (TextView) view.findViewById(R.id.tv_payname_orderlist_type2);
                    viewHolser2. mTvYunbenumOrderlistType2 = (TextView) view.findViewById(R.id.tv_yunbenum_orderlist_type2);
                    viewHolser2.mTvYunbenumType2 = (TextView) view.findViewById(R.id.tv_yunbenum_type2);
                    viewHolser2.mTvOrdertypeType2 = (TextView) view.findViewById(R.id.tv_ordertype_type2);
                    viewHolser2.mTvOrdertypeOrderlistType2 = (TextView) view.findViewById(R.id.tv_ordertype_orderlist_type2);
                    viewHolser2.mTevOrdertimeType2 = (TextView) view.findViewById(R.id.tev_ordertime_type2);
                    viewHolser2.mTvOrdertimeOrderlistType2 = (TextView) view.findViewById(R.id.tv_ordertime_orderlist_type2);

                    viewHolser2.mTvOrderNumbOrderlist1 = (TextView) view.findViewById(R.id.tv_orderNumb_orderlist);
                    viewHolser2.mTvStateOrderlistType3 = (TextView) view.findViewById(R.id.tv_state_orderlist_type3);
                    viewHolser2.mTvPaynameType3 = (TextView) view.findViewById(R.id.tv_payname_type3);
                    viewHolser2.mTvPaymoneyType3 = (TextView) view.findViewById(R.id.tv_paymoney_type3);
                    viewHolser2.mTvPaynameOrderlistType3 = (TextView) view.findViewById(R.id.tv_payname_orderlist_type3);
                    viewHolser2.mTvBackYunbeiType3 = (TextView) view.findViewById(R.id.tv_backYunbei_type3);
                    viewHolser2.mTvBackYunbeiOrderlistType3 = (TextView) view.findViewById(R.id.tv_backYunbei_orderlist_type3);
                    viewHolser2. mTvOrdertimeType3 = (TextView) view.findViewById(R.id.tv_ordertime_type3);
                    viewHolser2.mTvOrdertimeOrderlistType3 = (TextView) view.findViewById(R.id.tv_ordertime_orderlist_type3);
                    view.setTag(viewHolser2);
                    break;
               /* case TYPE_3:
                    view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orderlsit_typ3, null, false);
                    viewHolder3=new ViewHolder3();
                    viewHolder3.mTvOrderNumbOrderlist = (TextView) view.findViewById(R.id.tv_orderNumb_orderlist);
                    viewHolder3.mTvStateOrderlistType3 = (TextView) view.findViewById(R.id.tv_state_orderlist_type3);
                    viewHolder3.mTvPaynameType3 = (TextView) view.findViewById(R.id.tv_payname_type3);
                    viewHolder3.mTvPaymoneyType3 = (TextView) view.findViewById(R.id.tv_paymoney_type3);
                    viewHolder3.mTvPaynameOrderlistType3 = (TextView) view.findViewById(R.id.tv_payname_orderlist_type3);
                    viewHolder3.mTvBackYunbeiType3 = (TextView) view.findViewById(R.id.tv_backYunbei_type3);
                    viewHolder3.mTvBackYunbeiOrderlistType3 = (TextView) view.findViewById(R.id.tv_backYunbei_orderlist_type3);
                    viewHolder3. mTvOrdertimeType3 = (TextView) view.findViewById(R.id.tv_ordertime_type3);
                    viewHolder3.mTvOrdertimeOrderlistType3 = (TextView) view.findViewById(R.id.tv_ordertime_orderlist_type3);
                    view.setTag(viewHolder3);
                    break;*/
            }

        } else {
            switch (type){
                case TYPE_1:
                    viewHolder = (ViewHolder) view.getTag();
                    break;
                case TYPE_2:
                    viewHolser2= (ViewHolder2) view.getTag();
                    break;
              /*  case TYPE_3:
                    viewHolder3= (ViewHolder3) view.getTag();
                    break;*/
            }

        }
        String s = mList.get(position);
        switch (type){
            case  TYPE_1:
                viewHolder.mView.setVisibility(View.GONE);
                viewHolder.mTvOrderNumbOrderlist.setText(s);
                break;
            case TYPE_2:
                viewHolser2.mTvPaynameOrderlistType2.setText("张丰");
                break;
         /*   case TYPE_3:
                viewHolder3.mTvPaynameOrderlistType3.setText("王五");
                break;*/
        }




        return view;
    }
    static class ViewHolder {
        protected TextView mTvOrderlist;
        protected TextView mTvOrderNumbOrderlist;
        protected TextView mTvStateOrderlist;
        protected TextView mTvFoodnameOrderlist;
        protected TextView mTvShiplayOrderlist;
        protected TextView mTvPaymoneyOrderlist;
        protected TextView mTvOrderTypeOrderlist;
        protected TextView mTvTypeOrderlist;
        protected TextView mTvOrdertiemOrderlist;
        protected TextView mTvTimeOrderlist;
        private View mView;

    }
    static class ViewHolder2{
        protected TextView mTvOrderNumbOrderlist;
        protected TextView mTvStateOrderlistType2;
        protected TextView mTvPaynameType2;
        protected TextView mTvPaynameOrderlistType2;
        protected TextView mTvYunbenumOrderlistType2;
        protected TextView mTvYunbenumType2;
        protected TextView mTvOrdertypeType2;
        protected TextView mTvOrdertypeOrderlistType2;
        protected TextView mTevOrdertimeType2;
        protected TextView mTvOrdertimeOrderlistType2;
        protected TextView mTvOrderlistType3;
        protected TextView mTvOrderNumbOrderlist1;
        protected TextView mTvStateOrderlistType3;
        protected TextView mTvPaynameType3;
        protected TextView mTvPaymoneyType3;
        protected TextView mTvPaynameOrderlistType3;
        protected TextView mTvBackYunbeiType3;
        protected TextView mTvBackYunbeiOrderlistType3;
        protected TextView mTvOrdertimeType3;
        protected TextView mTvOrdertimeOrderlistType3;

    }
    /*static class ViewHolder3{
        protected TextView mTvOrderlistType3;
        protected TextView mTvOrderNumbOrderlist;
        protected TextView mTvStateOrderlistType3;
        protected TextView mTvPaynameType3;
        protected TextView mTvPaymoneyType3;
        protected TextView mTvPaynameOrderlistType3;
        protected TextView mTvBackYunbeiType3;
        protected TextView mTvBackYunbeiOrderlistType3;
        protected TextView mTvOrdertimeType3;
        protected TextView mTvOrdertimeOrderlistType3;
    }*/

}
