package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.bean.GroupDetalBean;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.vh.ShopdetalVH;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/6/5 0005.
 */

public class RecycleshopAdapter extends SwipeMenuAdapter<ShopdetalVH> {
 //   private List<String> mList;
 //上拉加载更多
 public static final int  PULLUP_LOAD_MORE=0;
    //正在加载中
    public static final int  LOADING_MORE=1;
    public static final int NO_MORE_DATA=2;
    //上拉加载更多状态-默认为0
    private int load_more_status=0;
    private  List< GroupDetalBean.MsgBean.DataBean> mList;
    private Context mContext;
    private OnItemClickLensener mOnItemClickLensener;
    private static final int TYPE_ITEM =0;  //普通Item View
    private static final int TYPE_FOOTER = 1;  //顶部FootView



    public interface OnItemClickLensener{
        void OnItemLenserner(View v,int posion);

    }
    public RecycleshopAdapter(Context context){
        mContext=context;
    }

    public void setOnItemClickLensener(OnItemClickLensener onItemClickLensener) {
        mOnItemClickLensener = onItemClickLensener;
    }

    public void setList(List< GroupDetalBean.MsgBean.DataBean> list) {
        mList = list;
    }


    @Override
    public View onCreateContentView(ViewGroup parent, int viewType) {

            return  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_saleovee_warehouse_adapter, parent, false);


    }


    @Override
    public ShopdetalVH onCompatCreateViewHolder(final View realContentView, int viewType) {

            ShopdetalVH shopdetalVH=new ShopdetalVH(realContentView);
            realContentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mOnItemClickLensener!=null){
                        mOnItemClickLensener.OnItemLenserner(realContentView, (Integer) v.getTag());
                    }
                }
            });
            return shopdetalVH;

    }

    /*@Override
    public View onCreateContentView(ViewGroup parent, int viewType) {
        if(viewType==TYPE_ITEM){
            return  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_saleovee_warehouse_adapter, parent, false);

        }else if(viewType==TYPE_FOOTER){
            return  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lordfootview, parent, false);
        }else{
            return null;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCompatCreateViewHolder(final View realContentView, int viewType) {

        if(viewType==TYPE_ITEM){
            ShopdetalVH shopdetalVH=new ShopdetalVH(realContentView);
            realContentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mOnItemClickLensener!=null){
                        mOnItemClickLensener.OnItemLenserner(realContentView, (Integer) v.getTag());
                    }
                }
            });
            return shopdetalVH;
        }else if(viewType==TYPE_FOOTER){
            Log.e("ceshi","我选中了");
            FootViewHolder footViewHolder=new FootViewHolder(realContentView);
            return footViewHolder;

        }
            return null;
    }

*/

    @Override
    public void onBindViewHolder(ShopdetalVH holder, int position) {
        GroupDetalBean.MsgBean.DataBean msgBean = mList.get(position);
        ShopdetalVH shopdetalVH=holder;
        shopdetalVH.itemView.setTag(position);
        shopdetalVH.goodname.setText(msgBean.getTitle());
        Glide.with(mContext).load("http://img1.iyunbei.net/300x300/attachs/"+msgBean.getPhoto()).into(shopdetalVH.mShopimg);
        shopdetalVH.nowpric.setText("￥"+msgBean.getGoods_price()+"");
        shopdetalVH.oldpric.setText("￥"+msgBean.getMarket_price()+"");
        shopdetalVH.oldpric.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        shopdetalVH.guige.setText("规格:"+msgBean.getGuige());
        shopdetalVH.stock.setText("库存:"+msgBean.getNum()+"");
        shopdetalVH.selectnum.setText("销售量:"+msgBean.getSold_num()+"");

    }
/*
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ShopdetalVH){
            GroupDetalBean.MsgBean.DataBean msgBean = mList.get(position);
            ShopdetalVH shopdetalVH= (ShopdetalVH) holder;
            shopdetalVH.itemView.setTag(position);
            shopdetalVH.goodname.setText(msgBean.getTitle());
            Glide.with(mContext).load("http://img1.iyunbei.net/300x300/attachs/"+msgBean.getPhoto()).into(shopdetalVH.mShopimg);
            shopdetalVH.nowpric.setText("￥"+msgBean.getGoods_price()+"");
            shopdetalVH.oldpric.setText("￥"+msgBean.getMarket_price()+"");
            shopdetalVH.oldpric.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            shopdetalVH.guige.setText("规格:"+msgBean.getGuige());
            shopdetalVH.stock.setText("库存:"+msgBean.getNum()+"");
            shopdetalVH.selectnum.setText("销售量:"+msgBean.getSold_num()+"");
        }else if(holder instanceof FootViewHolder){
            FootViewHolder footViewHolder=(FootViewHolder)holder;
            switch (load_more_status){
                case PULLUP_LOAD_MORE:
                    Log.e("ceshi","PULLUP_LOAD_MORE");
                    footViewHolder.foot_view_item_tv.setText("上拉加载更多...");
                    footViewHolder.ll_loder.setVisibility(View.GONE);
                    break;
                case LOADING_MORE:
                    Log.e("ceshi","LOADING_MORE");
                    footViewHolder.foot_view_item_tv.setText("正在加载更多数据...");
                    footViewHolder.ll_loder.setVisibility(View.GONE);
                    break;
                case NO_MORE_DATA:
                    Log.e("ceshi","NO_MORE_DATA");
                    footViewHolder.foot_view_item_tv.setText("没有更多了");
                    footViewHolder.progress.setVisibility(View.GONE);
                    footViewHolder.ll_loder.setVisibility(View.VISIBLE);
                    break;
            }
        }

    }*/

  /*  @Override
    public int getItemViewType(int position) {

        if(position+1==getItemCount()){

            return TYPE_FOOTER;
        }else {
            return TYPE_ITEM;
        }
    }*/

    @Override
    public int getItemCount() {
        return mList==null?0:mList.size();
    }
    public static class FootViewHolder extends  RecyclerView.ViewHolder{
        private TextView foot_view_item_tv;
        public LinearLayout ll_loder;
        public ProgressBar   progress;
        public FootViewHolder(View view) {
            super(view);
          /*  progress= (ProgressBar) view.findViewById(R.id.progress);
            foot_view_item_tv=(TextView)view.findViewById(R.id.tv_foot);
            ll_loder= (LinearLayout) view.findViewById(R.id.rl_loder);*/
        }
    }

    /**
     * //上拉加载更多
     * PULLUP_LOAD_MORE=0;
     *  //正在加载中
     * LOADING_MORE=1;
     * //加载完成已经没有更多数据了
     * NO_MORE_DATA=2;
     * @param status
     */
    public void changeMoreStatus(int status){
        load_more_status=status;
        notifyDataSetChanged();
    }
    public void goneOrvie(int status){
        if(status==1){

        }

    }
}
