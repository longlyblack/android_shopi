package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.EvauationBean;
import com.liminyunbao.iyunbeishop.interFace.Replay;
import com.liminyunbao.iyunbeishop.interFace.ReplayHuiF;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.custom.RatingBar;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/4/24 0024.
 */

public class NoReplyAdapter extends BaseAdapter {
    private List<EvauationBean.DataBeanX.DataBean> mDataBeen;
    private Replay mReplay;
    private Context mContext;


    /*  public EvauationAdapter(Context context, List<EvauationBean.DataBeanX.DataBean> list, List<String>huifu, BackAnserBtn btn){
          mContext=context;
          mList=list;
          mhuifu=huifu;
          mMyAnser=new MyAnser();
          mMyAnser.setAnserBtn(btn);
      }*/
    public NoReplyAdapter(Context context, ReplayHuiF  replayHuiF){
        mContext=context;
        mReplay=new Replay();
        mReplay.setReplayHuiF(replayHuiF);

    }
    public void setDataBeen(List<EvauationBean.DataBeanX.DataBean> dataBeen) {
        mDataBeen = dataBeen;
    }

    @Override
    public int getCount() {
        return mDataBeen == null ? 0 : mDataBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_noreply_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        EvauationBean.DataBeanX.DataBean dataBean = mDataBeen.get(position);
        viewHolder.mTvNopUsername.setText(dataBean.getNickname());
        viewHolder.mTvNopNeirong.setText(dataBean.getContents());
        viewHolder.mTvNopOrdernumEvauation.setText("订单号:"+dataBean.getOrder_id());
        String start=dataBean.getScore()+"";
        String substring = start.substring(0,1);
        String decimal=start.substring(1);
        int num=Integer.parseInt(substring);
        viewHolder.mRb.setStar(num);
        viewHolder.mRb.setClickable(false);
        viewHolder.mScore.setText(num+"."+decimal);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long time=Long.valueOf(dataBean.getCreate_time())*1000L;
        Date date = new Date(time);
        String d = format.format(date);
        viewHolder.mTvNopUserTime.setText(d);
        mReplay.RepRelay(viewHolder.mRlNopHuifuBtn,viewHolder.mRlNopSaveText,position);
        return view;
    }

    static class ViewHolder {
        protected TextView mTvNopUsername;
        protected RatingBar mRb;
        protected TextView mScore;
        protected TextView mTvNopOrdernumEvauation;
        protected TextView mTvNopNeirong;
        protected LinearLayout mLlNeirong;
        protected TextView mTvNopUserTime;
        protected Button mBtnNopHuifu;
        protected RelativeLayout mRlNopHuifuBtn;
        protected EditText mTvNopInsertText;
        protected Button mBtnNopSaveAnser;
        protected RelativeLayout mRlNopSaveText;
        ViewHolder(View rootView) {
            initView(rootView);
        }
        private void initView(View rootView) {
            mTvNopUsername = (TextView) rootView.findViewById(R.id.tv_nop_username);
            mRb = (RatingBar) rootView.findViewById(R.id.rb);
            mScore = (TextView) rootView.findViewById(R.id.score);
            mTvNopOrdernumEvauation = (TextView) rootView.findViewById(R.id.tv_nop_ordernum_evauation);
            mTvNopNeirong = (TextView) rootView.findViewById(R.id.tv_nop_neirong);
            mLlNeirong = (LinearLayout) rootView.findViewById(R.id.ll_neirong);
            mTvNopUserTime = (TextView) rootView.findViewById(R.id.tv_nop_user_time);
            mBtnNopHuifu = (Button) rootView.findViewById(R.id.btn_nop_huifu);
            mRlNopHuifuBtn = (RelativeLayout) rootView.findViewById(R.id.rl_nop_huifu_btn);
            mTvNopInsertText = (EditText) rootView.findViewById(R.id.tv_nop_insert_text);
            mBtnNopSaveAnser = (Button) rootView.findViewById(R.id.btn_nop_save_anser);
            mRlNopSaveText = (RelativeLayout) rootView.findViewById(R.id.rl_nop_save_text);
        }
    }
}
