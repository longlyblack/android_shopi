package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.PuTongListBean;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/4/6 0006.
 */

public class YanZhengAdapter extends BaseAdapter {
    private List<PuTongListBean.DataBean.OrderDmsg> mDataBeanXes;

    public void setStrings(List<PuTongListBean.DataBean.OrderDmsg> strings) {
        mDataBeanXes = strings;
    }

    @Override
    public int getCount() {
        return mDataBeanXes == null ? 0 : mDataBeanXes.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_yanzheng_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        PuTongListBean.DataBean.OrderDmsg orderDmsg = mDataBeanXes.get(position);
        viewHolder.mTvYanzhengName.setText(orderDmsg.getShop_name());
        viewHolder.mTvHexioma.setText(orderDmsg.getVerify_code());
        viewHolder.mTvYanzhengOrderhao.setText(orderDmsg.getOrder_id()+"");
       /* SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Long time=new Long(dataBean.getOrder_id());
        Date date = new Date(time);
        String d = format.format(date);*/
        viewHolder.mTvYanzhengTiem.setText(orderDmsg.getCreated_at());
        return view;
    }

    static class ViewHolder {
        protected ImageView mImgKaquan;
        protected TextView mTvYanzhengName;
        protected TextView mTextHexiao;
        protected TextView mTvHexioma;
        protected TextView mTextDingdanhao;
        protected TextView mTvYanzhengOrderhao;
        protected TextView mTextTiem;
        protected TextView mTvYanzhengTiem;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mImgKaquan = (ImageView) rootView.findViewById(R.id.img_kaquan);
            mTvYanzhengName = (TextView) rootView.findViewById(R.id.tv_yanzheng_name);
            mTextHexiao = (TextView) rootView.findViewById(R.id.text_hexiao);
            mTvHexioma = (TextView) rootView.findViewById(R.id.tv_hexioma);
            mTextDingdanhao = (TextView) rootView.findViewById(R.id.text_dingdanhao);
            mTvYanzhengOrderhao = (TextView) rootView.findViewById(R.id.tv_yanzheng_orderhao);
            mTextTiem = (TextView) rootView.findViewById(R.id.text_tiem);
            mTvYanzhengTiem = (TextView) rootView.findViewById(R.id.tv_yanzheng_tiem);
        }
    }
}
