package com.liminyunbao.iyunbeishop.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.ShopListBean;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/15.
 */

public class MainListAdapter extends BaseAdapter {
    private List<ShopListBean.DataBean> mList;

    public void setList(List<ShopListBean.DataBean> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList==null?0:mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mainlist_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        ShopListBean.DataBean dataBean = mList.get(position);

        viewHolder.txShopname.setText(dataBean.getShop_name());
        String mapaddr = dataBean.getMapaddr();
        if(TextUtils.isEmpty(mapaddr)){
            viewHolder.txShopadds.setText(dataBean.getAddr());
        }else{
            viewHolder.txShopadds.setText(dataBean.getMapaddr()+" - "+dataBean.getAddr());
        }

        return view;
    }

    static class ViewHolder {
        protected TextView txShopname;
        protected TextView txShopadds;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            txShopname = (TextView) rootView.findViewById(R.id.tx_shopname);
            txShopadds = (TextView) rootView.findViewById(R.id.tx_shopadds);
        }
    }
}
