package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.bean.DisplayWindowBean;
import com.liminyunbao.iyunbeishop.http.HttpAPI;

import java.util.List;

/**
 * Created by Administrator on 2017/6/23 0023.
 */

public class WindowDisplayAdapter extends BaseAdapter {
    private Context mContext;
    private List<DisplayWindowBean.DataBean> data;
    private getllcancle mGetllcancle;

    public void setGetllcancle(getllcancle getllcancle) {
        mGetllcancle = getllcancle;
    }

    public interface getllcancle {

        void getCancle(LinearLayout ll, int position);
    }

    public WindowDisplayAdapter(Context context, List<DisplayWindowBean.DataBean> data) {
        mContext = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_window_diaplay, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        DisplayWindowBean.DataBean dataBean = data.get(position);
        viewHolder.mTvGoodsname.setText(dataBean.getGoods_name());
        Glide.with(mContext).load(HttpAPI.HEAD + dataBean.getPhoto()).into(viewHolder.mImgGoods);
        viewHolder.mTvKucun.setText("库存:" + dataBean.getNum() + "");
        viewHolder.mTvSale.setText("销量:" + dataBean.getSold_num() + "");
        viewHolder.mTvNewpic.setText("￥" + dataBean.getGoods_price());
        viewHolder.mTvOldpic.setText("￥" + dataBean.getMarket_price());
        viewHolder.mTvOldpic.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        if(position==0){

            viewHolder.mLlLine.setVisibility(View.GONE);
        }

        mGetllcancle.getCancle(viewHolder.mLlCancle, position);
        return view;
    }

    static class ViewHolder {
        protected LinearLayout mLlLine;
        protected ImageView mImgGoods;
        protected TextView mTvGoodsname;
        protected TextView mTvKucun;
        protected TextView mTvSale;
        protected LinearLayout mLlMsg;
        protected TextView mTvNewpic;
        protected TextView mTvOldpic;
        protected LinearLayout mLlCancle;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mLlLine = (LinearLayout) rootView.findViewById(R.id.ll_line);
            mImgGoods = (ImageView) rootView.findViewById(R.id.img_goods);
            mTvGoodsname = (TextView) rootView.findViewById(R.id.tv_goodsname);
            mTvKucun = (TextView) rootView.findViewById(R.id.tv_kucun);
            mTvSale = (TextView) rootView.findViewById(R.id.tv_sale);
            mLlMsg = (LinearLayout) rootView.findViewById(R.id.ll_msg);
            mTvNewpic = (TextView) rootView.findViewById(R.id.tv_newpic);
            mTvOldpic = (TextView) rootView.findViewById(R.id.tv_oldpic);
            mLlCancle = (LinearLayout) rootView.findViewById(R.id.ll_cancle);
        }
    }
}
