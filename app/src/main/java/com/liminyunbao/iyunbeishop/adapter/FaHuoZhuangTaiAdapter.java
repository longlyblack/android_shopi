package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.bean.WaitingListBean;
import com.liminyunbao.iyunbeishop.custom.ChildListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/4/1.
 */

public class FaHuoZhuangTaiAdapter extends BaseAdapter {
    private List<WaitingListBean.DataBean.OrderMsg> mList;

    private ChildListAdapter mChildListAdapter;

    public void setList(List<WaitingListBean.DataBean.OrderMsg> list) {
        mList = list;
    }

    private Refund mRefund;

    public void setRefund(Refund refund) {
        mRefund = refund;
    }

    public interface Refund {

        void setRefund(Button mrefund,String orderid);
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_ordernew_fahuozhuangtai, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        WaitingListBean.DataBean.OrderMsg orderMsg = mList.get(position);
        viewHolder.mTvOrderNumWitesenW.setText("订单号:" + orderMsg.getOrder_id() + "");
        viewHolder.mTvOrderTimeWitesenW.setText(orderMsg.getCreated_at());
        int order_status = orderMsg.getOrder_status();
        if (order_status == 0) {
            viewHolder.mTvZhaungtaiWait.setText("待付款");
        } else if (order_status == 1) {
            viewHolder.mTvZhaungtaiWait.setText("待发货");

        } else if (order_status == 2) {
            viewHolder.mTvZhaungtaiWait.setText("已发货");

        } else if (order_status == 3) {
            viewHolder.mTvZhaungtaiWait.setText("待评价");
        } else if (order_status == 4) {

            viewHolder.mTvZhaungtaiWait.setText("已完成");
        } else if (order_status == 5) {
            viewHolder.mTvZhaungtaiWait.setText("已关闭");

        } else if (order_status == 6) {
            viewHolder.mTvZhaungtaiWait.setText("退款中");

        }else if (order_status == 7) {
            viewHolder.mTvZhaungtaiWait.setText("待接单");
        }else if (order_status == 8) {
            viewHolder.mTvZhaungtaiWait.setText("已退款");
        }else if (order_status == 9) {
            viewHolder.mTvZhaungtaiWait.setText("待商家确认收货");
            viewHolder.mBtnRefund.setVisibility(View.VISIBLE);
        }


        mChildListAdapter = new ChildListAdapter();
        final List<WaitingListBean.DataBean.OrderMsg.GoodsList> data = orderMsg.getData();
        List<WaitingListBean.DataBean.OrderMsg.GoodsList> mdata = new ArrayList<>();
        int sum = 0;
        if (data.size() < 2) {
            viewHolder.mTvSeemore.setVisibility(View.GONE);
            viewHolder.mImgJiantou.setVisibility(View.GONE);
            viewHolder.mViewLine.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mTvSeemore.setVisibility(View.VISIBLE);
            viewHolder.mImgJiantou.setVisibility(View.VISIBLE);
            viewHolder.mViewLine.setVisibility(View.GONE);

        }

        for (int i = 0; i < data.size(); i++) {
            sum = sum + data.get(i).getGoods_num();
            if (i < 2) {
                mdata.add(new WaitingListBean.DataBean.OrderMsg.GoodsList(data.get(i).getPhoto(),
                        data.get(i).getGoods_name(), data.get(i).getGoods_guige(), data.get(i).getGoods_num(), data.get(i).getGoods_price()));

            }
        }
        mChildListAdapter.setList(mdata);
        viewHolder.mChidlListvShopNameW.setAdapter(mChildListAdapter);
        setListViewHeightBasedOnChildren(viewHolder.mChidlListvShopNameW);
        viewHolder.mChidlListvShopNameW.setHaveScrollbar(false);
        viewHolder.mChidlListvShopNameW.setDividerHeight(0);
        viewHolder.mTvAllMoneyW.setText("" + orderMsg.getTotal_amount());
        viewHolder.mTvBuyNumW.setText("（共" + sum + "件商品）");
        mRefund.setRefund(viewHolder.mBtnRefund,orderMsg.getOrder_id()+"");
        return view;
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        // 获取ListView对应的Adapter
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
            // listAdapter.getCount()返回数据项的数目
            View listItem = listAdapter.getView(i, null, listView);
            // 计算子项View 的宽高
            listItem.measure(0, 0);
            // 统计所有子项的总高度
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    static class ViewHolder {
        protected TextView mTvOrderNumWitesenW;
        protected TextView mTvOrderTimeWitesenW;
        protected TextView mTvZhaungtaiWait;
        protected ChildListView mChidlListvShopNameW;
        protected View mViewLine;
        protected ImageView mImgJiantou;
        protected TextView mTvSeemore;
        protected TextView mTvAllEduW;
        protected TextView mTvAllMoneyW;
        protected TextView mTvBuyNumW;
        protected Button mBtnRefund;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvOrderNumWitesenW = (TextView) rootView.findViewById(R.id.tv_order_num_witesen_w);
            mTvOrderTimeWitesenW = (TextView) rootView.findViewById(R.id.tv_order_time_witesen_w);
            mTvZhaungtaiWait = (TextView) rootView.findViewById(R.id.tv_zhaungtai_wait);
            mChidlListvShopNameW = (ChildListView) rootView.findViewById(R.id.chidl_listv_shop_name_w);
            mViewLine = (View) rootView.findViewById(R.id.view_line);
            mImgJiantou = (ImageView) rootView.findViewById(R.id.img_jiantou);
            mTvSeemore = (TextView) rootView.findViewById(R.id.tv_seemore);
            mTvAllEduW = (TextView) rootView.findViewById(R.id.tv_all_edu_w);
            mTvAllMoneyW = (TextView) rootView.findViewById(R.id.tv_all_money_w);
            mTvBuyNumW = (TextView) rootView.findViewById(R.id.tv_buy_num_w);
            mBtnRefund = (Button) rootView.findViewById(R.id.btn_refund);
        }
    }
}
