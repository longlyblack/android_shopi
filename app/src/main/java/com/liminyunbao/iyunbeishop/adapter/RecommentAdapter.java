package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.bean.PackageRecommendation;
import com.liminyunbao.iyunbeishop.http.HttpAPI;

import java.util.List;

/**
 * Created by Administrator on 2017/6/21 0021.
 */

public class RecommentAdapter extends BaseAdapter {

    private List<PackageRecommendation.ShopMassageBean> mList;

    private Context mContext;
    private UporDown mUporDown;

    public RecommentAdapter(Context context) {
        mContext = context;
    }

    public void setUporDown(UporDown uporDown) {
        mUporDown = uporDown;
    }

    public interface UporDown {
        /**
         * 上架或者下架接口
         */
        void Upord( LinearLayout backg, RelativeLayout shang, RelativeLayout upord, RelativeLayout edit, RelativeLayout delet, TextView textViewupord, int mall);
    }

    public void setList(List<PackageRecommendation.ShopMassageBean> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recomment_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        PackageRecommendation.ShopMassageBean shopMassageBean = mList.get(position);
        viewHolder.mTvRecommendName.setText(shopMassageBean.getGoods_name());
        viewHolder.mTvMassage.setText(shopMassageBean.getMiaoshu());
        viewHolder.mTvRecommendPric.setText("" + shopMassageBean.getGoods_price());
        if (shopMassageBean.getIs_mall() == 1) {
           // viewHolder.mTvTypeUD.setText("上架");
            viewHolder.mRlShang.setVisibility(View.GONE);
            viewHolder.mRlXiajia.setVisibility(View.VISIBLE);
            viewHolder.mLlBackg.setBackgroundResource(R.color.colorWhite);
            //  viewHolder.mTvTypeUD.setTextColor(convertView.getResources().getColor(R.color.colorDianPuB));
        } else {
            viewHolder.mRlShang.setVisibility(View.VISIBLE);
            viewHolder.mRlXiajia.setVisibility(View.GONE);
            viewHolder.mLlBackg.setBackgroundResource(R.color.colorXiangqing);
           // viewHolder.mTvTypeUD.setText("下架");

        }
        if (position == 0) {
            viewHolder.mLlTop.setVisibility(View.GONE);
        } else {
            viewHolder.mLlTop.setVisibility(View.VISIBLE);
        }
        Glide.with(mContext).load(HttpAPI.HEAD + shopMassageBean.getPhoto()).animate(R.anim.item_photo).into(viewHolder.mImgRecommendShop);
        mUporDown.Upord(viewHolder.mLlBackg,viewHolder.mRlShang, viewHolder.mRlXiajia, viewHolder.mRlBianji, viewHolder.mRlShanchu, viewHolder.mTvTypeUD, shopMassageBean.getGoods_agent_id());
        return view;
    }

    static class ViewHolder {
        protected LinearLayout mLlTop;
        protected ImageView mImgRecommendShop;
        protected TextView mTvRecommendName;
        protected TextView mTvMassage;
        protected TextView mTvFuhao;
        protected TextView mTvRecommendPric;
        protected ImageView mImgXiashop;
        protected TextView mTvTypeUD;
        protected RelativeLayout mRlXiajia;
        protected ImageView mImgXshangpp;
        protected TextView mTvTypeSD;
        protected RelativeLayout mRlShang;
        protected RelativeLayout mRlBianji;
        protected RelativeLayout mRlShanchu;
        protected LinearLayout mLlBackg;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mLlTop = (LinearLayout) rootView.findViewById(R.id.ll_top);
            mImgRecommendShop = (ImageView) rootView.findViewById(R.id.img_recommend_shop);
            mTvRecommendName = (TextView) rootView.findViewById(R.id.tv_recommend_name);
            mTvMassage = (TextView) rootView.findViewById(R.id.tv_massage);
            mTvFuhao = (TextView) rootView.findViewById(R.id.tv_fuhao);
            mTvRecommendPric = (TextView) rootView.findViewById(R.id.tv_recommend_pric);
            mImgXiashop = (ImageView) rootView.findViewById(R.id.img_xiashop);
            mTvTypeUD = (TextView) rootView.findViewById(R.id.tv_type_u_d);
            mRlXiajia = (RelativeLayout) rootView.findViewById(R.id.rl_xiajia);
            mImgXshangpp = (ImageView) rootView.findViewById(R.id.img_xshangpp);
            mTvTypeSD = (TextView) rootView.findViewById(R.id.tv_type_s_d);
            mRlShang = (RelativeLayout) rootView.findViewById(R.id.rl_shang);
            mRlBianji = (RelativeLayout) rootView.findViewById(R.id.rl_bianji);
            mRlShanchu = (RelativeLayout) rootView.findViewById(R.id.rl_shanchu);
            mLlBackg = (LinearLayout) rootView.findViewById(R.id.ll_backg);
        }
    }
}
