package com.liminyunbao.iyunbeishop.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/3/23.
 */

public class EvaluationPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mFragments;


    private String mString[]={"全部","差评","未回复"};

    public void setFragments(List<Fragment> fragments) {
        mFragments = fragments;
    }



    public EvaluationPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return mString[position];
    }
}
