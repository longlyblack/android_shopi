package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.baidu.mapapi.search.core.PoiInfo;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/4/21 0021.
 */

public class RegiongetAddsAdapter extends BaseAdapter {
    private List<PoiInfo> mPoiInfos;

    public void setPoiInfos(List<PoiInfo> poiInfos) {
        mPoiInfos = poiInfos;
    }

    @Override
    public int getCount() {
        return mPoiInfos == null ? 0 : mPoiInfos.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_region_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        PoiInfo poiInfo = mPoiInfos.get(position);
        viewHolder.mTvGegionAddres.setText(poiInfo.address);
        viewHolder.mTvGegionName.setText(poiInfo.name);
        return view;
    }

    static class ViewHolder {
        protected TextView mTvGegionName;
        protected TextView mTvGegionAddres;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvGegionName = (TextView) rootView.findViewById(R.id.tv_gegion_name);
            mTvGegionAddres = (TextView) rootView.findViewById(R.id.tv_gegion_addres);
        }
    }
}
