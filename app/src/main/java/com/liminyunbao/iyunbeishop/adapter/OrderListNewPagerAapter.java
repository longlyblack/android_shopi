package com.liminyunbao.iyunbeishop.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/3/30.
 */

public class OrderListNewPagerAapter extends FragmentStatePagerAdapter {
    private List<Fragment>mFragments;

    private String title[]={"待接单","未完成","已完成"};
    public void setFragments(List<Fragment> fragments) {
        mFragments = fragments;
    }

    public OrderListNewPagerAapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
