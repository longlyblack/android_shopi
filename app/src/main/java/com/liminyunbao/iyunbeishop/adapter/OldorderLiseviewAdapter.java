package com.liminyunbao.iyunbeishop.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.interFace.ChickBack;
import com.liminyunbao.iyunbeishop.interFace.MyBack;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/18.
 */

public class OldorderLiseviewAdapter extends BaseAdapter {

    private List<String> mList;
    private MyBack mMyBack;
    public OldorderLiseviewAdapter(ChickBack chickBack){
          mMyBack=new MyBack();
       mMyBack.setChickBack(chickBack);

    }
    public void setList(List<String> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_oldorder_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mList.get(position);
        viewHolder.mTvNumbListvOldorder.setText(s);
        Log.e("这是啥",s);
        mMyBack.MybtnChick(viewHolder.mBtnChuliOldorder);

        return view;
    }

    static class ViewHolder {
        protected TextView mTvNumbListvOldorder;
        protected TextView mTvTimeListvOldorder;
        protected TextView mDingdanhaoOldorder;
        protected TextView mTvOrderNumbOldorder;
        protected TextView mTvEatPeoplenumOldorder;
        protected TextView mEatpepleOldorder;
        protected TextView mBeizhuOldorder;
        protected TextView mTvBeizhuOldorder;
        protected TextView mResultOldorder;
        protected TextView mTvResultOldorder;
        protected Button mBtnChuliOldorder;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvNumbListvOldorder = (TextView) rootView.findViewById(R.id.tv_numb_listv_oldorder);
            mTvTimeListvOldorder = (TextView) rootView.findViewById(R.id.tv_time_listv_oldorder);
            mDingdanhaoOldorder = (TextView) rootView.findViewById(R.id.dingdanhao_oldorder);
            mTvOrderNumbOldorder = (TextView) rootView.findViewById(R.id.tv_orderNumb_oldorder);
            mTvEatPeoplenumOldorder = (TextView) rootView.findViewById(R.id.tv_eatPeoplenum_oldorder);
            mEatpepleOldorder = (TextView) rootView.findViewById(R.id.eatpeple_oldorder);
            mBeizhuOldorder = (TextView) rootView.findViewById(R.id.beizhu_oldorder);
            mTvBeizhuOldorder = (TextView) rootView.findViewById(R.id.tv_beizhu_oldorder);
            mResultOldorder = (TextView) rootView.findViewById(R.id.result_oldorder);
            mTvResultOldorder = (TextView) rootView.findViewById(R.id.tv_result_oldorder);
            mBtnChuliOldorder = (Button) rootView.findViewById(R.id.btn_chuli_oldorder);
        }
    }
}
