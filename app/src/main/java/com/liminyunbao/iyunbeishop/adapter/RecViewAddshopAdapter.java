package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.vh.ImgviewVH;

import java.util.List;

/**
 * Created by Administrator on 2017/6/3 0003.
 */

public class RecViewAddshopAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private List<String> mBitmaps;
    private Context mContext;
    public RecViewAddshopAdapter(Context context){
        mContext=context;
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }
    private  OnItemClickListener mOnItemClickListener=null;

    @Override
    public void onClick(View v) {

    }

    /**
     * item点击事件的接口
     */
    public  interface OnItemClickListener{
        void onItemClick(View v,int posion);
        void imgclick(ImageView imgm,int posion);
    }
    public void setBitmaps(List<String> bitmaps) {
        mBitmaps = bitmaps;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View inflate = View.inflate(parent.getContext(), R.layout.item_adapter_more_picture, null);
        ImgviewVH imgviewVH=new ImgviewVH(inflate);
        final ImageView img= (ImageView) inflate.findViewById(R.id.img_pictuer);
        inflate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(inflate, (Integer) v.getTag());
            }
        });
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.imgclick(img, (Integer) v.getTag());
            }
        });
        return imgviewVH;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ImgviewVH imgviewVH= (ImgviewVH) holder;
        String s = mBitmaps.get(position);
        imgviewVH.itemView.setTag(position);
       // imgviewVH.mImageView.setTag(position);
        imgviewVH.mImageView.setTag(R.id.imageloader_uri,s);
        Glide.with(mContext).load(s).into(imgviewVH.mImageView);


    }

    @Override
    public int getItemCount() {
        return mBitmaps==null?0:mBitmaps.size();
    }


}
