package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.bean.SaleBean;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/1/001.
 */

public class ChoiceGoodsAdapter extends BaseAdapter {
    public static Map<Integer, Boolean> isSelected;
    private List<SaleBean.MassageBean.GoodsList> mLists;
    private chickbox mChickbox;

    private Context mContext;

   /* public ChoiceGoodsAdapter(Context context) {
        mContext = context;
    }*/
    public ChoiceGoodsAdapter(List<SaleBean.MassageBean.GoodsList> lists, Context context, Map<Integer, Boolean> msSelected) {
        mLists = lists;
        mContext = context;
        isSelected=msSelected;
        inidata();

    }

    public static Map<Integer, Boolean> getIsSelected() {
        return isSelected;
    }

    public static void setIsSelected(Map<Integer, Boolean> isSelected) {
        ChoiceGoodsAdapter.isSelected = isSelected;
    }


    public void setChickbox(chickbox chickbox) {
        mChickbox = chickbox;
    }
    public interface chickbox {
        void ischickbox(CheckBox checkBox, int posion);
    }
    public void setLists(List<SaleBean.MassageBean.GoodsList> lists) {
        mLists = lists;
    }



    private void inidata() {
        for (int i = 0; i < mLists.size(); i++) {
            isSelected.put(i, false);
        }

    }

    @Override
    public int getCount() {
        return mLists==null?0: mLists.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_choice_goods, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
            SaleBean.MassageBean.GoodsList goodsList = mLists.get(i);
            viewHolder.mTvGoodsname.setText(goodsList.getTitle());
            viewHolder.mTvNowPrice.setText(goodsList.getGoods_price()+"");
            viewHolder.mTvOldPrice.setText(goodsList.getMarket_price()+"");
            viewHolder.mTvGuige.setText("规格:"+goodsList.getGuige());
            viewHolder.mTvStock.setText("库存:"+goodsList.getSunnum());
            Glide.with(mContext).load("http://img1.iyunbei.net/180x180/attachs/"+ goodsList.getPhoto()).animate(R.anim.item_photo).into(viewHolder.mImgGoodspic);
            mChickbox.ischickbox(viewHolder.mCbChicked,i);
            viewHolder.mCbChicked.setChecked(getIsSelected().get(i));

        }
        return view;
    }

    static class ViewHolder {
        protected CheckBox mCbChicked;
        protected ImageView mImgGoodspic;
        protected TextView mTvGoodsname;
        protected TextView mTvGuige;
        protected View mLine;
        protected TextView mTvStock;
        protected TextView mSymbol;
        protected TextView mTvNowPrice;
        protected TextView mTvOldPrice;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mCbChicked = (CheckBox) rootView.findViewById(R.id.cb_chicked);
            mImgGoodspic = (ImageView) rootView.findViewById(R.id.img_goodspic);
            mTvGoodsname = (TextView) rootView.findViewById(R.id.tv_goodsname);
            mTvGuige = (TextView) rootView.findViewById(R.id.tv_guige);
            mLine = (View) rootView.findViewById(R.id.line);
            mTvStock = (TextView) rootView.findViewById(R.id.tv_stock);
            mSymbol = (TextView) rootView.findViewById(R.id.symbol);
            mTvNowPrice = (TextView) rootView.findViewById(R.id.tv_now_price);
            mTvOldPrice = (TextView) rootView.findViewById(R.id.tv_old_price);
        }
    }
}
