package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.bean.WaitingListBean;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/31.
 */

public class ChildListAdapter extends BaseAdapter {
    private List<WaitingListBean.DataBean.OrderMsg.GoodsList> mList;

    private setGoodNum mSetGoodNum;
    public interface setGoodNum{
        void Goodsnum(int num);
    }
    public void setSetGoodNum(setGoodNum setGoodNum) {
        mSetGoodNum = setGoodNum;
    }

    public void setList(List<WaitingListBean.DataBean.OrderMsg.GoodsList> list) {
        mList = list;
    }


    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;

            if (view == null || !(view.getTag() instanceof ViewHolder)) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_witesen_child, null, false);
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            WaitingListBean.DataBean.OrderMsg.GoodsList goodsList = mList.get(position);
            viewHolder.mTvFoodnameChaildl.setText(goodsList.getGoods_name());
            viewHolder.mTvBuyFoodnum.setText("×"+goodsList.getGoods_num());
            viewHolder.mTvGuigeChildl.setText("规格:"+goodsList.getGoods_guige());
            viewHolder.mTvPriceMone.setText("￥"+goodsList.getGoods_price());
            Glide.with(parent.getContext()).load("http://img1.iyunbei.net/300x300/attachs/"+goodsList.getPhoto())
                    .into(viewHolder.mImgPicFood);
            return view;

    }
    @Override
    public boolean isEnabled(int position) {
        return false;
    }
    static class ViewHolder {
        protected ImageView mImgPicFood;
        protected TextView mTvFoodnameChaildl;
        protected TextView mTvGuigeChildl;
        protected TextView mTvPriceMone;
        protected TextView mTvBuyFoodnum;
        ViewHolder(View rootView) {
            initView(rootView);
        }
        private void initView(View rootView) {
            mImgPicFood = (ImageView) rootView.findViewById(R.id.img_pic_food);
            mTvFoodnameChaildl = (TextView) rootView.findViewById(R.id.tv_foodname_chaildl);
            mTvGuigeChildl = (TextView) rootView.findViewById(R.id.tv_guige_childl);
            mTvPriceMone = (TextView) rootView.findViewById(R.id.tv_price_mone);
            mTvBuyFoodnum = (TextView) rootView.findViewById(R.id.tv_buy_foodnum);
        }
    }
}
