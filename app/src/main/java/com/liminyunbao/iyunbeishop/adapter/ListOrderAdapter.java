package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.custom.ChildListView;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/17.
 */

public class ListOrderAdapter extends BaseAdapter {
    private List<String> mStrings;
    private String foodname[]={"番茄炒蛋","麻辣豆腐","腐竹"};
    private ChildLisetViewAdapter mAdapter;



    public void setStrings(List<String> strings) {
        mStrings = strings;
    }

    @Override
    public int getCount() {
        return mStrings == null ? 0 : mStrings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_ordernumb_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        String s = mStrings.get(position);
        viewHolder.mTvNumbListvOrderN.setText(s);

        mAdapter=new ChildLisetViewAdapter();
        mAdapter.setStrings(mStrings);
        viewHolder.mListvTablayNewOrder.setAdapter(mAdapter);
        viewHolder.mListvTablayNewOrder.setDividerHeight(0);
        setListViewHeightBasedOnChildren(viewHolder.mListvTablayNewOrder);
        mAdapter.notifyDataSetChanged();
        viewHolder.mListvTablayNewOrder.setHaveScrollbar(false);

        return view;
    }

    /**
     * @param listview
     *            此方法是本次listview嵌套listview的核心方法：计算parentlistview item的高度。
     *            如果不使用此方法，无论innerlistview有多少个item，则只会显示一个item。
     **/
    public void setListViewHeightBasedOnChildren(ListView listView) {
        // 获取ListView对应的Adapter
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
            // listAdapter.getCount()返回数据项的数目
            View listItem = listAdapter.getView(i, null, listView);
            // 计算子项View 的宽高
            listItem.measure(0, 0);
            // 统计所有子项的总高度
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    static class ViewHolder {
        protected TextView mTvNumbListvOrderN;
        protected TextView mTvTimeListvOrderN;
        protected TextView mDingdanhao;
        protected TextView mTvOrderNumb;
        protected TextView mTvEatPeoplenum;
        protected TextView mEatpeple;
        protected TextView mBeizhu;
        protected TextView mTvBeizhuListnewOrder;
        protected ChildListView mListvTablayNewOrder;
        protected TextView mResult;
        protected TextView mTvResultListneworder;
        protected Button mBtnChuliLsitnewOrder;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvNumbListvOrderN = (TextView) rootView.findViewById(R.id.tv_numb_listv_orderN);
            mTvTimeListvOrderN = (TextView) rootView.findViewById(R.id.tv_time_listv_orderN);
            mDingdanhao = (TextView) rootView.findViewById(R.id.dingdanhao);
            mTvOrderNumb = (TextView) rootView.findViewById(R.id.tv_orderNumb);
            mTvEatPeoplenum = (TextView) rootView.findViewById(R.id.tv_eatPeoplenum);
            mEatpeple = (TextView) rootView.findViewById(R.id.eatpeple);
            mBeizhu = (TextView) rootView.findViewById(R.id.beizhu);
            mTvBeizhuListnewOrder = (TextView) rootView.findViewById(R.id.tv_beizhu_listnewOrder);
            mListvTablayNewOrder = (ChildListView) rootView.findViewById(R.id.listv_tablay_newOrder);
            mResult = (TextView) rootView.findViewById(R.id.result);
            mTvResultListneworder = (TextView) rootView.findViewById(R.id.tv_result_listneworder);
            mBtnChuliLsitnewOrder = (Button) rootView.findViewById(R.id.btn_chuli_lsitnewOrder);
        }
    }

}
