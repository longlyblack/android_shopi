package com.liminyunbao.iyunbeishop.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/3/20.
 */

public class OrderListAdapter extends FragmentStatePagerAdapter {

    private String title[]={"待消费","全部","退款"};
    private List<Fragment> mFragments;

    public void setFragments(List<Fragment> fragments) {
        mFragments = fragments;
    }

    public OrderListAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
