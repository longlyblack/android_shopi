package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/6/8 0008.
 */

public class PictureDisecbeAdapter extends BaseAdapter {
    private List<String> mList;

    private Context mContext;
    public PictureDisecbeAdapter(Context context){
        mContext=context;
    }
    public void setList(List<String> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_picture, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mList.get(position);
        Log.e("图片链接",s);
        Glide.with(mContext).load("http://img1.iyunbei.net/500-/attachs/" +s).into(viewHolder.mImgPhoto);
        return view;
    }

    static class ViewHolder {
        protected ImageView mImgPhoto;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mImgPhoto = (ImageView) rootView.findViewById(R.id.img_photo);
        }
    }
}
