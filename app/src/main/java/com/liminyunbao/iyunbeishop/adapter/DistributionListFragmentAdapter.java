package com.liminyunbao.iyunbeishop.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/3/21.
 */

public class DistributionListFragmentAdapter extends FragmentStatePagerAdapter {
    private List<Fragment>mFragments;
    private String title[]={"待发货","全部","退款"};

    public void setFragments(List<Fragment> fragments) {
        mFragments = fragments;
    }

    public DistributionListFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return title[position];
    }
}
