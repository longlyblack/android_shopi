package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/21.
 */

public class RefundOrderListAdapter extends BaseAdapter {
    private List<String> mList;

    public void setList(List<String> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orderlist_typ1, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mList.get(position);
        viewHolder.mRlAgree.setVisibility(View.VISIBLE);
        viewHolder.mTvOrderNumbOrderlist.setText(s);
        return view;
    }

    static class ViewHolder {
        protected TextView mTvOrderlist;
        protected TextView mTvOrderNumbOrderlist;
        protected TextView mTvStateOrderlist;
        protected TextView mTvFoodnameOrderlist;
        protected TextView mTvShiplayOrderlist;
        protected TextView mTvPaymoneyOrderlist;
        protected TextView mTvOrderTypeOrderlist;
        protected TextView mTvTypeOrderlist;
        protected TextView mTvOrdertiemOrderlist;
        protected TextView mTvTimeOrderlist;
        protected View mView;
        protected Button mBtnAgreeOrderlistTyp1;
        protected Button mBtnRefuseOrderlistTyp1;
        protected RelativeLayout mRlAgree;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvOrderlist = (TextView) rootView.findViewById(R.id.tv_orderlist);
            mTvOrderNumbOrderlist = (TextView) rootView.findViewById(R.id.tv_orderNumb_orderlist);
            mTvStateOrderlist = (TextView) rootView.findViewById(R.id.tv_state_orderlist);
            mTvFoodnameOrderlist = (TextView) rootView.findViewById(R.id.tv_foodname_orderlist);
            mTvShiplayOrderlist = (TextView) rootView.findViewById(R.id.tv_shiplay_orderlist);
            mTvPaymoneyOrderlist = (TextView) rootView.findViewById(R.id.tv_paymoney_orderlist);
            mTvOrderTypeOrderlist = (TextView) rootView.findViewById(R.id.tv_orderType_orderlist);
            mTvTypeOrderlist = (TextView) rootView.findViewById(R.id.tv_type_orderlist);
            mTvOrdertiemOrderlist = (TextView) rootView.findViewById(R.id.tv_ordertiem_orderlist);
            mTvTimeOrderlist = (TextView) rootView.findViewById(R.id.tv_time_orderlist);
            mView = (View) rootView.findViewById(R.id.view);
            mBtnAgreeOrderlistTyp1 = (Button) rootView.findViewById(R.id.btn_agree_orderlist_typ1);
            mBtnRefuseOrderlistTyp1 = (Button) rootView.findViewById(R.id.btn_refuse_orderlist_typ1);
            mRlAgree = (RelativeLayout) rootView.findViewById(R.id.rl_agree);
        }
    }
}
