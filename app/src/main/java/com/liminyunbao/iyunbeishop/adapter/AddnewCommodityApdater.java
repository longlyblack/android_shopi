package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.activity.AddNewCommodityActivity;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/3/25.
 */

public class AddnewCommodityApdater extends BaseAdapter {
    private List<String> mStrings;

    public void setStrings(List<String> strings) {
        mStrings = strings;
    }

    @Override
    public int getCount() {
        return mStrings == null ? 0 : mStrings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_addnew_lsit_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mStrings.get(position);
        viewHolder.mTvLsitAddnew.setText(s);
        if (AddNewCommodityActivity.stra == position) {

            viewHolder.mRlAddnewAdapter.setBackgroundResource(R.drawable.shap_commodity_select);
        } else {
            viewHolder.mRlAddnewAdapter.setBackgroundResource(R.drawable.shap_commodity_nomal);
        }
        return view;
    }

    static class ViewHolder {
        protected TextView mTvLsitAddnew;
        protected RelativeLayout mRlAddnewAdapter;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvLsitAddnew = (TextView) rootView.findViewById(R.id.tv_lsit_addnew);
            mRlAddnewAdapter = (RelativeLayout) rootView.findViewById(R.id.rl_addnew_adapter);
        }
    }
}
