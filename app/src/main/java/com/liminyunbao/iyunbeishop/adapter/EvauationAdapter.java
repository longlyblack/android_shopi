package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.EvauationBean;
import com.liminyunbao.iyunbeishop.interFace.AllAnser;
import com.liminyunbao.iyunbeishop.interFace.AllRePlay;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.custom.RatingBar;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/3/30.
 */

public class EvauationAdapter extends BaseAdapter {

    private List<EvauationBean.DataBeanX.DataBean> mList;
    private List<String> mhuifu;

    private AllAnser mReplay;
    private Context mContext;
    public void setMhuifu(List<EvauationBean.DataBeanX.DataBean> mhuifu) {
        this.mList = mhuifu;
    }

    public EvauationAdapter(Context context, AllRePlay replayHuiF){
        mContext=context;
        mReplay=new AllAnser();
        mReplay.Mallreplay(replayHuiF);

    }
  /*  public EvauationAdapter(Context context, List<EvauationBean.DataBeanX.DataBean> list, List<String>huifu, BackAnserBtn btn){
        mContext=context;
        mList=list;
        mhuifu=huifu;
        mMyAnser=new MyAnser();
        mMyAnser.setAnserBtn(btn);
    }*/

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;


        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_evauation_adapter, null, false);
            viewHolder = new ViewHolder(view);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        EvauationBean.DataBeanX.DataBean dataBean = mList.get(position);
        //String s1 = mhuifu.get(position);
        viewHolder.mTvUsername.setText(dataBean.getNickname());
        viewHolder.mTvNeirong.setText(dataBean.getContents());
        viewHolder.mTvOrdernumEvauation.setText("订单号:" + dataBean.getOrder_id());

        String start=dataBean.getScore()+"";
        if(start.length()==1){
            int num=Integer.parseInt(start);
            viewHolder.mRb.setStar(num);
            viewHolder.mRb.setClickable(false);
            viewHolder.mScore.setText("0."+num);
        }else {
            String substring = start.substring(0,1);
            String decimal=start.substring(1);
            int num=Integer.parseInt(substring);
            viewHolder.mRb.setStar(num);
            viewHolder.mRb.setClickable(false);
            viewHolder.mScore.setText(num+"."+decimal);
        }


         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         long time=Long.valueOf(dataBean.getCreate_time())*1000L;
         long times=Long.valueOf(dataBean.getReply_time())*1000L;
        Date dateS = new Date(times);
        Date date = new Date(time);
        String d = format.format(date);
        String ds=format.format(dateS);
        viewHolder.mTvUserTime.setText(d);
        viewHolder.mTvBossAnser.setText(ds);
        viewHolder.mTvUserAnser.setText(dataBean.getReply());
    /*    if(dataBean.getReply()!=null){
            viewHolder.mRlHuifuNeirong.setVisibility(View.VISIBLE);
            viewHolder.mRlHuifuBtn.setVisibility(View.GONE);
        }else{
            viewHolder.mRlHuifuNeirong.setVisibility(View.GONE);
            viewHolder.mRlHuifuBtn.setVisibility(View.VISIBLE);

        }*/
        mReplay.setAllRePlay(viewHolder.mRlHuifuBtn,viewHolder.mRlSaveText ,viewHolder.mRlHuifuNeirong,dataBean.getReply(),position);
        return view;
    }

    static class ViewHolder {
        protected RatingBar mRb;
        protected TextView mTvUsername;
        protected TextView mScore;
        protected TextView mTvOrdernumEvauation;
        protected TextView mTvNeirong;
        protected LinearLayout mLlNeirong;
        protected TextView mTvUserTime;
        protected TextView mTvUserh;
        protected TextView mTvUserAnser;
        protected TextView mTvBossAnser;
        protected LinearLayout mRlHuifuNeirong;
        protected Button mBtnHuifu;
        protected RelativeLayout mRlHuifuBtn;
        protected EditText mTvInsertText;
        protected Button mBtnSaveAnser;
        protected RelativeLayout mRlSaveText;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mRb = (RatingBar) rootView.findViewById(R.id.rb);
            mTvUsername = (TextView) rootView.findViewById(R.id.tv_username);
            mScore = (TextView) rootView.findViewById(R.id.score);
            mTvOrdernumEvauation = (TextView) rootView.findViewById(R.id.tv_ordernum_evauation);
            mTvNeirong = (TextView) rootView.findViewById(R.id.tv_neirong);
            mLlNeirong = (LinearLayout) rootView.findViewById(R.id.ll_neirong);
            mTvUserTime = (TextView) rootView.findViewById(R.id.tv_user_time);
            mTvUserh = (TextView) rootView.findViewById(R.id.tv_userh);
            mTvUserAnser = (TextView) rootView.findViewById(R.id.tv_user_anser);
            mTvBossAnser = (TextView) rootView.findViewById(R.id.tv_boss_anser);
            mRlHuifuNeirong = (LinearLayout) rootView.findViewById(R.id.rl_huifu_neirong);
            mBtnHuifu = (Button) rootView.findViewById(R.id.btn_huifu);
            mRlHuifuBtn = (RelativeLayout) rootView.findViewById(R.id.rl_huifu_btn);
            mTvInsertText = (EditText) rootView.findViewById(R.id.tv_insert_text);
            mBtnSaveAnser = (Button) rootView.findViewById(R.id.btn_save_anser);
            mRlSaveText = (RelativeLayout) rootView.findViewById(R.id.rl_save_text);
        }
    }
}
