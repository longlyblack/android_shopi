package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/4/11 0011.
 */

public class MingXiAdapter extends BaseAdapter {
    private List<String> mStrings;

    public void setStrings(List<String> strings) {
        mStrings = strings;
    }

    @Override
    public int getCount() {
        return mStrings == null ? 0 : mStrings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_capitalflow, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        String s = mStrings.get(position);
        viewHolder.mTvTimeCapitalflow.setText(s);
        return view;
    }

    static class ViewHolder {
        protected TextView mTvUsenameCapitalfow;
        protected TextView mTvMoneyCpitalflow;
        protected TextView mTvTimeCapitalflow;
        protected TextView mTvYveCapitalflow;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvUsenameCapitalfow = (TextView) rootView.findViewById(R.id.tv_usename_capitalfow);
            mTvMoneyCpitalflow = (TextView) rootView.findViewById(R.id.tv_money_cpitalflow);
            mTvTimeCapitalflow = (TextView) rootView.findViewById(R.id.tv_time_capitalflow);
            mTvYveCapitalflow = (TextView) rootView.findViewById(R.id.tv_yve_capitalflow);
        }
    }
}
