package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.bean.SaleBean;
import com.liminyunbao.iyunbeishop.interFace.BackImg;
import com.liminyunbao.iyunbeishop.interFace.MyImg;
import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.utils.SharePUtile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/24.
 */
public class OnSaleAdapter extends BaseAdapter {
    private List<SaleBean.MassageBean.GoodsList> mList;
    private MyImg mMyImg;
    private Context mContext;

    public void setList(List<SaleBean.MassageBean.GoodsList> list) {
        mList = list;
    }

    private Map<Integer, View> mViewMap;

    private int tyoe;
    private onListener mListener;

    public interface onListener {
        // void OnListener(ImageView imageView, int psion, Map<Integer, View> viewMap, ImageView photo, String url);
        void OnListener(RelativeLayout imageView, int psion, Map<Integer, View> viewMap, ImageView photo, String url,Integer is_recommend);
    }

    public void setListener(onListener listener) {
        mListener = listener;
    }

    public OnSaleAdapter(Context context, BackImg backImg) {
        mContext = context;
        mMyImg = new MyImg();
        mMyImg.setimg(backImg);
        mViewMap = new HashMap<>();
        tyoe = (int) SharePUtile.get(mContext, "shop_typ", 0);
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_onsale_adapter, null, false);
            viewHolder = new ViewHolder(view);
            mViewMap.put(position, view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        SaleBean.MassageBean.GoodsList dataBean = mList.get(position);
        viewHolder.mTvCommodityname.setText(dataBean.getTitle());
        String url = "http://img1.iyunbei.net/180x180/attachs/" + dataBean.getPhoto();
        Glide.with(parent.getContext()).load(url).animate(R.anim.item_photo).into(viewHolder.mImgShoppic);
        // mMyImg.MyImgView(viewHolder.mImgConndityMenu, position);
        mMyImg.MyImgView(viewHolder.mMenu, position);
        viewHolder.mTvObsolete.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.mTvObsolete.setText("￥" + dataBean.getMarket_price());
        viewHolder.mTvPrice.setText("￥" + dataBean.getGoods_price());
        viewHolder.mTvSalesvolume.setText("销售量:" + dataBean.getSold_num());
        viewHolder.mTvStock.setVisibility(View.VISIBLE);
        viewHolder.mTvStock.setText("库存:" + dataBean.getSunnum());

        int is_recommend = dataBean.getIs_recommend();
        if (is_recommend == 0) {
            viewHolder.mImgRecommend.setVisibility(View.GONE);
        } else if (is_recommend == 1) {
            viewHolder.mImgRecommend.setVisibility(View.VISIBLE);
        }
        String guige = dataBean.getGuige();

        if (guige != null) {
            viewHolder.mTvSpecifications.setText("规格:" + dataBean.getGuige());
        } else {
            viewHolder.mTvSpecifications.setVisibility(View.GONE);
        }

        mListener.OnListener(viewHolder.mMenu, position, mViewMap, viewHolder.mImgShoppic, url,is_recommend);
        return view;
    }

    static class ViewHolder {
        protected ImageView mImgShoppic;
        protected ImageView mImgRecommend;
        protected TextView mTvCommodityname;
        protected ImageView mImgConndityMenu;
        protected RelativeLayout mMenu;
        protected TextView mTvPrice;
        protected TextView mTvObsolete;
        protected TextView mTvSpecifications;
        protected TextView mTvStock;
        protected TextView mTvSalesvolume;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mImgShoppic = (ImageView) rootView.findViewById(R.id.img_shoppic);
            mImgRecommend = (ImageView) rootView.findViewById(R.id.img_recommend);
            mTvCommodityname = (TextView) rootView.findViewById(R.id.tv_commodityname);
            mImgConndityMenu = (ImageView) rootView.findViewById(R.id.img_conndity_menu);
            mMenu = (RelativeLayout) rootView.findViewById(R.id.menu);
            mTvPrice = (TextView) rootView.findViewById(R.id.tv_price);
            mTvObsolete = (TextView) rootView.findViewById(R.id.tv_obsolete);
            mTvSpecifications = (TextView) rootView.findViewById(R.id.tv_specifications);
            mTvStock = (TextView) rootView.findViewById(R.id.tv_stock);
            mTvSalesvolume = (TextView) rootView.findViewById(R.id.tv_salesvolume);
        }
    }
}
