package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.bean.TowGroupBean;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/6/7 0007.
 */

public class ChoicegroupAdatper extends BaseAdapter {
    private List<TowGroupBean.MsgBean> mList;
    private Context mContext;
    public Map<Integer,Boolean> isSelected;

    public TowGroupBean.MsgBean msgBean;

    private  Masgben mMasgben;
    public interface  Masgben{

        void setMasgStatus(TowGroupBean.MsgBean bean,Integer posion);
    }

    public void setMasgben(Masgben masgben) {
        mMasgben = masgben;
    }

    public Map<Integer, Boolean> getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Map<Integer, Boolean> isSelected) {
        this.isSelected = isSelected;
    }

    private Chicboxkchick mChicboxkchick;
    public interface  Chicboxkchick{
        void setchick(CheckBox cb,int posiont);
    }

    public void setChicboxkchick(Chicboxkchick chicboxkchick) {
        mChicboxkchick = chicboxkchick;
    }

    /**
     * 把map的值都赋false
     */
    private void init(){
        for(int i=0;i<mList.size();i++){
            isSelected.put(i,false);
        }
    }
    public ChoicegroupAdatper(Context context,Map<Integer,Boolean> map, List<TowGroupBean.MsgBean> list){
        mContext=context;
        mList=list;
        isSelected=map;
       init();
    }

    @Override
    public int getCount() {
        return mList==null?0:mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_choice_group, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        msgBean = mList.get(position);
        mMasgben.setMasgStatus(msgBean,position);
        viewHolder.mTvGroupName.setText(msgBean.getCate_name());
        mChicboxkchick.setchick(viewHolder.mCbSelect,position);
        viewHolder.mTvShopNum.setText("共有"+msgBean.getNum()+"件商品");
        if(msgBean.getStatus().equals("0")){
           isSelected.put(position,false);
           viewHolder.mCbSelect.setChecked(false);
        }else if(msgBean.getStatus().equals("1")){
            isSelected.put(position,true);
            viewHolder.mCbSelect.setChecked(true);
        }
        viewHolder.mCbSelect.setChecked(getIsSelected().get(position));//此处不这样设置会出现复用
        return view;
    }

    static class ViewHolder {
        protected CheckBox mCbSelect;
        protected TextView mTvGroupName;
        protected TextView mTvShopNum;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mCbSelect = (CheckBox) rootView.findViewById(R.id.cb_select);
            mTvGroupName = (TextView) rootView.findViewById(R.id.tv_group_name);
            mTvShopNum = (TextView) rootView.findViewById(R.id.tv_shop_num);
        }
    }
}
