package com.liminyunbao.iyunbeishop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liminyunbao.iyunbeishop.R;
import com.liminyunbao.iyunbeishop.bean.FullWhatBean;

import java.util.List;

/**
 * Created by Administrator on 2017/6/30/030.
 */

public class CardPullAlladapterHang extends BaseAdapter {

    private List<FullWhatBean.DataBean> mData;

    public void setmData(List<FullWhatBean.DataBean> mData) {
        this.mData = mData;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(convertView.getContext()).inflate(R.layout.item_card_pull_all_hang, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();

            FullWhatBean.DataBean dataBean = mData.get(i);
            viewHolder.mTvMoney.setText("满" + dataBean.getMoney() + "元使用");
            viewHolder.mTvMessageSomething.setText(dataBean.getTitle());
            viewHolder.mTvCardDiscount.setText(dataBean.getDiscount() + "");
            viewHolder.mTvCardCode.setText(dataBean.getNumber());
            viewHolder.mTvWhere.setText(dataBean.getShop_name());
            viewHolder.mTvUseTime.setText("有效期:" + dataBean.getStart_time() + "—" + dataBean.getEnd_time());
            int discount_type = dataBean.getDiscount_type();
            if (discount_type == 1) {
                viewHolder.mTvMoney.setText("满" + dataBean.getMoney() + "元使用");
                viewHolder.mTvCardDiscount.setVisibility(View.VISIBLE);
                viewHolder.mTvSysbol.setVisibility(View.VISIBLE);
            } else if (discount_type == 2) {
                viewHolder.mTvFolding.setVisibility(View.VISIBLE);
                viewHolder.mTextFloding.setVisibility(View.VISIBLE);
                viewHolder.mTvMoney.setText("满" + dataBean.getMoney() + "元使用");
            } else if (discount_type == 3) {
                viewHolder.mTvCardDiscount.setVisibility(View.VISIBLE);
                viewHolder.mTvSysbol.setVisibility(View.VISIBLE);
                viewHolder.mTvMoney.setText("礼品券");
            } else if (discount_type == 4) {
                viewHolder.mTvFree.setVisibility(View.VISIBLE);
                viewHolder.mTvMoney.setText("满" + dataBean.getMoney() + "元使用");
            }
            if (dataBean.getNum() == 0) {
                viewHolder.mRlLeftc.setBackgroundResource(R.mipmap.card_leftn);
                viewHolder.mLlRightc.setBackgroundResource(R.mipmap.card_rightn);
            }

        }
        return view;
    }

    static class ViewHolder {
        protected TextView mTvCardDiscount;
        protected TextView mTvSysbol;
        protected TextView mTvFolding;
        protected TextView mTextFloding;
        protected TextView mTvFree;
        protected TextView mTvMoney;
        protected RelativeLayout mRlLeftc;
        protected TextView mTextCode;
        protected TextView mTvCardCode;
        protected TextView mTvWhere;
        protected TextView mTvMessageSomething;
        protected TextView mTvUseTime;
        protected LinearLayout mLlEdit;
        protected View mVLine;
        protected LinearLayout mLlDelet;
        protected View mVLine2;
        protected LinearLayout mLlRightc;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mTvCardDiscount = (TextView) rootView.findViewById(R.id.tv_card_discount);
            mTvSysbol = (TextView) rootView.findViewById(R.id.tv_sysbol);
            mTvFolding = (TextView) rootView.findViewById(R.id.tv_folding);
            mTextFloding = (TextView) rootView.findViewById(R.id.text_floding);
            mTvFree = (TextView) rootView.findViewById(R.id.tv_free);
            mTvMoney = (TextView) rootView.findViewById(R.id.tv_money);
            mRlLeftc = (RelativeLayout) rootView.findViewById(R.id.rl_leftc);
            mTextCode = (TextView) rootView.findViewById(R.id.text_code);
            mTvCardCode = (TextView) rootView.findViewById(R.id.tv_card_code);
            mTvWhere = (TextView) rootView.findViewById(R.id.tv_where);
            mTvMessageSomething = (TextView) rootView.findViewById(R.id.tv_message_something);
            mTvUseTime = (TextView) rootView.findViewById(R.id.tv_use_time);
            mLlEdit = (LinearLayout) rootView.findViewById(R.id.ll_edit);
            mVLine = (View) rootView.findViewById(R.id.v_line);
            mLlDelet = (LinearLayout) rootView.findViewById(R.id.ll_delet);
            mVLine2 = (View) rootView.findViewById(R.id.v_line2);
            mLlRightc = (LinearLayout) rootView.findViewById(R.id.ll_rightc);
        }
    }
}
