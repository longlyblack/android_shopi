package com.liminyunbao.iyunbeishop.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liminyunbao.iyunbeishop.bean.GroupDetalBean;
import com.liminyunbao.iyunbeishop.R;

import java.util.List;

/**
 * Created by Administrator on 2017/6/14 0014.
 */

public class ShopDetailsAdapter extends BaseAdapter {

    private List<GroupDetalBean.MsgBean.DataBean> mList;

    private Context mContext;

    public void setList(List<GroupDetalBean.MsgBean.DataBean> list) {
        mList = list;
    }

    public ShopDetailsAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        ViewHolder viewHolder = null;
        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_saleovee_warehouse_adapter, null, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        GroupDetalBean.MsgBean.DataBean dataBean = mList.get(position);

        viewHolder.mTvSaleoverCommodityname.setText(dataBean.getTitle());
        Glide.with(mContext).load("http://img1.iyunbei.net/300x300/attachs/"+dataBean.getPhoto()).into(viewHolder.mImgSaleoverShoppic);
        viewHolder.mTvSaleoverPrice.setText("￥"+dataBean.getGoods_price()+"");
        viewHolder.mTvSaleoverObsolete.setText("￥"+dataBean.getMarket_price()+"");
        viewHolder.mTvSaleoverObsolete.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.mTvSaleoverSpecifications.setText("规格:"+dataBean.getGuige());
        viewHolder.mTvSaleoverStock.setText("库存:"+dataBean.getNum()+"");
        viewHolder.mTvSaleoverSalesvolume.setText("销售量:"+dataBean.getSold_num()+"");

        return view;
    }

    static class ViewHolder {
        protected ImageView mImgSaleoverShoppic;
        protected TextView mTvSaleoverCommodityname;
        protected TextView mTvSaleoverPrice;
        protected TextView mTvSaleoverObsolete;
        protected TextView mTvSaleoverSpecifications;
        protected TextView mTvSaleoverStock;
        protected TextView mTvSaleoverSalesvolume;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {
            mImgSaleoverShoppic = (ImageView) rootView.findViewById(R.id.img_saleover_shoppic);
            mTvSaleoverCommodityname = (TextView) rootView.findViewById(R.id.tv_saleover_commodityname);
            mTvSaleoverPrice = (TextView) rootView.findViewById(R.id.tv_saleover_price);
            mTvSaleoverObsolete = (TextView) rootView.findViewById(R.id.tv_saleover_obsolete);
            mTvSaleoverSpecifications = (TextView) rootView.findViewById(R.id.tv_saleover_specifications);
            mTvSaleoverStock = (TextView) rootView.findViewById(R.id.tv_saleover_stock);
            mTvSaleoverSalesvolume = (TextView) rootView.findViewById(R.id.tv_saleover_salesvolume);
        }
    }
}
